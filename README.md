# TSE Wintersemester 2023/24

* Students: Fabian Schroth, Arne Stramka, Piet Förster, Dominik Wlcek, Miro Luca Ackermann, Marcel-Marius Soltan
* Supervisors: [Bahareh Taghavi](mailto:bahareh.taghavi@kit.edu), [Tobias Hey](mailto:hey@kit.edu)

## Topic
TBD

## Useful Links
* [Git](https://sdq.kastel.kit.edu/wiki/Git)
* [Issue Board](https://gitlab.kit.edu/kit/kastel/sdq/stud/TSE/wise2324/awesomeandroid/-/boards)
* [Document Templates](https://sdq.kastel.kit.edu/wiki/Dokumentvorlagen)
* [LaTeX](https://sdq.kastel.kit.edu/wiki/LaTeX)


