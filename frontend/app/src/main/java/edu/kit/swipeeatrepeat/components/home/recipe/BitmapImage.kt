package edu.kit.swipeeatrepeat.components.home.recipe

import android.graphics.Bitmap
import androidx.compose.foundation.Image
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.asImageBitmap

/**
 * Converts a Bitmap into an Image and displays it
 */
@Composable
fun BitmapImage(bitmap: Bitmap?) {
    if (bitmap != null) {
        Image(
            bitmap = bitmap.asImageBitmap(),
            contentDescription = "some useful description",
        )
    }
}
