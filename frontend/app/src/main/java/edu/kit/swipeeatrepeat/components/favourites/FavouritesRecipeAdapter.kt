package edu.kit.swipeeatrepeat.components.favourites

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.recipeCard.viewHolders.FavouriteCardViewHolder
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A [BaseAdapter] to inflate the [FavouriteCardViewHolder] and bind the meals as grid-items.
 *
 * @author Piet Förster, Fabian Schroth
 * @version 1.1
 */
class FavouritesRecipeAdapter(
    private val favourites: List<Recipe>,
    private val onRemoveClicked: (Recipe) -> Unit,
    private val onClick: (Recipe) -> Unit,
) : BaseAdapter() {

    override fun getCount(): Int = favourites.size

    override fun getItem(position: Int): Recipe = favourites.elementAt(position)

    override fun getItemId(position: Int): Long = getItem(position).id

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertedView = convertView
        val viewHolder: FavouriteCardViewHolder

        if (convertedView == null) {
            val inflater = LayoutInflater.from(parent.context)
            convertedView = inflater.inflate(R.layout.card_favourite_recipe, parent, false)
            viewHolder = FavouriteCardViewHolder(convertedView)
            convertedView.tag = viewHolder
        } else {
            viewHolder = convertedView.tag as FavouriteCardViewHolder
        }

        convertedView!!.findViewById<ImageView>(R.id.image_view_card).setOnClickListener {
            onClick(favourites.elementAt(position))
        }

        viewHolder.bind(favourites.elementAt(position), onRemoveClicked)

        return convertedView
    }
}
