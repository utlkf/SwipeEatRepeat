package edu.kit.swipeeatrepeat.components.cardStackView.viewable

import androidx.recyclerview.widget.DiffUtil
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A [DiffUtil.Callback] subclass used for meal-Lists.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeDiffCallback(
    private val old: List<Recipe>,
    private val new: List<Recipe>,
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int {
        return old.size
    }

    override fun getNewListSize(): Int {
        return new.size
    }

    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition].id == new[newPosition].id
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        return old[oldPosition] == new[newPosition]
    }
}
