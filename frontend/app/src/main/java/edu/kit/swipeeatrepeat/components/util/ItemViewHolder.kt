package edu.kit.swipeeatrepeat.components.util

import android.view.View
import androidx.recyclerview.widget.RecyclerView

/**
 * A mock-class of [RecyclerView.ViewHolder] that supplies the function [bind].
 *
 * @author Piet Förster
 * @version 1.0
 */
abstract class ItemViewHolder<T>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(value: T, position: Int)
}
