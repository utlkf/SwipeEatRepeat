package edu.kit.swipeeatrepeat.components.recipeCard.components

import android.view.View
import android.widget.TextView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemViewHolder
import edu.kit.swipeeatrepeat.model.RecipeComponent

/**
 * An implementation of [ItemViewHolder] that displays [RecipeComponent]s.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeComponentViewHolder(itemView: View) : ItemViewHolder<RecipeComponent>(itemView) {

    private val textView: TextView = itemView.findViewById(R.id.ingredient)

    override fun bind(value: RecipeComponent, position: Int) {
        textView.text = getStringFormat(value)
    }

    private fun getStringFormat(ingredient: RecipeComponent): String {
        return "${ingredient.ingredient.name} ${ingredient.quantity}${ingredient.measure.unit}"
    }
}
