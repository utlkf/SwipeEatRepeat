package edu.kit.swipeeatrepeat.fragments.accountSelection

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.activities.MainActivity
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

private const val EMAIL_REGEX = "^[\\w+.%!?#\$&'*/=^`{|}~-]+@([\\w-]+\\.)+[\\w-]{2,}\$"
private const val MIN_USERNAME_LENGTH = 2
private const val mustNotBeEmptyErrorMessage = "Must not be empty!"
private const val invalidEmailErrorMessage = "Invalid email address!"
private const val passwordsDontMatchErrorMessage = "passwords do not match"
private const val inputErrorLogMessage = "Input Error"

/**
 * A simple registration [Fragment] that checks for syntax and creates
 * the user with the help of the [UserViewModel].
 *
 * @author Arne Stramka, Piet Förster
 * @version 3.1
 */
class RegisterFragment : Fragment() {

    private lateinit var usernameView: TextView
    private lateinit var emailView: TextView
    private lateinit var passwordView: TextView
    private lateinit var repeatPasswordView: TextView
    private lateinit var submitButton: View
    private lateinit var errorMessage: TextView

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_register, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usernameView = view.findViewById(R.id.edit_text_PersonName)
        emailView = view.findViewById(R.id.edit_eMail)
        passwordView = view.findViewById(R.id.edit_password)
        repeatPasswordView = view.findViewById(R.id.edit_repeat_password)
        submitButton = view.findViewById(R.id.button_register)
        errorMessage = view.findViewById(R.id.register_backend_error_message)

        observeLifeData()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun passesUsernameRequirements(username: String): Boolean {
        return username.length >= MIN_USERNAME_LENGTH
    }

    private fun passesEmailNotEmptyRequirement(email: String): Boolean {
        return email.isNotBlank()
    }

    private fun passesEmailSyntaxRequirement(email: String): Boolean {
        val emailRegex = EMAIL_REGEX.toRegex()
        return emailRegex.matches(email)
    }

    private fun passesPasswordRequirements(password: String): Boolean {
        return password.isNotBlank()
    }

    private fun createRegistrationRequestBody(
        username: String,
        email: String,
        password: String,
    ): RequestBody {
        val json = JSONObject().apply {
            put("username", username)
            put("email", email)
            put("password", password)
        }
        return json.toString().toRequestBody("application/json; charset=utf-8".toMediaType())
    }

    private fun setViewListeners() {
        submitButton.setOnClickListener {

            val username = usernameView.text.toString()
            val email = emailView.text.toString()
            val password = passwordView.text.toString()
            val repeatPassword = repeatPasswordView.text.toString()

            errorMessage.visibility = View.INVISIBLE

            var canRegister = true

            if (!passesEmailNotEmptyRequirement(email)) {
                emailView.error = mustNotBeEmptyErrorMessage
                Log.e(inputErrorLogMessage, "email is blank")
                canRegister = false
            }
            if (!passesEmailSyntaxRequirement(email)) {
                emailView.error = invalidEmailErrorMessage
                Log.e(inputErrorLogMessage, "email has invalid syntax")
                canRegister = false
            }
            if (!passesUsernameRequirements(username)) {
                usernameView.error = mustNotBeEmptyErrorMessage
                Log.e(inputErrorLogMessage, "username too short")
                canRegister = false
            }
            if (!passesPasswordRequirements(password)) {
                passwordView.error = mustNotBeEmptyErrorMessage
                Log.e(inputErrorLogMessage, "password is blank")
                canRegister = false
            }
            if (!passesPasswordRequirements(repeatPassword)) {
                repeatPasswordView.error = mustNotBeEmptyErrorMessage
                Log.e(inputErrorLogMessage, "repeat password is blank")
                canRegister = false
            }
            if (password != repeatPassword) {
                repeatPasswordView.error = passwordsDontMatchErrorMessage
                Log.e(inputErrorLogMessage, "passwords don't match!")
                canRegister = false
            }
            if (canRegister) {
                userViewModel.registerUser(createRegistrationRequestBody(username, email, password))
            }
        }
    }

    private fun resetViewListeners() {
        submitButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        userViewModel.registrationIsSuccess.observe(viewLifecycleOwner) { isSuccess ->
            if (isSuccess) {
                startActivity(Intent(activity, MainActivity::class.java))
            }
        }
        userViewModel.registrationResponse.observe(viewLifecycleOwner) { response ->
            if (response.errorBody() != null) {
                errorMessage.text = response.errorBody()!!.string()
                errorMessage.visibility = View.VISIBLE
            }
        }
    }
}
