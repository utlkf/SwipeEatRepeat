package edu.kit.swipeeatrepeat.components.home.recipe

import android.content.Context
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.FilledTonalButton
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import edu.kit.swipeeatrepeat.R

@Composable
fun Button(context: Context, textId: Int, icon: ImageVector, onClick: () -> Unit) {
    FilledTonalButton(
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(
            containerColor = Color(ContextCompat.getColor(context, R.color.primary))
        ),
        modifier = Modifier.fillMaxWidth(0.6F)
    ) {
        Icon(
            imageVector = icon,
            contentDescription = null
        )
        Text(
            text = stringResource(id = textId),
            modifier = Modifier.fillMaxWidth(),
            textAlign = TextAlign.Center,
            fontSize = 17.sp
        )
    }
}
