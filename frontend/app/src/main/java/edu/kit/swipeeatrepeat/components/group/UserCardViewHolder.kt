package edu.kit.swipeeatrepeat.components.group

import android.view.View
import android.widget.TextView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemViewHolder
import edu.kit.swipeeatrepeat.model.User

/**
 * An implementation of [ItemViewHolder] that displays groups.
 *
 * @author Piet Förster
 * @version 1.0
 */
class UserCardViewHolder(itemView: View) : ItemViewHolder<User>(itemView) {
    private val textView: TextView = itemView.findViewById(R.id.user)

    override fun bind(value: User, position: Int) {
        textView.text = getStringFormat(value)
    }

    private fun getStringFormat(user: User): String {
        return user.username
    }
}
