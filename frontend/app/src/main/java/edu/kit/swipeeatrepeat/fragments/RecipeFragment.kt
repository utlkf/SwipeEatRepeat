package edu.kit.swipeeatrepeat.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.cardStackView.viewable.CardStackAdapter
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A simple [Fragment] subclass that displays a recipe.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeFragment(private val recipe: Recipe) : Fragment() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var backButton: View

    private val adapter = CardStackAdapter()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_recipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView = view.findViewById(R.id.card_view)
        backButton = view.findViewById(R.id.close_button)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(context)
        adapter.meals.clear()
        adapter.meals.add(recipe)

        backButton.setOnClickListener {
            parentFragmentManager.popBackStack()
        }
    }
}
