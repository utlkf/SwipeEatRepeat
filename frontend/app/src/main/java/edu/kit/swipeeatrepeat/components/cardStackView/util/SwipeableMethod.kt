package edu.kit.swipeeatrepeat.components.cardStackView.util

/**
 * An [Enum] to describe different swiping methods.
 *
 * @author Piet Förster
 * @version 1.1
 */
enum class SwipeableMethod {
    AUTOMATIC_AND_MANUAL,
    AUTOMATIC,
    MANUAL;

    fun canSwipe(): Boolean {
        return canSwipeAutomatically() || canSwipeManually()
    }

    fun canSwipeAutomatically(): Boolean {
        return this == AUTOMATIC_AND_MANUAL || this == AUTOMATIC
    }

    fun canSwipeManually(): Boolean {
        return this == AUTOMATIC_AND_MANUAL || this == MANUAL
    }
}
