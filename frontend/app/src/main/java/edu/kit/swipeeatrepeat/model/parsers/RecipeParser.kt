package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.Ingredient
import edu.kit.swipeeatrepeat.model.Measure
import edu.kit.swipeeatrepeat.model.Preference
import edu.kit.swipeeatrepeat.model.PreparationStep
import edu.kit.swipeeatrepeat.model.Recipe
import edu.kit.swipeeatrepeat.model.RecipeComponent
import org.json.JSONObject
import java.time.LocalDateTime

/**
 * An implementation of a [Parser] used for [Recipe]s.
 * Also contains [getSampleMealData], which gives mock-data for a recipe.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeParser : Parser<Recipe>() {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"
        private const val PASSIVE_COOKING_TIME = "passiveCookingTime"
        private const val ACTIVE_COOKING_TIME = "activeCookingTime"
        private const val CREATOR_ID = "creatorId"
        private const val POST_DATE = "postDate"
        private const val FAV_COUNT = "favCount"
        private const val IMAGE_URL = "imageUrl"
        private const val COMPONENTS = "components"
        private const val PREPARATION_STEPS = "preparationSteps"
        private const val PREFERENCES = "preferences"
        private const val REPLACEMENT_IMAGE =
            "https://thumbs.wbm.im/pw/medium/63befe23b490b3cad7c7f2540e43630e.avif"
    }

    private val recipeComponentParser = RecipeComponentParser()
    private val preparationStepParser = PreparationStepParser()
    private val preferenceParser = PreferenceParser()

    override fun parse(from: JSONObject): Recipe = Recipe(
        from.optLong(ID, 0),
        from.optString(NAME, ""),
        from.optInt(PASSIVE_COOKING_TIME, 0),
        from.optInt(ACTIVE_COOKING_TIME, 0),
        from.optLong(CREATOR_ID, 0),
        getDate(from.optString(POST_DATE, "")),
        from.optInt(FAV_COUNT, 0),
        if (!from.optString(IMAGE_URL).isNullOrBlank() && from.optString(IMAGE_URL) != "null") {
            from.optString(
                IMAGE_URL, REPLACEMENT_IMAGE
            )
        } else {
            REPLACEMENT_IMAGE
        },
        recipeComponentParser.parse(from.optJSONArray(COMPONENTS)).toList(),
        preparationStepParser.parse(from.optJSONArray(PREPARATION_STEPS)).toList(),
        preferenceParser.parse(from.optJSONArray(PREFERENCES)).toList()
    )

    override fun get(from: Recipe): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(NAME, from.name)
            .put(PASSIVE_COOKING_TIME, from.passiveCookingTime)
            .put(ACTIVE_COOKING_TIME, from.activeCookingTime)
            .put(CREATOR_ID, from.creatorId)
            .put(POST_DATE, from.postDate)
            .put(FAV_COUNT, from.favCount)
            .put(IMAGE_URL, from.imageUrl)
            .put(COMPONENTS, recipeComponentParser.get(from.components))
            .put(PREPARATION_STEPS, preparationStepParser.get(from.preparationSteps))
            .put(PREFERENCES, preferenceParser.get(from.preferences))
    }

    fun getSampleMealData(): List<Recipe> {
        return listOf(
            Recipe(
                100,
                "No Backend Connection",
                0,
                0,
                0,
                LocalDateTime.now(),
                0,
                REPLACEMENT_IMAGE,
                listOf(
                    RecipeComponent(
                        0,
                        Ingredient(1, "network errors"),
                        Measure(0, "", ""),
                        1
                    )
                ),
                listOf(PreparationStep(0, "try loading again")),
                listOf(Preference(0, "error"))
            )
        )
    }
}