package edu.kit.swipeeatrepeat

/**
 * Constants used throughout the app.
 *
 * @author Arne Stramka
 * @version 1.0
 */
object AppConstants {
    /**
     * The message that is broadcasted when a token refresh failed.
     */
    const val ACTION_TOKEN_REFRESH_FAILED =
        "edu.kit.swipeeatrepeat.api.http.interceptors.TOKEN_REFRESH_FAILED"

    /**
     * The interval that needs to pass between two refresh attempts to avoid excessive retrying.
     */
    const val REFRESH_RETRY_INTERVAL = 10 * 1000 // in milliseconds

    /**
     * server:
     * "http://swipe-eat-repeat.de:8080/"
     *
     * localhost:
     * "http://10.0.2.2:8080"
     */
    const val BASE_URL = "http://swipe-eat-repeat.de:8080/"
}
