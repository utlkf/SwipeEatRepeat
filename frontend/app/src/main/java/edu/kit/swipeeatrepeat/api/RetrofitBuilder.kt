package edu.kit.swipeeatrepeat.api

import android.content.Context
import edu.kit.swipeeatrepeat.AppConstants
import edu.kit.swipeeatrepeat.api.http.interceptors.AuthenticationInterceptor
import edu.kit.swipeeatrepeat.api.http.interceptors.RefreshTokenInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit

object RetrofitBuilder {

    lateinit var apiService: ApiService

    fun getApiService(context: Context): ApiService {

        if (!::apiService.isInitialized) {

            val retrofit = Retrofit.Builder()
                .baseUrl(AppConstants.BASE_URL)
                .client(okhttpClient(context))
                .build()

            apiService = retrofit.create(ApiService::class.java)
        }

        return apiService
    }

    /**
     * Initialize OkhttpClient with our interceptor
     */
    private fun okhttpClient(context: Context): OkHttpClient {

        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

        return OkHttpClient.Builder()
            // add interceptor for adding authentication header and one for logging the request
            .addInterceptor(AuthenticationInterceptor(context))
            .addInterceptor(RefreshTokenInterceptor(context))
            .addInterceptor(loggingInterceptor)
            .build()
    }
}
