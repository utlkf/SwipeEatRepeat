package edu.kit.swipeeatrepeat.model

data class Preference(
    val id: Long,
    val name: String,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}
