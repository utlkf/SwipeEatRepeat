package edu.kit.swipeeatrepeat.components.cardStackView.settings

import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.view.animation.LinearInterpolator
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import edu.kit.swipeeatrepeat.components.cardStackView.util.Duration
import edu.kit.swipeeatrepeat.components.cardStackView.util.SwipeableMethod

/**
 * The CardStackSettings (which could be moved into the classes but are easier accessed here).
 *
 * @author Piet Förster
 * @version 1.4
 */
class CardStackSetting {

    /**
     * Amount of cards loaded at once.
     */
    val visibleCount = 3

    /**
     * Scale of a card behind another, adjusted when the card in front is moved.
     * A value between 0.0f and 1.0f.
     */
    val scaleInterval = 0.95f

    /**
     * Amount of swipe needed to complete an action.
     * A value between 0.0f and 1.0f.
     */
    val swipeThreshold = 0.3f

    /**
     * Maximum rotation degree, a card reaches when swiped manually.
     */
    val maxDegree = 20.0f

    /**
     * Directions a card can be swiped in.
     */
    val directions: List<Direction> = Direction.HORIZONTAL

    /**
     * The methods with which a card can be swiped.
     */
    val swipeableMethod: SwipeableMethod = SwipeableMethod.AUTOMATIC_AND_MANUAL

    /**
     * An [Interpolator] where the rate of change is constant
     */
    val overlayInterpolator: Interpolator = LinearInterpolator()

    /**
     * The [AnimationSetting] used to animate a cardSwipe being canceled.
     */
    val rewindAnimationSetting: AnimationSetting = object : AnimationSetting {
        override val direction: Direction = Direction.BOTTOM
        override val duration: Int = Duration.NORMAL.duration
        override val interpolator: Interpolator = DecelerateInterpolator()
    }

    /**
     * The [SwipeAnimationSetting] ued to described a normal swipe.
     */
    var swipeAnimationSetting: SwipeAnimationSetting = SwipeAnimationSetting.Builder().build()
}
