package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.PreparationStep
import org.json.JSONObject

/**
 * An implementation of a [Parser] used for [PreparationStep]s.
 *
 * @author Piet Förster
 * @version 1.0
 */
class PreparationStepParser : Parser<PreparationStep>() {
    companion object {
        private const val ID = "id"
        private const val DESCRIPTION = "description"
    }

    override fun parse(from: JSONObject): PreparationStep = PreparationStep(
        from.optLong(ID, 0),
        from.optString(DESCRIPTION, "")
    )

    override fun get(from: PreparationStep): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(DESCRIPTION, from.description)
    }
}