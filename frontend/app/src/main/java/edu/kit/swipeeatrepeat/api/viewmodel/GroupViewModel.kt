package edu.kit.swipeeatrepeat.api.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import edu.kit.swipeeatrepeat.api.RetrofitBuilder
import edu.kit.swipeeatrepeat.api.SessionManager
import edu.kit.swipeeatrepeat.model.Group
import edu.kit.swipeeatrepeat.model.Recipe
import edu.kit.swipeeatrepeat.model.parsers.GroupParser
import edu.kit.swipeeatrepeat.model.parsers.RecipeParser
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject

/**
 * An [AndroidViewModel] that is used to handle the backend requests and answers for [Group]s.
 *
 * To use this, after initializing add a listener to the [MutableLiveData] of the object
 * to which the answer regarding the used call to the backend writes.
 * Then when the observer noticed a change do the things you need based on that call.
 * This will help avoid any unwanted cancellations of the [launch]
 * and also help the consistency of the changed data.
 *
 * @author Piet Förster, Dominik Wlcek, Miro Ackermann
 * @version 1.12
 */
class GroupViewModel(application: Application) : AndroidViewModel(application) {

    // ignore warning, since it is apparently common practice to get the context inside a ViewModel this way
    private val context = application.applicationContext
    private val sessionManager by lazy { SessionManager(context) }

    private val requestBodyMediaType = "application/json; charset=utf-8".toMediaType()

    val recipeLiveData = MutableLiveData<Collection<Recipe>>()
    val joinGroupLiveData = MutableLiveData<Group>()
    val createGroupLiveData = MutableLiveData<Group>()
    val editGroupLiveData = MutableLiveData<Group>()
    val getGroupsLiveData = MutableLiveData<Collection<Group>>()
    val getGroupLiveData = MutableLiveData<Group>()
    val acceptedLiveData = MutableLiveData<String>()
    val startGroupLiveData = MutableLiveData<Boolean>()
    val decisionLiveData = MutableLiveData<Recipe>()

    private val groupParser = GroupParser()
    private val recipeParser = RecipeParser()

    fun createGroup() {
        viewModelScope.launch {
            try {
                val newGroupJson = groupParser.get(
                    Group(
                        0,
                        sessionManager.fetchUserId(),
                        "",
                        "",
                        setOf(),
                        setOf(),
                        mutableSetOf(),
                        mutableSetOf(),
                        mutableSetOf(),
                        mutableSetOf()
                    )
                )
                val requestBody = newGroupJson.toString().toRequestBody(requestBodyMediaType)
                val response = RetrofitBuilder.getApiService(context).createGroup(requestBody)

                if (response.isSuccessful && response.body() != null) {
                    val group = groupParser.parse(JSONObject(response.body()!!.string()))
                    createGroupLiveData.value = group
                    Log.d("GroupViewModel", "Created group: $group")
                } else {
                    Log.e("GroupViewModel", "Error Creating Group: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("GroupViewModel", "Error Creating Group: ${e.message}")
            }
        }
    }

    fun joinGroup(groupCode: String) {
        viewModelScope.launch {
            try {
                val requestJson = JSONObject()
                    .put("userId", sessionManager.fetchUserId())
                    .put("groupCode", groupCode)

                val requestBody = requestJson.toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context).joinGroup(requestBody)

                if (response.isSuccessful && response.body() != null) {
                    val group = groupParser.parse(JSONObject(response.body()!!.string()))
                    joinGroupLiveData.value = group
                    Log.d("GroupViewModel", "Fetched Group: $group")
                } else {
                    Log.e(
                        "GroupViewModel",
                        "Error Joining Group. Response Code: ${response.code()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("GroupViewModel", "Error Joining Group: ${e.message}")
            }
        }
    }

    fun getRecipes(group: Group) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.apiService.getGroupRecipes(
                    group.id,
                    sessionManager.fetchUserId()
                )
                if (response.isSuccessful && response.body() != null) {
                    recipeLiveData.value = recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("GroupRecipeDebug", "Fetched Recipes: ${recipeLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("GroupRecipeDebug", "Error Fetching Recipes: ${e.message}")
                recipeLiveData.value = recipeParser.getSampleMealData()
            }
        }
    }

    fun getGroups() {
        viewModelScope.launch {
            try {
                val response =
                    RetrofitBuilder.apiService.getAllGroupsOfUser(sessionManager.fetchUserId())
                if (response.isSuccessful && response.body() != null) {
                    val backendJson = JSONArray(response.body()!!.string())
                    getGroupsLiveData.value = groupParser.parse(backendJson)
                    Log.d("GroupDebug", "Fetched Groups: $backendJson")
                } else {
                    Log.e(
                        "GroupDebug",
                        "Error Fetching Groups. Response Code: ${response.code()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("GroupDebug", "Error Fetching Groups: ${e.message}")
            }
        }
    }

    fun getGroup(groupId: Long) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.apiService.getGroup(groupId)
                if (response.isSuccessful && response.body() != null) {
                    val backendJson = JSONObject(response.body()!!.string())
                    getGroupLiveData.value = groupParser.parse(backendJson)
                    Log.d("GroupDebug", "Fetched Group: $backendJson")
                } else {
                    Log.e(
                        "GroupDebug",
                        "Error Fetching Group. Response Code: ${response.code()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("GroupDebug", "Error Fetching Group: ${e.message}")
            }
        }
    }

    fun editGroup(group: Group) {
        viewModelScope.launch {
            try {
                val editGroupJson = groupParser.get(group)
                val requestBody = editGroupJson.toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context).editGroup(requestBody)

                if (response.isSuccessful && response.body() != null) {
                    val answerGroup = groupParser.parse(JSONObject(response.body()!!.string()))
                    editGroupLiveData.value = answerGroup
                    Log.d("GroupDebug", "Fetched Group: $answerGroup")
                } else {
                    Log.e(
                        "GroupDebug",
                        "Error Fetching Groups. Response Code: ${response.code()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("GroupDebug", "Error editing Group: ${e.message}")
            }
        }
    }

    fun leaveGroup(group: Group) {
        viewModelScope.launch {
            try {
                val requestJson = JSONObject()
                    .put("userId", sessionManager.fetchUserId())
                    .put("groupCode", group.groupCode)

                val requestBody = requestJson.toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context).leaveGroup(requestBody)

                if (response.isSuccessful && response.body() != null) {
                    Log.d("GroupDebug", "Left Group: $group")
                } else {
                    Log.e(
                        "GroupDebug",
                        "Error Fetching Groups. Response Code: ${response.code()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("GroupDebug", "Error leaving Group: ${e.message}")
            }
        }
    }

    fun startDecision(group: Group) {
        viewModelScope.launch {
            try {
                val requestJson = JSONObject()
                    .put("value", sessionManager.fetchUserId())

                val requestBody = requestJson.toString().toRequestBody(requestBodyMediaType)
                val response = RetrofitBuilder.getApiService(context)
                    .startGroupDecision(group.id, requestBody)

                if (response.isSuccessful && response.body() != null) {
                    startGroupLiveData.value =
                        JSONObject(response.body()!!.string()).getBoolean("started")
                    Log.d("GroupDebug", "started Group Decision: $group")
                } else {
                    Log.e(
                        "GroupDebug",
                        "Error starting Group Decision. Response Code: ${response.code()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("GroupDebug", "Error starting Group Decision: ${e.message}")
            }
        }
    }

    fun accept(group: Group, recipe: Recipe) {
        viewModelScope.launch {
            try {
                val requestJson = JSONObject()
                    .put("userId", sessionManager.fetchUserId())
                    .put("recipeId", recipe.id)

                val requestBody = requestJson.toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context).groupAccept(
                    group.id,
                    requestBody
                )
                if (response.isSuccessful && response.body() != null) {
                    acceptedLiveData.value = response.body()!!.string()
                    Log.d("GroupViewModel", "Accepted recipes: ${acceptedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("GroupViewModel", "Error accepting recipe: ${e.message}")
            }
        }
    }

    fun decline(group: Group, recipe: Recipe) {
        viewModelScope.launch {
            try {
                val requestJson = JSONObject()
                    .put("userId", sessionManager.fetchUserId())
                    .put("recipeId", recipe.id)

                val requestBody = requestJson.toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context).groupDecline(
                    group.id,
                    requestBody
                )
                if (response.isSuccessful && response.body() != null) {
                    acceptedLiveData.value = response.body()!!.string()
                    Log.d("GroupViewModel", "Declined recipes: ${acceptedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("GroupViewModel", "Error declining recipe: ${e.message}")
            }
        }
    }

    fun getDecision(group: Group) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).getGroupDecision(group.id)
                if (response.isSuccessful && response.body() != null) {
                    decisionLiveData.value =
                        recipeParser.parse(JSONObject(response.body()!!.string()))
                    Log.d("GroupViewModel", "Fetched decision: ${acceptedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("GroupViewModel", "Error fetching decision: ${e.message}")
            }
        }
    }
}
