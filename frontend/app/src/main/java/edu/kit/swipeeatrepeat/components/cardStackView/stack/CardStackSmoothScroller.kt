package edu.kit.swipeeatrepeat.components.cardStackView.stack

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SmoothScroller
import edu.kit.swipeeatrepeat.components.cardStackView.listeners.CardStackListener
import edu.kit.swipeeatrepeat.components.cardStackView.settings.AnimationSetting
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction

/**
 * A [SmoothScroller] class that handles basic tracking of the target view position and provides methods to trigger
 * a programmatic scroll. Intended to be used only once. (Create new Scroller each time)
 *
 * @author Piet Förster
 * @version 1.6
 */
class CardStackSmoothScroller(
    private val type: ScrollType,
    manager: CardStackLayoutManager,
) : SmoothScroller() {
    enum class ScrollType {
        AUTOMATIC_SWIPE,
        MANUAL_SWIPE,
        MANUAL_CANCEL
    }

    private val manager: CardStackLayoutManager

    init {
        this.manager = manager
    }

    override fun onSeekTargetStep(
        dx: Int,
        dy: Int,
        state: RecyclerView.State,
        action: Action,
    ) {
        // do nothing due to scroll implemented by android
    }

    override fun onTargetFound(
        targetView: View,
        state: RecyclerView.State,
        action: Action,
    ) {
        val x = targetView.translationX.toInt()
        val y = targetView.translationY.toInt()
        val setting: AnimationSetting
        when (type) {
            ScrollType.AUTOMATIC_SWIPE -> {
                setting = manager.cardStackSetting.swipeAnimationSetting
                action.update(
                    -getDx(setting),
                    -getDy(setting),
                    setting.duration,
                    setting.interpolator
                )
            }

            ScrollType.MANUAL_CANCEL -> {
                setting = manager.cardStackSetting.rewindAnimationSetting
                action.update(
                    x,
                    y,
                    setting.duration,
                    setting.interpolator
                )
            }

            ScrollType.MANUAL_SWIPE -> {
                val dx = -x * 10
                val dy = -y * 10
                setting = manager.cardStackSetting.swipeAnimationSetting
                action.update(
                    dx,
                    dy,
                    setting.duration,
                    setting.interpolator
                )
            }
        }
    }

    override fun onStart() {
        val state: CardStackState = manager.cardStackState
        when (type) {
            ScrollType.AUTOMATIC_SWIPE -> {
                state.next(CardStackState.Status.AUTOMATIC_SWIPE_ANIMATING)
            }

            ScrollType.MANUAL_SWIPE -> {
                state.next(CardStackState.Status.MANUAL_SWIPE_ANIMATING)
            }

            ScrollType.MANUAL_CANCEL -> state.next(CardStackState.Status.REWIND_ANIMATING)
        }
    }

    override fun onStop() {
        val listener: CardStackListener = manager.cardStackListener
        if (type == ScrollType.MANUAL_CANCEL) listener.onCardCanceled()
    }

    private fun getDx(setting: AnimationSetting): Int {
        val state: CardStackState = manager.cardStackState
        return when (setting.direction) {
            Direction.LEFT -> -state.width * 2
            Direction.RIGHT -> state.width * 2
            else -> 0
        }
    }

    private fun getDy(setting: AnimationSetting): Int {
        val state: CardStackState = manager.cardStackState
        return when (setting.direction) {
            Direction.TOP -> -state.height * 2
            Direction.BOTTOM -> state.height * 2
            else -> state.height / 4
        }
    }
}
