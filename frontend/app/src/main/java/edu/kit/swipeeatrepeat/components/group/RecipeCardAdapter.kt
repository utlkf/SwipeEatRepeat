package edu.kit.swipeeatrepeat.components.group

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemGridAdapter
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A [BaseAdapter] to create [RecipeCardViewHolder] objects from the [data] list.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeCardAdapter(
    data: MutableList<Recipe>,
    private val onClick: (Recipe) -> Unit,
) : ItemGridAdapter<Recipe>(data) {
    override fun getItemId(position: Int): Long = getItem(position).id

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertedView = convertView
        val viewHolder: RecipeCardViewHolder

        if (convertedView == null) {
            val inflater = LayoutInflater.from(parent.context)
            convertedView = inflater.inflate(R.layout.card_group_recipe, parent, false)
            viewHolder = RecipeCardViewHolder(convertedView)
            convertedView.tag = viewHolder
        } else {
            viewHolder = convertedView.tag as RecipeCardViewHolder
        }

        convertedView!!.setOnClickListener {
            onClick(data.elementAt(position))
        }

        viewHolder.bind(data.elementAt(position), position)

        return convertedView
    }
}
