package edu.kit.swipeeatrepeat.components.cardStackView.listeners

import android.util.Log
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction

/**
 * A listener called, when a card enters one of the following states:
 *
 * [onCardDragging] when a card is being dragged
 *
 * [onCardSwiped] when a card has been swiped outside a certain threshold and let go
 *
 * [onCardCanceled] when a card is not swiped outside a certain threshold and let go
 *
 * [onCardAppeared] when a card is loaded to the view
 *
 * [onCardDisappeared] after [onCardSwiped], when the card is removed from the view
 *
 * @author Piet Förster
 * @version 1.1
 */
interface CardStackListener {
    fun onCardDragging(direction: Direction?, ratio: Float) {
        Log.d("CardStackView", "onCardDragging: d = ${direction?.name}, r = $ratio")
    }

    fun onCardSwiped(direction: Direction?) {
        Log.d("CardStackView", "onCardSwiped: ($direction)")
    }

    fun onCardCanceled() {
        Log.d("CardStackView", "onCardCanceled")
    }

    fun onCardAppeared() {
        Log.d("CardStackView", "onCardAppeared:")
    }

    fun onCardDisappeared() {
        Log.d("CardStackView", "onCardDisappeared:")
    }
}
