package edu.kit.swipeeatrepeat.activities

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.tabs.TabLayout
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.SessionManager
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import edu.kit.swipeeatrepeat.fragments.accountSelection.LoginFragment
import edu.kit.swipeeatrepeat.fragments.accountSelection.RegisterFragment

/**
 * An [AppCompatActivity] within which the user gets to switch between the [LoginFragment] and
 * the [RegisterFragment] in order to enter the [MainActivity] with an account for requests.
 * For that a [TabLayout] is used.
 * The user can also enter the app without credentials, by using a [Button] and is then continued
 * on to the [GuestActivity].
 *
 * @author Piet Förster, Arne Stramka
 * Version 1.2
 */
class AccountSelectionActivity : AppCompatActivity() {

    private lateinit var topNavigation: TabLayout
    private lateinit var guestButton: Button

    private val loginFragment = LoginFragment()
    private val registerFragment = RegisterFragment()

    private val sessionManager by lazy { SessionManager(applicationContext) }
    private val userViewModel by lazy { ViewModelProvider(this)[UserViewModel::class.java] }

    private val onTabSelectedListener = object : TabLayout.OnTabSelectedListener {
        override fun onTabSelected(tab: TabLayout.Tab?) {
            when (tab?.position) {
                0 -> makeCurrentFragment(loginFragment)
                1 -> makeCurrentFragment(registerFragment)
                TabLayout.Tab.INVALID_POSITION -> println(tab)
            }
        }

        override fun onTabUnselected(tab: TabLayout.Tab?) {
            // do nothing
        }

        override fun onTabReselected(tab: TabLayout.Tab?) {
            // do nothing
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (sessionManager.fetchRefreshToken() != null) {
            Log.d("AccountSelectionActivity", "User has refresh token, starting MainActivity")
            userViewModel.refreshAccessToken()
        }

        setContentView(R.layout.activity_account_selection)
        topNavigation = findViewById(R.id.top_tab_layout)
        guestButton = findViewById(R.id.button_guest)

        // start with login tab
        makeCurrentFragment(loginFragment)
        topNavigation.getTabAt(0)?.select()

        observeLifeData()
    }

    override fun onResume() {
        super.onResume()
        setViewListeners()
    }

    override fun onPause() {
        super.onPause()
        resetViewListeners()
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(null)
            commit()
        }

    private fun setViewListeners() {
        topNavigation.addOnTabSelectedListener(onTabSelectedListener)

        guestButton.setOnClickListener {
            startActivity(Intent(this, GuestActivity::class.java)).apply {
                finish()
            }
        }
    }

    private fun resetViewListeners() {
        topNavigation.removeOnTabSelectedListener(onTabSelectedListener)
        guestButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        userViewModel.refreshExecuted.observe(this) { hasExecuted ->
            if (hasExecuted) {
                startActivity(Intent(this, MainActivity::class.java)).apply {
                    finish()
                }
            }
        }
    }
}
