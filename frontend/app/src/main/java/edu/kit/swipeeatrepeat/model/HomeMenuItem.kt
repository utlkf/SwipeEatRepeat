package edu.kit.swipeeatrepeat.model

data class HomeMenuItem(
    val id: Int,
    val icon: Int,
    val title: Int,
    val onClick: () -> Unit,
)
