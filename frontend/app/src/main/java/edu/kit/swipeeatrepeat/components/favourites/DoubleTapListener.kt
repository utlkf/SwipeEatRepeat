package edu.kit.swipeeatrepeat.components.favourites

import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import java.util.Timer
import kotlin.concurrent.schedule

private const val TIME_DELAY: Long = 2000

/**
 * An implementation of [OnClickListener] that awaits [TIME_DELAY] milliseconds
 * after a click. If in that time another click occurs, it runs the [onDoubleTap]
 * method. Otherwise, the [timer] simply runs out, [clicked] is set to false and
 * the next click will trigger the time delay again.
 *
 * @author Piet Förster
 * @version 1.0
 */
class DoubleTapListener(private val onDoubleTap: () -> Unit) : OnClickListener {

    private var clicked = false
    private val timer = Timer()

    override fun onClick(v: View?) {
        Log.d("OnClickListener", "click registered on" + v.toString())
        if (clicked) {
            onDoubleTap()
        } else {
            clicked = true
            timer.schedule(TIME_DELAY) {
                clicked = false
            }
        }
    }
}