package edu.kit.swipeeatrepeat.components.recipeCard.collections

import android.util.Log
import edu.kit.swipeeatrepeat.model.Recipe

class RecipeQueue {

    private val deque = ArrayDeque<Recipe>()
    private val observers: MutableList<DequeObserver> = mutableListOf()

    val size
        get() = deque.size

    fun addObserver(observer: DequeObserver) {
        observers.add(observer)
    }

    fun removeObserver(observer: DequeObserver) {
        observers.remove(observer)
    }

    operator fun get(position: Int): Recipe {
        Log.d("get($position)", deque[position].name)
        return deque[position]
    }

    operator fun plus(elements: Collection<Recipe>): List<Recipe> {
        return deque.plus(elements)
    }

    fun toList(): List<Recipe> {
        return deque.toList()
    }

    fun add(element: Recipe): Boolean {
        if (deque.contains(element)) return false
        val result = deque.add(element)
        notifyObserversAdded()
        return result
    }

    fun addAll(elements: Collection<Recipe>): Boolean {
        val result = elements.all { add(it) }
        notifyObserversAdded()
        return result
    }

    fun first(): Recipe {
        return deque.first()
    }

    fun removeFirst(): Recipe {
        notifyObserversRemoved()
        return deque.removeFirst()
    }

    fun clear() {
        deque.clear()
    }

    fun isEmpty(): Boolean {
        return deque.isEmpty()
    }

    private fun notifyObserversRemoved() {
        for (observer in observers) {
            observer.onItemRemoved()
        }
    }

    private fun notifyObserversAdded() {
        for (observer in observers) {
            observer.onItemAdded()
        }
    }

    interface DequeObserver {
        fun onItemRemoved()
        fun onItemAdded()
    }
}
