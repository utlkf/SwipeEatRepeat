package edu.kit.swipeeatrepeat.components.recent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.recipeCard.viewHolders.RecentCardViewHolder
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A [BaseAdapter] to inflate the [RecentCardViewHolder] and bind the meals as grid-items.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecentRecipeAdapter(
    private val recent: List<Recipe>,
    private val onClick: (Recipe) -> Unit,
) : BaseAdapter() {

    override fun getCount(): Int = recent.size

    override fun getItem(position: Int): Recipe = recent.elementAt(position)

    override fun getItemId(position: Int): Long = getItem(position).id

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertedView = convertView
        val viewHolder: RecentCardViewHolder

        if (convertedView == null) {
            val inflater = LayoutInflater.from(parent.context)
            convertedView = inflater.inflate(R.layout.card_recent_recipe, parent, false)
            viewHolder = RecentCardViewHolder(convertedView)
            convertedView.tag = viewHolder
        } else {
            viewHolder = convertedView.tag as RecentCardViewHolder
        }

        convertedView!!.findViewById<ImageView>(R.id.image_view_card).setOnClickListener {
            onClick(recent.elementAt(position))
        }

        viewHolder.bind(recent.elementAt(position))

        return convertedView
    }
}
