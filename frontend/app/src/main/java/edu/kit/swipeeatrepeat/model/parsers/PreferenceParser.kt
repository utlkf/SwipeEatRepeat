package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.Preference
import org.json.JSONObject

class PreferenceParser : Parser<Preference>() {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"
    }

    override fun parse(from: JSONObject): Preference = Preference(
        from.optLong(ID, 0),
        from.optString(NAME, "")
    )

    override fun get(from: Preference): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(NAME, from.name)
    }
}
