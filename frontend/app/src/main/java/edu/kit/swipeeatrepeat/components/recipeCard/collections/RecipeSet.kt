package edu.kit.swipeeatrepeat.components.recipeCard.collections

import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A Set-like [MutableCollection] based on id's, that does all methods in O(1).
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeSet(override var size: Int = 0) : MutableCollection<Recipe> {

    private val recipes: MutableMap<Long, Recipe> = mutableMapOf()

    override fun contains(element: Recipe): Boolean {
        return recipes.containsKey(element.id)
    }

    override fun containsAll(elements: Collection<Recipe>): Boolean {
        return elements.all { contains(it) }
    }

    override fun add(element: Recipe): Boolean {
        if (contains(element)) return false
        recipes[element.id] = element
        this.size++
        return true
    }

    override fun addAll(elements: Collection<Recipe>): Boolean {
        return elements.all { add(it) }
    }

    override fun clear() {
        recipes.clear()
    }

    override fun isEmpty(): Boolean {
        return recipes.isEmpty()
    }

    override fun iterator(): MutableIterator<Recipe> {
        return recipes.values.iterator()
    }

    override fun retainAll(elements: Collection<Recipe>): Boolean {
        return this.recipes.none {
            if (elements.stream().map { element ->
                    element.id
                }.noneMatch { id ->
                    id == it.key
                }) {
                remove(it.value)
            } else {
                true
            }
        }
    }

    override fun removeAll(elements: Collection<Recipe>): Boolean {
        return elements.all { remove(it) }
    }

    override fun remove(element: Recipe): Boolean {
        val recipe = this.recipes.remove(element.id)
        if (recipe != null) size--
        return recipe != null
    }
}
