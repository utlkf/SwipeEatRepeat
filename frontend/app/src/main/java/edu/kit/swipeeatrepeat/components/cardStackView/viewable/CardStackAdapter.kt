package edu.kit.swipeeatrepeat.components.cardStackView.viewable

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.recipeCard.collections.RecipeQueue
import edu.kit.swipeeatrepeat.components.recipeCard.viewHolders.RecipeCardViewHolder

/**
 * A [RecyclerView.Adapter] used to map a meal-list to the [RecipeCardViewHolder] in
 * [R.layout.card_recipe].
 *
 * @author Piet Förster
 * @version 1.3
 */
class CardStackAdapter : RecyclerView.Adapter<RecipeCardViewHolder>() {

    val meals = RecipeQueue()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeCardViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RecipeCardViewHolder(inflater.inflate(R.layout.card_recipe, parent, false), parent)
    }

    override fun onBindViewHolder(holder: RecipeCardViewHolder, position: Int) {
        holder.bind(meals[position])
    }

    override fun getItemCount(): Int {
        return meals.size
    }
}
