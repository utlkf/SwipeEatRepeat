package edu.kit.swipeeatrepeat.components.cardStackView.listeners

import android.util.Log
import edu.kit.swipeeatrepeat.api.viewmodel.GroupViewModel
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackLayoutManager
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import edu.kit.swipeeatrepeat.components.cardStackView.viewable.CardStackAdapter
import edu.kit.swipeeatrepeat.components.recipeCard.collections.RecipeQueue
import edu.kit.swipeeatrepeat.model.Group

/**
 * A [CardStackListener] used in the [edu.kit.swipeeatrepeat.fragments.group.GroupSelectionFragment]
 *
 * @author Piet Förster
 * @version 1.2
 */
class GroupStackListener(
    private val adapter: CardStackAdapter,
    private val groupViewModel: GroupViewModel,
    private val group: Group,
) : CardStackListener {

    lateinit var layoutManager: CardStackLayoutManager

    init {
        adapter.meals.addObserver(
            object : RecipeQueue.DequeObserver {
                override fun onItemRemoved() = paginate()
                override fun onItemAdded() {
                    // ignore for now
                }
            })
    }

    override fun onCardSwiped(direction: Direction?) {
        Log.d("CardStackView", "onCardSwiped: $direction")
        when (direction) {
            Direction.RIGHT -> groupViewModel.accept(group, adapter.meals.first())

            Direction.LEFT -> groupViewModel.decline(group, adapter.meals.first())

            else -> {
                // do nothing
            }
        }
    }

    override fun onCardDisappeared() {
        Log.d("CardStackView", "onCardDisappeared: ${adapter.meals.removeFirst().name}")
        adapter.notifyItemRemoved(0)
    }

    override fun onCardAppeared() {
        Log.d("CardStackView", "onCardAppeared: ${adapter.meals.first().name}")
    }

    /**
     * Loads the next meals using the [groupViewModel] and adds them to the current ones.
     */
    private fun paginate() {
        if (adapter.meals.size > 2) return
        groupViewModel.getRecipes(group)
    }
}
