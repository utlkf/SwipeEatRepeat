package edu.kit.swipeeatrepeat.components.recipeCard.viewHolders

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.chip.ChipGroup
import com.squareup.picasso.Picasso
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.preference.PreferenceViewHolder
import edu.kit.swipeeatrepeat.components.recipeCard.components.RecipeComponentAdapter
import edu.kit.swipeeatrepeat.components.recipeCard.components.StepsAdapter
import edu.kit.swipeeatrepeat.model.PreparationStep
import edu.kit.swipeeatrepeat.model.Recipe
import edu.kit.swipeeatrepeat.model.RecipeComponent

/**
 * This [RecyclerView.ViewHolder] maps the data of a meal to the view.
 *
 * Images are loaded using [Picasso].
 *
 * @author Piet Förster
 * @version 1.1
 */
class RecipeCardViewHolder(itemView: View, private val parent: ViewGroup) :
    RecyclerView.ViewHolder(itemView) {

    private val imageViewCard: ImageView = itemView.findViewById(R.id.image_view_card)
    private val textViewName: TextView = itemView.findViewById(R.id.meal_title)
    private val preferences: ChipGroup = itemView.findViewById(R.id.meal_preferences)
    private val ingredients: RecyclerView = itemView.findViewById(R.id.ingredients_view)
    private val steps: RecyclerView = itemView.findViewById(R.id.steps_view)
    private val prepTime: TextView = itemView.findViewById(R.id.prep_time_value)
    private val cookTime: TextView = itemView.findViewById(R.id.cook_time_value)

    private val componentsList = ArrayList<RecipeComponent>()
    private val stepsList = ArrayList<PreparationStep>()

    private val recipeComponentAdapter = RecipeComponentAdapter(parent.context, componentsList)
    private val stepsAdapter = StepsAdapter(parent.context, stepsList)

    init {
        this.ingredients.layoutManager = LinearLayoutManager(parent.context)
        this.steps.layoutManager = LinearLayoutManager(parent.context)

        this.ingredients.adapter = recipeComponentAdapter
        this.steps.adapter = stepsAdapter
    }

    fun bind(meal: Recipe) {
        this.textViewName.text = meal.name

        this.prepTime.text = getTimeString(meal.passiveCookingTime)
        this.cookTime.text = getTimeString(meal.activeCookingTime)

        Picasso.get().isLoggingEnabled = true

        Picasso.get()
            .load(meal.imageUrl)
            .placeholder(R.drawable.ic_food)
            .into(imageViewCard)

        meal.preferences.forEach {
            val chip = PreferenceViewHolder(parent.context).createChip(it)
            this.preferences.addView(chip)
        }

        if (meal.components.isNotEmpty()) {
            setIngredients(meal.components)
        }

        if (meal.preparationSteps.isNotEmpty()) {
            setSteps(meal.preparationSteps)
        }
    }

    private fun getTimeString(timeInMinutes: Int): String {
        return if (timeInMinutes <= 90) {
            "$timeInMinutes min"
        } else {
            val hours = timeInMinutes / 60
            val minutes = timeInMinutes - (hours * 60)
            "$hours:$minutes h"
        }
    }

    private fun setIngredients(components: List<RecipeComponent>) {
        for (i in components.indices) {
            componentsList.add(components[i])
            recipeComponentAdapter.notifyItemInserted(i)
        }
    }

    private fun setSteps(steps: List<PreparationStep>) {
        for (i in steps.indices) {
            stepsList.add(steps[i])
            stepsAdapter.notifyItemInserted(i)
        }
    }
}
