package edu.kit.swipeeatrepeat.components.preference

import edu.kit.swipeeatrepeat.model.Preference

interface SavePreferencesListener {
    fun onSave(selectedLabels: Collection<Preference>)

    fun onCancel()
}
