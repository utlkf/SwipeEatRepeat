package edu.kit.swipeeatrepeat.model

/**
 * Dataclass of a PreparationStep
 *
 * @author Piet Förster
 * @version 1.0
 */
data class PreparationStep(
    val id: Long,
    val description: String,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}
