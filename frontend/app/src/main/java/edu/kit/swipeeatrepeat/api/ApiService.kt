package edu.kit.swipeeatrepeat.api

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Multipart
import retrofit2.http.PATCH
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    // --------------- user endpoints ---------------
    /**
     * Endpoint to login a user.
     * @param requestBody The request body containing login credentials.
     * @return Response with the login result.
     */
    @POST("/login")
    suspend fun loginUser(@Body requestBody: RequestBody): Response<ResponseBody>

    /**
     * Frontend Endpoint to register a user.
     * @param requestBody Contains credentials for registration
     * @return http-response with status-code and explanation
     */
    @POST("/register")
    suspend fun registerUser(@Body requestBody: RequestBody): Response<ResponseBody>

    /**
     * Endpoint to logout a user.
     * @return Response with the logout result.
     */
    @GET("/logout")
    suspend fun logout(): Response<ResponseBody>

    /**
     * Endpoint to refresh the access token of a user.
     * @return Response with the refreshed access token.
     */
    @POST("/refresh-token")
    suspend fun refreshAccessToken(): Response<ResponseBody>

    /**
     * Endpoint to fetch recipes for a user.
     * @return Response with JSON string of fetched recipes.
     */
    @GET("/users/{userId}/recipes")
    suspend fun getRecipes(@Path("userId") userId: Long): Response<ResponseBody>

    @POST("/users/{userId}/favourites")
    suspend fun addFavourite(
        @Path("userId") userId: Long,
        @Body requestBody: RequestBody,
    ): Response<ResponseBody>

    @PATCH("/users/{userId}/favourites")
    suspend fun removeFavourite(
        @Path("userId") userId: Long,
        @Body requestBody: RequestBody,
    ): Response<ResponseBody>

    @GET("/users/{userId}/favourites")
    suspend fun getFavourites(@Path("userId") userId: Long): Response<ResponseBody>

    @POST("/users/{userId}/accepted")
    suspend fun addAccepted(
        @Path("userId") userId: Long,
        @Body requestBody: RequestBody,
    ): Response<ResponseBody>

    @GET("/users/{userId}/accepted")
    suspend fun getAccepted(@Path("userId") userId: Long): Response<ResponseBody>

    @PATCH("/users/{userId}/accepted")
    suspend fun resetAccepted(@Path("userId") userId: Long): Response<ResponseBody>

    @POST("/users/{userId}/declined")
    suspend fun addDeclined(
        @Path("userId") userId: Long,
        @Body requestBody: RequestBody,
    ): Response<ResponseBody>

    @GET("/users/{userId}/declined")
    suspend fun getDeclined(@Path("userId") userId: Long): Response<ResponseBody>

    @PATCH("/users/{userId}/declined")
    suspend fun resetDeclined(@Path("userId") userId: Long): Response<ResponseBody>

    /**
     * Endpoint to fetch all recipes created by a specific user.
     * @param userId The ID of the user whose created recipes are to be fetched.
     * @return Response with JSON string of the user's created recipes.
     */
    @GET("/users/{userId}/created-recipes")
    suspend fun getUserCreatedRecipes(@Path("userId") userId: Long): Response<ResponseBody>

    /**
     * Endpoint to set the recipe preferences of a user
     * @param userId The id of the user to save the preferences to
     * @param labels a list of food labels representing the preferences
     */
    @PUT("users/{userId}/preferences")
    suspend fun setUserPreferences(
        @Path("userId") userId: Long,
        @Body labels: RequestBody,
    ): Response<ResponseBody>

    /**
     * Get all preferences of a user
     * @param userId The id of the user to fetch the preferences from
     * @return A response containing the labels representing the users preferences
     */
    @GET("users/{userId}/preferences")
    suspend fun getUserPreferences(@Path("userId") userId: Long): Response<ResponseBody>

    /**
     * Endpoint to set the blacklist of a user
     * @param userId The id of the user to save the blacklist to
     * @param ingredients a list of ingredients representing the blacklist
     */
    @PUT("users/{userId}/blacklist")
    suspend fun setUserBlacklist(
        @Path("userId") userId: Long,
        @Body ingredients: RequestBody,
    ): Response<ResponseBody>

    /**
     * Get the blacklist of a user
     * @param userId The id of the user to fetch the blacklist from
     * @return A response containing the ingredients representing the users blacklist
     */
    @GET("users/{userId}/blacklist")
    suspend fun getUserBlacklist(@Path("userId") userId: Long): Response<ResponseBody>

    /**
     * Endpoint to set the superset-whitelist of a user
     * @param userId The id of the user to save the whitelist to
     * @param ingredients a list of ingredients representing the superset-whitelist
     */
    @PUT("users/{userId}/superset-whitelist")
    suspend fun setUserSupersetWhitelist(
        @Path("userId") userId: Long,
        @Body ingredients: RequestBody,
    ): Response<ResponseBody>

    /**
     * Get the superset-whitelist of a user
     * @param userId The id of the user to fetch the whitelist from
     * @return A response containing the ingredients representing the users superset-whitelist
     */
    @GET("users/{userId}/superset-whitelist")
    suspend fun getUserSupersetWhitelist(@Path("userId") userId: Long): Response<ResponseBody>

    /**
     * Endpoint to set the subset-whitelist of a user
     * @param userId The id of the user to save the whitelist to
     * @param ingredients a list of ingredients representing the subset-whitelist
     */
    @PUT("users/{userId}/subset-whitelist")
    suspend fun setUserSubsetWhitelist(
        @Path("userId") userId: Long,
        @Body ingredients: RequestBody,
    ): Response<ResponseBody>

    /**
     * Get the subset-whitelist of a user
     * @param userId The id of the user to fetch the whitelist from
     * @return A response containing the ingredients representing the users subset-whitelist
     */
    @GET("users/{userId}/subset-whitelist")
    suspend fun getUserSubsetWhitelist(@Path("userId") userId: Long): Response<ResponseBody>

    // --------------- recipe endpoints ---------------

    /**
     * Endpoint to update an existing recipe.
     * @param recipeAttributes The request body containing updated recipe data.
     * @param file The request body part containing the image
     * @return Response with JSON string of the updated recipe.
     */
    @Multipart
    @PATCH("/recipes")
    suspend fun alterRecipe(
        @Part recipeAttributes: MultipartBody.Part,
        @Part file: MultipartBody.Part,
    ): Response<ResponseBody>

    /**
     * Endpoint to delete a recipe by its ID.
     * @param id The ID of the recipe to be deleted.
     * @return Response indicating the result of the delete operation.
     */
    @DELETE("/recipes/{id}")
    suspend fun deleteRecipe(@Path("id") id: Long): Response<ResponseBody>

    /**
     * Endpoint to fetch all recipes.
     * @return Response with JSON string of all recipes.
     */
    @GET("recipes")
    suspend fun getAllRecipes(): Response<ResponseBody>

    /**
     * Endpoint to fetch a specific recipe by its ID.
     * @param id The ID of the recipe.
     * @return Response with JSON string of the recipe.
     */
    @GET("/recipes/{id}")
    suspend fun getRecipeById(@Path("id") id: Long): Response<ResponseBody>

    /**
     * Endpoint to create a recipe.
     * @param recipeAttributes The request body containing the recipe data.
     * @return Response with the creation result.
     */
    @Multipart
    @POST("/recipes")
    suspend fun createRecipe(
        @Part recipeAttributes: MultipartBody.Part,
        @Part file: MultipartBody.Part,
    ): Response<ResponseBody>

    /**
     * Retrieves a specified number of recipes from the server.
     * The [Query] annotation is used to append a query parameter to the URL. In this case,
     * it will append ?count=<number> to the URL
     *
     * @param count The number of recipes to retrieve.
     * @return A Response object containing the JSON string of recipes.
     */
    @GET("recipes/limited")
    suspend fun fetchNumberOfRecipes(@Query("count") count: Int): Response<ResponseBody>

    // --------------- measure endpoints ---------------
    @GET("/measures")
    suspend fun getMeasures(): Response<ResponseBody>

    // --------------- ingredients endpoints ---------------

    @GET("/ingredients")
    suspend fun getAllIngredients(): Response<ResponseBody>

    // --------------- preference endpoints ---------------

    @GET("/preferences")
    suspend fun getAllPreferences(): Response<ResponseBody>

    // --------------- group endpoints ---------------

    @GET("/groups/{groupId}")
    suspend fun getGroup(@Path("groupId") groupId: Long): Response<ResponseBody>

    /**
     * Create a new group.
     *
     * @param requestBody The request body containing the group data.
     * @return Response containing the created group.
     */
    @POST("/groups")
    suspend fun createGroup(@Body requestBody: RequestBody): Response<ResponseBody>

    /**
     * Edit an existing group.
     *
     * @param requestBody The request body containing the group data.
     * @return Response containing the edited group.
     */
    @PATCH("/groups/edit")
    suspend fun editGroup(@Body requestBody: RequestBody): Response<ResponseBody>

    @GET("/groups/{groupId}/recipes/{userId}")
    suspend fun getGroupRecipes(
        @Path("groupId") groupId: Long,
        @Path("userId") userId: Long,
    ): Response<ResponseBody>

    @PATCH("/groups/leave")
    suspend fun leaveGroup(@Body requestBody: RequestBody): Response<ResponseBody>

    @PATCH("/groups/join")
    suspend fun joinGroup(@Body requestBody: RequestBody): Response<ResponseBody>

    /**
     * Fetches all groups of a specific user.
     *
     * @param userId The ID of the user whose groups are to be fetched.
     * @return A list of Group objects.
     */
    @GET("/groups/all/{userId}")
    suspend fun getAllGroupsOfUser(@Path("userId") userId: Long): Response<ResponseBody>

    @PATCH("/groups/{groupId}/start")
    suspend fun startGroupDecision(
        @Path("groupId") groupId: Long,
        @Body userId: RequestBody,
    ): Response<ResponseBody>

    @PUT("groups/{groupId}/accept")
    suspend fun groupAccept(
        @Path("groupId") groupId: Long,
        @Body requestBody: RequestBody,
    ): Response<ResponseBody>

    @PUT("groups/{groupId}/decline")
    suspend fun groupDecline(
        @Path("groupId") groupId: Long,
        @Body requestBody: RequestBody,
    ): Response<ResponseBody>

    @GET("groups/{groupId}/result")
    suspend fun getGroupDecision(@Path("groupId") groupId: Long): Response<ResponseBody>
}
