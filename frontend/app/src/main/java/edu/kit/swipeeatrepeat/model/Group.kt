package edu.kit.swipeeatrepeat.model

data class Group(
    val id: Long,
    val adminId: Long,
    val name: String,
    val groupCode: String,
    val members: Set<User>,
    val recipes: Set<Recipe>,
    val preferences: Set<Preference>,
    val blacklist: Set<Ingredient>,
    val supersetWhitelist: Set<Ingredient>,
    val subsetWhitelist: Set<Ingredient>,
    val recentMatch: Recipe? = null,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}
