package edu.kit.swipeeatrepeat.model

import java.time.LocalDateTime

/**
 * Dataclass of a Recipe
 *
 * @author Piet Förster
 * @version 1.0
 */
data class Recipe(
    val id: Long = 0,
    val name: String,
    val passiveCookingTime: Int,
    val activeCookingTime: Int,
    val creatorId: Long,
    val postDate: LocalDateTime,
    val favCount: Int = 0,
    val imageUrl: String = "",
    val components: List<RecipeComponent>,
    val preparationSteps: List<PreparationStep>,
    val preferences: List<Preference>,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}
