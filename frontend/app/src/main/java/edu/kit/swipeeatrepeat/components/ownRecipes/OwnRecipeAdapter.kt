package edu.kit.swipeeatrepeat.components.ownRecipes

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.recipeCard.viewHolders.OwnRecipeCardViewHolder
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A [BaseAdapter] to inflate the [OwnRecipeCardViewHolder] and bind the meals as grid-items.
 *
 * @author Marcel-Marius Soltan
 * @version 1.1
 */
class OwnRecipeAdapter(
    private val ownRecipes: List<Recipe>,
    private val onEditClicked: (Recipe) -> Unit,
    private val onClick: (Recipe) -> Unit,
) : BaseAdapter() {

    override fun getCount(): Int = ownRecipes.size

    override fun getItem(position: Int): Recipe = ownRecipes.elementAt(position)

    override fun getItemId(position: Int): Long = getItem(position).id

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertedView = convertView
        val viewHolder: OwnRecipeCardViewHolder

        if (convertedView == null) {
            val inflater = LayoutInflater.from(parent.context)
            convertedView = inflater.inflate(R.layout.card_own_recipe, parent, false)
            viewHolder = OwnRecipeCardViewHolder(convertedView)
            convertedView.tag = viewHolder
        } else {
            viewHolder = convertedView.tag as OwnRecipeCardViewHolder
        }

        convertedView!!.findViewById<ImageView>(R.id.image_view_card).setOnClickListener {
            onClick(ownRecipes.elementAt(position))
        }

        viewHolder.bind(ownRecipes.elementAt(position), onEditClicked)

        return convertedView
    }
}
