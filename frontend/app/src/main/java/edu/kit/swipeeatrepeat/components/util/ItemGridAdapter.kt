package edu.kit.swipeeatrepeat.components.util

import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

abstract class ItemGridAdapter<T>(protected var data: Collection<T>) : BaseAdapter() {

    override fun getCount(): Int = data.size

    override fun getItem(position: Int): T = data.elementAt(position)

    abstract override fun getItemId(position: Int): Long

    abstract override fun getView(position: Int, convertView: View?, parent: ViewGroup): View

}
