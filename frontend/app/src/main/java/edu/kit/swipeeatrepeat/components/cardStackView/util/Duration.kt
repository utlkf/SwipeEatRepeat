package edu.kit.swipeeatrepeat.components.cardStackView.util

/**
 * An [Enum] to describe the swipe durations.
 *
 * @author Piet Förster
 * @version 1.0
 */
enum class Duration(val duration: Int) {
    FAST(100),
    NORMAL(200),
    SLOW(500);

    companion object {
        fun fromVelocity(velocity: Int): Duration {
            if (velocity < 1000) {
                return SLOW
            } else if (velocity < 5000) {
                return NORMAL
            }
            return FAST
        }
    }
}
