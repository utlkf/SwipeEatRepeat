package edu.kit.swipeeatrepeat.components.cardStackView.util

/**
 * An [Enum] to describe the swipe-direction.
 *
 * @author Piet Förster
 * @version 1.0
 */
enum class Direction {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM;

    companion object {
        val HORIZONTAL = listOf(LEFT, RIGHT)
    }
}
