package edu.kit.swipeeatrepeat.api

import android.content.Context
import android.util.Log
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.AuthenticationToken


/**
 * Session manager to save and fetch data from SharedPreferences
 */
class SessionManager(context: Context) {
    private val masterKey = MasterKey.Builder(context)
        .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
        .build()

    private val prefs = EncryptedSharedPreferences.create(
        context,
        context.getString(R.string.app_name),
        masterKey,
        EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
        EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
    )

    companion object {
        const val USER_ACCESS_TOKEN = "accessToken"
        const val USER_REFRESH_TOKEN = "refreshToken"
        const val USER_ID = "user_id"
    }

    // TODO: Whats the point of this attribute?
    var authenticationToken: AuthenticationToken = AuthenticationToken("", "", 0)

    fun saveAuthToken(token: String) {
        val editor = prefs.edit()
        //Todo: only for debugging, remove afterwards
        Log.d("AuthenticationDebug", token + "is saved")
        //overwrites a previous token
        editor.putString(USER_ACCESS_TOKEN, token)
        editor.apply()
    }

    fun fetchAuthToken(): String? {
        val token = prefs.getString(USER_ACCESS_TOKEN, null)
        // Todo remove only for debugging purposes
        Log.d("fetched token", token ?: "nothing")
        return token
    }

    fun saveRefreshToken(token: String) {
        val editor = prefs.edit()
        //Todo: only for debugging, remove afterwards
        Log.d("AuthenticationDebug", token + "is saved")
        //overwrites a previous token
        editor.putString(USER_REFRESH_TOKEN, token)
        editor.apply()
    }

    fun fetchRefreshToken(): String? {
        val token = prefs.getString(USER_REFRESH_TOKEN, null)
        // Todo remove only for debugging purposes
        Log.d("fetched token", token ?: "nothing")
        return token
    }


    fun saveUserId(userId: Long) {
        val editor = prefs.edit()
        //Todo: only for debugging, remove afterwards
        Log.d("UserIdDebug", "$userId is saved")
        //overwrites a previous token
        editor.putLong(USER_ID, userId)
        editor.apply()
    }

    fun fetchUserId(): Long {
        val userId = prefs.getLong(USER_ID, 0)
        Log.d("UserIdDebug", USER_ID)
        return userId
    }

    fun clearSession() {
        val editor = prefs.edit()
        editor.clear()
        editor.apply()
    }
}
