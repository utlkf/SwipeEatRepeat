package edu.kit.swipeeatrepeat.api.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import edu.kit.swipeeatrepeat.api.RetrofitBuilder
import edu.kit.swipeeatrepeat.model.Ingredient
import edu.kit.swipeeatrepeat.model.parsers.IngredientParser
import kotlinx.coroutines.launch
import org.json.JSONArray

/**
 * A [ViewModel] that is used to handle the backend requests and answers for [Ingredient]s.
 *
 * To use this, after initializing add a listener to the [MutableLiveData] of the object
 * to which the answer regarding the used call to the backend writes.
 * Then when the observer noticed a change do the things you need based on that call.
 * This will help avoid any unwanted cancellations of the [launch]
 * and also help the consistency of the changed data.
 *
 * @author Piet Förster
 * @version 1.12
 */
class IngredientViewModel : ViewModel() {

    private val ingredientParser = IngredientParser()
    val ingredientsLiveData = MutableLiveData<Collection<Ingredient>>()

    fun getAllIngredients() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.apiService.getAllIngredients()
                if (response.isSuccessful && response.body() != null) {
                    ingredientsLiveData.value =
                        ingredientParser.parse(JSONArray(response.body()!!.string()))
                    Log.d("IngredientDebug", "Fetched Ingredients: ${ingredientsLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("IngredientDebug", "Error Fetching Ingredients: ${e.message}")
            }
        }
    }
}
