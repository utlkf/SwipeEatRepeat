package edu.kit.swipeeatrepeat.model.parsers

import org.json.JSONArray
import org.json.JSONObject
import java.text.ParseException
import java.time.LocalDateTime

/**
 * A simple parser that uses [parse] to convert either a [JSONObject] to [T]
 * or a [JSONArray] to a [List] of [T].
 *
 * @author Piet Förster
 * @version 1.0
 */
abstract class Parser<T> {
    abstract fun parse(from: JSONObject): T

    abstract fun get(from: T): JSONObject

    fun parse(from: JSONArray?): Collection<T> {
        val parsedList = mutableListOf<T>()
        from?.let {
            for (i in 0 until it.length()) {
                parsedList.add(this.parse(it.getJSONObject(i)))
            }
        }
        return parsedList
    }

    fun get(from: Collection<T>): JSONArray {
        val jsonArray = JSONArray()
        from.forEach {
            jsonArray.put(get(it))
        }
        return jsonArray
    }

    protected fun getDate(dateString: String): LocalDateTime {
        return try {
            LocalDateTime.parse(dateString)
        } catch (e: ParseException) {
            LocalDateTime.now()
        }
    }
}