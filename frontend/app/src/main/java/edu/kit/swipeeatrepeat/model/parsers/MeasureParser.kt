package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.Measure
import org.json.JSONObject

class MeasureParser : Parser<Measure>() {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"
        private const val UNIT = "unit"
    }

    override fun parse(from: JSONObject): Measure = Measure(
        from.optLong(ID, 0),
        from.optString(NAME, ""),
        from.optString(UNIT, "")
    )

    override fun get(from: Measure): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(NAME, from.name)
            .put(UNIT, from.unit)
    }
}