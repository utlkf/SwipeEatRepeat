package edu.kit.swipeeatrepeat.fragments.favourites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.favourites.FavouritesRecipeAdapter
import edu.kit.swipeeatrepeat.fragments.RecipeFragment
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A simple [Fragment] subclass, that shows the favourite Recipes of the user.
 *
 * @author Piet Förster, Fabian Schroth
 * @version 1.3
 */
class FavouritesFragment : Fragment() {

    private lateinit var favourites: GridView

    private lateinit var soloViewModel: RecipeViewModel

    private val favouritesList = mutableListOf<Recipe>()
    private val adapter = FavouritesRecipeAdapter(
        favouritesList,
        this::removeMealFromFavorites,
        this::openMealFavourite
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.soloViewModel = ViewModelProvider(this)[RecipeViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        favourites = view.findViewById(R.id.favourites_grid)

        favourites.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        soloViewModel.getFavourites()
    }

    private fun openMealFavourite(meal: Recipe) {
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, RecipeFragment(meal))
            addToBackStack(null)
            commit()
        }
    }

    private fun removeMealFromFavorites(recipe: Recipe) {
        soloViewModel.removeFromFavourites(recipe)
        this.favouritesList.remove(recipe)
        adapter.notifyDataSetChanged()
    }

    private fun observeLifeData() {
        soloViewModel.favouritesLiveData.observe(this) { meals ->
            if (meals != null) {
                this.favouritesList.clear()
                this.favouritesList.addAll(meals)
                adapter.notifyDataSetChanged()
            }
        }
    }
}
