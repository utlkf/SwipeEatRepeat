package edu.kit.swipeeatrepeat.model

data class RecipeComponent(
    val id: Long,
    val ingredient: Ingredient,
    val measure: Measure,
    val quantity: Int,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}
