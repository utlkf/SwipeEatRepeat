package edu.kit.swipeeatrepeat.components.group

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemViewHolder
import edu.kit.swipeeatrepeat.model.Group

private const val SEPARATOR = ", "

/**
 * An implementation of [ItemViewHolder] that displays groups.
 *
 * @author Piet Förster
 * @version 1.1
 */
class GroupCardViewHolder(itemView: View) : ItemViewHolder<Group>(itemView) {

    private val picture: ImageView = itemView.findViewById(R.id.group_image)
    private val name: TextView = itemView.findViewById(R.id.group_name)
    private val code: TextView = itemView.findViewById(R.id.group_code)
    private val members: TextView = itemView.findViewById(R.id.group_members)

    override fun bind(value: Group, position: Int) {
        name.text = value.name
        code.text = value.groupCode

        Picasso.get()
            .load(
                try {
                    (value.recentMatch ?: value.recipes.random()).imageUrl
                } catch (e: NoSuchElementException) {
                    null
                }
            )
            .resize(240, 240)
            .centerCrop()
            .placeholder(R.drawable.swipe_eat_repeat_icon_2)
            .into(picture)

        var membersText = ""
        value.members.map { it.username }.sortedDescending().forEach {
            if ((membersText + it).length < 100) {
                membersText += "$membersText, $it"
            }
        }
        members.text = membersText.removePrefix(SEPARATOR)
    }
}
