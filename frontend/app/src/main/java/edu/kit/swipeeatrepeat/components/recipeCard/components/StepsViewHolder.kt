package edu.kit.swipeeatrepeat.components.recipeCard.components

import android.view.View
import android.widget.TextView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemViewHolder
import edu.kit.swipeeatrepeat.model.PreparationStep

/**
 * An implementation of [ItemViewHolder] that displays steps.
 *
 * @author Piet Förster
 * @version 1.0
 */
class StepsViewHolder(itemView: View) : ItemViewHolder<PreparationStep>(itemView) {

    private val count: TextView = itemView.findViewById(R.id.step_count)
    private val textView: TextView = itemView.findViewById(R.id.step)
    override fun bind(value: PreparationStep, position: Int) {
        count.text = String.format("%d.", position + 1)
        textView.text = getStringFormat(value)
    }

    private fun getStringFormat(step: PreparationStep): String {
        return step.description
    }
}
