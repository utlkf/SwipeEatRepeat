package edu.kit.swipeeatrepeat.components.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.util.Log
import kotlin.math.max
import kotlin.math.min

private const val IMAGE_HEIGHT = 1200f
private const val IMAGE_WIDTH = 900f

class ImageHelper(private val context: Context) {

    fun resizeAndCropImage(uri: Uri): Bitmap {
        var inputStream = context.contentResolver.openInputStream(uri)
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeStream(inputStream, null, options)
        inputStream?.close()

        val bitmapWidth = options.outWidth
        val bitmapHeight = options.outHeight

        val yScaleFactor = bitmapHeight / IMAGE_HEIGHT
        val xScaleFactor = bitmapWidth / IMAGE_WIDTH

        val scaleFactor = if (yScaleFactor > 1 && xScaleFactor > 1) {
            max(xScaleFactor, yScaleFactor)
        } else {
            min(xScaleFactor, yScaleFactor)
        }

        var inSampleSize = 1
        // 4 == crop when too big, 2 == scale down first
        while (scaleFactor >= inSampleSize * 4) {
            inSampleSize *= 2
        }

        options.inJustDecodeBounds = false
        options.inSampleSize = inSampleSize

        inputStream = context.contentResolver.openInputStream(uri)
        val scaledBitmap = BitmapFactory.decodeStream(inputStream, null, options)
        inputStream?.close()

        if (scaledBitmap == null) throw Exception() // can't happen

        val widthDiff = scaledBitmap.width - IMAGE_WIDTH
        val heightDiff = scaledBitmap.height - IMAGE_HEIGHT
        val left = max(widthDiff / 2, 0f)
        val top = max(heightDiff / 2, 0f)

        Log.d("scaled bitmap", "width: ${scaledBitmap.width}, height: ${scaledBitmap.height}")

        val croppedBitmap = Bitmap.createBitmap(
            scaledBitmap,
            left.toInt(),
            top.toInt(),
            min(scaledBitmap.width, IMAGE_WIDTH.toInt()),
            min(scaledBitmap.height, IMAGE_HEIGHT.toInt())
        )

        Log.d("cropped bitmap", "width: ${croppedBitmap.width}, height: ${croppedBitmap.height}")

        return croppedBitmap
    }
}
