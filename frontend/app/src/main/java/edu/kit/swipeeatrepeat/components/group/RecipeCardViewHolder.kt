package edu.kit.swipeeatrepeat.components.group

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemViewHolder
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * An implementation of [ItemViewHolder] that displays groups.
 *
 * @author Piet Förster
 * @version 1.1
 */
class RecipeCardViewHolder(itemView: View) : ItemViewHolder<Recipe>(itemView) {
    private val name: TextView = itemView.findViewById(R.id.recipe_name)
    private val picture: ImageView = itemView.findViewById(R.id.recipe_image)

    override fun bind(value: Recipe, position: Int) {
        name.text = value.name

        Picasso.get()
            .load(value.imageUrl)
            .resize(120, 120)
            .centerCrop()
            .placeholder(R.drawable.swipe_eat_repeat_icon_2)
            .into(picture)
    }
}
