package edu.kit.swipeeatrepeat.components.cardStackView.stack

import androidx.recyclerview.widget.RecyclerView
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackState.Status
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import kotlin.math.abs

/**
 * Describes the state of the cardStack as well as the [Status] of the top card.
 *
 * @author Piet Förster
 * @version 1.1
 */
class CardStackState {
    var status = Status.IDLE
    var width = 0
    var height = 0
    var dx = 0
    var dy = 0

    /**
     * Holds the pointer to the card currently shown.
     */
    val topPosition = 0
    var targetPosition = RecyclerView.NO_POSITION
    var proportion = 0.0f

    enum class Status {
        IDLE,
        DRAGGING,
        REWIND_ANIMATING,
        AUTOMATIC_SWIPE_ANIMATING,
        AUTOMATIC_SWIPE_ANIMATED,
        MANUAL_SWIPE_ANIMATING,
        MANUAL_SWIPE_ANIMATED;

        val isBusy: Boolean
            get() = this != IDLE
        val isDragging: Boolean
            get() = this == DRAGGING
        val isSwipeAnimating: Boolean
            get() = this == MANUAL_SWIPE_ANIMATING || this == AUTOMATIC_SWIPE_ANIMATING

        fun toAnimatedStatus(): Status {
            return when (this) {
                MANUAL_SWIPE_ANIMATING -> {
                    MANUAL_SWIPE_ANIMATED
                }

                AUTOMATIC_SWIPE_ANIMATING -> {
                    AUTOMATIC_SWIPE_ANIMATED
                }

                else -> IDLE
            }
        }
    }

    fun next(state: Status) {
        status = state
    }

    val direction: Direction
        get() = if (abs(dy) < abs(dx)) {
            if (dx < 0) {
                Direction.LEFT
            } else {
                Direction.RIGHT
            }
        } else {
            if (dy < 0) {
                Direction.TOP
            } else {
                Direction.BOTTOM
            }
        }
    val ratio: Float
        get() {
            val absDx = abs(dx)
            val absDy = abs(dy)
            val ratio: Float = if (absDx < absDy) {
                absDy / (height / 2.0f)
            } else {
                absDx / (width / 2.0f)
            }
            return ratio.coerceAtMost(1.0f)
        }
    val isSwipeCompleted: Boolean
        get() = status.isSwipeAnimating && topPosition < targetPosition
                && (width < abs(dx) || height < abs(dy))

    fun canScrollToPosition(position: Int, itemCount: Int): Boolean {
        if (position == topPosition) {
            return false
        }
        if (position < 0) {
            return false
        }
        if (itemCount < position) {
            return false
        }
        return !status.isBusy
    }
}
