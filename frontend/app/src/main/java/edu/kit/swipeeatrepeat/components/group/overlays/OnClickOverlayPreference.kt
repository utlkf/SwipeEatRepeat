package edu.kit.swipeeatrepeat.components.group.overlays

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.Preference

class OnClickOverlayPreference : OnClickOverlay<Preference>() {
    private lateinit var value: Preference
    private val labelName by lazy { requireView().findViewById<TextView>(R.id.user_display_edit) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return layoutInflater.inflate(R.layout.overlay_group_edit_preference, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton = requireView().findViewById(R.id.button_cancel)
        rightButton = requireView().findViewById(R.id.button_remove_label_from_group)
        closeButton = requireView().findViewById(R.id.button_close)
        super.onViewCreated(view, savedInstanceState)

        labelName.text = value.name

        initializeButtons(value)
    }
}