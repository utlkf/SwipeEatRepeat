package edu.kit.swipeeatrepeat.components.group.overlays

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.Recipe

class OnClickOverlayRecipe : OnClickOverlay<Recipe>() {
    private lateinit var value: Recipe
    private val recipeName by lazy { requireView().findViewById<TextView>(R.id.recipe_display_edit) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return layoutInflater.inflate(R.layout.overlay_group_edit_recipe, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton = requireView().findViewById(R.id.button_cancel)
        rightButton = requireView().findViewById(R.id.button_remove_recipe_from_group)
        closeButton = requireView().findViewById(R.id.button_close)
        super.onViewCreated(view, savedInstanceState)

        recipeName.text = value.name

        initializeButtons(value)
    }

    companion object {
        fun newInstance(recipe: Recipe): OnClickOverlayRecipe {
            val fragment = OnClickOverlayRecipe()
            fragment.value = recipe
            return fragment
        }
    }
}