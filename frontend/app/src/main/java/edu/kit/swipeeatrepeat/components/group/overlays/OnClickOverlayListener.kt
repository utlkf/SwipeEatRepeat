package edu.kit.swipeeatrepeat.components.group.overlays

interface OnClickOverlayListener<T> {
    fun leftButton(value: T)
    fun rightButton(value: T)
}