package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.Ingredient
import org.json.JSONObject

/**
 * An implementation of a [Parser] used for [Ingredient]s.
 *
 * @author Piet Förster
 * @version 1.0
 */
class IngredientParser : Parser<Ingredient>() {
    companion object {
        private const val ID = "id"
        private const val NAME = "name"
    }

    override fun parse(from: JSONObject): Ingredient = Ingredient(
        from.optLong(ID, 0),
        from.optString(NAME, "")
    )

    override fun get(from: Ingredient): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(NAME, from.name)
    }
}