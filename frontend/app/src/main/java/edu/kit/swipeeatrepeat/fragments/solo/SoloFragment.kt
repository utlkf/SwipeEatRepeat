package edu.kit.swipeeatrepeat.fragments.solo

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.cardStackView.listeners.SoloStackListener
import edu.kit.swipeeatrepeat.components.cardStackView.settings.SwipeAnimationSetting
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackLayoutManager
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackView
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import edu.kit.swipeeatrepeat.components.cardStackView.util.Duration
import edu.kit.swipeeatrepeat.components.cardStackView.viewable.CardStackAdapter
import edu.kit.swipeeatrepeat.components.favourites.DoubleTapListener

/**
 * A simple [Fragment] subclass that is used to contain a [CardStackView] of recipes.
 *
 * This [Fragment] is the main entry point of [edu.kit.swipeeatrepeat.activities.MainActivity].
 * It presents the user one [edu.kit.swipeeatrepeat.components.recipeCard.viewHolders.RecipeCardViewHolder] after another
 * with the meal provided by the [RecipeViewModel].
 * The user can swipe these cards left or right or add them to their favourites.
 *
 * @author Piet Förster
 * @version 1.2
 */
class SoloFragment : Fragment() {

    private lateinit var viewModel: RecipeViewModel

    private val adapter: CardStackAdapter = CardStackAdapter()

    private lateinit var listener: SoloStackListener
    private lateinit var manager: CardStackLayoutManager

    private lateinit var cardStackView: CardStackView
    private lateinit var declineButton: View
    private lateinit var favouriteButton: View
    private lateinit var acceptButton: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.viewModel = ViewModelProvider(this)[RecipeViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_solo, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardStackView = view.findViewById(R.id.card_stack_view)
        declineButton = view.findViewById(R.id.button_decline)
        favouriteButton = view.findViewById(R.id.button_favourite)
        acceptButton = view.findViewById(R.id.button_accept)

        listener = SoloStackListener(adapter, viewModel)
        manager = CardStackLayoutManager(listener)

        setupCardStackView()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getRecipes()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun setupCardStackView() {
        cardStackView.adapter = adapter
        cardStackView.layoutManager = manager
        cardStackView.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
        listener.layoutManager = manager
    }

    private fun setViewListeners() {
        declineButton.setOnClickListener { handleButtonSwipe(Direction.LEFT) }
        favouriteButton.setOnClickListener { performFavouriteActions() }
        cardStackView.setOnClickListener(DoubleTapListener(this::performFavouriteActions))
        acceptButton.setOnClickListener { handleButtonSwipe(Direction.RIGHT) }
    }

    private fun resetViewListeners() {
        declineButton.setOnClickListener(null)
        favouriteButton.setOnClickListener(null)
        cardStackView.setOnClickListener(null)
        acceptButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        viewModel.recipeLiveData.observe(this) { recipes ->
            val before = adapter.meals.size
            adapter.meals.addAll(recipes)
            val after = adapter.meals.size
            adapter.notifyItemRangeChanged(before, after)
        }
    }

    private fun handleButtonSwipe(direction: Direction) {
        val setting = SwipeAnimationSetting.Builder()
            .setDirection(direction)
            .setDuration(Duration.NORMAL.duration)
            .setInterpolator(AccelerateInterpolator())
            .build()
        manager.setSwipeAnimationSetting(setting)
        cardStackView.swipe()
    }

    private fun performFavouriteActions() {
        if (adapter.meals.isEmpty()) return
        viewModel.addToFavourites(adapter.meals.first())
        val topView = manager.topView

        if (topView != null) {
            val overlay = topView.findViewById<View>(R.id.swipe_overlay)
            val icon = topView.findViewById<ImageView>(R.id.swipe_icon_overlay)

            overlay.alpha = 0.5f

            overlay.setBackgroundResource(R.drawable.overlay_yellow)
            icon.setImageResource(R.drawable.ic_favourite2)

            // overlay fade-out
            val overlayAnimator = ValueAnimator.ofFloat(overlay.alpha, 0.0f)
            overlayAnimator.duration = 1000 // 1 second to disappear
            overlayAnimator.addUpdateListener { valueAnimator ->
                overlay.alpha = valueAnimator.animatedValue as Float
            }

            // icon resize
            val iconAnimator = ValueAnimator.ofFloat(1.0f, 2.0f, 1.0f)
            iconAnimator.duration = 600
            iconAnimator.addUpdateListener { valueAnimator ->
                val scale = valueAnimator.animatedValue as Float
                icon.scaleX = scale
                icon.scaleY = scale
            }

            overlayAnimator.start()
            iconAnimator.start()
        }
    }
}
