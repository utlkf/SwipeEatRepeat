package edu.kit.swipeeatrepeat.components.cardStackView.stack

import android.graphics.PointF
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.SmoothScroller.ScrollVectorProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.cardStackView.listeners.CardStackListener
import edu.kit.swipeeatrepeat.components.cardStackView.settings.CardStackSetting
import edu.kit.swipeeatrepeat.components.cardStackView.settings.SwipeAnimationSetting
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction

/**
 * A [RecyclerView.LayoutManager] used to position cards in a stack view.
 *
 * @author Piet Förster
 * @version 1.14
 */
class CardStackLayoutManager(
    private val listener: CardStackListener,
) : RecyclerView.LayoutManager(), ScrollVectorProvider {

    private val setting: CardStackSetting = CardStackSetting()
    private val state: CardStackState = CardStackState()

    val cardStackSetting: CardStackSetting
        get() = setting
    val cardStackState: CardStackState
        get() = state
    val cardStackListener: CardStackListener
        get() = listener

    val topView: View?
        get() = findViewByPosition(state.topPosition)

    val topPosition: Int
        get() = state.topPosition

    override fun generateDefaultLayoutParams(): RecyclerView.LayoutParams {
        return RecyclerView.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
        update(recycler)
        if (state.didStructureChange() && itemCount > 0) {
            listener.onCardAppeared()
        }
    }

    override fun canScrollHorizontally(): Boolean {
        return setting.swipeableMethod.canSwipe()
    }

    override fun scrollHorizontallyBy(
        dx: Int,
        recycler: RecyclerView.Recycler,
        s: RecyclerView.State,
    ): Int {
        if (state.topPosition == itemCount) {
            return 0
        }
        when (state.status) {
            CardStackState.Status.IDLE, CardStackState.Status.DRAGGING,
            CardStackState.Status.MANUAL_SWIPE_ANIMATING,
            -> if (setting.swipeableMethod.canSwipeManually()) {
                state.dx -= dx
                update(recycler)
                return dx
            }

            CardStackState.Status.REWIND_ANIMATING -> {
                state.dx -= dx
                update(recycler)
                return dx
            }

            CardStackState.Status.AUTOMATIC_SWIPE_ANIMATING,
            -> if (setting.swipeableMethod.canSwipeAutomatically()) {
                state.dx -= dx
                update(recycler)
                return dx
            }

            CardStackState.Status.MANUAL_SWIPE_ANIMATED, CardStackState.Status.AUTOMATIC_SWIPE_ANIMATED -> {
                swipeDone()
            }
        }
        return 0
    }

    override fun onScrollStateChanged(s: Int) {
        when (s) {
            RecyclerView.SCROLL_STATE_IDLE -> when {
                state.targetPosition == RecyclerView.NO_POSITION
                        || state.topPosition == state.targetPosition -> {
                    state.next(CardStackState.Status.IDLE)
                    state.targetPosition = RecyclerView.NO_POSITION
                }

                state.topPosition < state.targetPosition -> {
                    smoothScrollToNext(state.targetPosition)
                }
            }

            RecyclerView.SCROLL_STATE_DRAGGING -> if (setting.swipeableMethod.canSwipeManually()) {
                state.next(CardStackState.Status.DRAGGING)
            }

            RecyclerView.SCROLL_STATE_SETTLING -> {
                // do nothing
            }
        }
    }

    override fun computeScrollVectorForPosition(targetPosition: Int): PointF? {
        return null
    }

    override fun scrollToPosition(position: Int) {
        if (setting.swipeableMethod.canSwipeAutomatically() && state.canScrollToPosition(
                position,
                itemCount
            )
        ) {
            smoothScrollToPosition(position)
            requestLayout()
        }
    }

    override fun smoothScrollToPosition(
        recyclerView: RecyclerView,
        s: RecyclerView.State,
        position: Int,
    ) {
        if (setting.swipeableMethod.canSwipeAutomatically() && state.canScrollToPosition(
                position,
                itemCount
            )
        ) {
            smoothScrollToPosition(position)
        }
    }

    fun updateProportion(y: Float) {
        if (state.topPosition < itemCount) {
            val view = findViewByPosition(state.topPosition)
            if (view != null) {
                val half = height / 2.0f
                state.proportion = -(y - half - view.top) / half
            }
        }
    }

    private fun swipeDone() {
        val direction: Direction = state.direction
        state.dx = 0
        state.dy = 0
        if (state.topPosition + 1 == state.targetPosition) {
            state.targetPosition = RecyclerView.NO_POSITION
        }

        postOnAnimation {
            listener.onCardSwiped(direction)
            listener.onCardDisappeared()
            if (itemCount > 0) {
                listener.onCardAppeared()
            }
        }
        removeView(topView!!)
    }

    private fun update(recycler: RecyclerView.Recycler) {
        state.width = width
        state.height = height

        if (state.isSwipeCompleted) {
            state.next(state.status.toAnimatedStatus())
        }

        detachAndScrapAttachedViews(recycler)

        val parentTop = paddingTop
        val parentLeft = paddingLeft
        val parentRight = width - paddingLeft
        val parentBottom = height - paddingBottom

        for (i in state.topPosition until minOf(
            state.topPosition + setting.visibleCount,
            itemCount
        )) {
            val child = recycler.getViewForPosition(i)
            addView(child, 0)
            measureChildWithMargins(child, 0, 0)
            layoutDecoratedWithMargins(child, parentLeft, parentTop, parentRight, parentBottom)

            if (i == state.topPosition) {
                updateTranslation(child)
                updateRotation(child)
                updateOverlay(child)
                resetScale(child)
            } else {
                resetTranslation(child)
                resetRotation(child)
                resetOverlay(child)
                updateScale(child, i - state.topPosition)
            }
        }

        if (state.status.isDragging) {
            listener.onCardDragging(state.direction, state.ratio)
        }
    }

    private fun updateTranslation(view: View) {
        view.translationX = state.dx.toFloat()
        view.translationY = state.dy.toFloat()
    }

    private fun resetTranslation(view: View) {
        view.translationX = 0.0f
        view.translationY = 0.0f
    }

    private fun updateScale(view: View, index: Int) {
        val nextIndex = index - 1
        val currentScale: Float = 1.0f - index * (1.0f - setting.scaleInterval)
        val nextScale: Float = 1.0f - nextIndex * (1.0f - setting.scaleInterval)
        val targetScale: Float = currentScale + (nextScale - currentScale) * state.ratio

        view.scaleX = targetScale
        view.scaleY = targetScale
    }

    private fun resetScale(view: View) {
        view.scaleX = 1.0f
        view.scaleY = 1.0f
    }

    private fun updateRotation(view: View) {
        val degree: Float = state.dx * setting.maxDegree / width * state.proportion
        view.rotation = degree
    }

    private fun resetRotation(view: View) {
        view.rotation = 0.0f
    }

    private fun updateOverlay(view: View) {
        val overlay = view.findViewById<View>(R.id.swipe_overlay)
        val icon = view.findViewById<ImageView>(R.id.swipe_icon_overlay)

        val direction: Direction = state.direction
        val alpha: Float = setting.overlayInterpolator.getInterpolation(state.ratio / 2)

        overlay.alpha = 0.0f

        when (direction) {
            Direction.LEFT -> {
                overlay.setBackgroundResource(R.drawable.overlay_red)
                icon.setImageResource(R.drawable.ic_decline2)
                overlay.alpha = alpha
            }

            Direction.RIGHT -> {
                overlay.setBackgroundResource(R.drawable.overlay_green)
                icon.setImageResource(R.drawable.ic_accept2)
                overlay.alpha = alpha
            }

            else -> {
                // do nothing
            }
        }
    }

    private fun resetOverlay(view: View) {
        val overlay = view.findViewById<View>(R.id.swipe_overlay)
        if (overlay != null) overlay.alpha = 0.0f
    }

    private fun smoothScrollToPosition(position: Int) {
        if (state.topPosition < position) {
            smoothScrollToNext(position)
        }
    }

    private fun smoothScrollToNext(position: Int) {
        state.proportion = 0.0f
        state.targetPosition = position
        val scroller =
            CardStackSmoothScroller(CardStackSmoothScroller.ScrollType.AUTOMATIC_SWIPE, this)
        scroller.targetPosition = state.topPosition
        startSmoothScroll(scroller)
    }

    fun setSwipeAnimationSetting(swipeAnimationSetting: SwipeAnimationSetting) {
        setting.swipeAnimationSetting = swipeAnimationSetting
    }
}
