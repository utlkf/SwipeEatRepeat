package edu.kit.swipeeatrepeat.api.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import edu.kit.swipeeatrepeat.api.RetrofitBuilder
import edu.kit.swipeeatrepeat.api.SessionManager
import edu.kit.swipeeatrepeat.model.Preference
import edu.kit.swipeeatrepeat.model.PreparationStep
import edu.kit.swipeeatrepeat.model.Recipe
import edu.kit.swipeeatrepeat.model.RecipeComponent
import edu.kit.swipeeatrepeat.model.parsers.RecipeParser
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONArray
import org.json.JSONObject
import java.io.File
import java.time.LocalDateTime

/**
 * A simple [ViewModel] that enables the user to load meals from the backend
 *
 * To use this, after initializing add a listener to the [MutableLiveData] of the object
 * to which the answer regarding the used call to the backend writes.
 * Then when the observer noticed a change do the things you need based on that call.
 * This will help avoid any unwanted cancellations of the [launch]
 * and also help the consistency of the changed data.
 *
 * @authors Dominik Wlcek, Piet Förster
 * @version 2.1
 */
class RecipeViewModel(application: Application) : AndroidViewModel(application) {

    // ignore warning, since it is apparently common practice to get the context inside a ViewModel this way
    private val context = application.applicationContext
    private val sessionManager by lazy { SessionManager(context) }

    private val requestBodyMediaType = "application/json; charset=utf-8".toMediaType()

    val acceptedLiveData = MutableLiveData<Collection<Recipe>>()
    val declinedLiveData = MutableLiveData<Collection<Recipe>>()
    val favouritesLiveData = MutableLiveData<Collection<Recipe>>()
    val recipeLiveData = MutableLiveData<Collection<Recipe>>()
    val ownRecipesLiveData = MutableLiveData<Collection<Recipe>>()
    val createRecipeLiveData = MutableLiveData<Recipe>()

    private val recipeParser = RecipeParser()

    fun getRecipes() {
        viewModelScope.launch {
            try {
                val response =
                    RetrofitBuilder.getApiService(context).getRecipes(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    recipeLiveData.value = recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("RecipeViewModel", "Fetched Recipes JSON: ${recipeLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Error Fetching Recipes: ${e.message}")
                recipeLiveData.value = recipeParser.getSampleMealData()
            }
        }
    }

    fun createRecipe(
        recipeName: String,
        passiveCookingTime: Int,
        activeCookingTime: Int,
        components: List<RecipeComponent>,
        steps: List<PreparationStep>,
        preferences: List<Preference>,
        image: File,
    ) {
        viewModelScope.launch {
            try {
                val newRecipeJson = recipeParser.get(
                    Recipe(
                        name = recipeName,
                        passiveCookingTime = passiveCookingTime,
                        activeCookingTime = activeCookingTime,
                        creatorId = sessionManager.fetchUserId(),
                        postDate = LocalDateTime.now(),
                        components = components,
                        preparationSteps = steps,
                        preferences = preferences
                    )
                )

                Log.d("RecipeViewModel", "$newRecipeJson")

                // build request body
                val requestBody = newRecipeJson.toString().toRequestBody(requestBodyMediaType)
                val recipeAttributes =
                    MultipartBody.Part.createFormData("recipeAttributes", null, requestBody)

                // send request body
                val filePart =
                    MultipartBody.Part.createFormData("file", image.name, image.asRequestBody())
                val response =
                    RetrofitBuilder.getApiService(context).createRecipe(recipeAttributes, filePart)

                if (response.isSuccessful) {
                    createRecipeLiveData.value =
                        recipeParser.parse(JSONObject(response.body()!!.string()))
                    Log.d("RecipeViewModel", "Created recipe: ${createRecipeLiveData.value}")
                } else {
                    Log.e(
                        "RecipeViewModel",
                        "Error Creating Recipes: ${response.errorBody()?.string()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception Creating Recipes: ${e.message}")
            }
        }
    }

    fun addToFavourites(recipe: Recipe) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).addFavourite(
                    sessionManager.fetchUserId(),
                    recipeParser.get(recipe).toString().toRequestBody(requestBodyMediaType)
                )
                if (response.isSuccessful && response.body() != null) {
                    favouritesLiveData.value =
                        recipeParser.parse(JSONArray(response.body()!!.string()))
                    Log.d("RecipeViewModel", "new favourites list: ${favouritesLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception adding favourite: ${e.message}")
            }
        }
    }

    fun removeFromFavourites(recipe: Recipe) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).removeFavourite(
                    sessionManager.fetchUserId(),
                    recipeParser.get(recipe).toString().toRequestBody(requestBodyMediaType)
                )
                if (response.isSuccessful) {
                    favouritesLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("RecipeViewModel", "New Favourites List: ${favouritesLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception removing favourite: ${e.message}")
            }
        }
    }

    fun getFavourites() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getFavourites(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    favouritesLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("RecipeViewModel", "Fetched favourites list: ${favouritesLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception fetching favourites: ${e.message}")
            }
        }
    }

    fun accept(recipe: Recipe) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).addAccepted(
                    sessionManager.fetchUserId(),
                    recipeParser.get(recipe).toString().toRequestBody(requestBodyMediaType)
                )
                if (response.isSuccessful && response.body() != null) {
                    acceptedLiveData.value =
                        recipeParser.parse(JSONArray(response.body()!!.string()))
                    Log.d("RecipeViewModel", "Accepted recipes: ${acceptedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception accepting recipe: ${e.message}")
            }
        }
    }

    fun getAccepted() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getAccepted(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    acceptedLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("RecipeViewModel", "Fetched accepted: ${acceptedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception fetching accepted: ${e.message}")
            }
        }
    }

    fun resetAccepted() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .resetAccepted(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    acceptedLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d(
                        "RecipeViewModel",
                        "Resetted and fetched accepted: ${acceptedLiveData.value}"
                    )
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception resetting accepted: ${e.message}")
            }
        }
    }

    fun decline(recipe: Recipe) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).addDeclined(
                    sessionManager.fetchUserId(),
                    recipeParser.get(recipe).toString().toRequestBody(requestBodyMediaType)
                )
                if (response.isSuccessful && response.body() != null) {
                    declinedLiveData.value =
                        recipeParser.parse(JSONArray(response.body()!!.string()))
                    Log.d("RecipeViewModel", "declined recipes: ${declinedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception declining recipe: ${e.message}")
            }
        }
    }

    fun getDeclined() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getDeclined(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    declinedLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("RecipeViewModel", "Fetched declined: ${declinedLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception declining recipe: ${e.message}")
            }
        }
    }

    fun resetDeclined() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .resetDeclined(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    declinedLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d(
                        "RecipeViewModel",
                        "Resetted and fetched declined: ${declinedLiveData.value}"
                    )
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception resetting declined: ${e.message}")
            }
        }
    }

    fun getUserCreatedRecipes() {
        viewModelScope.launch {
            try {
                // Call the API to get recipes for the specific user
                val response = RetrofitBuilder.getApiService(context)
                    .getUserCreatedRecipes(sessionManager.fetchUserId())
                if (response.isSuccessful) {
                    // Parse the response body as a JSON array and update the live data
                    ownRecipesLiveData.value =
                        recipeParser.parse(JSONArray(response.body()?.string()))
                    Log.d("RecipeViewModel", "Fetched user recipes JSON: ${recipeLiveData.value}")
                } else {
                    Log.e(
                        "RecipeViewModel",
                        "Error fetching user recipes: ${response.errorBody()?.string()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception fetching user recipes: ${e.message}")
            }
        }
    }

    fun deleteRecipe(recipeId: Long) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).deleteRecipe(recipeId)
                if (response.isSuccessful) {
                    createRecipeLiveData.value =
                        recipeParser.parse(JSONObject(response.body()!!.string()))
                    Log.d("RecipeViewModel", "Deleted recipe with ID: $recipeId")
                } else {
                    // Handle the error case
                    Log.e(
                        "RecipeViewModel",
                        "Error Deleting Recipe: ${response.errorBody()?.string()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception Deleting Recipe: ${e.message}")
            }
        }
    }

    fun alterRecipe(
        id: Long,
        recipeName: String,
        passiveCookingTime: Int,
        activeCookingTime: Int,
        components: List<RecipeComponent>,
        steps: List<PreparationStep>,
        preferences: List<Preference>,
        image: File,
    ) {
        viewModelScope.launch {
            try {
                val newRecipeJson = recipeParser.get(
                    Recipe(
                        id = id,
                        name = recipeName,
                        passiveCookingTime = passiveCookingTime,
                        activeCookingTime = activeCookingTime,
                        creatorId = sessionManager.fetchUserId(),
                        postDate = LocalDateTime.now(),
                        components = components,
                        preparationSteps = steps,
                        preferences = preferences
                    )
                )

                Log.d("RecipeViewModel", "$newRecipeJson")

                // build request body
                val requestBody = newRecipeJson.toString().toRequestBody(requestBodyMediaType)
                val recipeAttributes =
                    MultipartBody.Part.createFormData("recipeAttributes", null, requestBody)

                // send request body
                val filePart =
                    MultipartBody.Part.createFormData("file", image.name, image.asRequestBody())
                val response =
                    RetrofitBuilder.getApiService(context).alterRecipe(recipeAttributes, filePart)

                if (response.isSuccessful) {
                    createRecipeLiveData.value =
                        recipeParser.parse(JSONObject(response.body()!!.string()))
                    Log.d("RecipeViewModel", "Created recipe: ${createRecipeLiveData.value}")
                } else {
                    Log.e(
                        "RecipeViewModel",
                        "Error Editing Recipe: ${response.errorBody()?.string()}"
                    )
                }
            } catch (e: Exception) {
                Log.e("RecipeViewModel", "Exception Editing Recipe: ${e.message}")
            }
        }
    }
}
