package edu.kit.swipeeatrepeat.model

data class Measure(
    val id: Long,
    val name: String,
    val unit: String,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }

    override fun toString(): String = this.name

    companion object {
        fun empty(): Measure = Measure(0, "", "")
    }
}