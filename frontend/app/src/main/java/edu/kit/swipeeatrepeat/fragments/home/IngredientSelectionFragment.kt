package edu.kit.swipeeatrepeat.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.material3.AssistChip
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.IngredientViewModel
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import edu.kit.swipeeatrepeat.components.home.recipe.Button
import edu.kit.swipeeatrepeat.components.home.recipe.IngredientOverlay
import edu.kit.swipeeatrepeat.components.home.recipe.IngredientOverlayListener
import edu.kit.swipeeatrepeat.components.util.SearchViewModel
import edu.kit.swipeeatrepeat.model.Ingredient

class IngredientSelectionFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel
    private lateinit var ingredientViewModel: IngredientViewModel

    private val allIngredients = mutableSetOf<Ingredient>()
    private var blacklist by mutableStateOf(listOf<Ingredient>())
    private var subsetWhiteList by mutableStateOf(listOf<Ingredient>())
    private var supersetWhiteList by mutableStateOf(listOf<Ingredient>())
    private var currentIngredients by mutableStateOf(listOf<Ingredient>())

    private var currentInput = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
        this.ingredientViewModel = ViewModelProvider(this)[IngredientViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                MaterialTheme {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(MaterialTheme.colorScheme.background)
                            .padding(horizontal = 20.dp)
                            .verticalScroll(rememberScrollState())
                    ) {
                        IngredientSelectionContent()
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        userViewModel.getUserBlacklist()
        userViewModel.getUserSubsetWhitelist()
        userViewModel.getUserSupersetWhitelist()
        ingredientViewModel.getAllIngredients()
    }

    @OptIn(ExperimentalLayoutApi::class, ExperimentalMaterial3Api::class)
    @Composable
    fun IngredientSelectionContent() {
        Spacer(modifier = Modifier.padding(15.dp))

        Text(text = stringResource(id = R.string.supersetWhiteList))

        Box(
            modifier = Modifier
                .padding(8.dp)
                .border(3.dp, color = Color.LightGray, shape = RoundedCornerShape(5.dp))
                .fillMaxWidth(1f)
                .heightIn(min = 60.dp, max = Dp.Infinity),
            contentAlignment = Alignment.Center
        ) {
            FlowRow(modifier = Modifier.padding(5.dp, 5.dp)) {
                supersetWhiteList.forEach {
                    AssistChip(onClick = { showOverlay(it) }, label = { Text(it.name) })
                }
            }
        }

        Text(text = stringResource(id = R.string.subsetWhiteList))

        Box(
            modifier = Modifier
                .padding(8.dp)
                .border(3.dp, color = Color.LightGray, shape = RoundedCornerShape(5.dp))
                .fillMaxWidth(1f)
                .heightIn(min = 60.dp, max = Dp.Infinity),
            contentAlignment = Alignment.Center
        ) {
            FlowRow(modifier = Modifier.padding(5.dp, 5.dp)) {
                subsetWhiteList.forEach {
                    AssistChip(onClick = { showOverlay(it) }, label = { Text(it.name) })
                }
            }
        }

        Text(text = stringResource(id = R.string.blacklist))

        Box(
            modifier = Modifier
                .padding(8.dp)
                .border(3.dp, color = Color.LightGray, shape = RoundedCornerShape(5.dp))
                .fillMaxWidth(1f)
                .heightIn(min = 60.dp, max = Dp.Infinity),
            contentAlignment = Alignment.Center
        ) {
            FlowRow(modifier = Modifier.padding(5.dp, 5.dp)) {
                blacklist.forEach {
                    AssistChip(onClick = { showOverlay(it) }, label = { Text(it.name) })
                }
            }
        }

        Text(text = stringResource(id = R.string.all_ingredients))
        SearchBar(
            SearchViewModel(allIngredients.map { it.name }, currentInput),
            onTextChanged = {
                currentInput = it
                currentIngredients = allIngredients.filter { ing ->
                    ing.name.contains(it)
                }
            },
        )
        FlowRow {
            currentIngredients.forEach {
                AssistChip(onClick = { showOverlay(it) }, label = { Text(it.name) })
            }
        }

        Button(
            context = requireContext(),
            textId = R.string.save,
            icon = Icons.Default.Done,
            onClick = { save() }
        )
    }

    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun SearchBar(
        viewModel: SearchViewModel<String>,
        onTextChanged: (String) -> Unit,
    ) {
        val searchText by viewModel.searchText.collectAsState()

        OutlinedTextField(
            singleLine = true,
            value = searchText,
            onValueChange = {
                viewModel.onSearchTextChange(it)
                onTextChanged(it)
            },
            label = { Text(text = stringResource(id = R.string.ingredient_hint)) },
        )
    }

    private fun showOverlay(ingredient: Ingredient) {
        val inBlacklist = blacklist.contains(ingredient)
        val inSubsetWhitelist = subsetWhiteList.contains(ingredient)
        val inSupersetWhitelist = supersetWhiteList.contains(ingredient)

        val ingredientOverlay = IngredientOverlay.newInstance(
            ingredient,
            inBlacklist,
            inSubsetWhitelist,
            inSupersetWhitelist
        )
        ingredientOverlay.show(
            requireActivity().supportFragmentManager,
            "ingredient_selection_overlay"
        )
        ingredientOverlay.addButtonListener(object : IngredientOverlayListener {
            override fun onSubsetWhiteListPress() {
                subsetWhiteList = subsetWhiteList.toMutableList().apply {
                    if (inSubsetWhitelist) remove(ingredient) else add(ingredient)
                }
            }

            override fun onBlackListPress() {
                blacklist = blacklist.toMutableList().apply {
                    if (inBlacklist) remove(ingredient) else add(ingredient)
                }
            }

            override fun onSupersetWhiteListPress() {
                supersetWhiteList = supersetWhiteList.toMutableList().apply {
                    if (inSupersetWhitelist) remove(ingredient) else add(ingredient)
                }
            }
        })
    }

    private fun save() {
        userViewModel.setUserBlacklist(blacklist)
        userViewModel.setUserSubsetWhitelist(subsetWhiteList)
        userViewModel.setUserSupersetWhitelist(supersetWhiteList)

        parentFragmentManager.popBackStack()
    }

    private fun observeLifeData() {
        userViewModel.userBlacklist.observe(this) {
            blacklist = it.toList()
        }
        userViewModel.userSubsetWhitelist.observe(this) {
            subsetWhiteList = it.toList()
        }
        userViewModel.userSupersetWhitelist.observe(this) {
            supersetWhiteList = it.toList()

        }
        ingredientViewModel.ingredientsLiveData.observe(this) {
            allIngredients.addAll(it)
            currentIngredients = it.toList()
        }
    }
}
