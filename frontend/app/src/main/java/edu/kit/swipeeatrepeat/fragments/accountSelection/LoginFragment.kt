package edu.kit.swipeeatrepeat.fragments.accountSelection

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.activities.MainActivity
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import org.json.JSONObject

private const val MIN_USERNAME_LENGTH = 2
private const val tooShortErrorMessage = "Must be at least $MIN_USERNAME_LENGTH digits!"
private const val inputErrorLogMessage = "Input Error"
private const val mustNotBeEmptyErrorMessage = "Must not be empty!"
private const val loginFailedError = "bad credentials"

/**
 * A [Fragment] that gives the user the ability to log into their account.
 *
 * @author Piet Förster, Arne Stramka
 * @version 1.3
 */
class LoginFragment : Fragment() {

    private lateinit var usernameView: TextView
    private lateinit var passwordView: TextView
    private lateinit var errorMessage: TextView
    private lateinit var loginButton: View

    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usernameView = view.findViewById(R.id.edit_text_PersonName)
        passwordView = view.findViewById(R.id.edit_password)
        errorMessage = view.findViewById(R.id.login_backend_error_message)
        loginButton = view.findViewById(R.id.button_login)

        observeLifeData()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun passesUsernameRequirements(username: String): Boolean {
        return username.length >= MIN_USERNAME_LENGTH
    }

    private fun passesPasswordNotEmptyRequirement(password: String): Boolean {
        return password.isNotBlank()
    }

    private fun createLoginRequestBody(
        username: String,
        password: String,
    ): RequestBody {
        val json = JSONObject().apply {
            put("username", username)
            put("password", password)
        }
        return json.toString().toRequestBody("application/json; charset=utf-8".toMediaType())
    }

    private fun setViewListeners() {
        loginButton.setOnClickListener {

            val username = usernameView.text.toString()
            val password = passwordView.text.toString()

            errorMessage.visibility = View.INVISIBLE

            var canLogin = true

            if (!passesPasswordNotEmptyRequirement(password)) {
                passwordView.error = mustNotBeEmptyErrorMessage
                Log.e(inputErrorLogMessage, "password is blank")
                canLogin = false
            }
            if (!passesUsernameRequirements(username)) {
                usernameView.error = tooShortErrorMessage
                Log.e(inputErrorLogMessage, "username is blank")
                canLogin = false
            }
            if (canLogin) {
                userViewModel.loginUser(createLoginRequestBody(username, password))
            }
        }
    }

    private fun resetViewListeners() {
        loginButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        userViewModel.loginIsSuccess.observe(viewLifecycleOwner) { isSuccess ->
            if (isSuccess) {
                startActivity(Intent(activity, MainActivity::class.java))
            } else {
                passwordView.text = ""
            }
        }
        userViewModel.loginResponse.observe(viewLifecycleOwner) { response ->
            if (response.errorBody() != null) {
                errorMessage.text = loginFailedError
                errorMessage.visibility = View.VISIBLE
            }
        }
    }
}
