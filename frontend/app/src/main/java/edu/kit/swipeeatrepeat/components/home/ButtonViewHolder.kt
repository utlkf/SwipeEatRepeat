package edu.kit.swipeeatrepeat.components.home

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.ImageView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.HomeMenuItem

class ButtonViewHolder(context: Context) : View(context) {
    fun from(itemView: View, button: HomeMenuItem): View {
        this.id = button.id
        itemView.id = button.id

        val icon = itemView.findViewById<ImageView>(R.id.home_button_image)
        val title = itemView.findViewById<Button>(R.id.home_button_text)

        icon.id = button.id + 1000
        title.id = button.id + 2000

        icon.setImageResource(button.icon)
        title.setText(button.title)

        return itemView
    }
}
