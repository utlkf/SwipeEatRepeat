package edu.kit.swipeeatrepeat.fragments.group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.GridView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.GroupViewModel
import edu.kit.swipeeatrepeat.components.group.GroupCardAdapter
import edu.kit.swipeeatrepeat.model.Group

/**
 * A [Fragment] that enables the user to join or create a group.
 *
 * @author Marcel-Marius Soltan, Piet Förster
 * @version 1.2
 */
class GroupSelectionFragment : Fragment() {

    private lateinit var groupViewModel: GroupViewModel

    private lateinit var groupCode: EditText
    private lateinit var joinButton: View
    private lateinit var createButton: View
    private lateinit var groupListView: GridView
    private lateinit var noGroupsText: TextView

    private val userGroups: MutableList<Group> = mutableListOf()

    private val groupAdapter = GroupCardAdapter(userGroups, this::onGroupClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.groupViewModel = ViewModelProvider(this)[GroupViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_group_selection, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        groupCode = view.findViewById(R.id.group_code_edit)
        joinButton = view.findViewById(R.id.button_group_join)
        createButton = view.findViewById(R.id.button_group_create)
        groupListView = view.findViewById(R.id.group_list)
        noGroupsText = view.findViewById(R.id.no_groups_text)

        groupListView.adapter = groupAdapter
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onResume() {
        super.onResume()
        groupViewModel.getGroups()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun onGroupClick(group: Group) {
        makeCurrentFragment(GroupFragment(group))
    }

    private fun makeCurrentFragment(fragment: Fragment) {
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(null)
            commit()
        }
    }

    private fun setViewListeners() {
        createButton.setOnClickListener {
            groupViewModel.createGroup()
        }
        joinButton.setOnClickListener {
            if (!groupCode.text.isNullOrBlank()) {
                groupViewModel.joinGroup(groupCode.text.toString())
            }
        }
    }

    private fun resetViewListeners() {
        createButton.setOnClickListener(null)
        joinButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        groupViewModel.getGroupsLiveData.observe(this) { groups ->
            if (groups != null) {
                this.userGroups.clear()
                this.userGroups.addAll(groups.sortedBy { it.name.lowercase() })
                groupAdapter.notifyDataSetChanged()
                groupListView.visibility = View.VISIBLE
                noGroupsText.visibility = View.GONE
            } else {
                groupListView.visibility = View.GONE
                noGroupsText.visibility = View.VISIBLE
            }
        }
        groupViewModel.joinGroupLiveData.observe(this) { group ->
            makeCurrentFragment(GroupFragment(group))
        }
        groupViewModel.createGroupLiveData.observe(this) { group ->
            makeCurrentFragment(EditGroupFragment(group))
        }
    }
}
