package edu.kit.swipeeatrepeat.fragments.group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.GridView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.ChipGroup
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.GroupViewModel
import edu.kit.swipeeatrepeat.components.group.RecipeCardAdapter
import edu.kit.swipeeatrepeat.components.group.UserCardAdapter
import edu.kit.swipeeatrepeat.components.group.overlays.OnClickOverlayListener
import edu.kit.swipeeatrepeat.components.group.overlays.OnClickOverlayRecipe
import edu.kit.swipeeatrepeat.components.group.overlays.OnClickOverlayUser
import edu.kit.swipeeatrepeat.components.preference.PreferenceSelector
import edu.kit.swipeeatrepeat.components.preference.PreferenceViewHolder
import edu.kit.swipeeatrepeat.components.preference.SavePreferencesListener
import edu.kit.swipeeatrepeat.model.Group
import edu.kit.swipeeatrepeat.model.Preference
import edu.kit.swipeeatrepeat.model.Recipe
import edu.kit.swipeeatrepeat.model.User

/**
 * A [Fragment] in which you can edit the [group].
 *
 * @author Piet Förster
 * @version 1.1
 */
class EditGroupFragment(private val group: Group) : Fragment() {

    private lateinit var groupViewModel: GroupViewModel

    private lateinit var code: TextView
    private lateinit var name: EditText
    private lateinit var nameEmptyMessage: TextView
    private lateinit var members: GridView
    private lateinit var recipes: GridView
    private lateinit var labels: ChipGroup
    private lateinit var cancelButton: View
    private lateinit var saveButton: View
    private lateinit var overlay: FrameLayout
    private lateinit var addPreferenceButton: View

    private val membersList = mutableListOf<User>()
    private val recipesList = mutableListOf<Recipe>()
    private val preferencesList = mutableListOf<Preference>()
    private val userAdapter = UserCardAdapter(
        membersList.sortedBy { it.username.lowercase() },
        this::onUserClick
    )
    private val recipeAdapter = RecipeCardAdapter(recipesList, this::onRecipeClick)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.groupViewModel = ViewModelProvider(this)[GroupViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        membersList.addAll(group.members)
        recipesList.addAll(group.recipes)
        preferencesList.addAll(group.preferences)
        return inflater.inflate(R.layout.fragment_group_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        code = view.findViewById(R.id.group_edit_code)
        name = view.findViewById(R.id.group_edit_name)
        nameEmptyMessage = view.findViewById(R.id.group_edit_name_empty)
        members = view.findViewById(R.id.group_edit_members)
        recipes = view.findViewById(R.id.group_edit_recipes)
        labels = view.findViewById(R.id.group_edit_preferences)
        cancelButton = view.findViewById(R.id.button_group_edit_cancel)
        saveButton = view.findViewById(R.id.button_group_edit_save)
        overlay = view.findViewById(R.id.group_edit_overlay)
        addPreferenceButton = view.findViewById(R.id.button_group_add_preferences)

        members.adapter = userAdapter
        recipes.adapter = recipeAdapter

        code.text = group.groupCode
        name.setText(group.name)

        updateChips()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onResume() {
        super.onResume()
        overlay.visibility = View.GONE
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun onUserClick(user: User) {
        val userOverlay = OnClickOverlayUser.newInstance(user)
        userOverlay.show(requireActivity().supportFragmentManager, "user_overlay")
        userOverlay.setListener(object : OnClickOverlayListener<User> {
            override fun leftButton(value: User) {
                // implement later, if needed
            }

            override fun rightButton(value: User) {
                membersList.remove(value)
                userAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun onRecipeClick(recipe: Recipe) {
        val recipeOverlay = OnClickOverlayRecipe.newInstance(recipe)
        recipeOverlay.show(requireActivity().supportFragmentManager, "recipe_overlay")
        recipeOverlay.setListener(object : OnClickOverlayListener<Recipe> {
            override fun leftButton(value: Recipe) {
                // implement later, if needed
            }

            override fun rightButton(value: Recipe) {
                recipesList.remove(value)
                recipeAdapter.notifyDataSetChanged()
            }
        })
    }

    private fun onCancel() {
        parentFragmentManager.popBackStack()
    }

    private fun onSave() {
        if (name.text.isEmpty()) {
            nameEmptyMessage.visibility = View.VISIBLE
        } else {
            nameEmptyMessage.visibility = View.INVISIBLE
            groupViewModel.editGroup(
                Group(
                    group.id,
                    group.adminId,
                    this.name.text.toString(),
                    group.groupCode,
                    this.membersList.toSet(),
                    this.recipesList.toSet(),
                    this.preferencesList.toSet(),
                    setOf(),
                    setOf(),
                    setOf()
                )
            )
        }
    }

    private fun selectPreferences() {
        val labelOverlay = PreferenceSelector.newInstance(this.preferencesList)
        labelOverlay.show(requireActivity().supportFragmentManager, "label_adding_overlay")
        labelOverlay.addSaveListener(object : SavePreferencesListener {
            override fun onSave(selectedLabels: Collection<Preference>) {
                preferencesList.clear()
                preferencesList.addAll(selectedLabels)
                updateChips()
            }

            override fun onCancel() {
                // do nothing, closes automatically
            }
        })
    }

    private fun updateChips() {
        this.labels.removeAllViews()
        preferencesList.forEach {
            val chip = PreferenceViewHolder(requireContext()).createChip(it)
            labels.addView(chip)
        }
    }

    private fun setViewListeners() {
        cancelButton.setOnClickListener { onCancel() }
        saveButton.setOnClickListener { onSave() }
        addPreferenceButton.setOnClickListener { selectPreferences() }
    }

    private fun resetViewListeners() {
        cancelButton.setOnClickListener(null)
        saveButton.setOnClickListener(null)
        addPreferenceButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        groupViewModel.editGroupLiveData.observe(this) { group ->
            if (group != null) makeCurrentFragment(GroupFragment(group))
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(null)
            commit()
        }
}
