package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.AuthenticationToken
import org.json.JSONObject

/**
 * An implementation of a [Parser] used for [AuthenticationToken]s.
 *
 * @author Piet Förster
 * @version 1.0
 */
// TODO This class should be renamed to AuthenticationResponseParser
class AuthenticationTokenParser : Parser<AuthenticationToken>() {
    companion object {
        private const val ID = "userId"
        private const val ACCESS_TOKEN = "accessToken"
        private const val REFRESH_TOKEN = "refreshToken"
    }

    override fun parse(from: JSONObject): AuthenticationToken = AuthenticationToken(
        from.optString(ACCESS_TOKEN, "accessToken"),
        from.optString(REFRESH_TOKEN, "refreshToken"),
        from.optLong(ID, 0)
    )

    override fun get(from: AuthenticationToken): JSONObject {
        return JSONObject().put(ACCESS_TOKEN, from.accessToken)
            .put(REFRESH_TOKEN, from.refreshToken)
            .put(ID, from.userId)
    }
}