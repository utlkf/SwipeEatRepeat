package edu.kit.swipeeatrepeat.components.cardStackView.settings

import android.view.animation.Interpolator
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction

/**
 * Describes an animation in a [direction] for a [duration] using an [interpolator].
 *
 * @author Piet Förster
 * @version 1.0
 */
interface AnimationSetting {
    val direction: Direction
    val duration: Int
    val interpolator: Interpolator
}
