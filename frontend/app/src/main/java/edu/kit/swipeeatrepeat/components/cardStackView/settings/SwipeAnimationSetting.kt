package edu.kit.swipeeatrepeat.components.cardStackView.settings

import android.view.animation.AccelerateInterpolator
import android.view.animation.Interpolator
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import edu.kit.swipeeatrepeat.components.cardStackView.util.Duration

/**
 * A [AnimationSetting] used for swiping manually and automatically
 *
 * @author Piet Förster
 * @version 1.0
 */
class SwipeAnimationSetting private constructor(
    override val direction: Direction,
    override val duration: Int,
    override val interpolator: Interpolator,
) : AnimationSetting {

    class Builder {
        private var direction = Direction.RIGHT
        private var duration = Duration.NORMAL.duration
        private var interpolator: Interpolator = AccelerateInterpolator()
        fun setDirection(direction: Direction): Builder {
            this.direction = direction
            return this
        }

        fun setDuration(duration: Int): Builder {
            this.duration = duration
            return this
        }

        fun setInterpolator(interpolator: Interpolator): Builder {
            this.interpolator = interpolator
            return this
        }

        fun build(): SwipeAnimationSetting {
            return SwipeAnimationSetting(
                direction,
                duration,
                interpolator
            )
        }
    }
}
