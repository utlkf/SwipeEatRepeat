package edu.kit.swipeeatrepeat.model

/**
 * Dataclass of an Ingredient
 *
 * @author Piet Förster
 * @version 1.0
 */
data class Ingredient(
    val id: Long,
    val name: String,
) {

    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }

    override fun toString(): String = this.name

    companion object {
        fun empty(): Ingredient = Ingredient(0, "")
    }
}
