package edu.kit.swipeeatrepeat.api.http.interceptors

import android.content.Context
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import edu.kit.swipeeatrepeat.AppConstants
import edu.kit.swipeeatrepeat.api.SessionManager
import edu.kit.swipeeatrepeat.model.parsers.AuthenticationTokenParser
import okhttp3.Interceptor
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.json.JSONObject

/**
 * An [Interceptor] that intercepts every response made by Retrofit to check if
 * the [Response] code is 401 (Unauthorized). If so, the refresh token is used
 * to get a new access token and the request is retried.
 *
 * @param context The context of the application
 *
 * @author Arne Stramka
 */
class RefreshTokenInterceptor(private val context: Context) : Interceptor {

    private val sessionManager = SessionManager(context)

    /**
     * Intercepts every response made by Retrofit to check if the response code is 401 (Unauthorized).
     * If so, the refresh token is used to get a new access token and the request is retried.
     */
    override fun intercept(chain: Interceptor.Chain): Response {
        Log.d("RefreshTokenInterceptor", "inside refresh token interceptor")
        val originalRequest = chain.request()
        val originalResponse = chain.proceed(originalRequest)

        if (originalResponse.code == 401) {
            // avoid excessive retrying leading to a infinte loop
            if (hasRefreshedRecently()) {
                Log.d("RefreshTokenInterceptorDebug", "Refreshed recently")
                return originalResponse
            }
            markRefreshAttempt()

            if (originalRequest.url.toString().endsWith("/refresh-token")) {
                Log.d("RefreshTokenInterceptorDebug", "sending token refresh broadcast [1]")
                sendTokenRefreshFailedBroadcast()
                return originalResponse
            }

            // Construct request for the '/refresh-token' endpoint
            val refreshRequest = originalRequest.newBuilder()
                .url(AppConstants.BASE_URL + "/refresh-token")
                .header("Authorization", "Bearer ${sessionManager.fetchRefreshToken()}")
                .post(ByteArray(0).toRequestBody(null, 0, 0))
                .build()

            val refreshResponse = chain.proceed(refreshRequest)

            return if (refreshResponse.isSuccessful) {
                val newAuthToken = extractAccessToken(refreshResponse.body!!.string())
                sessionManager.saveAuthToken(newAuthToken)

                // Retry the original failed request with the new token
                chain.proceed(
                    originalRequest.newBuilder()
                        .header("Authorization", "Bearer $newAuthToken")
                        .build()
                )
            } else {
                Log.d("RefreshTokenInterceptorDebug", "sending token refresh broadcast [2]")
                sendTokenRefreshFailedBroadcast()
                originalResponse
            }
        }
        return originalResponse
    }

    private fun extractAccessToken(responseBody: String): String {
        val authToken = AuthenticationTokenParser().parse(JSONObject(responseBody))
        return authToken.accessToken
    }

    private var lastRefreshTimestamp: Long = 0

    private fun markRefreshAttempt() {
        lastRefreshTimestamp = System.currentTimeMillis()
    }

    private fun hasRefreshedRecently(): Boolean {
        val currentTime = System.currentTimeMillis()
        return currentTime - lastRefreshTimestamp < AppConstants.REFRESH_RETRY_INTERVAL
    }

    private fun sendTokenRefreshFailedBroadcast() {
        val intent = Intent(AppConstants.ACTION_TOKEN_REFRESH_FAILED)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }
}
