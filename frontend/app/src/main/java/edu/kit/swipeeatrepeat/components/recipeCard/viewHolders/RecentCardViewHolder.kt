package edu.kit.swipeeatrepeat.components.recipeCard.viewHolders

import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A [RecyclerView.ViewHolder] used to [bind] the meal to its [View].
 * The [View] currently contains an [ImageView] displaying the picture of the meal and
 * a [TextView] containing the name of the meal.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecentCardViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val imageViewCard: ImageView = itemView.findViewById(R.id.image_view_card)
    private val textViewName: TextView = itemView.findViewById(R.id.meal_title)

    fun bind(meal: Recipe) {
        textViewName.text = meal.name

        Picasso.get()
            .load(meal.imageUrl)
            .into(imageViewCard)
    }
}
