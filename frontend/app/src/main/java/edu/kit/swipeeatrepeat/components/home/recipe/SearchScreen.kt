package edu.kit.swipeeatrepeat.components.home.recipe

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusChanged
import androidx.compose.ui.res.stringResource
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.SearchViewModel

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SearchScreen(
    viewModel: SearchViewModel<String>,
    onTextChanged: (String) -> Unit,
) {
    val searchText by viewModel.searchText.collectAsState()
    val elementsList by viewModel.elementsList.collectAsState()

    var expanded by remember { mutableStateOf(true) }
    var focussed by remember { mutableStateOf(false) }

    ExposedDropdownMenuBox(
        modifier = Modifier.fillMaxWidth(0.5F),
        expanded = focussed,
        onExpandedChange = { },
    ) {
        OutlinedTextField(
            singleLine = true,
            value = searchText,
            onValueChange = {
                viewModel.onSearchTextChange(it)
                onTextChanged(it)
                expanded = true
            },
            label = { Text(text = stringResource(id = R.string.ingredient_hint)) },
            modifier = Modifier
                .menuAnchor()
                .onFocusChanged { focusState ->
                    focussed = focusState.isFocused
                    expanded = focussed
                },
        )
        ExposedDropdownMenu(
            expanded = expanded && elementsList.isNotEmpty(),
            onDismissRequest = {
                expanded = false
            }
        ) {
            elementsList.forEach {
                DropdownMenuItem(
                    text = {
                        Text(text = it)
                    },
                    onClick = {
                        expanded = false
                        focussed = false
                        onTextChanged(it)
                    }
                )
            }
        }
    }
}
