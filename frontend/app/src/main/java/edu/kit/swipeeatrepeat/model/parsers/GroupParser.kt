package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.Group
import org.json.JSONObject

/**
 * An implementation of a [Parser] used for [Group]s.
 *
 * @author Piet Förster
 * @version 1.0
 */
class GroupParser : Parser<Group>() {
    companion object {
        private const val ID = "id"
        private const val ADMIN_ID = "adminId"
        private const val NAME = "name"
        private const val GROUP_CODE = "groupCode"
        private const val MEMBERS = "members"
        private const val RECIPES = "recipes"
        private const val PREFERENCES = "preferences"
        private const val BLACKLIST = "blacklist"
        private const val SUPERSET_WHITELIST = "supersetWhitelist"
        private const val SUBSET_WHITELIST = "subsetWhitelist"
        private const val RECENT_MATCH = "match"
    }

    private val userParser = UserParser()
    private val recipeParser = RecipeParser()
    private val preferenceParser = PreferenceParser()
    private val ingredientParser = IngredientParser()

    override fun parse(from: JSONObject): Group = Group(
        from.optLong(ID, 0),
        from.optLong(ADMIN_ID, 0),
        from.optString(NAME, ""),
        from.optString(GROUP_CODE, "EMPTY"),
        userParser.parse(from.optJSONArray(MEMBERS)).toSet(),
        recipeParser.parse(from.optJSONArray(RECIPES)).toSet(),
        preferenceParser.parse(from.optJSONArray(PREFERENCES)).toSet(),
        ingredientParser.parse(from.optJSONArray(BLACKLIST)).toSet(),
        ingredientParser.parse(from.optJSONArray(SUPERSET_WHITELIST)).toSet(),
        ingredientParser.parse(from.optJSONArray(SUBSET_WHITELIST)).toSet(),
        if (from.has(RECENT_MATCH) && !from.isNull(RECENT_MATCH)) {
            recipeParser.parse(from.optJSONObject(RECENT_MATCH)!!)
        } else null
    )

    override fun get(from: Group): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(ADMIN_ID, from.adminId)
            .put(NAME, from.name)
            .put(GROUP_CODE, from.groupCode)
            .put(MEMBERS, userParser.get(from.members))
            .put(RECIPES, recipeParser.get(from.recipes))
            .put(PREFERENCES, preferenceParser.get(from.preferences))
            .put(BLACKLIST, ingredientParser.get(from.blacklist))
            .put(SUPERSET_WHITELIST, ingredientParser.get(from.supersetWhitelist))
            .put(SUBSET_WHITELIST, ingredientParser.get(from.subsetWhitelist))
    }
}