package edu.kit.swipeeatrepeat.activities

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import edu.kit.swipeeatrepeat.AppConstants
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.http.interceptors.RefreshTokenInterceptor
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import edu.kit.swipeeatrepeat.fragments.favourites.FavouritesFragment
import edu.kit.swipeeatrepeat.fragments.group.GroupSelectionFragment
import edu.kit.swipeeatrepeat.fragments.home.HomeFragment
import edu.kit.swipeeatrepeat.fragments.recent.RecentFragment
import edu.kit.swipeeatrepeat.fragments.solo.SoloFragment
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

/**
 * An [AppCompatActivity] that contains the main interactions the user does on the app.
 * It contains a [BottomNavigationView] in which the user can switch between the three main features.
 *
 *  (until version 1.3 this included five fragments, however since then the [FavouritesFragment] and
 * the [RecentFragment] have been moved to the [HomeFragment]
 *
 * Currently (version 1.4 of this file) the user gets to select one of the following:
 * [HomeFragment],
 * [SoloFragment] and
 * [GroupSelectionFragment].
 *
 * @author Piet Förster, Arne Stramka
 * @version 1.4
 */
class MainActivity : AppCompatActivity() {

    private val homeFragment = HomeFragment()
    private val soloFragment = SoloFragment()
    private val groupSelectionFragment = GroupSelectionFragment()

    private lateinit var bottomNavigation: BottomNavigationView

    private val userViewModel by lazy { ViewModelProvider(this)[UserViewModel::class.java] }
    private val logoutScope = CoroutineScope(Job() + Dispatchers.IO)

    /**
     * Receiver for the token refresh failed event broadcasted by the [RefreshTokenInterceptor]
     */
    private val tokenRefreshFailedReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            Log.d("MainActivity", "Received token refresh failed broadcast")
            if (intent?.action == AppConstants.ACTION_TOKEN_REFRESH_FAILED) {
                userViewModel.logout(logoutScope)
                startActivity(Intent(context, AccountSelectionActivity::class.java)).apply {
                    finish()
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        bottomNavigation = findViewById(R.id.bottom_navigation)

        makeCurrentFragment(soloFragment)
        bottomNavigation.selectedItemId = R.id.ic_food

        LocalBroadcastManager.getInstance(this)
            .registerReceiver(
                tokenRefreshFailedReceiver,
                IntentFilter(AppConstants.ACTION_TOKEN_REFRESH_FAILED)
            )

        observeLifeData()
    }

    override fun onResume() {
        super.onResume()
        setViewListeners()
    }

    override fun onPause() {
        super.onPause()
        resetViewListeners()
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(fragment.tag)
            commit()
        }

    /**
     * Unregister the receiver when the activity is destroyed to avoid leaks
     */
    override fun onDestroy() {
        super.onDestroy()
        LocalBroadcastManager.getInstance(this).unregisterReceiver(tokenRefreshFailedReceiver)
    }

    private fun setViewListeners() {
        bottomNavigation.setOnItemSelectedListener {
            when (it.itemId) {
                R.id.ic_home -> makeCurrentFragment(homeFragment)
                R.id.ic_food -> makeCurrentFragment(soloFragment)
                R.id.ic_group -> makeCurrentFragment(groupSelectionFragment)
            }
            true
        }
    }

    private fun resetViewListeners() {
        bottomNavigation.setOnClickListener(null)
    }

    private fun observeLifeData() {
        userViewModel.logoutExecuted.observe(this) { isExecuted ->
            if (isExecuted) {
                startActivity(Intent(this, AccountSelectionActivity::class.java))
            }
        }
    }
}
