package edu.kit.swipeeatrepeat.fragments.home

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddCircle
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.AssistChip
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.IngredientViewModel
import edu.kit.swipeeatrepeat.api.viewmodel.MeasureViewModel
import edu.kit.swipeeatrepeat.api.viewmodel.PreferenceViewModel
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.home.recipe.BitmapImage
import edu.kit.swipeeatrepeat.components.home.recipe.Button
import edu.kit.swipeeatrepeat.components.home.recipe.CookingTimeTextField
import edu.kit.swipeeatrepeat.components.home.recipe.MeasureSelection
import edu.kit.swipeeatrepeat.components.home.recipe.SearchScreen
import edu.kit.swipeeatrepeat.components.preference.PreferenceSelector
import edu.kit.swipeeatrepeat.components.preference.SavePreferencesListener
import edu.kit.swipeeatrepeat.components.util.ImageHelper
import edu.kit.swipeeatrepeat.components.util.SearchViewModel
import edu.kit.swipeeatrepeat.model.Ingredient
import edu.kit.swipeeatrepeat.model.Measure
import edu.kit.swipeeatrepeat.model.Preference
import edu.kit.swipeeatrepeat.model.PreparationStep
import edu.kit.swipeeatrepeat.model.Recipe
import edu.kit.swipeeatrepeat.model.RecipeComponent
import java.io.File
import java.io.FileOutputStream

private const val EMPTY_INT = -1
private const val EMPTY_COOKING_TIME = EMPTY_INT
private const val EMPTY_QUANTITY = EMPTY_INT
private const val EMPTY_STRING = ""
private const val EMPTY_INGREDIENT = EMPTY_STRING
private const val EMPTY_PREPARATION_STEP = EMPTY_STRING

class EditRecipeFragment(private val recipe: Recipe) : Fragment() {

    private lateinit var pickImage: ActivityResultLauncher<String>

    private lateinit var recipeViewModel: RecipeViewModel
    private lateinit var ingredientViewModel: IngredientViewModel
    private lateinit var measureViewModel: MeasureViewModel
    private lateinit var preferenceViewModel: PreferenceViewModel

    private val allIngredients = mutableListOf<Ingredient>()
    private val allMeasures = mutableListOf<Measure>()
    private val allPreferences = mutableListOf<Preference>()
    private val selectedComponents = mutableListOf<RecipeComponent>()

    private val primaryColor by lazy { ContextCompat.getColor(requireContext(), R.color.primary) }
    private val redColor by lazy { ContextCompat.getColor(requireContext(), R.color.red_error) }
    private val grayColor by lazy { ContextCompat.getColor(requireContext(), R.color.gray_900) }

    private var recipeName by mutableStateOf(recipe.name)
    private var passiveCookingTime by mutableStateOf(recipe.passiveCookingTime)
    private var activeCookingTime by mutableStateOf(recipe.activeCookingTime)
    private var ingredients by mutableStateOf(recipe.components.map {
        it.ingredient.name
    }.toList())
    private var quantities by mutableStateOf(recipe.components.map {
        it.quantity
    }.toList())
    private var measures by mutableStateOf(recipe.components.map {
        it.measure
    }.toList())
    private var preparationSteps by mutableStateOf(recipe.preparationSteps.map {
        it.description
    }.toList())
    private var preferences by mutableStateOf(recipe.preferences)
    private var showImage by mutableStateOf(false)
    private var recentBitmaps by mutableStateOf(listOf<Bitmap>())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        ingredientViewModel = ViewModelProvider(this)[IngredientViewModel::class.java]
        measureViewModel = ViewModelProvider(this)[MeasureViewModel::class.java]
        recipeViewModel = ViewModelProvider(this)[RecipeViewModel::class.java]
        preferenceViewModel = ViewModelProvider(this)[PreferenceViewModel::class.java]

        pickImage = registerForActivityResult(ActivityResultContracts.GetContent()) { uri: Uri? ->
            uri?.let {
                val resizedAndCroppedBitmap = ImageHelper(requireContext()).resizeAndCropImage(it)
                recentBitmaps = recentBitmaps + resizedAndCroppedBitmap
            }
        }
        loadImage()

        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        return ComposeView(requireContext()).apply {
            setContent {
                MaterialTheme {
                    //Idea for future: trying to implement the whole with a LazyColumn for better performance
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(MaterialTheme.colorScheme.background)
                            .padding(horizontal = 20.dp)
                            .verticalScroll(rememberScrollState())
                    ) {
                        CreateRecipeContent()
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ingredientViewModel.getAllIngredients()
        measureViewModel.getAllMeasures()
        preferenceViewModel.getAllPreferences()
    }

    @OptIn(ExperimentalMaterial3Api::class, ExperimentalLayoutApi::class)
    @Composable
    fun CreateRecipeContent() {
        Spacer(modifier = Modifier.padding(10.dp))

        Text(text = stringResource(id = R.string.recipe))
        OutlinedTextField(
            singleLine = true,
            value = recipeName,
            onValueChange = { recipeName = it },
            label = { Text(text = stringResource(id = R.string.recipe_name_hint)) },
            modifier = Modifier.fillMaxWidth(),
        )

        Text(text = stringResource(id = R.string.cooking_time))
        CookingTimeTextField(
            value = passiveCookingTime,
            onValueChange = {
                passiveCookingTime = if (it.isBlank()) {
                    EMPTY_COOKING_TIME
                } else {
                    it.toIntOrNull() ?: passiveCookingTime
                }
            },
            textId = R.string.passive_cooking_time_hint
        )
        CookingTimeTextField(
            value = activeCookingTime,
            onValueChange = {
                activeCookingTime = if (it.isBlank()) {
                    EMPTY_COOKING_TIME
                } else {
                    it.toIntOrNull() ?: activeCookingTime
                }
            },
            textId = R.string.active_cooking_time_hint
        )

        Text(text = stringResource(id = R.string.ingredients))
        ingredients.forEachIndexed { index, ingredient ->
            Row(modifier = Modifier.fillMaxWidth()) {

                SearchScreen(
                    SearchViewModel(allIngredients.map { it.name }, ingredient),
                    onTextChanged = {
                        ingredients = ingredients.toMutableList().apply { this[index] = it }
                    },
                )

                Spacer(modifier = Modifier.width(5.dp))

                OutlinedTextField(
                    value = getIntOrEmpty(quantities[index]),
                    onValueChange = {
                        quantities = quantities.toMutableList().apply {
                            this[index] = if (it.isBlank()) {
                                EMPTY_QUANTITY
                            } else {
                                it.toIntOrNull() ?: quantities[index]
                            }
                        }
                    },
                    label = { Text(text = stringResource(id = R.string.amount_hint)) },
                    modifier = Modifier.fillMaxWidth(0.5F),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                        imeAction = ImeAction.Done
                    ),
                )

                Spacer(modifier = Modifier.width(5.dp))

                MeasureSelection(
                    elements = allMeasures,
                    value = measures[index],
                    onUnitSelected = {
                        measures = measures.toMutableList().apply { this[index] = it }
                    }
                )
            }
        }

        Button(
            context = requireContext(),
            textId = R.string.add_ingredient,
            icon = Icons.Default.AddCircle,
            onClick = {
                ingredients = ingredients + EMPTY_INGREDIENT
                quantities = quantities + EMPTY_QUANTITY
                measures = measures + Measure.empty()
            }
        )

        Text(text = stringResource(id = R.string.preparation_steps))
        preparationSteps.forEachIndexed { index, step ->
            OutlinedTextField(
                value = step,
                onValueChange = {
                    preparationSteps = preparationSteps.toMutableList().apply { this[index] = it }
                },
                label = { Text(text = stringResource(id = R.string.preparation_step_hint)) },
                modifier = Modifier.fillMaxWidth()
            )
        }

        Button(
            context = requireContext(),
            textId = R.string.add_preparation_step,
            icon = Icons.Default.AddCircle,
            onClick = { preparationSteps = preparationSteps + EMPTY_PREPARATION_STEP },
        )

        FlowRow {
            preferences.forEach {
                AssistChip(onClick = {}, label = { Text(it.name) })
            }
        }

        Button(
            context = requireContext(),
            textId = R.string.select_preferences,
            icon = Icons.Default.Info,
            onClick = { selectPreferences() }
        )

        BitmapImage(recentBitmaps.lastOrNull())

        Button(
            context = requireContext(),
            textId = R.string.add_image,
            icon = Icons.Default.AddCircle,
            onClick = { addImage() },
        )

        Button(
            context = requireContext(),
            textId = R.string.save_changes,
            icon = Icons.Default.Done,
            onClick = { saveRecipe() }
        )

        Button(
            context = requireContext(),
            textId = R.string.cancel,
            icon = Icons.Default.Close,
            onClick = { cancel() }
        )

        Button(
            context = requireContext(),
            textId = R.string.delete_recipe,
            icon = Icons.Default.Delete,
            onClick = { deleteRecipe() }
        )

        Spacer(modifier = Modifier.padding(40.dp))
    }

    private fun loadImage() {
        Picasso.get().load(recipe.imageUrl).into(
            object : com.squareup.picasso.Target {
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if (bitmap != null) {
                        recentBitmaps = recentBitmaps + bitmap
                    }
                }

                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    // Handle error loading bitmap
                }

                override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                    // Placeholder image is being prepared
                }
            }
        )
    }

    private fun selectPreferences() {
        val labelOverlay = PreferenceSelector.newInstance(preferences)
        labelOverlay.show(requireActivity().supportFragmentManager, "label_adding_overlay")
        labelOverlay.addSaveListener(object : SavePreferencesListener {
            override fun onSave(selectedLabels: Collection<Preference>) {
                preferences = selectedLabels.toList()
            }

            override fun onCancel() {
                // do nothing, closes automatically
            }
        })
    }

    private fun addImage() {
        pickImage.launch("image/*")
        showImage = true
    }

    private fun saveRecipe() {
        filterFalseComponents()
        if (allFieldsNotEmpty()) {
            notCreated()
            return
        }

        recipeViewModel.alterRecipe(
            recipe.id,
            recipeName,
            passiveCookingTime,
            activeCookingTime,
            selectedComponents,
            preparationSteps.filter { it.isNotBlank() }.map { step -> PreparationStep(0, step) },
            preferences,
            bitmapToFile(recentBitmaps.last(), "image_${System.currentTimeMillis()}.jpg"),
        )

        saved()
    }

    private fun cancel() {
        parentFragmentManager.popBackStack()
    }

    private fun deleteRecipe() {
        recipeViewModel.deleteRecipe(recipe.id)
    }

    private fun filterFalseComponents() {
        selectedComponents.clear()
        for (i in ingredients.indices) {
            if (ingredients[i].isNotBlank() &&
                quantities[i] != EMPTY_QUANTITY &&
                measures[i] != Measure.empty()
            ) {
                selectedComponents.add(
                    RecipeComponent(
                        0,
                        allIngredients.stream().filter {
                            it.name == ingredients[i]
                        }.findFirst().orElse(Ingredient(0, ingredients[i])),
                        measures[i],
                        quantities[i]
                    )
                )
            }
        }
    }

    private fun notCreated() = showSnackBar("Please fill all required fields!", redColor)

    private fun saved() = showSnackBar("Successfully edited Recipe: $recipeName!", grayColor)

    private fun showSnackBar(message: String, color: Int) {
        val snackBar = Snackbar.make(
            requireView(), message, Snackbar.LENGTH_LONG
        )
        snackBar.view.setBackgroundColor(primaryColor)
        snackBar.setTextColor(color).show()
    }

    private fun allFieldsNotEmpty(): Boolean {
        return recipeName.isBlank() ||
                passiveCookingTime == EMPTY_COOKING_TIME ||
                activeCookingTime == EMPTY_COOKING_TIME ||
                ingredients.all { it.isBlank() } ||
                quantities.all { it == EMPTY_QUANTITY } ||
                measures.all { it == Measure.empty() } ||
                selectedComponents.isEmpty() ||
                preparationSteps.all { it.isBlank() } ||
                recentBitmaps.isEmpty()
    }

    private fun bitmapToFile(bitmap: Bitmap, filename: String): File {
        // a previous file in the cacheDir with similar name as the param filename gets overwritten
        val file = File(requireContext().cacheDir, filename)
        val fos = FileOutputStream(file)
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
        fos.close()
        return file
    }

    private fun getIntOrEmpty(value: Int): String =
        if (value >= 0) value.toString() else EMPTY_STRING

    private fun observeLifeData() {
        ingredientViewModel.ingredientsLiveData.observe(this) {
            this.allIngredients.clear()
            this.allIngredients.addAll(it)
        }
        measureViewModel.measuresLiveData.observe(this) {
            this.allMeasures.clear()
            this.allMeasures.addAll(it)
        }
        preferenceViewModel.preferencesLiveData.observe(this) {
            this.allPreferences.clear()
            this.allPreferences.addAll(it)
        }
        recipeViewModel.createRecipeLiveData.observe(this) {
            this.cancel()
        }
    }
}
