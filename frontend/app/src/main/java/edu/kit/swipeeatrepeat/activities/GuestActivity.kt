package edu.kit.swipeeatrepeat.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.fragments.solo.SoloFragment

/**
 * An [AppCompatActivity] in which the user can get a basic idea of the app without having
 * to create an account.
 *
 * Currently (version 1.0 of this file) the user gets to use the [SoloFragment],
 * although further features might be added in the future.
 *
 * @author Piet Förster
 * @version 1.0
 */
class GuestActivity : AppCompatActivity() {

    private lateinit var signUpButton: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_guest)

        signUpButton = findViewById(R.id.button_sign_up)

        makeCurrentFragment(SoloFragment())
    }

    override fun onResume() {
        super.onResume()
        setViewListeners()
    }

    override fun onPause() {
        super.onPause()
        resetViewListeners()
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            commit()
        }

    private fun setViewListeners() {
        signUpButton.setOnClickListener {
            startActivity(Intent(this, AccountSelectionActivity::class.java)).apply {
                finish()
            }
        }
    }

    private fun resetViewListeners() {
        signUpButton.setOnClickListener(null)
    }
}
