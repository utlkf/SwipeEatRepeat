package edu.kit.swipeeatrepeat.fragments.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.activities.AccountSelectionActivity
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import edu.kit.swipeeatrepeat.components.home.ButtonViewHolder
import edu.kit.swipeeatrepeat.fragments.favourites.FavouritesFragment
import edu.kit.swipeeatrepeat.fragments.recent.RecentFragment
import edu.kit.swipeeatrepeat.model.HomeMenuItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job

/**
 * A [Fragment] used for home-actions
 *
 * @author Piet Förster, Dominik Wlcek
 * @version 2.1
 */
class HomeFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel

    private lateinit var menu: GridLayout

    private val logoutScope = CoroutineScope(Job() + Dispatchers.IO)

    private val preferenceFragment = PreferenceFragment()
    private val ingredientSelectionFragment = IngredientSelectionFragment()
    private val ownRecipesFragment = OwnRecipesFragment()
    private val createRecipeFragment = CreateRecipeFragment()
    private val favouritesFragment = FavouritesFragment()
    private val recipeFragment = RecentFragment()

    private val menuItems = listOf(
        HomeMenuItem(
            id = R.id.button_manage_preferences,
            icon = R.drawable.icon_manage_preference,
            title = R.string.manage_preferences,
            onClick = { setupButton(preferenceFragment) }
        ),
        HomeMenuItem(
            id = R.id.button_filter_ingredients,
            icon = R.drawable.icon_filter_ingedients,
            title = R.string.filter_ingredients,
            onClick = { setupButton(ingredientSelectionFragment) }
        ),
        HomeMenuItem(
            id = R.id.button_own_recipes,
            icon = R.drawable.icon_own,
            title = R.string.own_recipe,
            onClick = { setupButton(ownRecipesFragment) }
        ),
        HomeMenuItem(
            id = R.id.button_create_recipe,
            icon = R.drawable.icon_create,
            title = R.string.create_recipe,
            onClick = { setupButton(createRecipeFragment) }
        ),
        HomeMenuItem(
            id = R.id.button_favourites,
            icon = R.drawable.icon_favorite_4,
            title = R.string.favourites,
            onClick = { setupButton(favouritesFragment) }
        ),
        HomeMenuItem(
            id = R.id.button_recent,
            icon = R.drawable.icon_recents2,
            title = R.string.recent,
            onClick = { setupButton(recipeFragment) }
        ),
        HomeMenuItem(
            id = R.id.button_logout,
            icon = R.drawable.icon_logout,
            title = R.string.logout,
            onClick = this::logout
        ),
        HomeMenuItem(
            id = R.id.button_share,
            icon = R.drawable.icon_share,
            title = R.string.share,
            onClick = this::share
        ),
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        this.userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        menu = view.findViewById(R.id.buttons)

        menuItems.forEach {
            val button = ButtonViewHolder(requireContext()).from(
                layoutInflater.inflate(R.layout.card_home_menu_button, menu, false), it
            )
            menu.addView(button)
        }

        observeLifeData()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun logout() = userViewModel.logout(logoutScope)
    private fun share() {
        // share ?
    }

    private fun setupButton(fragment: Fragment) {
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(null)
            commit()
        }
    }

    private fun setViewListeners() {
        menuItems.forEach {
            menu.findViewById<View>(it.id).setOnClickListener { _ -> it.onClick() }
        }
    }

    private fun resetViewListeners() {
        menuItems.forEach {
            menu.findViewById<View>(it.id).setOnClickListener(null)
        }
    }

    private fun observeLifeData() {
        userViewModel.logoutExecuted.observe(viewLifecycleOwner) { isExecuted ->
            if (isExecuted) startActivity(Intent(activity, AccountSelectionActivity::class.java))
        }
    }
}
