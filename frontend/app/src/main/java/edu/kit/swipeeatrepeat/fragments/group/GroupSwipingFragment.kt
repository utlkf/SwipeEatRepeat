package edu.kit.swipeeatrepeat.fragments.group

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.core.animation.doOnEnd
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.GroupViewModel
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.cardStackView.listeners.GroupStackListener
import edu.kit.swipeeatrepeat.components.cardStackView.settings.SwipeAnimationSetting
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackLayoutManager
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackView
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import edu.kit.swipeeatrepeat.components.cardStackView.util.Duration
import edu.kit.swipeeatrepeat.components.cardStackView.viewable.CardStackAdapter
import edu.kit.swipeeatrepeat.components.favourites.DoubleTapListener
import edu.kit.swipeeatrepeat.components.recipeCard.collections.RecipeQueue
import edu.kit.swipeeatrepeat.fragments.RecipeFragment
import edu.kit.swipeeatrepeat.model.Group
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A simple [Fragment] subclass that is used to contain a [CardStackView] of recipes.
 *
 * This [Fragment] lets a group swipe meals left and right individually
 *
 * @author Piet Förster
 * @version 1.2
 */
class GroupSwipingFragment(private val group: Group) : Fragment() {

    private lateinit var soloViewModel: RecipeViewModel
    private lateinit var groupViewModel: GroupViewModel

    private val adapter: CardStackAdapter = CardStackAdapter()

    private lateinit var listener: GroupStackListener
    private lateinit var manager: CardStackLayoutManager

    private lateinit var cardStackView: CardStackView
    private lateinit var declineButton: View
    private lateinit var favouriteButton: View
    private lateinit var acceptButton: View
    private lateinit var loadDecisionButton: View
    private lateinit var overlay: FrameLayout

    private val buttonToggleObserver = object : RecipeQueue.DequeObserver {
        override fun onItemRemoved() {
            toggleSwipeButtons()
        }

        override fun onItemAdded() {
            toggleSwipeButtons()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.soloViewModel = ViewModelProvider(this)[RecipeViewModel::class.java]
        this.groupViewModel = ViewModelProvider(this)[GroupViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_group_swiping, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        cardStackView = view.findViewById(R.id.card_stack_view)
        declineButton = view.findViewById(R.id.button_decline)
        favouriteButton = view.findViewById(R.id.button_favourite)
        acceptButton = view.findViewById(R.id.button_accept)
        loadDecisionButton = view.findViewById(R.id.button_get_group_decision)
        overlay = view.findViewById(R.id.overlay_match)

        listener = GroupStackListener(adapter, groupViewModel, group)
        manager = CardStackLayoutManager(listener)

        setupCardStackView()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
        toggleSwipeButtons()
    }

    override fun onResume() {
        super.onResume()
        groupViewModel.getRecipes(group)
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun observeLifeData() {
        groupViewModel.recipeLiveData.observe(this) { recipes ->
            val before = adapter.meals.size
            adapter.meals.addAll(recipes)
            val after = adapter.meals.size
            adapter.notifyItemRangeChanged(before, after)
        }
        groupViewModel.decisionLiveData.observe(this) {
            handleMatch(it)
        }
    }

    private fun setupCardStackView() {
        cardStackView.layoutManager = manager
        cardStackView.adapter = adapter
        cardStackView.itemAnimator.apply {
            if (this is DefaultItemAnimator) {
                supportsChangeAnimations = false
            }
        }
        listener.layoutManager = manager
    }

    private fun setViewListeners() {
        declineButton.setOnClickListener { handleButtonSwipe(Direction.LEFT) }
        favouriteButton.setOnClickListener { performFavouriteActions() }
        cardStackView.setOnClickListener(DoubleTapListener(this::performFavouriteActions))
        acceptButton.setOnClickListener { handleButtonSwipe(Direction.RIGHT) }
        loadDecisionButton.setOnClickListener { groupViewModel.getDecision(group) }

        adapter.meals.addObserver(buttonToggleObserver)
    }

    private fun resetViewListeners() {
        declineButton.setOnClickListener(null)
        favouriteButton.setOnClickListener(null)
        acceptButton.setOnClickListener(null)
        cardStackView.setOnClickListener(null)
        loadDecisionButton.setOnClickListener(null)

        adapter.meals.removeObserver(buttonToggleObserver)
    }

    private fun handleButtonSwipe(direction: Direction) {
        val setting = SwipeAnimationSetting.Builder()
            .setDirection(direction)
            .setDuration(Duration.NORMAL.duration)
            .setInterpolator(AccelerateInterpolator())
            .build()
        manager.setSwipeAnimationSetting(setting)
        cardStackView.swipe()
    }

    private fun performFavouriteActions() {
        if (adapter.meals.isEmpty()) return
        soloViewModel.addToFavourites(adapter.meals.first())
        val topView = manager.topView

        if (topView != null) {
            val overlay = topView.findViewById<View>(R.id.swipe_overlay)
            val icon = topView.findViewById<ImageView>(R.id.swipe_icon_overlay)

            overlay.alpha = 0.5f

            overlay.setBackgroundResource(R.drawable.overlay_yellow)
            icon.setImageResource(R.drawable.ic_favourite2)

            // overlay fade-out
            val overlayAnimator = ValueAnimator.ofFloat(overlay.alpha, 0.0f)
            overlayAnimator.duration = 1000 // 1 second to disappear
            overlayAnimator.addUpdateListener { valueAnimator ->
                overlay.alpha = valueAnimator.animatedValue as Float
            }

            // icon resize
            val iconAnimator = ValueAnimator.ofFloat(1.0f, 2.0f, 1.0f)
            iconAnimator.duration = 600
            iconAnimator.addUpdateListener { valueAnimator ->
                val scale = valueAnimator.animatedValue as Float
                icon.scaleX = scale
                icon.scaleY = scale
            }

            overlayAnimator.start()
            iconAnimator.start()
        }
    }

    private fun handleMatch(matchedRecipe: Recipe) {
        acceptButton.visibility = View.GONE
        declineButton.visibility = View.GONE
        favouriteButton.visibility = View.GONE
        loadDecisionButton.visibility = View.GONE
        overlay.visibility = View.VISIBLE

        overlay.alpha = 0.0f

        val overlayAnimator = ValueAnimator.ofFloat(overlay.alpha, 1.0f, 1.0f)
        overlayAnimator.duration = 2000

        overlayAnimator.addUpdateListener { valueAnimator ->
            overlay.alpha = valueAnimator.animatedValue as Float
        }
        overlayAnimator.doOnEnd {
            makeCurrentFragment(RecipeFragment(matchedRecipe))
        }

        overlayAnimator.start()
    }

    private fun toggleSwipeButtons() {
        if (adapter.meals.size == 0) {
            loadDecisionButton.visibility = View.VISIBLE
            acceptButton.visibility = View.GONE
            declineButton.visibility = View.GONE
            favouriteButton.visibility = View.GONE

            groupViewModel.getDecision(group)
        } else {
            loadDecisionButton.visibility = View.GONE
            acceptButton.visibility = View.VISIBLE
            declineButton.visibility = View.VISIBLE
            favouriteButton.visibility = View.VISIBLE
        }
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(null)
            commit()
        }
}
