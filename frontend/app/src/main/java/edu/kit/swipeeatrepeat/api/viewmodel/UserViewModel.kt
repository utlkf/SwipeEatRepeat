package edu.kit.swipeeatrepeat.api.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import edu.kit.swipeeatrepeat.api.RetrofitBuilder
import edu.kit.swipeeatrepeat.api.SessionManager
import edu.kit.swipeeatrepeat.model.Ingredient
import edu.kit.swipeeatrepeat.model.Preference
import edu.kit.swipeeatrepeat.model.parsers.AuthenticationTokenParser
import edu.kit.swipeeatrepeat.model.parsers.IngredientParser
import edu.kit.swipeeatrepeat.model.parsers.PreferenceParser
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Response

/**
 * A simple [AndroidViewModel] that enables user authentication
 *
 * @authors Arne Stramka, Dominik Wlcek
 * @version 2.1
 */
class UserViewModel(application: Application) : AndroidViewModel(application) {

    // ignore warning, since it is apparently common practice to get the context inside a ViewModel this way
    private val context = application.applicationContext
    private val sessionManager by lazy { SessionManager(context) }

    val loginIsSuccess = MutableLiveData<Boolean>()
    val loginResponse = MutableLiveData<Response<ResponseBody>>()
    val registrationIsSuccess = MutableLiveData<Boolean>()
    val registrationResponse = MutableLiveData<Response<ResponseBody>>()
    val userPreferences = MutableLiveData<Collection<Preference>>()
    val userBlacklist = MutableLiveData<Collection<Ingredient>>()
    val userSupersetWhitelist = MutableLiveData<Collection<Ingredient>>()
    val userSubsetWhitelist = MutableLiveData<Collection<Ingredient>>()
    val logoutExecuted = MutableLiveData<Boolean>()
    val refreshExecuted = MutableLiveData<Boolean>()

    private val labelParser = PreferenceParser()
    private val ingredientParser = IngredientParser()

    private val requestBodyMediaType = "application/json; charset=utf-8".toMediaType()

    fun loginUser(requestBody: RequestBody) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).loginUser(requestBody)
                loginResponse.value = response
                val responseBody = response.body()?.string()
                loginIsSuccess.value = response.isSuccessful
                Log.d("LoginDebug", "Response: ${response.isSuccessful}, Body: $responseBody")
                if (loginIsSuccess.value!! && responseBody != null) {
                    saveUserTokenAndId(responseBody)
                }
            } catch (e: Exception) {
                Log.e("LoginDebug", "Error: ${e.message}")
                loginIsSuccess.value = false
            }
        }
    }

    fun registerUser(requestBody: RequestBody) {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).registerUser(requestBody)
                registrationResponse.value = response
                val responseBody = response.body()?.string()
                registrationIsSuccess.value = response.isSuccessful
                Log.d(
                    "RegisterDebug",
                    "Response: ${response.isSuccessful}, Body: ${
                        response.body()?.string()
                    }, ErrorBody: ${response.errorBody()?.string()}"
                )
                if (registrationIsSuccess.value!! && responseBody != null) {
                    saveUserTokenAndId(responseBody)
                }
            } catch (e: Exception) {
                Log.e("RegisterDebug", "Error: ${e.message}")
                registrationIsSuccess.value = false // Handle exceptions
            }
        }
    }

    fun setUserPreferences(preferences: Collection<Preference>) {
        viewModelScope.launch {
            try {
                val requestBody: RequestBody =
                    labelParser.get(preferences).toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context)
                    .setUserPreferences(sessionManager.fetchUserId(), requestBody)

                if (response.isSuccessful) {
                    Log.d("PreferencesDebug", response.body()!!.string())
                } else {
                    Log.d("PreferencesDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("PreferencesDebug", "Error: ${e.message}")
            }
        }
    }

    fun getUserPreferences() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getUserPreferences(sessionManager.fetchUserId())

                if (response.isSuccessful) {
                    userPreferences.value = labelParser.parse(JSONArray(response.body()!!.string()))
                } else {
                    Log.d("PreferencesDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("PreferencesDebug", "Error: ${e.message}")
            }
        }
    }

    fun setUserBlacklist(ingredients: Collection<Ingredient>) {
        viewModelScope.launch {
            try {
                val requestBody: RequestBody =
                    ingredientParser.get(ingredients).toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context)
                    .setUserBlacklist(sessionManager.fetchUserId(), requestBody)

                if (response.isSuccessful) {
                    Log.d("BlacklistDebug", response.body()!!.string())
                } else {
                    Log.d("BlacklistDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("BlacklistDebug", "Error: ${e.message}")
            }
        }
    }

    fun getUserBlacklist() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getUserBlacklist(sessionManager.fetchUserId())

                if (response.isSuccessful) {
                    userBlacklist.value =
                        ingredientParser.parse(JSONArray(response.body()!!.string()))
                } else {
                    Log.d("BlacklistDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("BlacklistDebug", "Error: ${e.message}")
            }
        }
    }

    fun setUserSupersetWhitelist(ingredients: Collection<Ingredient>) {
        viewModelScope.launch {
            try {
                val requestBody: RequestBody =
                    ingredientParser.get(ingredients).toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context)
                    .setUserSupersetWhitelist(sessionManager.fetchUserId(), requestBody)

                if (response.isSuccessful) {
                    Log.d("SupersetWhitelistDebug", response.body()!!.string())
                } else {
                    Log.d("SupersetWhitelistDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("SupersetWhitelistDebug", "Error: ${e.message}")
            }
        }
    }

    fun getUserSupersetWhitelist() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getUserSupersetWhitelist(sessionManager.fetchUserId())

                if (response.isSuccessful) {
                    userSupersetWhitelist.value =
                        ingredientParser.parse(JSONArray(response.body()!!.string()))
                } else {
                    Log.d("SupersetWhitelistDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("SupersetWhitelistDebug", "Error: ${e.message}")
            }
        }
    }

    fun setUserSubsetWhitelist(ingredients: Collection<Ingredient>) {
        viewModelScope.launch {
            try {
                val requestBody: RequestBody =
                    ingredientParser.get(ingredients).toString().toRequestBody(requestBodyMediaType)

                val response = RetrofitBuilder.getApiService(context)
                    .setUserSubsetWhitelist(sessionManager.fetchUserId(), requestBody)

                if (response.isSuccessful) {
                    Log.d("SubsetWhitelistDebug", response.body()!!.string())
                } else {
                    Log.d("SubsetWhitelistDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("SubsetWhitelistDebug", "Error: ${e.message}")
            }
        }
    }

    fun getUserSubsetWhitelist() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context)
                    .getUserSubsetWhitelist(sessionManager.fetchUserId())

                if (response.isSuccessful) {
                    userSubsetWhitelist.value =
                        ingredientParser.parse(JSONArray(response.body()!!.string()))
                } else {
                    Log.d("SubsetWhitelistDebug", response.errorBody()!!.string())
                }
            } catch (e: Exception) {
                Log.e("SubsetWhitelistDebug", "Error: ${e.message}")
            }
        }
    }

    private fun saveUserTokenAndId(responseBody: String) {
        val authToken = AuthenticationTokenParser().parse(JSONObject(responseBody))
        sessionManager.saveAuthToken(authToken.accessToken)
        sessionManager.saveRefreshToken(authToken.refreshToken)
        sessionManager.saveUserId(authToken.userId)
        sessionManager.authenticationToken = authToken
        Log.d("Saved", authToken.toString())
    }

    /**
     * Logout the user by sending the authentication token to the backend where it is revoked.
     * @param externalScope The scope in which the logout coroutine is executed. It is independent of the ViewModel's scope and therefore the request is not cancelled when the ViewModel is destroyed.
     */
    fun logout(externalScope: CoroutineScope) {
        externalScope.launch {
            try {
                RetrofitBuilder.getApiService(context).logout()
                sessionManager.clearSession()
                // Post value instead of setValue for thread safety
                logoutExecuted.postValue(true)
            } catch (e: Exception) {
                Log.e("LogoutDebug", "Error: ${e.message} + from class: ${e.javaClass}")
            }
        }
    }

    fun refreshAccessToken() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.getApiService(context).refreshAccessToken()
                if (response.isSuccessful && response.body() != null) {
                    refreshExecuted.value = true
                    saveUserTokenAndId(response.body()!!.string())
                } else {
                    Log.e("RefreshTokenDebug", "Error: ${response.errorBody()!!.string()}")
                }
            } catch (e: Exception) {
                Log.e("RefreshTokenDebug", "Error: ${e.message}; from class: ${e.javaClass}")
            }
        }
    }
}
