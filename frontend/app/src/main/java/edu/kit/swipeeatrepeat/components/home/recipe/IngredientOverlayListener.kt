package edu.kit.swipeeatrepeat.components.home.recipe

interface IngredientOverlayListener {
    fun onSubsetWhiteListPress(): Any
    fun onBlackListPress(): Any
    fun onSupersetWhiteListPress(): Any
}
