package edu.kit.swipeeatrepeat.components.group.overlays

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.User

class OnClickOverlayUser : OnClickOverlay<User>() {
    private lateinit var value: User
    private val userName by lazy { requireView().findViewById<TextView>(R.id.user_display_edit) }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return layoutInflater.inflate(R.layout.overlay_group_edit_user, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        leftButton = requireView().findViewById(R.id.button_make_admin_user)
        rightButton = requireView().findViewById(R.id.button_remove_user_from_group)
        closeButton = requireView().findViewById(R.id.button_close)
        super.onViewCreated(view, savedInstanceState)

        userName.text = value.username

        initializeButtons(value)
    }

    companion object {
        fun newInstance(user: User): OnClickOverlayUser {
            val fragment = OnClickOverlayUser()
            fragment.value = user
            return fragment
        }
    }
}
