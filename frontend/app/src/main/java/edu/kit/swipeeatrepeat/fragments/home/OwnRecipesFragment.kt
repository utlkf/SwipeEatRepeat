package edu.kit.swipeeatrepeat.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.ownRecipes.OwnRecipeAdapter
import edu.kit.swipeeatrepeat.fragments.RecipeFragment
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A simple [Fragment] subclass, that shows own Recipes of the user.
 *
 * @author Marcel-Marius Soltan
 * @version 1.2
 */
class OwnRecipesFragment : Fragment() {

    private lateinit var ownRecipesGrid: GridView

    private lateinit var soloViewModel: RecipeViewModel

    private val ownRecipesList = mutableListOf<Recipe>()

    private val adapter = OwnRecipeAdapter(ownRecipesList, this::editRecipe, this::openOwnMeal)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.soloViewModel = ViewModelProvider(this)[RecipeViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_own_recipes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        ownRecipesGrid = view.findViewById(R.id.own_recipes_grid)

        ownRecipesGrid.adapter = adapter
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onResume() {
        super.onResume()
        soloViewModel.getUserCreatedRecipes()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun openOwnMeal(meal: Recipe) {
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, RecipeFragment(meal))
            addToBackStack(null)
            commit()
        }
    }

    private fun editRecipe(recipe: Recipe) {
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, EditRecipeFragment(recipe))
            addToBackStack(null)
            commit()
        }
    }

    private fun setViewListeners() {
        ownRecipesGrid.onItemClickListener =
            AdapterView.OnItemClickListener { parent, _, position, _ ->
                parentFragmentManager.beginTransaction().apply {
                    replace(
                        parent.id,
                        RecipeFragment(adapter.getItem(position))
                    ) // Navigate to a detailed view for editing
                    addToBackStack(null)
                    commit()
                }
            }
    }

    private fun resetViewListeners() {
        ownRecipesGrid.onItemClickListener = null
    }

    private fun observeLifeData() {
        soloViewModel.ownRecipesLiveData.observe(this) { recipes ->
            if (recipes != null) {
                this.ownRecipesList.clear()
                this.ownRecipesList.addAll(recipes)
                adapter.notifyDataSetChanged()
            }
        }
    }
}
