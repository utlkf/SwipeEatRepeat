package edu.kit.swipeeatrepeat.components.preference

import android.content.Context
import com.google.android.material.chip.Chip
import edu.kit.swipeeatrepeat.components.util.ItemViewHolder
import edu.kit.swipeeatrepeat.model.Preference

/**
 * An implementation of [ItemViewHolder] that displays labels.
 *
 * @author Piet Förster
 * @version 1.0
 */
class PreferenceViewHolder(
    context: Context,
) : Chip(context) {

    /**
     * Creates a [Preference]-[Chip] containing the [Preference.name] as text.
     * The [Chip]-tag is the [preference] itself.
     */
    fun createChip(preference: Preference): Chip {
        val chip = Chip(context)
        chip.text = preference.name
        chip.tag = preference
        chip.isClickable = false
        chip.isCheckable = true
        return chip
    }
}
