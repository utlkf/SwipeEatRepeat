package edu.kit.swipeeatrepeat.components.cardStackView.stack

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SnapHelper
import edu.kit.swipeeatrepeat.components.cardStackView.settings.CardStackSetting
import edu.kit.swipeeatrepeat.components.cardStackView.settings.SwipeAnimationSetting
import edu.kit.swipeeatrepeat.components.cardStackView.util.Duration
import kotlin.math.abs

/**
 * A [SnapHelper] that helps the cards animate correctly.
 *
 * @author Piet Förster
 * @version 2.3
 */
class CardStackSnapHelper : SnapHelper() {
    private var velocityX = 0
    private var velocityY = 0

    override fun calculateDistanceToFinalSnap(
        layoutManager: RecyclerView.LayoutManager,
        targetView: View,
    ): IntArray {
        if (layoutManager !is CardStackLayoutManager) {
            return IntArray(2)
        }

        val manager: CardStackLayoutManager = layoutManager

        val x = targetView.translationX.toInt()
        val y = targetView.translationY.toInt()

        if (x == 0 && y == 0) return IntArray(2)

        val setting: CardStackSetting = manager.cardStackSetting
        val horizontal = abs(x) / targetView.width.toFloat()
        val vertical = abs(y) / targetView.height.toFloat()
        val duration: Duration =
            Duration.fromVelocity(if (velocityY < velocityX) velocityX else velocityY)

        if (shouldSwipe(manager, setting, horizontal, vertical, duration)) {
            handleSwipe(manager, setting, duration)
        } else {
            handleCancel(manager)
        }

        return IntArray(2)
    }

    private fun shouldSwipe(
        manager: CardStackLayoutManager,
        setting: CardStackSetting,
        horizontal: Float,
        vertical: Float,
        duration: Duration,
    ): Boolean {
        val state: CardStackState = manager.cardStackState

        return state.direction in setting.directions &&
                (duration === Duration.FAST || setting.swipeThreshold < horizontal || setting.swipeThreshold < vertical)
    }

    private fun handleSwipe(
        manager: CardStackLayoutManager,
        setting: CardStackSetting,
        duration: Duration,
    ) {
        val state: CardStackState = manager.cardStackState
        state.targetPosition = state.topPosition + 1

        val swipeAnimationSetting: SwipeAnimationSetting =
            SwipeAnimationSetting.Builder()
                .setDirection(setting.swipeAnimationSetting.direction)
                .setDuration(duration.duration)
                .setInterpolator(setting.swipeAnimationSetting.interpolator)
                .build()

        manager.setSwipeAnimationSetting(swipeAnimationSetting)
        velocityX = 0
        velocityY = 0

        val scroller =
            CardStackSmoothScroller(CardStackSmoothScroller.ScrollType.MANUAL_SWIPE, manager)
        scroller.targetPosition = manager.topPosition
        manager.startSmoothScroll(scroller)
    }

    private fun handleCancel(manager: CardStackLayoutManager) {
        val scroller =
            CardStackSmoothScroller(CardStackSmoothScroller.ScrollType.MANUAL_CANCEL, manager)
        scroller.targetPosition = manager.topPosition
        manager.startSmoothScroll(scroller)
    }

    override fun findSnapView(layoutManager: RecyclerView.LayoutManager): View? {
        if (layoutManager is CardStackLayoutManager) {
            val manager: CardStackLayoutManager = layoutManager
            val view: View? = manager.findViewByPosition(manager.topPosition)
            val x = view?.translationX?.toInt()
            val y = view?.translationY?.toInt()
            return if (x == 0 && y == 0) {
                null
            } else view
        }
        return null
    }

    override fun findTargetSnapPosition(
        layoutManager: RecyclerView.LayoutManager,
        velocityX: Int,
        velocityY: Int,
    ): Int {
        this.velocityX = abs(velocityX)
        this.velocityY = abs(velocityY)
        if (layoutManager is CardStackLayoutManager) {
            val manager: CardStackLayoutManager = layoutManager
            return manager.topPosition
        }
        return RecyclerView.NO_POSITION
    }
}
