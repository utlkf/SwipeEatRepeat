package edu.kit.swipeeatrepeat.components.preference

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.PreferenceViewModel
import edu.kit.swipeeatrepeat.model.Preference

class PreferenceSelector : DialogFragment() {

    private val selectedPreferences by lazy { requireView().findViewById<ChipGroup>(R.id.preferences_selected) }
    private val notSelectedPreferences by lazy { requireView().findViewById<ChipGroup>(R.id.preferences_unselected) }
    private val cancelButton by lazy { requireView().findViewById<Button>(R.id.button_preferences_cancel) }
    private val saveButton by lazy { requireView().findViewById<Button>(R.id.button_preferences_save) }

    private val selectedPreferencesSet = mutableSetOf<Preference>()
    private val notSelectedPreferencesSet = mutableSetOf<Preference>()

    private val listeners = mutableListOf<SavePreferencesListener>()

    private lateinit var preferenceViewModel: PreferenceViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferenceViewModel = ViewModelProvider(this)[PreferenceViewModel::class.java]
        this.notSelectedPreferencesSet
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return layoutInflater.inflate(R.layout.overlay_edit_preferences, container, false)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        preferenceViewModel.getAllPreferences()

        preferenceViewModel.preferencesLiveData.observe(viewLifecycleOwner) { labels ->
            labels.forEach {
                if (!this.selectedPreferencesSet.contains(it)) {
                    this.notSelectedPreferencesSet.add(it)
                }
            }
            setupLabelGroups()
        }

        selectedPreferences.setOnCheckedStateChangeListener { group, checkedIds ->
            checkedIds.forEach {
                val chip = group.findViewById<Chip>(it)
                chip.isChecked = false
                if (chip != null) {
                    selectedPreferences.removeView(chip)
                    notSelectedPreferences.addView(chip)

                    selectedPreferencesSet.remove(chip.tag as Preference)
                    notSelectedPreferencesSet.add(chip.tag as Preference)
                }
            }
        }

        notSelectedPreferences.setOnCheckedStateChangeListener { group, checkedIds ->
            checkedIds.forEach {
                val chip = group.findViewById<Chip>(it)
                chip.isChecked = false
                if (chip != null) {
                    notSelectedPreferences.removeView(chip)
                    selectedPreferences.addView(chip)

                    notSelectedPreferencesSet.remove(chip.tag as Preference)
                    selectedPreferencesSet.add(chip.tag as Preference)
                }
            }
        }

        cancelButton.setOnClickListener {
            this.listeners.forEach {
                it.onCancel()
            }
            dismiss()
        }

        saveButton.setOnClickListener {
            this.listeners.forEach {
                it.onSave(this.selectedPreferencesSet)
            }
            dismiss()
        }

        setupLabelGroups()
    }

    fun addSaveListener(listener: SavePreferencesListener) {
        this.listeners.add(listener)
    }

    private fun setupLabelGroups() {
        selectedPreferences.removeAllViews()
        notSelectedPreferences.removeAllViews()

        selectedPreferencesSet.forEach {
            val chip = PreferenceViewHolder(requireContext()).createChip(it)
            chip.isClickable = true
            this.selectedPreferences.addView(chip)
        }

        notSelectedPreferencesSet.forEach {
            val chip = PreferenceViewHolder(requireContext()).createChip(it)
            chip.isClickable = true
            this.notSelectedPreferences.addView(chip)
        }
    }

    companion object {
        fun newInstance(preferences: Collection<Preference>): PreferenceSelector {
            val fragment = PreferenceSelector()
            fragment.selectedPreferencesSet.addAll(preferences)
            fragment.notSelectedPreferencesSet.removeAll(preferences.toSet())
            return fragment
        }
    }
}
