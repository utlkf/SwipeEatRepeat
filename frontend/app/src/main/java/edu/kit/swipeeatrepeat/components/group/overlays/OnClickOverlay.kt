package edu.kit.swipeeatrepeat.components.group.overlays

import android.widget.Button
import android.widget.ImageButton
import androidx.fragment.app.DialogFragment

abstract class OnClickOverlay<T> : DialogFragment() {
    private lateinit var userActionListener: OnClickOverlayListener<T>
    protected lateinit var leftButton: Button
    protected lateinit var rightButton: Button
    protected lateinit var closeButton: ImageButton

    protected fun initializeButtons(value: T) {
        closeButton.setOnClickListener {
            dismiss()
        }

        leftButton.setOnClickListener {
            userActionListener.leftButton(value)
            dismiss()
        }

        rightButton.setOnClickListener {
            userActionListener.rightButton(value)
            dismiss()
        }
    }

    fun setListener(listener: OnClickOverlayListener<T>) {
        userActionListener = listener
    }
}
