package edu.kit.swipeeatrepeat.model

data class User(
    val id: Long,
    val username: String,
    val email: String,
) {
    override fun hashCode(): Int {
        return this.id.toInt()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}
