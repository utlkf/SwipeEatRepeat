package edu.kit.swipeeatrepeat.components.recipeCard.components

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemListAdapter
import edu.kit.swipeeatrepeat.model.RecipeComponent

/**
 * A [ItemListAdapter] to create [RecipeComponentViewHolder] objects from the [data] list.
 *
 * @author Piet Förster
 * @version 1.0
 */
class RecipeComponentAdapter(
    private val context: Context,
    data: List<RecipeComponent>,
) : ItemListAdapter<RecipeComponentViewHolder, RecipeComponent>(data) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecipeComponentViewHolder {
        return RecipeComponentViewHolder(
            LayoutInflater.from(context).inflate(R.layout.card_recipe_ingredient, parent, false)
        )
    }
}
