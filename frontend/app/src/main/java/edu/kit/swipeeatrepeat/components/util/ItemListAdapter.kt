package edu.kit.swipeeatrepeat.components.util

import androidx.recyclerview.widget.RecyclerView

/**
 * An [RecyclerView.Adapter] to create [T] objects from the [data] list.
 *
 * @author Piet Förster
 * @version 1.0
 */
abstract class ItemListAdapter<T : ItemViewHolder<E>, E>(
    private val data: List<E>,
) : RecyclerView.Adapter<T>() {

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: T, position: Int) {
        holder.bind(data[position], position)
    }
}
