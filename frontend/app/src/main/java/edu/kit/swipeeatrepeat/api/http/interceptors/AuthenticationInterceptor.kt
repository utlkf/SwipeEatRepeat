package edu.kit.swipeeatrepeat.api.http.interceptors

import android.content.Context
import edu.kit.swipeeatrepeat.api.SessionManager
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

/**
 * An [Interceptor], that intercepts every request made by Retrofit
 * to add an authentication token (that is based on the user) to it.
 *
 * @param context The context of the application
 *
 * @author Arne Stramka
 * @version 1.1
 */
class AuthenticationInterceptor(context: Context) : Interceptor {

    private val sessionManager = SessionManager(context)

    override fun intercept(chain: Interceptor.Chain): Response {
        val requestBuilder = chain.request().newBuilder()
        val request = chain.request()

        if (request.url.toString().endsWith("/refresh-token")) {
            sessionManager.fetchRefreshToken()?.let {
                addAuthorizationHeader(requestBuilder, it)
            }
        } else {
            sessionManager.fetchAuthToken()?.let {
                addAuthorizationHeader(requestBuilder, it)
            }
        }

        return chain.proceed(requestBuilder.build())
    }

    private fun addAuthorizationHeader(requestBuilder: Request.Builder, token: String) {
        requestBuilder.addHeader("Authorization", "Bearer $token")
    }
}