package edu.kit.swipeeatrepeat.fragments.recent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.recent.RecentRecipeAdapter
import edu.kit.swipeeatrepeat.fragments.RecipeFragment
import edu.kit.swipeeatrepeat.model.Recipe

/**
 * A simple [Fragment] subclass, that shows the recent Recipes of the user.
 *
 * @author Piet Förster
 * @version 1.3
 */
class RecentFragment : Fragment() {

    private lateinit var accepted: GridView
    private lateinit var declined: GridView
    private lateinit var resetAcceptedButton: View
    private lateinit var resetDeclinedButton: View

    private lateinit var soloViewModel: RecipeViewModel

    private val acceptedList = mutableListOf<Recipe>()
    private val declinedList = mutableListOf<Recipe>()

    private val acceptedAdapter = RecentRecipeAdapter(acceptedList, this::openMealRecent)
    private val declinedAdapter = RecentRecipeAdapter(declinedList, this::openMealRecent)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.soloViewModel = ViewModelProvider(this)[RecipeViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_recent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accepted = view.findViewById(R.id.recent_grid_accepted)
        declined = view.findViewById(R.id.recent_grid_declined)
        resetAcceptedButton = view.findViewById(R.id.button_reset_accepted)
        resetDeclinedButton = view.findViewById(R.id.button_reset_declined)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        setupAdapters()
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onResume() {
        super.onResume()

        soloViewModel.getAccepted()
        soloViewModel.getDeclined()
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun setupAdapters() {
        accepted.adapter = acceptedAdapter
        declined.adapter = declinedAdapter
    }

    private fun setViewListeners() {
        resetAcceptedButton.setOnClickListener { soloViewModel.resetAccepted() }
        resetDeclinedButton.setOnClickListener { soloViewModel.resetDeclined() }
    }

    private fun resetViewListeners() {
        resetAcceptedButton.setOnClickListener(null)
        resetDeclinedButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        soloViewModel.acceptedLiveData.observe(this) { meals ->
            this.acceptedList.clear()
            this.acceptedList.addAll(meals)
            acceptedAdapter.notifyDataSetChanged()
        }

        soloViewModel.declinedLiveData.observe(this) { meals ->
            this.declinedList.clear()
            this.declinedList.addAll(meals)
            declinedAdapter.notifyDataSetChanged()
        }
    }

    private fun openMealRecent(meal: Recipe) {
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, RecipeFragment(meal))
            addToBackStack(null)
            commit()
        }
    }
}
