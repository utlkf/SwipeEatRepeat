package edu.kit.swipeeatrepeat.components.recipeCard.components

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.components.util.ItemListAdapter
import edu.kit.swipeeatrepeat.model.PreparationStep

/**
 * A [ItemListAdapter] to create [StepsViewHolder] objects from the [data] list.
 *
 * @author Piet Förster
 * @version 1.0
 */
class StepsAdapter(private val context: Context, data: List<PreparationStep>) :
    ItemListAdapter<StepsViewHolder, PreparationStep>(data) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StepsViewHolder {
        return StepsViewHolder(
            LayoutInflater.from(context).inflate(R.layout.card_recipe_step, parent, false)
        )
    }
}
