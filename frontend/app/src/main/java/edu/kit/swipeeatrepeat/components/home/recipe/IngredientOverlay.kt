package edu.kit.swipeeatrepeat.components.home.recipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.DialogFragment
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.Ingredient

private const val ADD_TO = "add to %s"
private const val REMOVE_FROM = "remove from %s"
private const val BLACKLIST = "blacklist"
private const val SUBSET_WHITELIST = "soft whitelist"
private const val SUPERSET_WHITELIST = "hard whitelist"

class IngredientOverlay : DialogFragment() {

    private val cancelButton by lazy { requireView().findViewById<ImageButton>(R.id.button_close) }
    private val addToBlacklist by lazy { requireView().findViewById<Button>(R.id.button_blacklist) }
    private val addToSubsetWhitelist by lazy { requireView().findViewById<Button>(R.id.button_subset_whitelist) }
    private val addToSupersetWhitelist by lazy { requireView().findViewById<Button>(R.id.button_superset_whitelist) }
    private val name by lazy { requireView().findViewById<TextView>(R.id.ingredient_display_edit) }

    private val listeners = mutableListOf<IngredientOverlayListener>()
    private lateinit var ingredient: Ingredient
    private var inBlacklist = false
    private var inSubsetWhitelist = false
    private var inSupersetWhitelist = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return layoutInflater.inflate(R.layout.overlay_home_edit_ingredients, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        name.text = ingredient.name

        if (inBlacklist) {
            addToBlacklist.text = REMOVE_FROM.format(BLACKLIST)
        } else {
            addToBlacklist.text = ADD_TO.format(BLACKLIST)
        }
        if (inSubsetWhitelist) {
            addToSubsetWhitelist.text = REMOVE_FROM.format(SUBSET_WHITELIST)
        } else {
            addToSubsetWhitelist.text = ADD_TO.format(SUBSET_WHITELIST)
        }
        if (inSupersetWhitelist) {
            addToSupersetWhitelist.text = REMOVE_FROM.format(SUPERSET_WHITELIST)
        } else {
            addToSupersetWhitelist.text = ADD_TO.format(SUPERSET_WHITELIST)
        }

        cancelButton.setOnClickListener {
            dismiss()
        }
        addToBlacklist.setOnClickListener {
            this.listeners.forEach {
                it.onBlackListPress()
            }
            dismiss()
        }
        addToSubsetWhitelist.setOnClickListener {
            this.listeners.forEach {
                it.onSubsetWhiteListPress()
            }
            dismiss()
        }
        addToSupersetWhitelist.setOnClickListener {
            this.listeners.forEach {
                it.onSupersetWhiteListPress()
            }
            dismiss()
        }
    }


    fun addButtonListener(listener: IngredientOverlayListener) {
        this.listeners.add(listener)
    }

    companion object {
        fun newInstance(
            ingredient: Ingredient,
            inBlacklist: Boolean,
            inSubsetWhitelist: Boolean,
            inSupersetWhitelist: Boolean,
        ): IngredientOverlay {
            val instance = IngredientOverlay()
            instance.ingredient = ingredient
            instance.inBlacklist = inBlacklist
            instance.inSubsetWhitelist = inSubsetWhitelist
            instance.inSupersetWhitelist = inSupersetWhitelist
            return instance
        }
    }
}
