package edu.kit.swipeeatrepeat.components.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.combine
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update

class SearchViewModel<T>(elements: List<T>, current: T) : ViewModel() {

    private val _isSearching = MutableStateFlow(false)
    val isSearching = _isSearching.asStateFlow()

    private val _searchText = MutableStateFlow(current.toString())
    val searchText = _searchText.asStateFlow()

    private val _elementsList = MutableStateFlow(elements)
    val elementsList = searchText
        .onEach { _isSearching.update { true } }
        .combine(_elementsList) { text, elements ->
            if (text.isBlank() || text.length < 2) {
                listOf()
            } else {
                elements.filter {
                    it.toString().contains(text.trim(), true)
                }
            }
        }.onEach { _isSearching.update { false } }
        .stateIn(
            scope = viewModelScope,
            started = SharingStarted.WhileSubscribed(5000),
            initialValue = _elementsList.value
        )

    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }
}
