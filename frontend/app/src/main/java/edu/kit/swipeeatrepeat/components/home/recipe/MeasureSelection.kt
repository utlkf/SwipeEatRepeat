package edu.kit.swipeeatrepeat.components.home.recipe

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.ExposedDropdownMenuBox
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.model.Measure

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MeasureSelection(
    elements: List<Measure>,
    value: Measure,
    onUnitSelected: (Measure) -> Unit,
) {
    var expanded by remember { mutableStateOf(false) }

    ExposedDropdownMenuBox(
        modifier = Modifier.fillMaxWidth(),
        expanded = expanded,
        onExpandedChange = { expanded = !expanded },
    ) {
        OutlinedTextField(
            readOnly = true,
            value = value.unit,
            onValueChange = {},
            label = { Text(stringResource(R.string.unit_hint)) },
            modifier = Modifier.menuAnchor()
        )
        ExposedDropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false }
        ) {
            elements.forEach {
                DropdownMenuItem(
                    text = {
                        Text(text = it.unit)
                    },
                    onClick = {
                        expanded = false
                        onUnitSelected(it)
                    }
                )
            }
        }
    }
}
