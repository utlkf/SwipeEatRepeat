package edu.kit.swipeeatrepeat.fragments.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.UserViewModel
import edu.kit.swipeeatrepeat.components.preference.PreferenceSelector
import edu.kit.swipeeatrepeat.components.preference.SavePreferencesListener
import edu.kit.swipeeatrepeat.model.Preference

class PreferenceFragment : Fragment() {

    private lateinit var userViewModel: UserViewModel

    private lateinit var overlay: FrameLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.userViewModel = ViewModelProvider(this)[UserViewModel::class.java]
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_home_preferences, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        overlay = view.findViewById(R.id.label_selection_overlay)

        observeLifeData()
    }

    override fun onResume() {
        super.onResume()
        userViewModel.getUserPreferences()
    }

    private fun observeLifeData() {
        userViewModel.userPreferences.observe(viewLifecycleOwner) { preferences ->
            showOverlay(preferences)
        }
    }

    private fun showOverlay(labels: Collection<Preference>) {
        overlay.visibility = View.VISIBLE
        val labelOverlay = PreferenceSelector.newInstance(labels)
        labelOverlay.show(requireActivity().supportFragmentManager, "label_adding_overlay")
        labelOverlay.addSaveListener(object : SavePreferencesListener {
            override fun onSave(selectedLabels: Collection<Preference>) {
                userViewModel.setUserPreferences(selectedLabels)
                parentFragmentManager.popBackStack()
            }

            override fun onCancel() {
                parentFragmentManager.popBackStack()
            }
        })
    }
}
