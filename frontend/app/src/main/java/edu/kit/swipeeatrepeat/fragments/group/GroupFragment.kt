package edu.kit.swipeeatrepeat.fragments.group

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import edu.kit.swipeeatrepeat.R
import edu.kit.swipeeatrepeat.api.viewmodel.GroupViewModel
import edu.kit.swipeeatrepeat.components.group.UserCardAdapter
import edu.kit.swipeeatrepeat.components.recent.RecentRecipeAdapter
import edu.kit.swipeeatrepeat.fragments.RecipeFragment
import edu.kit.swipeeatrepeat.model.Group

/**
 * A [Fragment] in which you can view the [group].
 *
 * @author Piet Förster
 * @version 1.0
 */
class GroupFragment(private var group: Group) : Fragment() {

    private lateinit var groupViewModel: GroupViewModel

    private val primaryColor by lazy { ContextCompat.getColor(requireContext(), R.color.primary) }
    private val redColor by lazy { ContextCompat.getColor(requireContext(), R.color.red_error) }

    private lateinit var code: TextView
    private lateinit var name: TextView
    private lateinit var members: GridView
    private lateinit var leaveButton: View
    private lateinit var editButton: View
    private lateinit var startButton: View
    private lateinit var recentMatchText: TextView
    private lateinit var recentMatch: GridView

    private val userAdapter = UserCardAdapter(group.members.sortedBy { it.username.lowercase() }) {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.groupViewModel = ViewModelProvider(this)[GroupViewModel::class.java]
        observeLifeData()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.fragment_group, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        code = view.findViewById(R.id.group_code)
        name = view.findViewById(R.id.group_name)
        members = view.findViewById(R.id.group_members)
        leaveButton = view.findViewById(R.id.button_group_leave)
        editButton = view.findViewById(R.id.button_group_edit)
        startButton = view.findViewById(R.id.button_group_start)
        recentMatchText = view.findViewById(R.id.recent_match_title)
        recentMatch = view.findViewById(R.id.recent_match)

        code.text = group.groupCode
        name.text = group.name
        members.adapter = userAdapter

        if (group.recentMatch != null) {
            recentMatchText.visibility = View.VISIBLE
            recentMatch.adapter =
                RecentRecipeAdapter(
                    listOf(group.recentMatch!!),
                ) { makeCurrentFragment(RecipeFragment(group.recentMatch!!)) }
        }
    }

    override fun onStart() {
        super.onStart()
        setViewListeners()
    }

    override fun onResume() {
        super.onResume()
        groupViewModel.getGroup(group.id)
    }

    override fun onStop() {
        super.onStop()
        resetViewListeners()
    }

    private fun showWaitForAdminMessage() {
        val snackBar = Snackbar.make(
            requireView(), "Wait for the group-admin to start the session.", Snackbar.LENGTH_LONG
        )
        snackBar.view.setBackgroundColor(primaryColor)
        snackBar.setTextColor(redColor).show()
    }

    private fun makeCurrentFragment(fragment: Fragment) =
        parentFragmentManager.beginTransaction().apply {
            replace(R.id.fl_wrapper, fragment)
            addToBackStack(null)
            commit()
        }

    private fun setViewListeners() {
        leaveButton.setOnClickListener {
            groupViewModel.leaveGroup(group)
            parentFragmentManager.popBackStack()
        }
        editButton.setOnClickListener {
            makeCurrentFragment(EditGroupFragment(group))
        }
        startButton.setOnClickListener {
            groupViewModel.startDecision(group)
        }
    }

    private fun resetViewListeners() {
        leaveButton.setOnClickListener(null)
        editButton.setOnClickListener(null)
        startButton.setOnClickListener(null)
    }

    private fun observeLifeData() {
        groupViewModel.getGroupLiveData.observe(this) { group ->
            this.group = group
        }
        groupViewModel.startGroupLiveData.observe(this) {
            if (it) {
                makeCurrentFragment(GroupSwipingFragment(group))
            } else {
                showWaitForAdminMessage()
            }
        }
    }
}
