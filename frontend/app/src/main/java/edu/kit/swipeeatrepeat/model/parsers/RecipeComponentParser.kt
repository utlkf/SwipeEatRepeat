package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.RecipeComponent
import org.json.JSONObject

class RecipeComponentParser : Parser<RecipeComponent>() {
    companion object {
        private const val ID = "id"
        private const val INGREDIENT = "ingredient"
        private const val QUANTITY = "quantity"
        private const val MEASURE = "measure"
    }

    private val ingredientParser = IngredientParser()
    private val measureParser = MeasureParser()

    override fun parse(from: JSONObject): RecipeComponent = RecipeComponent(
        from.optLong(ID, 0),
        ingredientParser.parse(from.optJSONObject(INGREDIENT)!!),
        measureParser.parse(from.optJSONObject(MEASURE)!!),
        from.optInt(QUANTITY, 0)
    )

    override fun get(from: RecipeComponent): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(INGREDIENT, ingredientParser.get(from.ingredient))
            .put(QUANTITY, from.quantity)
            .put(MEASURE, measureParser.get(from.measure))
    }
}