package edu.kit.swipeeatrepeat.api.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import edu.kit.swipeeatrepeat.api.RetrofitBuilder
import edu.kit.swipeeatrepeat.model.Measure
import edu.kit.swipeeatrepeat.model.parsers.MeasureParser
import kotlinx.coroutines.launch
import org.json.JSONArray

/**
 * A [ViewModel] that is used to handle the backend requests and answers for [Measure]s.
 *
 * To use this, after initializing add a listener to the [MutableLiveData] of the object
 * to which the answer regarding the used call to the backend writes.
 * Then when the observer noticed a change do the things you need based on that call.
 * This will help avoid any unwanted cancellations of the [launch]
 * and also help the consistency of the changed data.
 *
 * @author Piet Förster
 * @version 1.12
 */
class MeasureViewModel : ViewModel() {

    private val measureParser = MeasureParser()
    val measuresLiveData = MutableLiveData<Collection<Measure>>()

    fun getAllMeasures() {
        viewModelScope.launch {
            try {
                val response = RetrofitBuilder.apiService.getMeasures()
                if (response.isSuccessful && response.body() != null) {
                    measuresLiveData.value =
                        measureParser.parse(JSONArray(response.body()!!.string()))
                    Log.d("MeasureDebug", "measures: ${measuresLiveData.value}")
                } else {
                    throw Exception("Response Code: ${response.code()}")
                }
            } catch (e: Exception) {
                Log.e("MeasureDebug", "Error fetching measures: ${e.message}")
            }
        }
    }
}
