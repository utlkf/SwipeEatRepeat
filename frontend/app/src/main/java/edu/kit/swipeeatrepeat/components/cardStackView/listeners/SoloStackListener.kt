package edu.kit.swipeeatrepeat.components.cardStackView.listeners

import android.util.Log
import edu.kit.swipeeatrepeat.api.viewmodel.RecipeViewModel
import edu.kit.swipeeatrepeat.components.cardStackView.stack.CardStackLayoutManager
import edu.kit.swipeeatrepeat.components.cardStackView.util.Direction
import edu.kit.swipeeatrepeat.components.cardStackView.viewable.CardStackAdapter
import edu.kit.swipeeatrepeat.components.recipeCard.collections.RecipeQueue

/**
 * A [CardStackListener] used in the [edu.kit.swipeeatrepeat.fragments.group.GroupSwipingFragment]
 *
 * @author Piet Förster
 * @version 1.2
 */
class SoloStackListener(
    private val adapter: CardStackAdapter,
    private val viewModel: RecipeViewModel,
) : CardStackListener {

    lateinit var layoutManager: CardStackLayoutManager

    init {
        adapter.meals.addObserver(
            object : RecipeQueue.DequeObserver {
                override fun onItemRemoved() = paginate()
                override fun onItemAdded() {
                    // ignore for now
                }
            })
    }

    override fun onCardSwiped(direction: Direction?) {
        Log.d("CardStackView", "onCardSwiped: $direction")
        when (direction) {
            Direction.LEFT -> viewModel.decline(adapter.meals.first())

            Direction.RIGHT -> viewModel.accept(adapter.meals.first())

            else -> {
                // do nothing
            }
        }
    }

    override fun onCardDisappeared() {
        Log.d("CardStackView", "onCardDisappeared: ${adapter.meals.removeFirst().name}")
        adapter.notifyItemRemoved(0)
    }

    override fun onCardAppeared() {
        Log.d("CardStackView", "onCardAppeared: ${adapter.meals.first().name}")
    }

    /**
     * Loads the next meals using the [viewModel] and adds them to the current ones.
     */
    private fun paginate() {
        if (adapter.meals.size > 1) return
        viewModel.getRecipes()
    }
}
