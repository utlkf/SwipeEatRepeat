package edu.kit.swipeeatrepeat.model

// TODO This class should be renamed to AuthenticationResponse
data class AuthenticationToken(
    val accessToken: String,
    val refreshToken: String,
    val userId: Long,
) {
    override fun hashCode(): Int {
        return this.userId.toInt() + this.accessToken.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return other != null && other.javaClass == this.javaClass && other.hashCode() == this.hashCode()
    }
}