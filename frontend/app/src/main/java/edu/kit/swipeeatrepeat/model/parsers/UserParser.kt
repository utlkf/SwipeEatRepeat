package edu.kit.swipeeatrepeat.model.parsers

import edu.kit.swipeeatrepeat.model.User
import org.json.JSONObject

/**
 * An implementation of a [Parser] used for [User]s.
 *
 * @author Piet Förster
 * @version 1.0
 */
class UserParser : Parser<User>() {
    companion object {
        private const val ID = "id"
        private const val USERNAME = "username"
        private const val E_MAIL = "email"
    }

    override fun parse(from: JSONObject): User = User(
        from.optLong(ID, 0),
        from.optString(USERNAME, "guest"),
        from.optString(E_MAIL, "")
    )

    override fun get(from: User): JSONObject {
        return JSONObject().put(ID, from.id)
            .put(USERNAME, from.username)
            .put(E_MAIL, from.email)
    }
}