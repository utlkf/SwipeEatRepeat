#!/bin/sh

#This script will rebuild our backend container where our Spring application is running in.
sudo docker compose down
sudo docker system prune
sudo docker compose build kotlinapp
sudo docker compose up -d
sudo docker ps