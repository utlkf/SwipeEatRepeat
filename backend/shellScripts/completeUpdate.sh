#!/bin/sh

#This script will reset and rebuilt our complete backend
sudo docker compose down
sudo docker system prune
sudo docker volume rm backend_pg-data
sudo docker volume rm backend_images
sudo docker compose build kotlinapp
sudo docker compose up -d
sudo docker ps