package edu.kit.backend.exceptions

/**
 * Is thrown when a registration process is started with an email that is already registered
 */
private const val MESSAGE: String = "email %s is already registered"

class DuplicateEmailException(email: String) : Exception(String.format(MESSAGE, email))
