package edu.kit.backend.exceptions

private const val MESSAGE = "The user with the id %d does not have the rights to edit the group %s"

class InsufficientRightsException(userId: Long, group: String) : Exception(MESSAGE.format(userId, group))
