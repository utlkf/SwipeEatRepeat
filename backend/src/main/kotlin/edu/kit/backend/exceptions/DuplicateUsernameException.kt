package edu.kit.backend.exceptions

/**
 * Is thrown when a registration process is started with an username that is already registered
 */
private const val MESSAGE: String = "username %s is already registered"

class DuplicateUsernameException(username: String) : Exception(String.format(MESSAGE, username))
