package edu.kit.backend.exceptions

import edu.kit.backend.data.model.Group

private const val MESSAGE = "The group %s (and code %s) has been deleted because the membersList is empty."

class GroupEmptyException(group: Group) : Exception(MESSAGE.format(group.name, group.groupCode))
