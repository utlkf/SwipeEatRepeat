package edu.kit.backend.exceptions

private const val MESSAGE = "Could not find a %s with the id %d"

class IdNotFoundException(type: String, id: Long) : Exception(MESSAGE.format(type, id))
