package edu.kit.backend.exceptions

private const val MESSAGE = "Could not find a %s with the id %s"

class NameNotFoundException(type: String, name: String) : Exception(MESSAGE.format(type, name))
