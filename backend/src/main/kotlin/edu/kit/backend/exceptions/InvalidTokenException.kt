package edu.kit.backend.exceptions

private const val MESSAGE = "The token %s is not a valid JWT Bearer token."

/**
 * This [Exception] is thrown when a token is not in the correct format.
 */
class InvalidTokenException(token: String) : Exception(MESSAGE.format(token))
