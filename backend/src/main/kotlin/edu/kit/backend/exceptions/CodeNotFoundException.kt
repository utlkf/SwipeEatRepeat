package edu.kit.backend.exceptions

private const val MESSAGE = "Could not find a %s with the code %s"

class CodeNotFoundException(type: String, code: String) : Exception(MESSAGE.format(type, code))
