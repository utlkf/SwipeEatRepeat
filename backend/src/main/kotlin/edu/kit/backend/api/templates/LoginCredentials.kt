package edu.kit.backend.api.templates

data class LoginCredentials(
    //made it nullable for JSON easiest fix for and gets checked in UserController login fun:
    //  value failed for JSON property username due to missing (therefore NULL)
    //  value for creator parameter username which is a non-nullable type
    val username: String?,
    val password: String?
)
