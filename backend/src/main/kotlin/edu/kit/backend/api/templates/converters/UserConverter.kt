package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.UserTemplate
import edu.kit.backend.data.model.AppUser

class UserConverter : Converter<UserTemplate, AppUser>() {
    override fun convert(from: AppUser): UserTemplate {
        return UserTemplate(
            from.id,
            from.username,
            from.email
        )
    }
}
