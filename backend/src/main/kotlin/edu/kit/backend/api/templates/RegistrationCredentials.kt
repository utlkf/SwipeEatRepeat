package edu.kit.backend.api.templates

data class RegistrationCredentials(
    val username: String,
    val password: String,
    val email: String
)
