package edu.kit.backend.api.security

import edu.kit.backend.data.model.AppUser
import jakarta.persistence.*

@Entity
data class Token(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private val id: Long = 0,

    private val token: String,

    @Enumerated(EnumType.STRING)
    private val tokenType: TokenType,

    var expired: Boolean,

    var revoked: Boolean,

    @ManyToOne
    @JoinColumn(name = "user_id")
    private val user: AppUser,
)
