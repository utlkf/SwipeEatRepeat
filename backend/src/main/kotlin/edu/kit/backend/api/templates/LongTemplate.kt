package edu.kit.backend.api.templates

data class LongTemplate(
    val value: Long
)
