package edu.kit.backend.api.repository

import edu.kit.backend.data.model.Group
import edu.kit.backend.data.model.Preference
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * Repository interface for [Group] entities.
 * This interface implements [JpaRepository], providing standard CRUD operations and custom query methods.
 */
interface GroupRepository : JpaRepository<Group, Long> {

    /**
     * Allows us to find a group entity by its Code.
     * @param code The code of the group to join
     * @return Group if found else null
     */
    fun findByGroupCode(code: String): Group?

    /**
     * Finds all groups that have any of the given preferences.
     * @param preferences The preferences to be matched.
     * @return A list of recipes that have any of the given preferences.
     */
    @Query("SELECT r FROM Group r JOIN r.preferences l WHERE l IN :preferences")
    fun findByAnyPreference(@Param("preferences") preferences: Collection<Preference>): List<Group>
}
