package edu.kit.backend.api.controller

import edu.kit.backend.api.service.IngredientService
import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.converters.IngredientConverter
import edu.kit.backend.exceptions.NameNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/ingredients")
class IngredientController {

    @Autowired
    private lateinit var ingredientService: IngredientService

    private val ingredientConverter = IngredientConverter()

    @GetMapping
    fun getAllIngredients(): ResponseEntity<List<IngredientTemplate>> {
        return ResponseEntity.ok(
            ingredientConverter.convert(
                ingredientService.getAllIngredients()
            )
        )
    }

    @PostMapping
    fun createIngredient(@RequestBody ingredient: IngredientTemplate): ResponseEntity<Any> {
        return ResponseEntity.ok(
            ingredientConverter.convert(
                ingredientService.getIngredientByName(ingredient.name)
            )
        )
    }

    @DeleteMapping
    fun deleteIngredient(@RequestBody ingredient: IngredientTemplate): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                ingredientConverter.convert(
                    ingredientService.deleteIngredient(ingredient.name)
                )
            )
        } catch (e: NameNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }
}
