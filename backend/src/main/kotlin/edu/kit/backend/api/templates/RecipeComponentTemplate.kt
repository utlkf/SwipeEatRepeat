package edu.kit.backend.api.templates

data class RecipeComponentTemplate(
    val id: Long,

    val ingredient: IngredientTemplate,
    val measure: MeasureTemplate,

    val quantity: Int,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RecipeComponentTemplate) return false

        if (id != other.id) return false
        if (ingredient != other.ingredient) return false
        if (measure != other.measure) return false
        if (quantity != other.quantity) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + ingredient.hashCode()
        result = 31 * result + measure.hashCode()
        result = 31 * result + quantity
        return result
    }
}
