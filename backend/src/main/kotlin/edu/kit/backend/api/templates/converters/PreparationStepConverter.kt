package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.PreparationStepTemplate
import edu.kit.backend.data.model.PreparationStep

class PreparationStepConverter : Converter<PreparationStepTemplate, PreparationStep>() {
    override fun convert(from: PreparationStep): PreparationStepTemplate {
        return PreparationStepTemplate(
            from.id,
            from.description
        )
    }
}
