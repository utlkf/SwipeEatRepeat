package edu.kit.backend.api.event

import edu.kit.backend.data.model.Recipe
import org.slf4j.LoggerFactory

class GroupDecision(
    private val groupId: Long,
    private val amountOfGroupMembers: Int,
    private val listOfRecipeIds: List<Long>
) {

    /**
     * Map: <recipeId, userIds that accepted the recipe>
     */
    private val acceptedRecipes = mutableMapOf<Long, MutableList<Long>>()

    /**
     * Map: <recipeId, userIds that declined the recipe>
     */
    private val declinedRecipes = mutableMapOf<Long, MutableList<Long>>()

    /**
     * Map: <userId, recipeIds that the user already loaded>
     */
    private val loadedRecipes = mutableMapOf<Long, MutableList<Long>>()

    private var swipeCounter = 0
    private var recipeId: Long = 0
    private val logger = LoggerFactory.getLogger(GroupDecision::class.java)
    private val counterMap: MutableMap<Long, Int> = mutableMapOf()

    var isEvaluated: Boolean = false

    fun getResultId(): Long {
        // if every member declines every recipe choose one randomly
        if (recipeId == 0L && isEvaluated) {
            recipeId = listOfRecipeIds.random()
        }
        return recipeId
    }

    fun getLoadedRecipes(userId: Long): List<Long> {
        return loadedRecipes[userId].orEmpty()
    }

    fun addLoadedRecipes(userId: Long, sentRecipes: List<Recipe>) {
        if (!loadedRecipes.keys.contains(userId)) loadedRecipes[userId] = mutableListOf()
        loadedRecipes[userId]?.addAll(sentRecipes.map { it.id })
    }

    fun getUserDecisions(): Map<Long, Int> {
        return counterMap
    }

    fun addAccepted(event: AcceptedEvent) {
        logger.info("Adding a decision to decisions.acceptedRecipes, ${acceptedRecipes}, path:${groupId}")
        logger.info(" recipeId: ${event.recipeId}")

        if (!acceptedRecipes.keys.contains(event.recipeId)) acceptedRecipes[event.recipeId] = mutableListOf()
        acceptedRecipes[event.recipeId]?.add(event.userId)
        counterMap[event.recipeId]?.inc()
        swipeCounter++

        if (canEvaluate()) evaluate()
    }

    fun addDeclined(event: DeclinedEvent) {
        logger.info("Increasing declined counter, path:${groupId}")
        logger.info(" recipeId: ${event.recipeId}")

        if (!declinedRecipes.keys.contains(event.recipeId)) declinedRecipes[event.recipeId] = mutableListOf()
        declinedRecipes[event.recipeId]?.add(event.userId)
        swipeCounter++

        if (canEvaluate()) evaluate()
    }

    private fun canEvaluate(): Boolean {
        if (acceptedRecipes.values.any { it.size == amountOfGroupMembers }) {
            logger.info("TRUE MATCH: all users swiped at least one recipe right")
        }
        if (swipeCounter == amountOfGroupMembers * listOfRecipeIds.size) {
            logger.info("all users swiped all recipes")
            return true
        }
        return false
    }

    private fun evaluate() {
        listOfRecipeIds.forEach {
            counterMap[it] = acceptedRecipes[it].orEmpty().size
        }

        val maxValue = counterMap.maxOf { it.value }
        val maxRecipes = mutableListOf<Long>()
        listOfRecipeIds.forEach { recipeId ->
            if (counterMap[recipeId] == maxValue) maxRecipes.add(recipeId)
        }
        logger.info("all recipeId in maxRecipes: $maxRecipes")
        recipeId = maxRecipes.random()
        logger.info("result is recipe with id: $recipeId")
        isEvaluated = true
    }
}
