package edu.kit.backend.api.service

import edu.kit.backend.api.repository.TokenRepository
import edu.kit.backend.api.security.JwtService
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.Authentication
import org.springframework.security.web.authentication.logout.LogoutHandler
import org.springframework.stereotype.Service

@Service
class LogoutService : LogoutHandler {

    @Autowired
    private lateinit var tokenRepository: TokenRepository

    @Autowired
    private lateinit var jwtService: JwtService

    /**
     * Causes a logout to be completed by invalidating the token sent in the request.
     * @param request the HTTP request
     * @param response the HTTP response
     * @param authentication the current principal details
     */
    override fun logout(request: HttpServletRequest?, response: HttpServletResponse?, authentication: Authentication?) {
        val authHeader: String? = request?.getHeader("Authorization")
        if (jwtService.doesNotContainBearerToken(authHeader) || authHeader == null) {
            return
        }
        val jwtToken = jwtService.extractTokenValue(authHeader)
        val storedToken = tokenRepository.findByToken(jwtToken)
        if (storedToken != null) {
            storedToken.expired = true
            storedToken.revoked = true
            tokenRepository.save(storedToken)
        }
    }
}
