package edu.kit.backend.api.controller

import edu.kit.backend.api.service.UserService
import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.api.templates.UserTemplate
import edu.kit.backend.api.templates.converters.IngredientConverter
import edu.kit.backend.api.templates.converters.PreferenceConverter
import edu.kit.backend.api.templates.converters.RecipeConverter
import edu.kit.backend.api.templates.converters.UserConverter
import edu.kit.backend.exceptions.IdNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 * This class should handle the login logic.
 * Ensure it uses UserService to authenticate users and responds appropriately.
 */
@RestController
class UserController {

    private val logger = LoggerFactory.getLogger(UserController::class.java)

    @Autowired
    private lateinit var userService: UserService

    private val preferenceConverter = PreferenceConverter()
    private val userConverter = UserConverter()
    private val recipeConverter = RecipeConverter()
    private val ingredientConverter = IngredientConverter()

    /**
     * Gets all user entities from the data storage.
     * @return The HTTP response containing a list of all stored users.
     */
    @GetMapping("/users")
    fun getAllUsers(): ResponseEntity<List<UserTemplate>> {
        return ResponseEntity.ok(
            userConverter.convert(
                userService.getAllUsers()
            )
        )
    }

    /**
     * Deletes a User specified by the id.
     * @return A warning when the user is not found and else the user that was deleted.
     */
    @DeleteMapping("/users/{id}")
    fun deleteUser(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                userConverter.convert(
                    userService.deleteUser(id)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Sets the new sets of preferences for a User
     * @param userId The id of the user
     * @param labels The list of labels describing its preferences
     */
    @PutMapping("/users/{userId}/preferences")
    fun setPreferences(@PathVariable userId: Long, @RequestBody labels: List<PreferenceTemplate>): ResponseEntity<Any> {
        logger.info("inside setPreferences in UserController")
        return try {
            ResponseEntity.ok(
                userService.setPreferences(userId, labels)
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/users/{userId}/preferences")
    fun getPreferences(@PathVariable("userId") userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                preferenceConverter.convert(userService.getPreferences(userId))
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PutMapping("/users/{userId}/blacklist")
    fun setBlacklist(@PathVariable userId: Long, @RequestBody labels: List<IngredientTemplate>): ResponseEntity<Any> {
        logger.info("inside setBlacklist in UserController")
        return try {
            ResponseEntity.ok(
                userService.setBlacklist(userId, labels)
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/users/{userId}/blacklist")
    fun getBlacklist(@PathVariable("userId") userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                ingredientConverter.convert(userService.getBlacklist(userId))
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PutMapping("/users/{userId}/superset-whitelist")
    fun setSupersetWhitelist(
        @PathVariable userId: Long,
        @RequestBody labels: List<IngredientTemplate>
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                userService.setSupersetWhitelist(userId, labels)
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }


    @GetMapping("/users/{userId}/superset-whitelist")
    fun getSupersetWhitelist(@PathVariable("userId") userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                ingredientConverter.convert(userService.getSupersetWhitelist(userId))
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PutMapping("/users/{userId}/subset-whitelist")
    fun setSubsetWhitelist(
        @PathVariable userId: Long,
        @RequestBody labels: List<IngredientTemplate>
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                userService.setSubsetWhitelist(userId, labels)
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/users/{userId}/subset-whitelist")
    fun getSubsetWhitelist(@PathVariable("userId") userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                ingredientConverter.convert(userService.getSubsetWhitelist(userId))
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Fetches all recipes created by a user with the given user ID.
     *
     * @param userId The ID of the user whose recipes are to be fetched.
     * @return A ResponseEntity containing the list of recipes created by the user or a suitable error message.
     */
    @GetMapping("/users/{userId}/created-recipes")
    fun getUserCreatedRecipes(@PathVariable userId: Long): ResponseEntity<Any> {
        logger.info("Fetching all recipes created by user with ID: $userId")
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.getAllRecipesCreatedByUser(userId)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }


    /**
     * Get all recipes that a user can swipe
     * @return ResponseEntity containing a list of all recipes.
     */
    @GetMapping("/users/{userId}/recipes")
    fun getRecipes(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    if (userId != 0L) {
                        userService.fetchNumberOfRecipesForUser(10, userId)
                    } else {
                        userService.recipeService.fetchNumberOfRecipesRandom(10)
                    }
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PostMapping("/users/{userId}/favourites")
    fun addFavourite(
        @PathVariable userId: Long,
        @RequestBody recipeTemplate: RecipeTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.addFavourite(userId, recipeTemplate).favouriteRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/users/{userId}/favourites")
    fun getFavourites(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.getUser(userId).favouriteRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/users/{userId}/favourites")
    fun removeFavourite(
        @PathVariable userId: Long,
        @RequestBody recipeTemplate: RecipeTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.removeFavourite(userId, recipeTemplate).favouriteRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PostMapping("/users/{userId}/accepted")
    fun addAccepted(
        @PathVariable userId: Long,
        @RequestBody recipeTemplate: RecipeTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.addAccepted(userId, recipeTemplate).acceptedRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/users/{userId}/accepted")
    fun getAccepted(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.getUser(userId).acceptedRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/users/{userId}/accepted")
    fun resetAccepted(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.resetAcceptedRecipes(userId).acceptedRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PostMapping("/users/{userId}/declined")
    fun addDeclined(
        @PathVariable userId: Long,
        @RequestBody recipeTemplate: RecipeTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.addDeclined(userId, recipeTemplate).declinedRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/users/{userId}/declined")
    fun getDeclined(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.getUser(userId).declinedRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/users/{userId}/declined")
    fun resetDeclined(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    userService.resetDeclinedRecipes(userId).declinedRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }
}
