package edu.kit.backend.api.service

import edu.kit.backend.api.repository.MeasureRepository
import edu.kit.backend.data.model.Measure
import edu.kit.backend.exceptions.IdNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

private const val TYPE: String = "measure"

@Service
class MeasureService {

    @Autowired
    private lateinit var measureRepository: MeasureRepository

    /**
     * Gets the [Measure] matching the given [id].
     *
     * @param id is the id of the [Measure].
     * @throws IdNotFoundException if the [measureRepository] does not contain an [Measure] with that [Measure.id]
     * @return the [Measure] that was found.
     */
    fun getMeasureById(id: Long): Measure {
        return measureRepository.findById(id).orElseThrow { IdNotFoundException(TYPE, id) }
    }

    /**
     * Gets all [Measure]s in the [measureRepository].
     *
     * @return a [List] of all [Measure]s.
     */
    fun getAllMeasures(): List<Measure> {
        return measureRepository.findAll()
    }
}
