package edu.kit.backend.api.service

import edu.kit.backend.api.repository.GroupRepository
import edu.kit.backend.api.repository.PreferenceRepository
import edu.kit.backend.api.repository.RecipeRepository
import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.data.model.Preference
import edu.kit.backend.exceptions.IdNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

private const val TYPE: String = "preference"

@Service
class PreferenceService(
    private val preferenceRepository: PreferenceRepository,
    private val recipeRepository: RecipeRepository,
    private val userRepository: UserRepository,
    private val groupRepository: GroupRepository
) {

    private val logger = LoggerFactory.getLogger(PreferenceService::class.java)

    @Transactional
    fun createFoodPreference(preference: Preference): Preference {
        logger.info("Creating food preference: $preference")
        return preferenceRepository.save(preference)
    }

    /**
     * Alters an existing food preference.
     * @param id The id of the food preference to be altered.
     * @param updatedPreference The updated food preference.
     * @return The altered food preference.
     */
    @Transactional
    fun alterPreference(id: Long, updatedPreference: Preference): Preference {
        return preferenceRepository.findById(id)
            .map { existingPreference ->
                val updated = Preference(
                    id = existingPreference.id,
                    name = updatedPreference.name,
                    isStrict = updatedPreference.isStrict,
                    setByUsers = existingPreference.setByUsers,
                    setByGroups = existingPreference.setByGroups,
                    setForRecipes = existingPreference.setForRecipes,
                )
                preferenceRepository.save(updated)
            }.orElseThrow {
                throw IdNotFoundException(TYPE, id)
            }
    }

    /**
     * Deletes a food preference from the database.
     * Also disassociates the preference from any recipes to avoid foreign key exceptions in the joined table.
     * @param id The id of the food preference to be deleted.
     * @return The deleted food preference.
     */
    @Transactional
    fun deletePreference(id: Long): Preference {
        val preferenceToDelete = preferenceRepository.findById(id).orElseThrow()

        // Disassociate preference from the Objects having preferences as attributes to avoid foreign key exceptions
        disassociatePreferencesFromRecipes(preferenceToDelete)
        disassociatePreferencesFromUsers(preferenceToDelete)
        disassociatePreferencesFromGroups(preferenceToDelete)

        // Safe food preference deletion
        preferenceRepository.deleteById(id)
        return preferenceToDelete
    }

    /**
     * Disassociates a food preference from all recipes.
     * @param preference The food preference to be disassociated.
     * @return The disassociated food preference.
     */
    private fun disassociatePreferencesFromRecipes(preference: Preference) {
        val associatedRecipes = recipeRepository.findByAnyPreference(listOf(preference))
        associatedRecipes.forEach { recipe ->
            recipe.preferences.remove(preference)
            recipeRepository.save(recipe)
        }
    }

    private fun disassociatePreferencesFromUsers(preference: Preference) {
        val associatedUsers = userRepository.findByAnyPreference(listOf(preference))
        associatedUsers.forEach { user ->
            user.preferences.remove(preference)
            userRepository.save(user)
        }
    }

    private fun disassociatePreferencesFromGroups(preference: Preference) {
        val associatedGroups = groupRepository.findByAnyPreference(listOf(preference))
        associatedGroups.forEach { group ->
            group.preferences.remove(preference)
            groupRepository.save(group)
        }
    }

    fun getAllPreferences(): List<Preference> {
        return preferenceRepository.findAll()
    }

    /**
     * Gets an existing [Preference] by its [Preference.id].
     *
     * @param id is the [Preference.id].
     * @throws [IdNotFoundException] if the given [id] is not used by any [Preference] in the database.
     * @return the [Preference] with matching [Preference.id].
     */
    fun getPreferenceById(id: Long): Preference {
        return preferenceRepository.findById(id).orElseThrow {
            throw IdNotFoundException(TYPE, id)
        }
    }
}
