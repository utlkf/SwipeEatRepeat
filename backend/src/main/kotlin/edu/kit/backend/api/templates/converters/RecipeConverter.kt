package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.data.model.Recipe

class RecipeConverter : Converter<RecipeTemplate, Recipe>() {

    private val recipeComponentConverter = RecipeComponentConverter()
    private val preparationStepConverter = PreparationStepConverter()
    private val preferenceConverter = PreferenceConverter()

    override fun convert(from: Recipe): RecipeTemplate {
        return RecipeTemplate(
            from.id,
            from.name,
            from.passiveCookingTime,
            from.activeCookingTime,
            from.creator.id,
            from.postDate,
            recipeComponentConverter.convert(from.recipeComponents),
            preparationStepConverter.convert(from.preparationSteps),
            preferenceConverter.convert(from.preferences),
            from.imageUrl
        )
    }
}
