package edu.kit.backend.api.service

import edu.kit.backend.api.repository.RecipeComponentRepository
import edu.kit.backend.data.model.RecipeComponent
import edu.kit.backend.exceptions.IdNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

private const val TYPE: String = "RecipeComponent"

@Service
class RecipeComponentService {

    @Autowired
    private lateinit var recipeComponentRepository: RecipeComponentRepository

    /**
     * @param id is the id of the [RecipeComponent]
     * @throws IdNotFoundException
     * @return the [RecipeComponent]
     */
    fun getRecipeComponentById(id: Long): RecipeComponent {
        return recipeComponentRepository.findById(id).orElseThrow { IdNotFoundException(TYPE, id) }
    }

    fun deleteRecipeComponent(id: Long) {
        return recipeComponentRepository.deleteById(id)
    }
}
