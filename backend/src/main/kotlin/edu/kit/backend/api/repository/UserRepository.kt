package edu.kit.backend.api.repository

import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Preference
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * Repository interface for [AppUser] entities.
 * This interface implements [JpaRepository], providing standard CRUD operations and custom query methods.
 */
interface UserRepository : JpaRepository<AppUser, Long> {

    /**
     * Finds a AppUser by their username.
     * @param username The username of the user to be found.
     * @return AppUser if found, or null if no user exists with the given username.
     */
    fun findByUsername(username: String): AppUser?

    /**
     * Finds a AppUser by their email address.
     *
     * @param email The email of the user to be found
     * @return AppUser if found, or null if no user with the email address exists.
     */
    fun findByEmail(email: String): AppUser?

    /**
     * Finds all users that have any of the given labels.
     * @param labels The labels to be matched.
     * @return A list of users that have any of the given labels.
     */
    @Query("SELECT r FROM AppUser r JOIN r.preferences l WHERE l IN :labels")
    fun findByAnyPreference(@Param("labels") labels: Collection<Preference>): List<AppUser>

    @Query("SELECT r FROM AppUser r JOIN r.preferences l WHERE l IN :labels")
    fun findByAnyFoodIngredient(@Param("labels") labels: Collection<Preference>): List<AppUser>
}
