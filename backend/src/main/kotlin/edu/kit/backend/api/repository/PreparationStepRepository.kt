package edu.kit.backend.api.repository

import edu.kit.backend.data.model.PreparationStep
import org.springframework.data.jpa.repository.JpaRepository

interface PreparationStepRepository : JpaRepository<PreparationStep, Long> {
}
