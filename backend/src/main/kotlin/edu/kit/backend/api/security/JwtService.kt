package edu.kit.backend.api.security

import io.jsonwebtoken.Claims
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.security.Keys
import mu.KotlinLogging
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Service
import java.util.*

/**
 * Service class with utility methods to process, validate and generate JWT Tokens
 */
@Service
class JwtService(
    jwtProperties: JwtProperties
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Used to sign and verify JWT tokens in our system.
     * SecretKey is based on the HMAC-SHA algorithm and our configured random key.
     */
    private val secretKey = Keys.hmacShaKeyFor(
        jwtProperties.key.toByteArray()
    )

    /**
     * The expiration time for the access token in milliseconds.
     */
    private val accessTokenExpiration = jwtProperties.accessTokenExpiration

    /**
     * The expiration time for the refresh token in milliseconds.
     */
    private val refreshTokenExpiration = jwtProperties.refreshTokenExpiration

    /**
     * Method to generate a Jwt token.
     * @param userDetails the attributes of the user the token is for
     * @param additionalClaims additional data about the user
     * @return a Jwt token
     */
    fun generateAccessToken(
        userDetails: UserDetails,
        additionalClaims: Map<String, Any> = emptyMap()
    ): String {
        return buildToken(userDetails, additionalClaims, accessTokenExpiration)
    }

    /**
     * Method to generate a refresh token.
     * @param userDetails the attributes of the user the token is for
     * @return a refresh token
     */
    fun generateRefreshToken(
        userDetails: UserDetails,
    ): String {
        return buildToken(userDetails, emptyMap(), refreshTokenExpiration)

    }

    private fun buildToken(
        userDetails: UserDetails,
        additionalClaims: Map<String, Any> = emptyMap(),
        expirationTime: Long
    ): String {
        return Jwts.builder()
            .claims()
            .subject(userDetails.username)
            .issuedAt(Date(System.currentTimeMillis()))
            .expiration(Date(System.currentTimeMillis() + expirationTime))
            .add(additionalClaims)
            .and()
            .signWith(secretKey)
            .compact()
    }

    /**
     * Checks whether token is valid.
     * @param token the token to check
     * @param userDetails the user stating to be the owner of the token
     * @return boolean whether the user is the owner, and the token is not expired
     */
    fun isTokenValid(token: String, userDetails: UserDetails): Boolean {
        val username = extractUsername(token)

        return userDetails.username == username && !isExpired(token)
    }

    /**
     * Method to extract the username from a Jwt token.
     * @param token The Jwt token.
     * @return The username or null if not found
     */
    fun extractUsername(token: String): String? {
        return getAllClaims(token)
            .subject
    }


    /**
     * Checks whether token is expired by using the expiration property
     * @param token the Jwt token to check
     * @return whether the token is expired
     */
    fun isExpired(token: String): Boolean {
        return getAllClaims(token)
            .expiration
            .before(Date(System.currentTimeMillis()))
    }


    /**
     * Extract the payload data from the token.
     * @param token the token
     * @return a map of the payload data (claims)
     */
    private fun getAllClaims(token: String): Claims {
        logger.info { "inside getAllClaims in JwtService" }
        val parser = Jwts.parser()
            .verifyWith(secretKey)
            .build()
        logger.info { "parser ready" }

        return parser
            .parseSignedClaims(token)
            .payload
    }

    fun doesNotContainBearerToken(authHeader: String?): Boolean {
        return (authHeader == null) || !authHeader.startsWith("Bearer ")
    }


    fun extractTokenValue(authHeader: String): String {
        return authHeader.substringAfter("Bearer ")
    }
}
