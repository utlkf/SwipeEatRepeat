package edu.kit.backend.api.event

import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component

@Component
class DecisionPublisher(private val publisher: ApplicationEventPublisher) {

    fun publishEvent(acceptedEvent: AcceptedEvent) {
        publisher.publishEvent(acceptedEvent)
    }

    fun publishEvent(declinedEvent: DeclinedEvent) {
        publisher.publishEvent(declinedEvent)
    }
}
