package edu.kit.backend.api.controller

import edu.kit.backend.api.service.AuthenticationService
import edu.kit.backend.api.templates.LoginCredentials
import edu.kit.backend.api.templates.RegistrationCredentials
import edu.kit.backend.exceptions.DuplicateEmailException
import edu.kit.backend.exceptions.DuplicateUsernameException
import edu.kit.backend.exceptions.InvalidTokenException
import io.jsonwebtoken.JwtException
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

/**
 * Class for handling api calls that result in returning an authentication token.
 */
@RestController
class AuthenticationController {

    @Autowired
    private lateinit var authenticationService: AuthenticationService

    private val logger = LoggerFactory.getLogger(AuthenticationController::class.java)

    /**
     * Handles the login request for a user.
     *
     * @param loginCredentials The login credentials containing username and password.
     * @return ResponseEntity indicating the result of the login attempt.
     *         Returns OK (HTTP 200) with a JWT Token if the user is authenticated,
     *         or Bad Request (HTTP 400) with an error message if authentication fails.
     */
    @PostMapping("/login")
    fun authenticate(
        @RequestBody loginCredentials: LoginCredentials
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                authenticationService.authentication(loginCredentials)
            )
        } catch (e: UsernameNotFoundException) {
            return ResponseEntity.badRequest().body(e.message)
        } catch (e: Exception) {
            logger.info("Exception class: " + e::class.simpleName)
            return ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Handles the registration requests of users
     *
     * @param registrationCredentials The registration credentials filled into the registration form.
     * @return A response entity with status 201 indicating that a new user has been created
     * or with code 409 or 400 indicating a conflict or a bad request respectively
     */
    @PostMapping("/register")
    fun register(@RequestBody registrationCredentials: RegistrationCredentials): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                authenticationService.registerUser(registrationCredentials)
            )
        } catch (e: DuplicateEmailException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: DuplicateUsernameException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PostMapping("/refresh-token")
    fun refreshToken(request: HttpServletRequest, response: HttpServletResponse): ResponseEntity<Any> {
        logger.info("inside refresh token in AuthenticationController")
        logger.info("request: $request, response: $response")
        return try {
            ResponseEntity.ok(authenticationService.refreshToken(request, response))
        } catch (e: JwtException) {
            logger.info("caught jwt exception")
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.message)
        } catch (e: InvalidTokenException) {
            ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.message)
        } catch (e: UsernameNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }
}
