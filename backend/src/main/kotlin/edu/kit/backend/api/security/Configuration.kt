package edu.kit.backend.api.security

import edu.kit.backend.InitializationProperties
import edu.kit.backend.api.repository.UserRepository
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder

/**
 * Configuration class for Beans, that can be used e.g. for injection
 */
@Configuration
@EnableConfigurationProperties(JwtProperties::class, InitializationProperties::class)
class Configuration {
    @Bean
    fun userDetailsService(userRepository: UserRepository): UserDetailsService =
        SERUserDetailsService(userRepository)

    /**
     * Bean for password encryption
     */
    @Bean
    fun encoder(): PasswordEncoder = BCryptPasswordEncoder()

    @Bean
    fun authenticationProvider(userRepository: UserRepository): AuthenticationProvider =
        DaoAuthenticationProvider()
            .also {
                // configure UserDetailsService and PasswordEncoder to be used for authentication
                it.setUserDetailsService(userDetailsService(userRepository))
                it.setPasswordEncoder(encoder())
            }

    @Bean
    fun authenticationManager(config: AuthenticationConfiguration): AuthenticationManager =
        config.authenticationManager

    /**
     * A List containing all endpoints permitted without authentication
     */
    @Bean
    fun whiteList(): List<String> {
        return listOf(
            "/login",
            "/register",
            "/error",
            "/recipes",
            "/users/0/recipes",
            "/recipes/images/**",
            "/refresh-token",   // there is a extra security validation for this endpoint in the AuthenticationService refreshToken method
        )
        // “/error” is in this list because of the way Spring handles errors internally.
        // Without that, every exception we throw will return 403 Forbidden, instead of the HTTP status code we provided
    }
}
