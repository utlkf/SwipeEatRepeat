package edu.kit.backend.api.templates

data class GroupStartedTemplate(
    val started: Boolean
)
