package edu.kit.backend.api.templates.converters

abstract class Converter<T, F> {
    abstract fun convert(from: F): T

    fun convert(from: Collection<F>): List<T> {
        val to = mutableListOf<T>()
        from.forEach {
            to.add(convert(it))
        }
        return to
    }
}
