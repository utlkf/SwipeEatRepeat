package edu.kit.backend.api.repository

import edu.kit.backend.data.model.Preference
import org.springframework.data.jpa.repository.JpaRepository

interface PreferenceRepository : JpaRepository<Preference, Long> {

    fun findByName(name: String): Preference?

    fun existsByName(name: String): Boolean
}
