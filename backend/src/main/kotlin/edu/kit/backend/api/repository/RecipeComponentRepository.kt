package edu.kit.backend.api.repository

import edu.kit.backend.data.model.RecipeComponent
import org.springframework.data.jpa.repository.JpaRepository

interface RecipeComponentRepository : JpaRepository<RecipeComponent, Long> {
}
