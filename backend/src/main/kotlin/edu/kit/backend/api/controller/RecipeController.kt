package edu.kit.backend.api.controller

import edu.kit.backend.api.service.RecipeService
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.api.templates.converters.RecipeConverter
import edu.kit.backend.exceptions.IdNotFoundException
import okio.IOException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

/**
 * Controller for managing recipe-related HTTP requests.
 * Provides endpoints for fetching all recipes and specific recipes by their ID.
 */
@RestController
@RequestMapping("/recipes")
class RecipeController {

    @Autowired
    private lateinit var recipeService: RecipeService

    private val logger = LoggerFactory.getLogger(RecipeController::class.java)
    private val recipeConverter = RecipeConverter()

    /**
     * Get all recipes.
     * @return ResponseEntity containing a list of all recipes.
     */
    @GetMapping
    fun getAllRecipes(): ResponseEntity<List<RecipeTemplate>> {
        return ResponseEntity.ok(
            recipeConverter.convert(
                recipeService.getAllRecipes()
            )
        )
    }

    @GetMapping("/limited")
    fun fetchNumberOfRecipes(@RequestParam("count") count: Long): ResponseEntity<List<RecipeTemplate>> {
        return ResponseEntity.ok(
            recipeConverter.convert(
                recipeService.fetchNumberOfRecipesRandom(count.toInt())
            )
        )
    }


    /**
     * Get a specific recipe by its ID.
     * @param id The ID of the recipe.
     * @return ResponseEntity containing the recipe or not found status.
     */
    @GetMapping("/{id}")
    fun getRecipeById(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    recipeService.getRecipeById(id)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Add a Recipe to the Database.
     * @param recipeTemplate The recipe attributes except for the file url.
     * @param file The file corresponding to the recipe image.
     * @return An HTTP response either containing the successfully added recipe or a badRequest-response
     */
    @PostMapping
    fun createRecipe(
        @RequestPart(name = "recipeAttributes") recipeTemplate: RecipeTemplate,
        @RequestPart(name = "file") file: MultipartFile
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    recipeService.createRecipe(recipeTemplate, file)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: RuntimeException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Delete Recipe from the Database.
     *
     * @param id ID of the recipe to delete
     */
    @DeleteMapping("/{id}")
    fun deleteRecipe(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    recipeService.deleteRecipe(id)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Alter the entry of a recipe.
     *
     * @param recipeToAlter The altered version of the recipe. The id is used to identify the recipe to alter.
     * If the id has no correspondent, the recipe will be saved as a new entry.
     * @return The HTTP-ResponseEntity
     */
    @PatchMapping
    fun alterRecipe(
        @RequestPart(name = "recipeAttributes") recipeToAlter: RecipeTemplate,
        @RequestPart(name = "file") file: MultipartFile
    ): ResponseEntity<Any> {
        logger.info("ALTER RECIPE !Controller!")
        return try {
            ResponseEntity.ok(
                recipeConverter.convert(
                    recipeService.alterRecipe(recipeToAlter, file)
                )
            )
        } catch (e: IdNotFoundException) {
            logger.info(e.message)
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * jpg
     */
    @GetMapping(path = ["/images/{fileName}"], produces = [MediaType.IMAGE_JPEG_VALUE])
    fun getImage(@PathVariable fileName: String): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                recipeService.getImageByteArray(fileName)
            )
        } catch (e: IOException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }
}
