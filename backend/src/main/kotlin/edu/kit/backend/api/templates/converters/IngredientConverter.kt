package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.data.model.Ingredient

class IngredientConverter : Converter<IngredientTemplate, Ingredient>() {

    override fun convert(from: Ingredient): IngredientTemplate {
        return IngredientTemplate(
            from.id,
            from.name
        )
    }
}
