package edu.kit.backend.api.service

import edu.kit.backend.api.repository.PreparationStepRepository
import edu.kit.backend.data.model.PreparationStep
import edu.kit.backend.exceptions.IdNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

private const val TYPE: String = "PreparationStep"

@Service
class PreparationStepService {
    @Autowired
    private lateinit var preparationStepRepository: PreparationStepRepository

    /**
     * @param id is the id of the [PreparationStep]
     * @throws IdNotFoundException
     * @return the [PreparationStep]
     */
    fun getRecipePreparationStepById(id: Long): PreparationStep {
        return preparationStepRepository.findById(id).orElseThrow { IdNotFoundException(TYPE, id) }
    }

    fun deletePreparationStep(id: Long) {
        return preparationStepRepository.deleteById(id)
    }
}
