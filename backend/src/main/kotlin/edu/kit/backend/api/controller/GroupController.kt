package edu.kit.backend.api.controller

import edu.kit.backend.api.event.*
import edu.kit.backend.api.service.GroupService
import edu.kit.backend.api.service.RecipeService
import edu.kit.backend.api.service.UserService
import edu.kit.backend.api.templates.*
import edu.kit.backend.api.templates.converters.GroupConverter
import edu.kit.backend.api.templates.converters.RecipeConverter
import edu.kit.backend.exceptions.CodeNotFoundException
import edu.kit.backend.exceptions.GroupEmptyException
import edu.kit.backend.exceptions.IdNotFoundException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

/**
 *  Controller for managing group-related HTTP requests.
 *  Provides endpoints for fetching specific group by their code
 */
@RestController
@RequestMapping("/groups")
class GroupController {

    @Autowired
    private lateinit var decisionListener: DecisionListener

    @Autowired
    private lateinit var decisionPublisher: DecisionPublisher

    @Autowired
    private lateinit var groupService: GroupService

    @Autowired
    private lateinit var userService: UserService

    @Autowired
    private lateinit var recipeService: RecipeService

    private var logger = LoggerFactory.getLogger(GroupController::class.java)

    private val groupConverter = GroupConverter()
    private val recipeConverter = RecipeConverter()

    @PostMapping
    fun createGroup(@RequestBody groupCreationTemplate: GroupTemplate): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.createGroup(groupCreationTemplate)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/edit")
    fun editGroup(@RequestBody groupTemplate: GroupTemplate): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.editGroup(groupTemplate)
                )
            )
        } catch (e: CodeNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: GroupEmptyException) {
            ResponseEntity.ok(e.message)
        }
    }

    @PatchMapping("/join")
    fun joinGroup(@RequestBody groupMemberTemplate: GroupMemberTemplate): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.joinGroup(groupMemberTemplate.groupCode, groupMemberTemplate.userId)
                )
            )
        } catch (e: CodeNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/leave")
    fun leaveGroup(@RequestBody groupMemberTemplate: GroupMemberTemplate): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.leaveGroup(groupMemberTemplate.groupCode, groupMemberTemplate.userId)
                )
            )
        } catch (e: CodeNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        } catch (e: GroupEmptyException) {
            ResponseEntity.ok(e.message)
        }
    }

    @GetMapping("/all/{userId}")
    fun getAllGroupsOfUser(@PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    userService.getUser(userId).groups
                )
            )
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/{groupId}") //should be used for updating a group in the frontend
    fun getGroup(@PathVariable groupId: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.getGroupById(groupId)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping("/{groupId}/recipes/{userId}")
    fun getGroupRecipes(@PathVariable groupId: Long, @PathVariable userId: Long): ResponseEntity<Any> {
        return try {
            val group = groupService.getGroupById(groupId)
            val user = userService.getUser(userId)

            val groupDecision = decisionListener.getGroupDecision(groupId)
                ?: return ResponseEntity.badRequest().body("Group session is not active")

            if (!group.members.contains(user)) {
                return ResponseEntity.badRequest().body("User is not a member of this group")
            }

            val alreadySent = groupDecision.getLoadedRecipes(userId)

            val sentRecipes = groupService.getGroupById(groupId).recipeStack.filter {
                !alreadySent.contains(it.id)
            }.shuffled().take(10)

            groupDecision.addLoadedRecipes(userId, sentRecipes)

            ResponseEntity.ok(
                recipeConverter.convert(
                    sentRecipes
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/{groupId}/fillRecipes")
    fun fillGroupWithRecipes(
        @PathVariable groupId: Long,
        @RequestBody groupMember: GroupMemberTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.fillGroupWithRecipes(groupId)
                )
            )
        } catch (e: IdNotFoundException) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }

    @PatchMapping("/{groupId}/addRecipes")
    fun addRecipesToGroup(
        @PathVariable groupId: Long,
        @RequestBody groupMember: GroupMemberTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.addRecipesToGroup(
                        groupId, recipeService.fetchNumberOfRecipesRandom(10)
                    )
                )
            )
        } catch (e: Exception) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @PutMapping("/{groupId}/clearRecipes")
    fun clearGroupRecipes(
        @PathVariable groupId: Long,
        @RequestBody groupMember: GroupMemberTemplate
    ): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.clearRecipes(groupId, groupMember.userId)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @DeleteMapping
    fun deleteGroup(@RequestBody groupMember: GroupMemberTemplate): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                groupConverter.convert(
                    groupService.deleteGroup(groupMember.groupCode, groupMember.userId)
                )
            )
        } catch (e: CodeNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }


    /**
     * Endpoint for single group member to send his swiping decision to the server
     * @param userDecision Array of Recipe Ids which the user would like to eat
     */
    @PutMapping("/{groupId}/accept")
    fun sendAcceptDecision(
        @PathVariable groupId: Long,
        @RequestBody userDecision: UserDecisionTemplate
    ): ResponseEntity<Any> {
        // TODO allocate to a service
        try {
            val group = groupService.getGroupById(groupId)
            val user = userService.getUser(userDecision.userId)

            val groupDecision = decisionListener.getGroupDecision(groupId)
                ?: return ResponseEntity.badRequest().body("Group session is not active")

            if (!group.members.contains(user)) {
                return ResponseEntity.badRequest().body("User is not a member of this group")
            }

            decisionPublisher.publishEvent(
                AcceptedEvent(
                    userId = userDecision.userId,
                    groupId = groupId,
                    recipeId = userDecision.recipeId
                )
            )
            logger.info(groupDecision.hashCode().toString())
            return ResponseEntity.ok(groupDecision.getUserDecisions())
        } catch (e: IdNotFoundException) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }

    @PutMapping("/{groupId}/decline")
    fun sendDeclineDecision(
        @PathVariable groupId: Long,
        @RequestBody userDecision: UserDecisionTemplate
    ): ResponseEntity<Any> {
        // TODO allocate to a service
        try {
            val group = groupService.getGroupById(groupId)
            val user = userService.getUser(userDecision.userId)

            val groupDecision = decisionListener.getGroupDecision(groupId)
                ?: return ResponseEntity.badRequest().body("Group session is not active")

            if (!group.members.contains(user)) {
                return ResponseEntity.badRequest().body("User is not a member of this group")
            }

            decisionPublisher.publishEvent(
                DeclinedEvent(
                    userId = userDecision.userId,
                    groupId = groupId,
                    recipeId = userDecision.recipeId
                )
            )
            logger.info(groupDecision.hashCode().toString())
            return ResponseEntity.ok(groupDecision.getUserDecisions())
        } catch (e: IdNotFoundException) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Endpoint for the admin of the group to start the session
     * Necessary to start the evaluation.
     * Admin has to use this before /{groupId}/send endpoint gets accessed by members
     */
    @PatchMapping("/{groupId}/start")
    fun startDecision(@PathVariable groupId: Long, @RequestBody userId: LongTemplate): ResponseEntity<Any> {
        // TODO allocate to a service
        try {
            val user = userService.getUser(userId.value)
            val group = groupService.getGroupById(groupId)

            if (decisionListener.groupIsActive(groupId) && !decisionListener.groupIsFinished(groupId)) {
                return ResponseEntity.ok(GroupStartedTemplate(true))
            }

            if (user != group.admin) return ResponseEntity.ok(GroupStartedTemplate(false))

            decisionListener.putGroupDecision(
                groupId = groupId,
                groupDecision = GroupDecision(
                    groupId = groupId,
                    listOfRecipeIds = group.recipeStack.map { it.id },
                    amountOfGroupMembers = group.members.size
                )
            )

            return ResponseEntity.ok(GroupStartedTemplate(true))
        } catch (e: IdNotFoundException) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Returns the id of the selected recipe (evaluation) via Callable
     */
    @GetMapping("/{groupId}/result")
    fun getDecision(@PathVariable groupId: Long): ResponseEntity<Any> { // Callable ist eine Alternative aber hab ich au nicht hinbekommen
        try {
            if (decisionListener.groupIsActive(groupId) && !decisionListener.groupIsFinished(groupId)) {
                return ResponseEntity.badRequest().body("not ready yet")
            }
            val result = decisionListener.getResultId(groupId)
            return if (result == 0L) {
                ResponseEntity.badRequest().body("No user accepted a recipe")
            } else {
                ResponseEntity.ok(
                    recipeConverter.convert(
                        groupService.saveMatch(groupId, result)
                    )
                )
            }
        } catch (e: IdNotFoundException) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }


    @PatchMapping("/{groupId}/reset")
    fun resetDecision(@PathVariable groupId: Long): ResponseEntity<Any> {
        logger.info(" reset GroupDecisionComponent with  groupId: $groupId")
        decisionListener.removeGroupDecision(groupId)

        return ResponseEntity.ok().body("Successfully reset decisions")
    }
}
