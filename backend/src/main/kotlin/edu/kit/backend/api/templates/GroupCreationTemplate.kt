package edu.kit.backend.api.templates

/**
 * This class is only for the purpose of using it as an JSON RequestBody
 */
data class GroupCreationTemplate(
    val adminId: Long,
    val name: String
)
