package edu.kit.backend.api.security

import edu.kit.backend.api.repository.UserRepository
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

// using an alias in this class to avoid conflict with the org.springframework.security.core.userdetails.User class
typealias ApplicationUser = edu.kit.backend.data.model.AppUser

/**
 * Class to load user specific data in our application.
 */
@Service
class SERUserDetailsService(
    private val userRepository: UserRepository
) : UserDetailsService {
    override fun loadUserByUsername(username: String): UserDetails =
        userRepository.findByUsername(username)
            ?.mapToUserDetails()
            ?: throw UsernameNotFoundException("username $username not found!")

    /**
     * Maps a User in our Application to org.springframework.security.core.userdetails.UserDetails
     */
    private fun ApplicationUser.mapToUserDetails(): UserDetails =
        // User is implementation of UserDetails holding the core information of a User
        User.builder()
            .username(this.username)
            .password(this.password)
            .roles(this.role.name)
            .build()
}
