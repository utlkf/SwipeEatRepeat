package edu.kit.backend.api.event

class DeclinedEvent(userId: Long, groupId: Long, recipeId: Long) : DecisionEvent(userId, groupId, recipeId) {
}
