package edu.kit.backend.api.controller

import edu.kit.backend.api.service.AuthenticationService
import edu.kit.backend.api.templates.RegistrationCredentials
import edu.kit.backend.data.model.AuthenticationResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController

/**
 * Controller for endpoint calls from admins
 */
@RestController
class AdminController {

    @Autowired
    private lateinit var authenticationService: AuthenticationService

    @PostMapping("/admin/register")
    fun registerAdmin(@RequestBody registrationCredentials: RegistrationCredentials): AuthenticationResponse {
        return authenticationService.registerAdmin(registrationCredentials)
    }

    @GetMapping("/admin/demo")
    fun demo(): ResponseEntity<Any> {
        return ResponseEntity.ok("You are an admin")
    }
}
