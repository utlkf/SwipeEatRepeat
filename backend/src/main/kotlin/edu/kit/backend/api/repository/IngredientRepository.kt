package edu.kit.backend.api.repository

import edu.kit.backend.data.model.Ingredient
import org.springframework.data.jpa.repository.JpaRepository

interface IngredientRepository : JpaRepository<Ingredient, Long> {
    fun findByName(name: String): Ingredient?
    fun existsByName(name: String): Boolean
}
