package edu.kit.backend.api.service

import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.data.model.Preference
import edu.kit.backend.data.model.Recipe
import edu.kit.backend.exceptions.IdNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.stereotype.Service

private const val TYPE = "user"

/**
 * Service class that is responsible for handling business logic related to user operations.
 * Interacts with the UserRepository for accessing and managing user data.
 */
@Configuration
@Service
class UserService {

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var recipeService: RecipeService

    @Autowired
    lateinit var preferenceService: PreferenceService

    @Autowired
    lateinit var ingredientService: IngredientService

    @Autowired
    lateinit var filterService: FilterService

    fun getAllUsers(): List<AppUser> {
        return userRepository.findAll()
    }

    /**
     * Delete a user from the database.
     * @param id The id of the user to delete.
     * @return whether the user to delete was found
     */
    fun deleteUser(id: Long): AppUser {
        val user: AppUser = getUser(id)
        userRepository.deleteById(id)
        return user
    }

    /**
     * Gets the [AppUser] with the given [id].
     *
     * @param id of the user
     * @throws [IdNotFoundException] if no user of the [id] is in the database
     * @return [AppUser] of the given [id]
     */
    fun getUser(id: Long): AppUser {
        return userRepository.findById(id).orElseThrow {
            IdNotFoundException(TYPE, id)
        }
    }

    /**
     * fetches the user from the database, changes his preferences attribute and saves it again
     * @param userId The id of the user
     * @param preferences The list of labels describing the user preferences
     * @throws [IdNotFoundException] if no user of the [userId] is in the database
     * @return The updated user if successful, an error response else
     */
    fun setPreferences(userId: Long, preferences: List<PreferenceTemplate>): AppUser {
        val user = getUser(userId)
        val preferenceList = mutableListOf<Preference>()
        preferences.forEach {
            preferenceList.add(preferenceService.getPreferenceById(it.id))
        }
        user.preferences.clear()
        user.preferences.addAll(preferenceList)
        return userRepository.save(user)
    }

    fun getPreferences(userId: Long): List<Preference> {
        val user = getUser(userId)
        return user.preferences
    }

    fun setBlacklist(userId: Long, blacklistTemplate: List<IngredientTemplate>): AppUser {
        val user = getUser(userId)
        val blacklist = mutableListOf<Ingredient>()
        blacklistTemplate.forEach {
            blacklist.add(ingredientService.getIngredientById(it.id))
        }
        user.blacklist.clear()
        user.blacklist.addAll(blacklist)
        return userRepository.save(user)
    }

    fun getBlacklist(userId: Long): Collection<Ingredient> {
        val user = getUser(userId)
        return user.blacklist
    }

    fun setSupersetWhitelist(userId: Long, supersetWhitelistTemplate: List<IngredientTemplate>): AppUser {
        val user = getUser(userId)
        val supersetWhitelist = mutableListOf<Ingredient>()
        supersetWhitelistTemplate.forEach {
            supersetWhitelist.add(ingredientService.getIngredientById(it.id))
        }
        user.supersetWhitelist.clear()
        user.supersetWhitelist.addAll(supersetWhitelist)
        return userRepository.save(user)
    }

    fun getSupersetWhitelist(userId: Long): Collection<Ingredient> {
        val user = getUser(userId)
        return user.supersetWhitelist
    }

    fun setSubsetWhitelist(userId: Long, subsetWhitelistTemplate: List<IngredientTemplate>): AppUser {
        val user = getUser(userId)
        val subsetWhitelist = mutableListOf<Ingredient>()
        subsetWhitelistTemplate.forEach {
            subsetWhitelist.add(ingredientService.getIngredientById(it.id))
        }
        user.subsetWhitelist.clear()
        user.subsetWhitelist.addAll(subsetWhitelist)
        return userRepository.save(user)
    }

    fun getSubsetWhitelist(userId: Long): Collection<Ingredient> {
        val user = getUser(userId)
        return user.subsetWhitelist
    }

    /**
     * Retrieve a specific amount of [Recipe]s based on the provided limit and the [AppUser]'s preferences.
     *
     * @param numberOfRecipes the maximum number of [Recipe]s to retrieve.
     * @param userID is the [AppUser.id] of the user whose [Recipe]s are fetched.
     * @throws [IdNotFoundException] if no user of the [userID] is in the database
     * @return a list of [Recipe]s up to the specified limit.
     */
    fun fetchNumberOfRecipesForUser(numberOfRecipes: Int, userID: Long): List<Recipe> {
        val user = getUser(userID)
        // only get labels that match user preferences
        val possibleRecipes = filterService.getFilteredRecipes(
            preferences = user.preferences,
            blacklist = user.blacklist,
            supersetWhitelist = user.supersetWhitelist,
            subsetWhitelist = user.subsetWhitelist
        )

        val alreadySent = user.declinedRecipes + user.acceptedRecipes

        return possibleRecipes.filter {
            !alreadySent.contains(it)
        }.shuffled().take(numberOfRecipes)
    }

    /**
     * Retrieves all [Recipe]s created by a specific [AppUser].
     *
     * @param id is the [AppUser.id] of the [AppUser].
     * @throws [IdNotFoundException] if no user of the [id] is in the database
     * @return a list of the [Recipe]s created by the [AppUser].
     */
    fun getAllRecipesCreatedByUser(id: Long): List<Recipe> {
        val user = getUser(id)
        return recipeService.findByCreator(user)
    }

    /**
     * Adds the [Recipe] matching the [recipeTemplate] to the [AppUser.favouriteRecipes]
     * of the [AppUser] with the id [userId].
     *
     * @param userId is the [AppUser.id] of the [AppUser].
     * @param recipeTemplate is the [Recipe] that is added to the [AppUser.favouriteRecipes].
     * @throws IdNotFoundException if the database has no [AppUser] with the [userId]
     *   or no [Recipe] has the [RecipeTemplate.id].
     * @return the [AppUser] whose [AppUser.favouriteRecipes] were updated.
     */
    fun addFavourite(userId: Long, recipeTemplate: RecipeTemplate): AppUser {
        val user = getUser(userId)
        val recipe = recipeService.getRecipeById(recipeTemplate.id)

        user.favouriteRecipes.add(recipe)
        return userRepository.save(user)
    }

    /**
     * Removes the [Recipe] matching the [recipeTemplate] from the [AppUser.favouriteRecipes]
     * of the [AppUser] with the id [userId].
     *
     * @param userId is the [AppUser.id] of the [AppUser].
     * @param recipeTemplate is the [Recipe] that is removed from the [AppUser.favouriteRecipes].
     * @throws IdNotFoundException if the database has no [AppUser] with the [userId]
     *   or no [Recipe] has the [RecipeTemplate.id].
     * @return the [AppUser] whose [AppUser.favouriteRecipes] were updated.
     */
    fun removeFavourite(userId: Long, recipeTemplate: RecipeTemplate): AppUser {
        val user = getUser(userId)
        val recipe = recipeService.getRecipeById(recipeTemplate.id)

        user.favouriteRecipes.remove(recipe)
        return userRepository.save(user)
    }

    /**
     * Adds the [Recipe] matching the [recipeTemplate] to the [AppUser.acceptedRecipes]
     * of the [AppUser] with the id [userId].
     *
     * @param userId is the [AppUser.id] of the [AppUser].
     * @param recipeTemplate is the [Recipe] that is added to the [AppUser.acceptedRecipes].
     * @throws IdNotFoundException if the database has no [AppUser] with the [userId]
     *   or no [Recipe] has the [RecipeTemplate.id].
     * @return the [AppUser] whose [AppUser.acceptedRecipes] were updated.
     */
    fun addAccepted(userId: Long, recipeTemplate: RecipeTemplate): AppUser {
        val user = getUser(userId)
        val recipe = recipeService.getRecipeById(recipeTemplate.id)
        user.acceptedRecipes.add(recipe)
        return userRepository.save(user)
    }

    /**
     * Adds the [Recipe] matching the [recipeTemplate] to the [AppUser.declinedRecipes]
     * of the [AppUser] with the id [userId].
     *
     * @param userId is the [AppUser.id] of the [AppUser].
     * @param recipeTemplate is the [Recipe] that is added to the [AppUser.declinedRecipes].
     * @throws IdNotFoundException if the database has no [AppUser] with the [userId]
     *   or no [Recipe] has the [RecipeTemplate.id].
     * @return the [AppUser] whose [AppUser.declinedRecipes] were updated.
     */
    fun addDeclined(userId: Long, recipeTemplate: RecipeTemplate): AppUser {
        val user = getUser(userId)
        val recipe = recipeService.getRecipeById(recipeTemplate.id)
        user.declinedRecipes.add(recipe)
        return userRepository.save(user)
    }

    fun resetAcceptedRecipes(userId: Long): AppUser {
        val user = getUser(userId)
        user.acceptedRecipes.clear()
        return userRepository.save(user)
    }

    fun resetDeclinedRecipes(userId: Long): AppUser {
        val user = getUser(userId)
        user.declinedRecipes.clear()
        return userRepository.save(user)
    }
}
