package edu.kit.backend.api.security

import edu.kit.backend.data.model.Role
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpMethod
import org.springframework.security.authentication.AuthenticationProvider
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.DefaultSecurityFilterChain
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.authentication.logout.LogoutHandler
import org.springframework.security.web.util.matcher.AntPathRequestMatcher

/**
 *  This config is copied to have a working config
 *  https://github.com/spring-projects/spring-security-samples/
 *  blob/main/servlet/spring-boot/kotlin/hello-security/src
 *  /main/kotlin/org/springframework/security/samples/config/SecurityConfig.kt
 *
 *  but is already edited.
 *  Class resembles our whole security configuration so far
 *  @author Miro
 */
@Configuration
@EnableWebSecurity
class SecurityConfig(
    private val authenticationProvider: AuthenticationProvider,
    private val whiteList: List<String>,
    private val logoutHandler: LogoutHandler,
) {

    /**
     * A bean to change the default security chain.
     * The security chain contains filters applied for each incoming request
     */
    @Bean
    fun securityFilterChain(
        http: HttpSecurity,
        jwtAuthenticationFilter: JwtAuthenticationFilter
    ): DefaultSecurityFilterChain {
        val matchers = whiteList.map { AntPathRequestMatcher(it) }
        http
            .csrf { it.disable() }
            .authorizeHttpRequests {
                it
                    // permit access to all endpoints in the white-list
                    .requestMatchers(*matchers.toTypedArray())
                    .permitAll()
                    .requestMatchers("/admin/**").hasRole(Role.ADMIN.name)
                    .requestMatchers(HttpMethod.POST, "/preferences").hasRole(Role.ADMIN.name)
                    .requestMatchers(HttpMethod.PUT, "/preferences").hasRole(Role.ADMIN.name)
                    .requestMatchers(HttpMethod.DELETE, "/preferences").hasRole(Role.ADMIN.name)
                    // getAllGroups is only accessible for admins
                    .requestMatchers(HttpMethod.GET, "/groups").hasRole(Role.ADMIN.name)
                    .anyRequest()
                    .fullyAuthenticated()
            }
            .sessionManagement {
                // do not create a HttpSession
                it.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            }
            .authenticationProvider(authenticationProvider)
            .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter::class.java)
            .logout { logout ->
                logout.logoutUrl("/logout") // Defines the URL to trigger logout
                    .addLogoutHandler(logoutHandler)    // Adds your custom logout handler
                    .logoutSuccessHandler { request, response, authentication ->
                        SecurityContextHolder.clearContext() // Clears security context on success
                    }
            }
        return http.build()
    }
}
