package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.MeasureTemplate
import edu.kit.backend.data.model.Measure

class MeasureConverter : Converter<MeasureTemplate, Measure>() {
    override fun convert(from: Measure): MeasureTemplate {
        return MeasureTemplate(
            from.id,
            from.name,
            from.unit
        )
    }
}
