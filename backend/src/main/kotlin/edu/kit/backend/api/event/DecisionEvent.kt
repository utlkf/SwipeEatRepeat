package edu.kit.backend.api.event

abstract class DecisionEvent(val userId: Long, val groupId: Long, val recipeId: Long)
