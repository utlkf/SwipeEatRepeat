package edu.kit.backend.api.security

import edu.kit.backend.api.repository.TokenRepository
import edu.kit.backend.exceptions.InvalidTokenException
import io.jsonwebtoken.JwtException
import jakarta.servlet.FilterChain
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.util.AntPathMatcher
import org.springframework.web.filter.OncePerRequestFilter

/**
 * A getFilteredRecipes for authenticating incoming requests to the api.
 * This getFilteredRecipes is used in the SecurityConfig
 */
@Component
class JwtAuthenticationFilter(
    private val userDetailsService: SERUserDetailsService,
    private val jwtService: JwtService,
    private val tokenRepository: TokenRepository,
    private val whiteList: List<String>
) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        logger.info("inside jwt authentication getFilteredRecipes")
        logger.info("path: ${request.servletPath}")
        val pathMatcher = AntPathMatcher()
        // If the request is pointed to a permited endpoint the jwtAuthenticationFilter is passed
        for (w in whiteList) {
            if (pathMatcher.match(w, request.servletPath)) {
                logger.info("${request.servletPath} in white list")
                filterChain.doFilter(request, response)
                return
            }
        }
        val authHeader: String? = request.getHeader("Authorization")
        if (jwtService.doesNotContainBearerToken(authHeader) || authHeader == null) {
            //do not proceed with this getFilteredRecipes and pass to the next one
            filterChain.doFilter(request, response)
            return
        }
        val jwtToken = jwtService.extractTokenValue(authHeader)
        try {
            // Exception is thrown when token is expired
            val username = jwtService.extractUsername(jwtToken)
            if (username != null && SecurityContextHolder.getContext().authentication == null) {
                // fetch userDetails
                val foundUser = userDetailsService.loadUserByUsername(username)
                // check if token is valid on the database side
                val isTokenValid = tokenRepository.findByToken(jwtToken)?.let { !it.revoked && !it.expired } ?: false
                // check if token is also valid on the jwt logic side
                if (jwtService.isTokenValid(jwtToken, foundUser) && isTokenValid)
                    updateContext(foundUser, request)
                else throw InvalidTokenException(jwtToken)
                filterChain.doFilter(request, response)
            }
        } catch (e: JwtException) {
            logger.info("caught jwt exception")
            response.status = HttpServletResponse.SC_UNAUTHORIZED
            val writer = response.writer
            writer.write(e.message ?: "no valid Jwt token passed")
            writer.flush()
        } catch (e: InvalidTokenException) {
            logger.info("caught exception")
            response.status = HttpServletResponse.SC_UNAUTHORIZED
            val writer = response.writer
            writer.write(e.message ?: "no valid Jwt token passed")
            writer.flush()
        }
    }

    /**
     * Method to update the systems security context with the found user and his authorities.
     * @param foundUser the core information about the found User
     * @param request the incoming http request
     */
    private fun updateContext(foundUser: UserDetails, request: HttpServletRequest) {
        val authToken = UsernamePasswordAuthenticationToken(foundUser, null, foundUser.authorities)
        authToken.details = WebAuthenticationDetailsSource().buildDetails(request)
        SecurityContextHolder.getContext().authentication = authToken
    }
}
