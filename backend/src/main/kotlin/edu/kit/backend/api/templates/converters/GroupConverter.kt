package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.GroupTemplate
import edu.kit.backend.data.model.Group

class GroupConverter : Converter<GroupTemplate, Group>() {

    private val userConverter = UserConverter()
    private val recipeConverter = RecipeConverter()
    private val preferenceConverter = PreferenceConverter()
    private val ingredientConverter = IngredientConverter()

    override fun convert(from: Group): GroupTemplate {
        return GroupTemplate(
            from.admin.id,
            from.name,
            from.id,
            from.groupCode,
            userConverter.convert(from.members),
            recipeConverter.convert(from.recipeStack),
            preferenceConverter.convert(from.preferences),
            ingredientConverter.convert(from.blacklist),
            ingredientConverter.convert(from.supersetWhitelist),
            ingredientConverter.convert(from.subsetWhitelist),
            if (from.match != null) recipeConverter.convert(from.match!!) else null,
        )
    }
}
