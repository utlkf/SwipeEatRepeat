package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.data.model.Preference

class PreferenceConverter : Converter<PreferenceTemplate, Preference>() {

    override fun convert(from: Preference): PreferenceTemplate {
        return PreferenceTemplate(
            from.id,
            from.name,
        )
    }
}
