package edu.kit.backend.api.service

import edu.kit.backend.api.repository.TokenRepository
import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.api.security.JwtService
import edu.kit.backend.api.security.SERUserDetailsService
import edu.kit.backend.api.security.Token
import edu.kit.backend.api.security.TokenType
import edu.kit.backend.api.templates.LoginCredentials
import edu.kit.backend.api.templates.RegistrationCredentials
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.AuthenticationResponse
import edu.kit.backend.data.model.Role
import edu.kit.backend.exceptions.DuplicateEmailException
import edu.kit.backend.exceptions.DuplicateUsernameException
import edu.kit.backend.exceptions.InvalidTokenException
import jakarta.servlet.http.HttpServletRequest
import jakarta.servlet.http.HttpServletResponse
import org.slf4j.LoggerFactory
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service

/**
 * Class to provide and execute logic for user login and registration
 */
@Service
class AuthenticationService(
    private val authManager: AuthenticationManager,
    private val userDetailsService: SERUserDetailsService,
    private val jwtService: JwtService,
    private val userRepository: UserRepository,
    private val tokenRepository: TokenRepository,
    private val passwordEncoder: PasswordEncoder
) {

    private val logger = LoggerFactory.getLogger(AuthenticationService::class.java)

    /**
     * Method to authenticate a user via his login credentials
     */
    fun authentication(loginCredentials: LoginCredentials): AuthenticationResponse {
        // if username and password values don’t match with any of the users in our system,
        // the authenticate method will throw AuthenticationException and the API will return 403 Forbidden
        authManager.authenticate(
            UsernamePasswordAuthenticationToken(
                loginCredentials.username,
                loginCredentials.password
            )
        )
        // if authentication does not fail fetch the userDetails
        val userDetails = userDetailsService.loadUserByUsername(loginCredentials.username!!)
        val user = userRepository.findByUsername(loginCredentials.username)
            ?: throw UsernameNotFoundException("username ${loginCredentials.username} not found")
        val accessToken = jwtService.generateAccessToken(userDetails)
        val refreshToken = jwtService.generateRefreshToken(userDetails)
        revokeAllUserTokens(user)
        saveUserToken(user, accessToken)
        //To get usedId to the frontend
        return AuthenticationResponse(
            accessToken = accessToken,
            refreshToken = refreshToken,
            userId = user.id
        )

    }

    /**
     * Method to execute the logic for creating a new User.
     *
     * @param registrationCredentials the credentials used for the registration process
     * @return a JWT Token for the newly registered user
     * @throws DuplicateEmailException when the email in the registration credentials is already registered
     * @throws DuplicateUsernameException when the username in the registration credentials is already in use
     */
    fun registerUser(registrationCredentials: RegistrationCredentials): AuthenticationResponse {
        logger.info("inside AuthenticationService registerUser")
        // username and email must be unique,
        // so if there is a user in the repository with similar credentials the registration can not be executed.
        if (userRepository.findByUsername(registrationCredentials.username) != null)
            throw DuplicateUsernameException(registrationCredentials.username)
        if (userRepository.findByEmail(registrationCredentials.email) != null)
            throw DuplicateEmailException(registrationCredentials.email)

        val createdUser = createUserEntry(registrationCredentials)
        val userDetails = userDetailsService.loadUserByUsername(registrationCredentials.username)
        val accessToken = jwtService.generateAccessToken(userDetails)
        val refreshToken = jwtService.generateRefreshToken(userDetails)
        saveUserToken(createdUser, accessToken)
        //Get userId to the frontend
        return AuthenticationResponse(accessToken, refreshToken, createdUser.id)
    }

    /**
     * creates a user entry from registrationCredentials
     */
    private fun createUserEntry(registrationCredentials: RegistrationCredentials): AppUser {
        val encodedPassword = passwordEncoder.encode(registrationCredentials.password)
        val user = AppUser(
            username = registrationCredentials.username,
            password = encodedPassword,
            email = registrationCredentials.email,
            role = Role.USER
        )
        return userRepository.save(user)
    }

    fun registerAdmin(registrationCredentials: RegistrationCredentials): AuthenticationResponse {
        // username and email must be unique,
        // so if there is a user in the repository with similar credentials the registration can not be executed.
        if (userRepository.findByUsername(registrationCredentials.username) != null)
            throw DuplicateUsernameException(registrationCredentials.username)
        if (userRepository.findByEmail(registrationCredentials.email) != null)
            throw DuplicateEmailException(registrationCredentials.email)

        val createdAdmin = createAdminEntry(registrationCredentials)
        val userDetails = userDetailsService.loadUserByUsername(registrationCredentials.username)
        val accessToken = jwtService.generateAccessToken(userDetails)
        val refreshToken = jwtService.generateRefreshToken(userDetails)
        saveUserToken(createdAdmin, accessToken)
        return AuthenticationResponse(accessToken, refreshToken, createdAdmin.id)
    }

    /**
     * creates a user entry from registrationCredentials
     */
    private fun createAdminEntry(registrationCredentials: RegistrationCredentials): AppUser {
        val encodedPassword = passwordEncoder.encode(registrationCredentials.password)
        val user = AppUser(
            username = registrationCredentials.username,
            password = encodedPassword,
            email = registrationCredentials.email,
            role = Role.ADMIN
        )
        return userRepository.save(user)
    }

    private fun saveUserToken(user: AppUser, token: String) {
        logger.info("inside saveUserToken in AuthenticationService")
        val tokenEntity =
            Token(token = token, tokenType = TokenType.BEARER, expired = false, revoked = false, user = user)
        tokenRepository.save(tokenEntity)
    }

    private fun revokeAllUserTokens(user: AppUser) {
        val userTokens = tokenRepository.findAllValidTokensByUser(user.id)
        for (token in userTokens) {
            token.revoked = true
            token.expired = true
        }
        tokenRepository.saveAll(userTokens)
    }

    fun refreshToken(request: HttpServletRequest, response: HttpServletResponse): AuthenticationResponse {
        logger.info("inside AuthenticatioService refreshToken")
        logger.info("request: $request, response: $response")
        val authHeader: String? = request.getHeader(org.springframework.http.HttpHeaders.AUTHORIZATION)
        if (jwtService.doesNotContainBearerToken(authHeader) || authHeader == null) {
            throw InvalidTokenException(authHeader ?: "")
        }
        val refreshToken = jwtService.extractTokenValue(authHeader)
        // Exception is thrown when token is expired
        val username = jwtService.extractUsername(refreshToken)
        if (username != null) {
            // fetch userDetails
            val userDetails = userDetailsService.loadUserByUsername(username)
            val user = userRepository.findByUsername(username)
                ?: throw UsernameNotFoundException("username $username not found")
            // check if token is also valid on the jwt logic side
            if (jwtService.isTokenValid(refreshToken, userDetails)) {
                val accessToken = jwtService.generateAccessToken(userDetails)
                revokeAllUserTokens(user)
                saveUserToken(user, accessToken)
                return AuthenticationResponse(accessToken, refreshToken, user.id)
            } else {
                throw InvalidTokenException(refreshToken)
            }
        } else {
            throw InvalidTokenException(refreshToken)
        }
    }
}
