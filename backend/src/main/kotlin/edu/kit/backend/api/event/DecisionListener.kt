package edu.kit.backend.api.event

import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class DecisionListener {

    val requestToGroup: MutableMap<Long, GroupDecision> = mutableMapOf()

    @EventListener
    fun addDeclined(acceptedEvent: AcceptedEvent) {
        requestToGroup[acceptedEvent.groupId]?.addAccepted(acceptedEvent)
    }

    @EventListener
    fun addAccepted(declinedEvent: DeclinedEvent) {
        requestToGroup[declinedEvent.groupId]?.addDeclined(declinedEvent)
    }

    fun getGroupDecision(groupId: Long): GroupDecision? {
        return requestToGroup[groupId]
    }

    fun putGroupDecision(groupId: Long, groupDecision: GroupDecision) {
        requestToGroup[groupId] = groupDecision
    }

    fun groupIsActive(groupId: Long): Boolean {
        return (requestToGroup[groupId] != null)
    }

    fun groupIsFinished(groupId: Long): Boolean {
        return requestToGroup[groupId]?.isEvaluated ?: false
    }

    fun getResultId(groupId: Long): Long {
        return requestToGroup[groupId]?.getResultId() ?: 0
    }

    fun removeGroupDecision(groupId: Long) {
        requestToGroup.remove(groupId)
    }
}
