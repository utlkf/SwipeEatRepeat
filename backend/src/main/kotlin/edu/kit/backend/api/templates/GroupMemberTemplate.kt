package edu.kit.backend.api.templates

/**
 * This class is only for the purpose of using it as an JSON RequestBody
 */
data class GroupMemberTemplate(
    val userId: Long,
    val groupCode: String
)
