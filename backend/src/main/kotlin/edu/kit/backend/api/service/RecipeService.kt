package edu.kit.backend.api.service

import edu.kit.backend.api.repository.GroupRepository
import edu.kit.backend.api.repository.RecipeRepository
import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.api.templates.PreparationStepTemplate
import edu.kit.backend.api.templates.RecipeComponentTemplate
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.data.model.*
import edu.kit.backend.exceptions.IdNotFoundException
import okio.IOException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.nio.file.Files
import java.nio.file.Paths
import java.util.*
import kotlin.math.min

private const val TYPE: String = "recipe"
private const val IMAGE_DIRECTORY = "/images"

// localhost for testing with emulator 10.0.2.2
private const val BASE_URL = "http://swipe-eat-repeat.de:8080"

/**
 * Service class for handling recipe-related business logic.
 * Interacts with RecipeRepository for data access.
 */
@Service
class RecipeService {

    companion object {
        private val logger = LoggerFactory.getLogger(RecipeService::class.java)
    }

    @Autowired
    lateinit var recipeRepository: RecipeRepository

    @Autowired
    lateinit var userRepository: UserRepository

    @Autowired
    lateinit var ingredientService: IngredientService

    @Autowired
    lateinit var measureService: MeasureService

    @Autowired
    lateinit var preferenceService: PreferenceService

    /**
     * Retrieve all recipes from the repository.
     * @return List of all recipes.
     */
    fun getAllRecipes(): List<Recipe> {
        return recipeRepository.findAll()
    }

    /**
     * Retrieve a specific amount of recipes based on the provided limit but random.
     * @param numberOfRecipes The maximum number of recipes to retrieve.
     * @return List of random recipes up to the specified limit.
     */
    fun fetchNumberOfRecipesRandom(numberOfRecipes: Int): List<Recipe> {
        // Check if the limit is within the bounds of the list size
        val validAmount = min(numberOfRecipes, recipeRepository.findAll().size)
        return recipeRepository.findAll().shuffled().take(validAmount)
    }

    /**
     * Retrieve a specific recipe by its ID.
     * @param id The ID of the recipe to retrieve.
     * @return The requested recipe
     * @throws [IdNotFoundException] if the recipe does not exist
     */
    fun getRecipeById(id: Long): Recipe {
        return recipeRepository.findById(id).orElseThrow {
            IdNotFoundException(TYPE, id)
        }
    }

    /**
     * Save a new Recipe in the Database.
     * @param recipeTemplate The Recipe to be saved
     * @return The saved recipe or null if an error occurs
     */
    fun createRecipe(recipeTemplate: RecipeTemplate, file: MultipartFile): Recipe {
        val imageUrl = processAndSaveImageFile(file)

        val createdRecipe = createRecipeFromTemplate(recipeTemplate, imageUrl)

        logger.info("recipe image url: ${createdRecipe.imageUrl}")
        return recipeRepository.save(createdRecipe)
    }

    // Function to process and save the image file, and return its URL
    private fun processAndSaveImageFile(file: MultipartFile): String {
        val imageDirectory = Paths.get(IMAGE_DIRECTORY)
        if (!Files.exists(imageDirectory)) {
            Files.createDirectories(imageDirectory)
        }

        logger.info("In process AndSaveImageFile()")

        val originalFileName = file.originalFilename
        val dotIndex = originalFileName?.lastIndexOf('.') ?: -1
        val extension = if (dotIndex > -1) originalFileName?.substring(dotIndex) else ""
        val fileName = UUID.randomUUID().toString() + extension // make file name random
        val filePath = imageDirectory.resolve(fileName)

        try {
            // Save the file to the directory
            file.transferTo(filePath)

            // Return the path that can be used to access the image
            return "$BASE_URL/recipes$IMAGE_DIRECTORY/$fileName"
        } catch (e: IllegalStateException) {
            logger.error("Could not store image file ${file.originalFilename}", e)
            throw RuntimeException("Could not store image file: ${e.message}")
        } catch (e: IOException) {
            logger.error("Could not copy image file ${file.originalFilename}", e)
            throw RuntimeException("Could not copy image file: ${e.message}")
        }
    }

    /**
     * Gets the images byte array.
     *
     * @throws [java.io.IOException] if it cannot read a byte
     * @return the image as a byte array.
     */
    fun getImageByteArray(fileName: String): ByteArray {
        return Files.readAllBytes(Paths.get("/$IMAGE_DIRECTORY/$fileName"))
    }

    private fun createRecipeFromTemplate(recipeToCreate: RecipeTemplate, imageUrl: String): Recipe {

        val creator = userRepository.findById(recipeToCreate.creatorId)
            .orElseThrow { IdNotFoundException("user", recipeToCreate.creatorId) }

        val createdRecipe = Recipe(
            name = recipeToCreate.name,
            passiveCookingTime = recipeToCreate.passiveCookingTime,
            activeCookingTime = recipeToCreate.activeCookingTime,
            creator = creator,
            imageUrl = imageUrl, // Assign the URL obtained after saving the image
            postDate = recipeToCreate.postDate
        )

        createdRecipe.recipeComponents.addAll(parseRecipeComponents(recipeToCreate.components, createdRecipe))
        createdRecipe.preparationSteps.addAll(parsePreparationSteps(recipeToCreate.preparationSteps, createdRecipe))
        createdRecipe.preferences.addAll(parsePreferences(recipeToCreate.preferences))

        return createdRecipe
    }

    private fun parseRecipeComponents(
        recipeComponents: List<RecipeComponentTemplate>,
        recipe: Recipe
    ): MutableSet<RecipeComponent> {
        val list: MutableList<RecipeComponent> = mutableListOf()
        recipeComponents.forEach {
            val ingredient = ingredientService.getIngredientByName(it.ingredient.name)
            val measure = measureService.getMeasureById(it.measure.id)

            val recipeComponent = RecipeComponent(
                recipe = recipe,
                ingredient = ingredient,
                measure = measure,
                quantity = it.quantity,
            )
            list.add(recipeComponent)
        }
        return list.toMutableSet()
    }

    private fun parsePreparationSteps(
        preparationSteps: List<PreparationStepTemplate>,
        recipe: Recipe
    ): MutableSet<PreparationStep> {
        val list: MutableList<PreparationStep> = mutableListOf()
        preparationSteps.forEach {
            list.add(PreparationStep(description = it.description, recipe = recipe))
        }
        return list.toMutableSet()
    }

    private fun parsePreferences(
        preferences: List<PreferenceTemplate>,
    ): MutableList<Preference> {
        val list: MutableList<Preference> = mutableListOf()
        preferences.forEach {
            val preference = preferenceService.getPreferenceById(it.id)
            list.add(preference)
        }
        return list
    }

    /**
     * Delete a recipe from the database.
     * @param id The id of the recipe to delete.
     * @throws [IdNotFoundException] if the recipe does not exist
     * @return the deleted recipe.
     */
    fun deleteRecipe(id: Long): Recipe {
        val recipe = getRecipeById(id)

        recipeRepository.deleteById(id)
        return recipe
    }

    /**
     * Change an existing Recipe
     * @param recipeToAlter The new Version of the recipe.
     * @throws [IdNotFoundException] if the recipe does not exist
     * @return the altered recipe.
     * @throws [IdNotFoundException] if the recipe does not exist
     * @return the altered recipe.
     */
    fun alterRecipe(recipeToAlter: RecipeTemplate, file: MultipartFile): Recipe {
        val imageUrl = processAndSaveImageFile(file)

        val oldRecipe = getRecipeById(recipeToAlter.id)

        oldRecipe.recipeComponents.clear()
        oldRecipe.preparationSteps.clear()
        oldRecipe.preferences.clear()

        oldRecipe.name = recipeToAlter.name
        oldRecipe.recipeComponents.addAll(parseRecipeComponents(recipeToAlter.components, oldRecipe))
        oldRecipe.preparationSteps.addAll(parsePreparationSteps(recipeToAlter.preparationSteps, oldRecipe))
        oldRecipe.activeCookingTime = recipeToAlter.activeCookingTime
        oldRecipe.passiveCookingTime = recipeToAlter.passiveCookingTime
        oldRecipe.preferences.addAll(parsePreferences(recipeToAlter.preferences))
        oldRecipe.imageUrl = imageUrl

        return recipeRepository.save(oldRecipe)
    }

    /**
     * Retrieves all recipes created by a specific user.
     *
     * @param user The user whose recipes are to be fetched.
     * @return List of recipes created by the user.
     */
    fun findByCreator(user: AppUser): List<Recipe> {
        logger.info("Fetching all recipes created by user with ID: ${user.id}")
        // Assuming that there's a field in Recipe that links to the creator (user)
        return recipeRepository.findByCreator(user)
    }
}
