package edu.kit.backend.api.service

import edu.kit.backend.api.repository.GroupRepository
import edu.kit.backend.api.templates.GroupTemplate
import edu.kit.backend.data.model.*
import edu.kit.backend.exceptions.CodeNotFoundException
import edu.kit.backend.exceptions.GroupEmptyException
import edu.kit.backend.exceptions.IdNotFoundException
import edu.kit.backend.exceptions.InsufficientRightsException
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import kotlin.random.Random

private const val TYPE: String = "group"

@Service
class GroupService {

    private val logger = LoggerFactory.getLogger(GroupService::class.java)

    @Autowired
    lateinit var groupRepository: GroupRepository

    @Autowired
    lateinit var userService: UserService

    @Autowired
    lateinit var recipeService: RecipeService

    @Autowired
    lateinit var filterService: FilterService

    @Autowired
    lateinit var preferenceService: PreferenceService

    @Autowired
    lateinit var ingredientService: IngredientService

    /**
     * Gets an existing [Group] by its [Group.id].
     *
     * @param id is the [Group.id].
     * @throws [IdNotFoundException] if the given [id] is not used by any [Group] in the database.
     * @return the [Group] with matching [Group.id].
     */
    fun getGroupById(id: Long): Group {
        return groupRepository.findById(id).orElseThrow { IdNotFoundException(TYPE, id) }
    }

    /**
     * Method to get all Groups from the database.
     * Currently only used for http inference by admins.
     */
    fun getAllGroups(): List<Group> {
        return groupRepository.findAll()
    }

    /**
     * Creates a new [Group] from the given [groupTemplate].
     *
     * @param groupTemplate is the [GroupTemplate] that includes what the new [Group] is initialized with.
     * @throws IdNotFoundException if the database has no [AppUser] with the id [GroupTemplate.adminId].
     */
    fun createGroup(groupTemplate: GroupTemplate): Group {
        val admin: AppUser = userService.getUser(groupTemplate.adminId)
        val groupCode = generateGroupCode()

        val group = Group(
            admin = admin,
            name = groupTemplate.name,
            members = mutableSetOf(admin),
            groupCode = groupCode
        )

        logger.info("groupCode: ${group.groupCode}, name: ${group.name}, admin.username: ${admin.username} ")

        return groupRepository.save(group)
    }

    /**
     * Generates a Group Code with which users can Join the Group
     * Checks for duplicates before returning
     * @return groupCode
     */
    private fun generateGroupCode(): String {

        var groupCode = ""

        do {
            val builder = StringBuilder()
            for (x in 0..<5) {
                val y = Random.nextInt(65, 90)
                builder.append(y.toChar())
                groupCode = builder.toString()
            }
        } while (groupRepository.findByGroupCode(groupCode) != null)

        return groupCode
    }

    /**
     * Deletes the [Group] with the given [code].
     *
     * @param code is the of the [Group] which is deleted.
     * @param userId is the [AppUser.id] of the [AppUser] trying to edit the [Group].
     * @throws [CodeNotFoundException] if no [Group] with that [code] exists in the database.
     * @throws [IdNotFoundException] if no [AppUser] with that [userId] exists in the database.
     * @throws [InsufficientRightsException] if the [AppUser] does not have the rights to delete the [Group].
     * @return the [Group] that was deleted.
     */
    fun deleteGroup(code: String, userId: Long): Group {
        val group = groupRepository.findByGroupCode(code) ?: throw CodeNotFoundException(TYPE, code)
        val user = userService.getUser(userId)

        if (!hasSufficientRights(user, group)) throw InsufficientRightsException(user.id, group.name)

        groupRepository.delete(group)

        return group
    }

    /**
     * Adds the [AppUser] (defined by the [userId]) to the [Group] (defined by the [groupCode]).
     * .
     * @param groupCode is the [Group.groupCode] of the [Group].
     * @param userId is the [AppUser.id] of the [AppUser] who joins the [Group].
     * @throws [CodeNotFoundException] if the given [groupCode] is not used by any [Group] in the database.
     * @throws [IdNotFoundException] if the given [userId] is not used by any [AppUser] in the database.
     * @return the [Group] that the user joined.
     */
    fun joinGroup(groupCode: String, userId: Long): Group {
        val group = groupRepository.findByGroupCode(groupCode) ?: throw CodeNotFoundException(TYPE, groupCode)
        val user = userService.getUser(userId)

        group.members.add(user)
        logger.info("user: ${user.username} joins name: ${group.name} with groupCode ${group.groupCode}")

        return groupRepository.save(group)
    }

    /**
     * Removes the [AppUser] (defined by the [userId]) from the [Group] (defined by the [groupCode]).
     *
     * @param groupCode is the [Group.groupCode] of the [Group].
     * @param userId is the [AppUser.id] of the [AppUser] who leaves the [Group].
     * @throws [CodeNotFoundException] if the given [groupCode] is not used by any [Group] in the database.
     * @throws [IdNotFoundException] if the given [userId] is not used by any [AppUser] in the database.
     * @throws [GroupEmptyException] if the [Group.members] is empty after leaving.
     * @return the [Group] that the user left.
     */
    fun leaveGroup(groupCode: String, userId: Long): Group {
        val group = groupRepository.findByGroupCode(groupCode) ?: throw CodeNotFoundException(TYPE, groupCode)
        val user = userService.getUser(userId)

        group.members.remove(user)
        logger.info("user: ${user.username} leaves group ${group.name} with groupCode ${group.groupCode}")

        removeIfMembersEmpty(group)

        return groupRepository.save(group)
    }

    /**
     * Adds [Recipe]s to a [Group]
     *
     * @param groupId is the id that identifies the [Group].
     * @param recipes
     * @throws [IdNotFoundException] if the given [groupId] is not used by any [Group] in the database.
     * @return the [Group] with the now added [Recipe]s.
     */
    fun addRecipesToGroup(groupId: Long, recipes: List<Recipe>): Group {
        val group = getGroupById(groupId)

        group.recipeStack.addAll(recipes)
        logger.info("Recipes got added to group.name: ${group.name}")

        return groupRepository.save(group)
    }

    /**
     * Calculates the [Recipe]s for a [Group] using the [Preference]s, aka preferences, of the [Group].
     *
     * @param groupId is the id that identifies the [Group].
     * @throws IdNotFoundException when the database does not contain a [Group] with id [groupId].
     * @return the [Group] with the now added [Recipe]s.
     */
    // comparable with getRecipesByLabels() in GroupService but uses the more efficient direct database queries to fetch the fitting recipes
    fun fillGroupWithRecipes(groupId: Long): Group {
        val group = getGroupById(groupId)

        val filteredRecipes = filterService.getFilteredRecipes(
            preferences = group.preferences,
            blacklist = group.blacklist,
            supersetWhitelist = group.supersetWhitelist,
            subsetWhitelist = group.subsetWhitelist
        )

        group.recipeStack.addAll(filteredRecipes)
        return groupRepository.save(group)
    }

    /**
     * Resets the [Group.recipeStack].
     *
     * @param groupId is the [Group.id].
     * @param userId is the [AppUser.id] of the [AppUser] trying to clear the [Group.recipeStack].
     * @throws [CodeNotFoundException] if the [groupId] or the [userId] is not present in the database.
     * @throws [IdNotFoundException] if no [AppUser] with that [userId] exists in the database.
     * @throws [InsufficientRightsException] if the [AppUser] is not allowed to clear the [Recipe]s of the [Group].
     * @return the edited [Group] that now has an empty [Group.recipeStack].
     */
    fun clearRecipes(groupId: Long, userId: Long): Group {
        val group = getGroupById(groupId)
        val user = userService.getUser(userId)

        if (!hasSufficientRights(user, group)) throw InsufficientRightsException(user.id, group.name)

        group.recipeStack.clear()
        logger.info("Recipes got removed from group.name: ${group.name}")

        return groupRepository.save(group)
    }

    /**
     * Edits an existing [Group] to fit the [groupTemplate].
     *
     * @param groupTemplate is the [GroupTemplate] that defines how the [Group] should come out.
     * @throws CodeNotFoundException if the [groupTemplate] has a [Group.groupCode] that is not in the database.
     * @throws IdNotFoundException if either [GroupTemplate.recipes] or [GroupTemplate.members]
     *         contains an element that is not defined in the database.
     * @throws [GroupEmptyException] if the [Group.members] is empty after editing.
     * @return the [Group] that was edited in its new form.
     */
    fun editGroup(groupTemplate: GroupTemplate): Group {
        logger.info("groupTemplate: $groupTemplate")
        val group = getGroupById(groupTemplate.id)

        val members = mutableSetOf<AppUser>()
        groupTemplate.members.forEach {
            members.add(userService.getUser(it.id))
        }

        val preferences = mutableSetOf<Preference>()
        groupTemplate.preferences.forEach {
            preferences.add(preferenceService.getPreferenceById(it.id))
        }

        val blacklist = mutableSetOf<Ingredient>()
        groupTemplate.blacklist.forEach {
            blacklist.add(ingredientService.getIngredientById(it.id))
        }

        val supersetWhitelist = mutableSetOf<Ingredient>()
        groupTemplate.supersetWhitelist.forEach {
            supersetWhitelist.add(ingredientService.getIngredientById(it.id))
        }

        val subsetWhitelist = mutableSetOf<Ingredient>()
        groupTemplate.subsetWhitelist.forEach {
            subsetWhitelist.add(ingredientService.getIngredientById(it.id))
        }

        var recipes = group.recipeStack
        groupTemplate.recipes.forEach {
            recipes.add(recipeService.getRecipeById(it.id))
        }
        // adjust old recipes to new preferences
        recipes = recipes.intersect(
            filterService.filterRecipesByPreferencesAndIngredients(
                recipes,
                preferences,
                blacklist,
                supersetWhitelist,
                subsetWhitelist
            )
        ).toMutableSet()
        if (recipes.isEmpty()) recipes.addAll(
            filterService.getFilteredRecipes(
                preferences,
                blacklist,
                supersetWhitelist,
                subsetWhitelist
            )
        )

        val newGroup = Group(
            group.id,
            group.admin,
            groupTemplate.name.ifBlank {
                "name" // a default group name, so that if somehow the name is empty, it's not
            },
            groupTemplate.groupCode,
            members,
            recipes,
            preferences,
            blacklist,
            supersetWhitelist,
            subsetWhitelist,
        )

        removeIfMembersEmpty(newGroup)

        return groupRepository.save(newGroup)
    }

    private fun hasSufficientRights(user: AppUser, group: Group): Boolean {
        return user.id == group.admin.id || user.role == Role.ADMIN
    }

    /**
     * Removes the [group] from the [groupRepository], when the [Group.members] is empty.
     *
     * @param group is the [Group] which is looked at.
     * @throws GroupEmptyException after the [group] is deleted.
     */
    private fun removeIfMembersEmpty(group: Group) {
        if (group.members.isEmpty()) {
            groupRepository.delete(group)
            throw GroupEmptyException(group)
        }
    }

    fun saveMatch(groupId: Long, matchId: Long): Recipe {
        val match = recipeService.getRecipeById(matchId)
        val group = getGroupById(groupId)
        group.match = match
        groupRepository.save(group)
        return match

    }
}
