package edu.kit.backend.api.repository

import edu.kit.backend.data.model.Measure
import org.springframework.data.jpa.repository.JpaRepository

/**
 * Repository interface for [Measure] entities.
 * Inherits standard CRUD operations from [JpaRepository].
 */
interface MeasureRepository : JpaRepository<Measure, Long> {
    // JpaRepository provides methods like findAll() and findById() and many more by default.
    fun findByName(name: String): Measure?
    fun existsByName(name: String): Boolean
}
