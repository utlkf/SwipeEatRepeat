package edu.kit.backend.api.controller

import edu.kit.backend.api.service.PreferenceService
import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.api.templates.converters.PreferenceConverter
import edu.kit.backend.data.model.Preference
import edu.kit.backend.exceptions.IdNotFoundException
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/preferences")
class PreferenceController(private val preferenceService: PreferenceService) {

    private val preferenceConverter = PreferenceConverter()

    /**
     * Creates a food label.
     * Only accessible for admins.
     * @param preference The food label to be created.
     * @return The created food label.
     */
    @PostMapping
    fun createFoodLabel(@RequestBody preference: Preference): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                preferenceConverter.convert(
                    preferenceService.createFoodPreference(preference)
                )
            )
        } catch (e: RuntimeException) {
            return ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Alters an existing food label.
     * Only accessible for admins.
     * @param id The id of the food label to be altered.
     * @param updatedPreference The updated food label.
     * @return The altered food label.
     */
    @PutMapping("/{id}")
    fun alterFoodLabel(@PathVariable id: Long, @RequestBody updatedPreference: Preference): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                preferenceConverter.convert(
                    preferenceService.alterPreference(id, updatedPreference)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    /**
     * Deletes a food label.
     * Only accessible for admins.
     * @param id The id of the food label to be deleted.
     * @return The deleted food label.
     */
    @DeleteMapping("/{id}")
    fun deleteFoodLabel(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            return ResponseEntity.ok(
                preferenceConverter.convert(
                    preferenceService.deletePreference(id)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }

    @GetMapping
    fun getAllFoodLabels(): ResponseEntity<List<PreferenceTemplate>> {
        return ResponseEntity.ok(
            preferenceConverter.convert(
                preferenceService.getAllPreferences()
            )
        )
    }

    /**
     * Gets a FoodLabel by its id.
     * @param id The id of the FoodLabel.
     * @return A FoodLabelTemplate of the FoodLabel.
     */
    @GetMapping("/{id}")
    fun getFoodLabelById(@PathVariable id: Long): ResponseEntity<Any> {
        return try {
            ResponseEntity.ok(
                preferenceConverter.convert(
                    preferenceService.getPreferenceById(id)
                )
            )
        } catch (e: IdNotFoundException) {
            ResponseEntity.badRequest().body(e.message)
        }
    }
}
