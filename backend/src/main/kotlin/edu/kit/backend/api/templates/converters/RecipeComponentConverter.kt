package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.RecipeComponentTemplate
import edu.kit.backend.data.model.RecipeComponent

class RecipeComponentConverter : Converter<RecipeComponentTemplate, RecipeComponent>() {

    private val ingredientConverter = IngredientConverter()
    private val measureConverter = MeasureConverter()

    override fun convert(from: RecipeComponent): RecipeComponentTemplate {
        return RecipeComponentTemplate(
            from.id,
            ingredientConverter.convert(from.ingredient),
            measureConverter.convert(from.measure),
            from.quantity
        )
    }
}
