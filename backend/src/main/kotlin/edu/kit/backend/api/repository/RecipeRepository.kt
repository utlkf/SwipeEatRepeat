package edu.kit.backend.api.repository

import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.data.model.Preference
import edu.kit.backend.data.model.Recipe
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param

/**
 * Repository interface for [Recipe] entities.
 * Inherits standard CRUD operations from [JpaRepository].
 */
interface RecipeRepository : JpaRepository<Recipe, Long> {

    /**
     * Finds all recipes that have any of the given preferences.
     * @param preferences The preferences to be matched.
     * @return A list of recipes that have any of the given preferences.
     */
    @Query("SELECT r FROM Recipe r JOIN r.preferences l WHERE l IN :preferences")

    fun findByAnyPreference(@Param("preferences") preferences: Collection<Preference>): List<Recipe>

    /**
     * Finds all recipes that have all the given preferences.
     * @param preferences The preferences to be matched.
     * @param preferenceCount The number of preferences to be matched (recipe must be associated with as many preferences as the size of the preferences list).
     * @return A list of recipes that have all the given preferences.
     */
    @Query("SELECT r FROM Recipe r JOIN r.preferences l WHERE l IN :preferences GROUP BY r HAVING COUNT(DISTINCT l) = :preferenceCount")
    fun findByAllPreferences(
        @Param("preferences") preferences: Collection<Preference>,
        @Param("preferenceCount") preferenceCount: Int
    ): List<Recipe>

    /**
     * Finds all recipes that contain all the given ingredients.
     * @param ingredients The ingredients to be matched.
     * @return A list of recipes that have all the given ingredients.
     */
    @Query("SELECT r FROM Recipe r JOIN r.recipeComponents rc WHERE rc.ingredient IN :ingredients GROUP BY r.id HAVING COUNT(DISTINCT rc.ingredient) = (SELECT COUNT(i) FROM Ingredient i WHERE i IN :ingredients)")
    fun findAllRecipesContainingAllIngredients(@Param("ingredients") ingredients: Set<Ingredient>): Set<Recipe>

    @Query("SELECT r FROM Recipe r JOIN r.recipeComponents rc WHERE rc.ingredient NOT IN :ingredients")
    fun findRecipesNotContainingIngredients(@Param("ingredients") ingredients: Set<Ingredient>): Set<Recipe>

    @Query("SELECT r FROM Recipe r JOIN r.recipeComponents rc WHERE rc.ingredient IN :ingredients GROUP BY r.id HAVING COUNT(DISTINCT rc.ingredient) <= (SELECT COUNT(ri) FROM RecipeComponent ri JOIN ri.recipe rr JOIN ri.ingredient i WHERE rr = r AND i IN :ingredients)")
    fun findRecipesWithIngredientAsSubsetOrMatch(@Param("ingredients") ingredients: Set<Ingredient>): Set<Recipe>


    /**
     * Finds all recipes created by the specified user.
     *
     * @param creator The user whose recipes are to be found.
     * @return List of recipes created by the user.
     */
    fun findByCreator(creator: AppUser): List<Recipe>
}
