package edu.kit.backend.api.service

import edu.kit.backend.api.repository.IngredientRepository
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.exceptions.IdNotFoundException
import edu.kit.backend.exceptions.NameNotFoundException
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

private const val TYPE: String = "ingredient"

@Service
class IngredientService {

    @Autowired
    private lateinit var ingredientRepository: IngredientRepository

    /**
     * Gets the [Ingredient] matching the given [id].
     *
     * @param id is the id of the [Ingredient].
     * @throws IdNotFoundException if the [ingredientRepository] does not contain an [Ingredient] with that [Ingredient.id]
     * @return the [Ingredient] that was found.
     */
    fun getIngredientById(id: Long): Ingredient {
        return ingredientRepository.findById(id).orElseThrow { IdNotFoundException(TYPE, id) }
    }

    fun getIngredientByName(name: String): Ingredient {
        return ingredientRepository.findByName(name) ?: ingredientRepository.save(Ingredient(name = name))
    }

    /**
     * Gets all [Ingredient]s in the [ingredientRepository].
     *
     * @return a [List] of all [Ingredient]s.
     */
    fun getAllIngredients(): List<Ingredient> {
        return ingredientRepository.findAll()
    }

    /**
     * deletes the [Ingredient] with the given [name].
     * @param name is the name of the [Ingredient] to be deleted.
     * @return the deleted [Ingredient].
     */
    fun deleteIngredient(name: String): Ingredient {
        val ingredient = ingredientRepository.findByName(name) ?: throw NameNotFoundException(TYPE, name)
        ingredientRepository.delete(ingredient)
        return ingredient
    }
}
