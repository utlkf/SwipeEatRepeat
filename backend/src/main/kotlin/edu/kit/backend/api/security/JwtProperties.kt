package edu.kit.backend.api.security

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Reads in properties defined in the configuration file and inject values into the properties
 */
@ConfigurationProperties("jwt")
data class JwtProperties(
    val key: String,
    val accessTokenExpiration: Long,
    val refreshTokenExpiration: Long,
)
