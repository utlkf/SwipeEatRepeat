package edu.kit.backend.api.templates

data class UserDecisionTemplate(val recipeId: Long, val userId: Long) {
}
