package edu.kit.backend.api.templates

data class GroupTemplate(
    val adminId: Long,
    val name: String,
    val id: Long = 0,
    val groupCode: String = "400",
    val members: List<UserTemplate> = listOf(),
    val recipes: List<RecipeTemplate> = listOf(),
    val preferences: List<PreferenceTemplate> = listOf(),
    val blacklist: List<IngredientTemplate> = listOf(),
    val supersetWhitelist: List<IngredientTemplate> = listOf(),
    val subsetWhitelist: List<IngredientTemplate> = listOf(),
    val match: RecipeTemplate? = null,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is GroupTemplate) return false

        if (adminId != other.adminId) return false
        if (name != other.name) return false
        if (id != other.id) return false
        if (groupCode != other.groupCode) return false
        if (members != other.members) return false
        if (recipes != other.recipes) return false
        if (preferences != other.preferences) return false
        if (blacklist != other.blacklist) return false
        if (supersetWhitelist != other.supersetWhitelist) return false
        if (subsetWhitelist != other.subsetWhitelist) return false
        if (match != other.match) return false

        return true
    }

    override fun hashCode(): Int {
        var result = adminId.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + id.hashCode()
        result = 31 * result + groupCode.hashCode()
        result = 31 * result + members.hashCode()
        result = 31 * result + recipes.hashCode()
        result = 31 * result + preferences.hashCode()
        result = 31 * result + blacklist.hashCode()
        result = 31 * result + supersetWhitelist.hashCode()
        result = 31 * result + subsetWhitelist.hashCode()
        result = 31 * result + match.hashCode()
        return result
    }
}
