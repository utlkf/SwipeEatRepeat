package edu.kit.backend.api.event

class AcceptedEvent(userId: Long, groupId: Long, recipeId: Long) : DecisionEvent(userId, groupId, recipeId) {
}
