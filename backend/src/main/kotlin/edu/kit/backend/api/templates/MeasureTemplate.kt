package edu.kit.backend.api.templates

data class MeasureTemplate(
    val id: Long,
    val name: String,
    val unit: String
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is MeasureTemplate) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (unit != other.unit) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + unit.hashCode()
        return result
    }
}
