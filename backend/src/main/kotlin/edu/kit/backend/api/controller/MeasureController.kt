package edu.kit.backend.api.controller

import edu.kit.backend.api.service.MeasureService
import edu.kit.backend.api.templates.MeasureTemplate
import edu.kit.backend.api.templates.converters.MeasureConverter
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/measures")
class MeasureController {

    @Autowired
    private lateinit var measureService: MeasureService

    private val measureConverter = MeasureConverter()

    @GetMapping
    fun getAllMeasures(): ResponseEntity<List<MeasureTemplate>> {
        return ResponseEntity.ok(
            measureConverter.convert(
                measureService.getAllMeasures()
            )
        )
    }
}
