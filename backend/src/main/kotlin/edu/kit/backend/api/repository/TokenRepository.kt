package edu.kit.backend.api.repository

import edu.kit.backend.api.security.Token
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface TokenRepository : JpaRepository<Token, Long> {

    /**
     * Finds all valid tokens for a given user.
     * @param userId The id of the user to find the tokens for.
     * @return A list of all valid tokens for the given user.
     */
    @Query("SELECT t FROM Token t inner join AppUser u on t.user.id=u.id where u.id=:userId and t.expired=false and t.revoked=false")
    fun findAllValidTokensByUser(userId: Long): List<Token>

    fun findByToken(token: String): Token?
}
