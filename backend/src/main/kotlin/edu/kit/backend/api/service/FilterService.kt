package edu.kit.backend.api.service

import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.data.model.Preference
import edu.kit.backend.data.model.Recipe
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class FilterService {

    @Autowired
    private lateinit var recipeService: RecipeService

    /**
     * Filters the recipes by the given preferences, blacklist, subsetWhitelist and supersetWhitelist.
     *
     * @param preferences The preferences to getFilteredRecipes by.
     * @param blacklist The blacklist to getFilteredRecipes by.
     * @param subsetWhitelist The subset whitelist to getFilteredRecipes by.
     * @param supersetWhitelist The superset whitelist to getFilteredRecipes by.
     * @return The filtered recipes.
     */
    fun getFilteredRecipes(
        preferences: Collection<Preference>,
        blacklist: Set<Ingredient>,
        supersetWhitelist: Set<Ingredient>?,
        subsetWhitelist: Set<Ingredient>?,
    ): Set<Recipe> {
        return filterRecipesByPreferencesAndIngredients(
            recipeService.getAllRecipes(),
            preferences,
            blacklist,
            supersetWhitelist,
            subsetWhitelist
        )
    }

    /**
     * Filters out [Recipe]s according to the given [Preference]s.
     * @param recipes The recipes to getFilteredRecipes.
     * @param preferences The preferences to getFilteredRecipes by.
     * @param blacklist The blacklist to getFilteredRecipes by.
     * @param supersetWhitelist The superset whitelist to getFilteredRecipes by.
     * @param subsetWhitelist The subset whitelist to getFilteredRecipes by.
     * @return The filtered recipes.
     */
    fun filterRecipesByPreferencesAndIngredients(
        recipes: Collection<Recipe>,
        preferences: Collection<Preference>,
        blacklist: Set<Ingredient>,
        supersetWhitelist: Set<Ingredient>?,
        subsetWhitelist: Set<Ingredient>?,
    ): Set<Recipe> {
        var remainingRecipes: Set<Recipe> = filterRecipesByPreferences(preferences, recipes)

        remainingRecipes = filterRecipesByBlacklist(blacklist, remainingRecipes)

        if (!supersetWhitelist.isNullOrEmpty()) {
            remainingRecipes = filterRecipesBySupersetWhitelist(supersetWhitelist, remainingRecipes)
        }

        if (!subsetWhitelist.isNullOrEmpty()) {
            remainingRecipes = filterRecipesBySubsetWhitelist(subsetWhitelist, remainingRecipes)
        }

        return remainingRecipes
    }


    /**
     * Filters out [Recipe]s that contain any [Ingredient] in [blacklist].
     */
    private fun filterRecipesByBlacklist(blacklist: Set<Ingredient>, recipes: Set<Recipe>): Set<Recipe> {
        return recipes.filter { recipe -> recipe.recipeComponents.none { blacklist.contains(it.ingredient) } }.toSet()
    }

    /**
     * Filters out [Recipe]s that don't contain a subset of [Ingredient]s in [supersetWhitelist].
     */
    private fun filterRecipesBySupersetWhitelist(
        supersetWhitelist: Set<Ingredient>,
        recipes: Set<Recipe>
    ): Set<Recipe> {
        return recipes.filter { recipe -> recipe.recipeComponents.all { supersetWhitelist.contains(it.ingredient) } }
            .toSet()
    }

    private fun filterRecipesBySubsetWhitelist(
        subsetWhitelist: Set<Ingredient>,
        recipes: Set<Recipe>
    ): Set<Recipe> {
        return recipes.filter { recipe -> recipe.recipeComponents.map { it.ingredient }.containsAll(subsetWhitelist) }
            .toSet()
    }


    /**
     * Filters out [Recipe]s according to the given [Preference]s.
     * @param preferences The preferences to getFilteredRecipes by.
     * @param recipes The recipes to getFilteredRecipes.
     * @return The filtered recipes.
     */
    private fun filterRecipesByPreferences(
        preferences: Collection<Preference>,
        recipes: Collection<Recipe>
    ): Set<Recipe> {
        var remainingRecipes = recipes.toSet()

        val (strictPreferences, nonStrictPreferences) = preferences.partition { it.isStrict }

        if (strictPreferences.isNotEmpty()) {
            remainingRecipes = filterStrict(strictPreferences, remainingRecipes).toSet()
        }

        if (nonStrictPreferences.isNotEmpty()) {
            remainingRecipes = filterNotStrict(nonStrictPreferences, remainingRecipes).toSet()
        }
        return remainingRecipes
    }

    /**
     * Filters out [Recipe]s that don't contain all [Preference]s in [strictPreferences].
     */
    private fun filterStrict(strictPreferences: List<Preference>, recipes: Collection<Recipe>): List<Recipe> {
        val matchingRecipes = mutableListOf<Recipe>()
        for (recipe in recipes) {
            if (recipe.preferences.containsAll(strictPreferences)) {
                matchingRecipes.add(recipe)
            }
        }
        return matchingRecipes
    }

    /**
     * Filters out [Recipe]s that don't contain any [Preference] from [nonStrictPreferences].
     */
    private fun filterNotStrict(nonStrictPreferences: List<Preference>, recipes: Collection<Recipe>): List<Recipe> {
        val matchingRecipes = mutableListOf<Recipe>()
        for (recipe in recipes) {
            if (nonStrictPreferences.intersect(recipe.preferences).isNotEmpty()) {
                matchingRecipes.add(recipe)
            }
        }
        return matchingRecipes
    }
}
