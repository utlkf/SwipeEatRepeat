package edu.kit.backend.api.templates

import java.time.LocalDateTime

/**
 * Represents a recipe which is created in the frontend.
 * Each recipe has a name, cooking times, postdate, and is associated with a creator.
 * It also has relationships with ingredients and preparation steps.
 * In future there should also be the possibility to add a URL to a recipe
 *
 * This is used to create a Recipe Entity in the database by parsing these values.
 */
data class RecipeTemplate(
    val id: Long,
    val name: String,
    val passiveCookingTime: Int,
    val activeCookingTime: Int,
    val creatorId: Long,
    val postDate: LocalDateTime,
    val components: List<RecipeComponentTemplate>,
    val preparationSteps: List<PreparationStepTemplate>,
    val preferences: List<PreferenceTemplate>,
    val imageUrl: String,
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is RecipeTemplate) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (passiveCookingTime != other.passiveCookingTime) return false
        if (activeCookingTime != other.activeCookingTime) return false
        if (creatorId != other.creatorId) return false
        if (postDate != other.postDate) return false
        if (components != other.components) return false
        if (preparationSteps != other.preparationSteps) return false
        if (preferences != other.preferences) return false
        if (imageUrl != other.imageUrl) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + name.hashCode()
        result = 31 * result + passiveCookingTime
        result = 31 * result + activeCookingTime
        result = 31 * result + creatorId.hashCode()
        result = 31 * result + postDate.hashCode()
        result = 31 * result + components.hashCode()
        result = 31 * result + preparationSteps.hashCode()
        result = 31 * result + preferences.hashCode()
        result = 31 * result + imageUrl.hashCode()
        return result
    }
}
