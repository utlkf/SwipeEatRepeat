package edu.kit.backend

import edu.kit.backend.api.repository.IngredientRepository
import edu.kit.backend.api.repository.MeasureRepository
import edu.kit.backend.api.repository.PreferenceRepository
import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.api.service.AuthenticationService
import edu.kit.backend.api.templates.RegistrationCredentials
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.data.model.Measure
import edu.kit.backend.data.model.Preference
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class BackendApplication {
    @Autowired
    private lateinit var initializationProperties: InitializationProperties

    @Autowired
    private lateinit var userRepository: UserRepository

    @Autowired
    private lateinit var measureRepository: MeasureRepository

    @Autowired
    private lateinit var ingredientRepository: IngredientRepository

    @Autowired
    private lateinit var preferenceRepository: PreferenceRepository

    private val logger = LoggerFactory.getLogger(BackendApplication::class.java)

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            runApplication<BackendApplication>(*args)
        }
    }

    /**
     * Executed on application startup.
     * Adds all measures and food labels to the database and creates a default admin user if not already present.
     * @param authenticationService the authentication service for creating the default admin user
     * @return the application runner
     *
     * @author Arne
     */
    @Bean
    @Profile("!test") // This bean will not be active in the "test" profile since it caused problems when the app was booted for tests
    fun applicationRunner(authenticationService: AuthenticationService): ApplicationRunner {
        return ApplicationRunner {
            // add all measures and food labels to the database
            for (measure in Measure.getAllMeasures()) {
                if (!measureRepository.existsByName(measure.name)) {
                    logger.info("creation of new measure ${measure.name}")
                    measureRepository.save(measure)
                }
            }
            for (preference in Preference.getAllFoodLabels()) {
                if (!preferenceRepository.existsByName(preference.name)) {
                    logger.info("creation of new preference ${preference.name}")
                    preferenceRepository.save(preference)
                }
            }
            for (ingredient in Ingredient.getAllIngredients()) {
                if (!ingredientRepository.existsByName(ingredient.name)) {
                    logger.info("creation of new ingredient ${ingredient.name}")
                    ingredientRepository.save(ingredient)
                }
            }
            // add a default admin user if not already present
            if (userRepository.findByUsername(initializationProperties.username) == null) {
                logger.info("creation of new admin")
                val registrationCredentials =
                    RegistrationCredentials(initializationProperties.username, initializationProperties.password, "")
                authenticationService.registerAdmin(registrationCredentials).accessToken
            }
        }
    }
}
