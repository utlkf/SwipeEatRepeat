package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import edu.kit.backend.api.security.Token
import jakarta.persistence.*
import org.springframework.security.core.GrantedAuthority

/**
 * A class representing a logged-in user, extending from the base User class.
 * Includes properties for username and password.
 */
@Entity
class AppUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    val username: String,

    @JsonIgnore
    val password: String,

    @JsonIgnore
    @OneToMany(mappedBy = "admin")
    val adminGroups: MutableSet<Group> = mutableSetOf(),

    @JsonIgnore
    @OneToMany(mappedBy = "creator")
    val recipesAuthored: MutableSet<Recipe> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinTable(
        name = "favourised",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "recipe_id", referencedColumnName = "id")]
    )
    val favouriteRecipes: MutableSet<Recipe> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinTable(
        name = "accepted",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "recipe_id", referencedColumnName = "id")]
    )
    val acceptedRecipes: MutableSet<Recipe> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinTable(
        name = "declined",
        joinColumns = [JoinColumn(name = "user_id", referencedColumnName = "id")],
        inverseJoinColumns = [JoinColumn(name = "recipe_id", referencedColumnName = "id")]
    )
    val declinedRecipes: MutableSet<Recipe> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(mappedBy = "members")
    val groups: MutableSet<Group> = mutableSetOf(),

    val email: String,

    @Enumerated(EnumType.STRING)
    val role: Role,

    @ManyToMany()
    @JoinTable(
        name = "user_preferences",
        joinColumns = [JoinColumn(name = "app_user_id")],
        inverseJoinColumns = [JoinColumn(name = "food_label_id")]
    )
    val preferences: MutableList<Preference> = mutableListOf(),

    @ManyToMany()
    @JoinTable(
        name = "user_ingredient_blacklist",
        joinColumns = [JoinColumn(name = "app_user_id")],
        inverseJoinColumns = [JoinColumn(name = "ingredient_id")]
    )
    val blacklist: MutableSet<Ingredient> = mutableSetOf(),   //choose set over list since hibernate can handle them more efficiently

    @ManyToMany()
    @JoinTable(
        name = "user_ingredient_supersetWhitelist",
        joinColumns = [JoinColumn(name = "app_user_id")],
        inverseJoinColumns = [JoinColumn(name = "ingredient_id")]
    )
    val supersetWhitelist: MutableSet<Ingredient> = mutableSetOf(),

    @ManyToMany()
    @JoinTable(
        name = "user_ingredient_subsetWhitelist",
        joinColumns = [JoinColumn(name = "app_user_id")],
        inverseJoinColumns = [JoinColumn(name = "ingredient_id")]
    )
    val subsetWhitelist: MutableSet<Ingredient> = mutableSetOf(),

    /**
     * A list of access tokens for the user.
     * A user can have multiple access tokens.
     * A refresh token is used to generate new access tokens and not saved in the backend.
     */
    @OneToMany(mappedBy = "user", cascade = [CascadeType.ALL], orphanRemoval = true)
    private val accessTokens: MutableList<Token> = mutableListOf(),

    ) {
    fun getAuthorities(): GrantedAuthority {
        return role.getAuthorities()
    }
}
