package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

/**
 * Represents a preparation step in a recipe.
 * Each step is associated with a recipe and contains a description of the step.
 */
@Entity
class PreparationStep(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @JsonIgnore
    var recipe: Recipe,

    val description: String,
)
