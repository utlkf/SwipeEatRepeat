package edu.kit.backend.data.model

import org.springframework.security.core.authority.SimpleGrantedAuthority

/**
 * Defines the roles a user can have.
 * This determines for example the access rights of a user.
 */
enum class Role {
    USER,
    ADMIN,
    // GUEST and MANAGER roles are not used in the current implementation but can be useful additions to the role model
    GUEST,
    MANAGER;

    fun getAuthorities(): SimpleGrantedAuthority {
        return SimpleGrantedAuthority("ROLE_$name")
    }
}
