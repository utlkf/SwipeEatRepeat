package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction

@Entity
class Ingredient(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    val name: String,

    @ManyToMany(mappedBy = "subsetWhitelist")
    // enforce referential integrity primarily at database level
    // if ingredient is deleted, all the references to it in the join table are deleted as well
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    var whitelistedAsSubsetBy: MutableSet<AppUser> = mutableSetOf(),

    @ManyToMany(mappedBy = "supersetWhitelist")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    var whitelistedAsSupersetBy: MutableSet<AppUser> = mutableSetOf(),

    @ManyToMany(mappedBy = "blacklist")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    var blacklistedBy: MutableSet<AppUser> = mutableSetOf(),
) {
    companion object {
        private val NONE = Ingredient(0, "")
        private val EGG_White = Ingredient(1, "egg-white")
        private val EGG_Yoke = Ingredient(2, "egg-yoke")
        private val FLOUR = Ingredient(3, "flour")
        private val MILK = Ingredient(4, "milk")
        private val RICE = Ingredient(5, "rice")

        fun getAllIngredients(): List<Ingredient> {
            return listOf(EGG_White, EGG_Yoke, FLOUR, MILK, RICE)
        }
    }
}
