package edu.kit.backend.data.model

import jakarta.persistence.*

/**
 * represents an abstract group class with attributes all groups have in common
 */
@Entity
@Table(name = "groups")
class Group(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @ManyToOne
    val admin: AppUser,

    var name: String,

    val groupCode: String,

    @ManyToMany
    val members: MutableSet<AppUser> = mutableSetOf(),  // subclasses must define set with subclass of User or User itself

    @ManyToMany
    val recipeStack: MutableSet<Recipe> = mutableSetOf(),

    @ManyToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinTable(
        name = "group_preferences",
        joinColumns = [JoinColumn(name = "group_id")],
        inverseJoinColumns = [JoinColumn(name = "food_label_id")]
    )
    val preferences: MutableSet<Preference> = mutableSetOf(),

    @ManyToMany()
    @JoinTable(
        name = "group_ingredient_blacklist",
        joinColumns = [JoinColumn(name = "group_id")],
        inverseJoinColumns = [JoinColumn(name = "ingredient_id")]
    )
    val blacklist: MutableSet<Ingredient> = mutableSetOf(),   //choose set over list since hibernate can handle them more efficiently

    @ManyToMany()
    @JoinTable(
        name = "group_ingredient_supersetWhitelist",
        joinColumns = [JoinColumn(name = "group_id")],
        inverseJoinColumns = [JoinColumn(name = "ingredient_id")]
    )
    val supersetWhitelist: MutableSet<Ingredient> = mutableSetOf(),

    @ManyToMany()
    @JoinTable(
        name = "group_ingredient_subsetWhitelist",
        joinColumns = [JoinColumn(name = "group_id")],
        inverseJoinColumns = [JoinColumn(name = "ingredient_id")]
    )
    val subsetWhitelist: MutableSet<Ingredient> = mutableSetOf(),

    // needs to be a list to keep the order in place
    @ManyToOne()
    var match: Recipe? = null,
)
