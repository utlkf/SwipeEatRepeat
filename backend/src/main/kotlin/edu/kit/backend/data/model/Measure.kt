package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

@Entity
class Measure(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val name: String,
    val unit: String,

    @OneToMany(mappedBy = "measure", cascade = [CascadeType.ALL])
    @JsonIgnore
    val recipeComponents: MutableList<RecipeComponent> = mutableListOf()
) {
    companion object {
        val NONE = Measure(0, "", "")
        private val GRAM = Measure(1, "gram", "g")
        private val MILLILITER = Measure(2, "milliliter", "ml")
        private val PIECE = Measure(3, "piece", "pc")

        // predefined measures
        fun getAllMeasures(): List<Measure> {
            return listOf(GRAM, MILLILITER, PIECE)
        }
    }
}
