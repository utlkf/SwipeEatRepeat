package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import java.time.LocalDateTime

/**
 * Represents a recipe entity in the database.
 * Each recipe has a unique ID, a name, cooking times, and is associated with a creator.
 * It also has relationships with ingredients and preparation steps.
 */
@Entity
class Recipe(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    var name: String,
    var passiveCookingTime: Int,
    var activeCookingTime: Int,

    @ManyToOne
    val creator: AppUser,

    val postDate: LocalDateTime = LocalDateTime.now(),

    //TODO: Is the default url still necessary or supposed to be removed?
    var imageUrl: String, //= "https://img.ccnull.de/1085000/preview/1085226_eb96577de6d119d2de69627709cfde64.jpg",

    @OneToMany(mappedBy = "recipe", cascade = [CascadeType.ALL], orphanRemoval = true)
    val recipeComponents: MutableSet<RecipeComponent> = mutableSetOf(),

    @OneToMany(mappedBy = "recipe", cascade = [CascadeType.ALL], orphanRemoval = true)
    val preparationSteps: MutableList<PreparationStep> = mutableListOf(),

    @ManyToMany
    @JoinTable(
        name = "recipe_label",
        joinColumns = [JoinColumn(name = "recipe_id")],
        inverseJoinColumns = [JoinColumn(name = "food_label_id")]
    )
    val preferences: MutableSet<Preference> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(mappedBy = "recipeStack", cascade = [CascadeType.ALL])
    val groups: MutableSet<Group> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(mappedBy = "favouriteRecipes", cascade = [CascadeType.ALL])
    val favourisedBy: MutableSet<AppUser> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(mappedBy = "acceptedRecipes", cascade = [CascadeType.ALL])
    val acceptedBy: MutableSet<AppUser> = mutableSetOf(),

    @JsonIgnore
    @ManyToMany(mappedBy = "declinedRecipes", cascade = [CascadeType.ALL])
    val declinedBy: MutableSet<AppUser> = mutableSetOf(),

    @JsonIgnore
    @OneToMany(mappedBy = "match")
    val matchedBy: MutableSet<Group> = mutableSetOf(),
)
