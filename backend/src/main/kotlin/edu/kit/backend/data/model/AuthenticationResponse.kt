package edu.kit.backend.data.model

/**
 * Response Entity containing an access token
 */
data class AuthenticationResponse(
    val accessToken: String,
    val refreshToken: String,
    val userId: Long,
)
