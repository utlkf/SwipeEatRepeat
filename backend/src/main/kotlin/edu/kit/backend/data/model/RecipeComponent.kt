package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*

/**
 * Represents a junction entity for the many-to-many relationship between [Recipe], [Ingredient] and [Measure].
 * It also holds the [quantity] of the [ingredient] of that [measure] in the [recipe].
 */
@Entity
class RecipeComponent(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @ManyToOne
    @JoinColumn(name = "recipe_id")
    @JsonIgnore
    var recipe: Recipe,

    @ManyToOne
    @JoinColumn(name = "ingredient_id")
    val ingredient: Ingredient,

    @ManyToOne
    @JoinColumn(name = "measure_id")
    val measure: Measure,

    val quantity: Int,
)
