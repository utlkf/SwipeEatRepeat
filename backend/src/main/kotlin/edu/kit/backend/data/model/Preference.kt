package edu.kit.backend.data.model

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*
import org.hibernate.annotations.OnDelete
import org.hibernate.annotations.OnDeleteAction

@Entity
class Preference(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    val name: String,

    val isStrict: Boolean,

    @ManyToMany(mappedBy = "preferences")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    var setByUsers: MutableSet<AppUser> = mutableSetOf(),

    @ManyToMany(mappedBy = "preferences")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    var setByGroups: MutableSet<Group> = mutableSetOf(),

    @ManyToMany(mappedBy = "preferences")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    var setForRecipes: MutableSet<Recipe> = mutableSetOf(),


    ) {
    /**
     * Companion object to define predefined food labels
     */
    companion object {
        private val VEGAN = Preference(1, "vegan", false)
        private val VEGETARIAN = Preference(2, "vegetarian", false)
        private val MEAT = Preference(3, "meat", false)
        private val FISH = Preference(4, "fish", false)
        private val GLUTEN_FREE = Preference(5, "gluten-free", true)
        private val LACTOSE_FREE = Preference(6, "lactose-free", true)

        // predefined measures
        fun getAllFoodLabels(): List<Preference> {
            return listOf(VEGAN, VEGETARIAN, MEAT, FISH, GLUTEN_FREE, LACTOSE_FREE)
        }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Preference) return false

        if (id != other.id) return false
        if (name != other.name) return false
        if (isStrict != other.isStrict) return false

        return true
    }
}
