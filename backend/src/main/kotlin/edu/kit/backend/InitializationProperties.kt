package edu.kit.backend

import org.springframework.boot.context.properties.ConfigurationProperties

/**
 * Reads in properties defined in the configuration file and inject values into the properties
 */
@ConfigurationProperties("admin")
data class InitializationProperties(
    val username: String,
    val password: String,
)
