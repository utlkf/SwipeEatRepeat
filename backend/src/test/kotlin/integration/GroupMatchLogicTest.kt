package integration

import edu.kit.backend.api.event.AcceptedEvent
import edu.kit.backend.api.event.DeclinedEvent
import edu.kit.backend.api.event.GroupDecision
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import kotlin.random.Random

/**
 * In our Backend we are using a Listener to handle the acceptance or decline of recipes
 * But because this Listener is part of the Spring Framework we simulate the Listening by manually
 * call the functions which the Listener would automatically call.
 */
class GroupMatchLogicTest {
    // mocked data
    private val groupId: Long = 1234
    private val amountOfGroupMembers = 5
    private var listOfUserIds: List<Long> = createMockIds(amountOfGroupMembers)
    private var listOfRecipeIds: List<Long> = createMockIds(15)
    private lateinit var groupDecision: GroupDecision

    companion object {
        fun createMockIds(amount: Int): List<Long> {
            val mockIds = mutableSetOf<Long>()
            while (mockIds.size < amount) {
                mockIds.add(Random.nextLong(1, Long.MAX_VALUE))
            }
            return mockIds.toList()
        }
    }

    @BeforeEach
    fun createNewDecision() {
        groupDecision = GroupDecision(groupId, amountOfGroupMembers, listOfRecipeIds)
    }


    /**
     * if every Group Member send a decision than the group should be evaluated
     */
    @Test
    fun simulateUserDecisions() {
        for (u in listOfUserIds) {
            for (r in listOfRecipeIds) {
                if (Random.nextBoolean()) {
                    groupDecision.addAccepted(AcceptedEvent(u, groupId, r))
                } else {
                    groupDecision.addDeclined(DeclinedEvent(u, groupId, r))
                }
            }
        }
        assert(groupDecision.isEvaluated)
        assert(listOfRecipeIds.contains(groupDecision.getResultId()))

    }

    /**
     * if all user decline the group should be evaluated and a random recipe from the list should be selected
     */
    @Test
    fun simulateAllUserDecline() {
        for (u in listOfUserIds) {
            for (r in listOfRecipeIds) {
                groupDecision.addDeclined(DeclinedEvent(u, groupId, r))
            }
        }
        assert(groupDecision.isEvaluated)
        assert(groupDecision.getResultId() != 0L)
        assert(listOfRecipeIds.contains(groupDecision.getResultId()))
    }

    /**
     * if not every user swiped AND also not every user accepted the same recipe
     * (OR conditions of isEvaluated) then it should not be evaluated
     * and the resultId should still be 0L
     */
    @Test
    fun simulateNotFinishedSession() {
        for (u in listOfUserIds) {
            for (r in listOfRecipeIds.subList(0, 10)) {
                if (Random.nextBoolean()) {
                    groupDecision.addAccepted(AcceptedEvent(u, groupId, r))
                } else {
                    groupDecision.addDeclined(DeclinedEvent(u, groupId, r))
                }
            }
        }
        assert(!groupDecision.isEvaluated)
        assert(!listOfRecipeIds.contains(groupDecision.getResultId()))
    }
}
