package edu.kit.backend.api.security

import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.MalformedJwtException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.springframework.security.core.userdetails.UserDetails

/**
 * Test class for JwtService
 *
 * The code within this class was assisted by GitHub Copilot Chat, a large language model based AI tool.
 */
class JwtServiceTest {

    private val testEncryptionKey = "404A975804556B586E82127535A8D82F613F46283728FB6250644367566B5870"
    private val accessTokenExpiration = 1000L
    private val refreshTokenExpiration = 1000L
    private val jwtProperties = JwtProperties(testEncryptionKey, accessTokenExpiration, refreshTokenExpiration)



    private val jwtService = JwtService(jwtProperties)
    @Test
    fun `generateAccessToken generates valid token`() {
        val userDetails = mock(UserDetails::class.java)
        `when`(userDetails.username).thenReturn("testUser")

        val token = jwtService.generateAccessToken(userDetails)

        assertTrue(jwtService.isTokenValid(token, userDetails))
    }

    @Test
    fun `generateRefreshToken generates valid token`() {
        val userDetails = mock(UserDetails::class.java)
        `when`(userDetails.username).thenReturn("testUser")

        val token = jwtService.generateRefreshToken(userDetails)

        assertTrue(jwtService.isTokenValid(token, userDetails))
    }

    @Test
    fun `isTokenValid returns false for invalid token`() {
        val userDetails = mock(UserDetails::class.java)
        `when`(userDetails.username).thenReturn("testUser")

        val token = "invalidToken"

        assertThrows(MalformedJwtException::class.java){jwtService.isTokenValid(token, userDetails)}
    }

    @Test
    fun `extractUsername returns correct username`() {
        val userDetails = mock(UserDetails::class.java)
        `when`(userDetails.username).thenReturn("testUser")

        val token = jwtService.generateAccessToken(userDetails)

        assertEquals("testUser", jwtService.extractUsername(token))
    }

    @Test
    fun `isExpired returns true for expired token`() {
        val userDetails = mock(UserDetails::class.java)
        `when`(userDetails.username).thenReturn("testUser")

        val token = jwtService.generateAccessToken(userDetails)
        Thread.sleep(accessTokenExpiration + 1000)

        assertThrows(ExpiredJwtException::class.java) {jwtService.isExpired(token)}
    }

    @Test
    fun `doesNotContainBearerToken returns true for non-Bearer token`() {
        assertTrue(jwtService.doesNotContainBearerToken("Token abc"))
    }

    @Test
    fun `doesNotContainBearerToken returns false for Bearer token`() {
        assertFalse(jwtService.doesNotContainBearerToken("Bearer abc"))
    }

    @Test
    fun `extractTokenValue returns token value after Bearer`() {
        assertEquals("abc", jwtService.extractTokenValue("Bearer abc"))
    }
}