package edu.kit.backend.api.service

import edu.kit.backend.api.repository.RecipeRepository
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Recipe
import edu.kit.backend.data.model.Role
import edu.kit.backend.exceptions.IdNotFoundException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyLong
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.whenever
import java.time.LocalDateTime
import java.util.*

/**
 * Test class for the RecipeService.
 *
 * The code within this class was assisted by Phind, a large language model based AI tool.
 */
class RecipeServiceTest {

    @InjectMocks
    private lateinit var recipeService: RecipeService

    //dependencies
    @Mock
    private lateinit var recipeRepository: RecipeRepository

    private lateinit var testCreator: AppUser

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        testCreator = AppUser(id = 1, username = "Test User", password = "d", email = "d", role = Role.USER)
    }

    @Test
    fun `fetchNumberOfRecipesRandom returns correct number of recipes when number lower than amount of available recipes`() {
        //Given
        val allRecipes = listOf(mock(Recipe::class.java), mock(Recipe::class.java), mock(Recipe::class.java), mock(Recipe::class.java))
        val numberOfRecipes = 3
        //Mocking
        whenever(recipeRepository.findAll()).thenReturn(allRecipes)
        //When
        val result = recipeService.fetchNumberOfRecipesRandom(numberOfRecipes)
        //Then
        // verify(recipeRepository).findAll() // Verify interaction with recipeRepository
        assertEquals(numberOfRecipes, result.size)
    }

    @Test
    fun `fetchNumberOfRecipesRandom returns correct number of recipes when number higher than amount of available recipes`() {
        //Given
        val allRecipes = listOf(mock(Recipe::class.java), mock(Recipe::class.java), mock(Recipe::class.java), mock(Recipe::class.java))
        val numberOfRecipes = 5
        //Mocking
        whenever(recipeRepository.findAll()).thenReturn(allRecipes)
        //When
        val result = recipeService.fetchNumberOfRecipesRandom(numberOfRecipes)
        //Then
        // verify(recipeRepository).findAll() // Verify interaction with recipeRepository
        assertEquals(result.size, allRecipes.size)
    }

    @Test
    fun `getRecipeById throws IdNotFoundException when recipe does not exist`() {
        whenever(recipeRepository.findById(anyLong())).thenReturn(Optional.empty())
        assertThrows(IdNotFoundException::class.java) { recipeService.getRecipeById(1L) }
    }

    /* When executing this method the following error occurs in GithubActions:
    java.nio.file.AccessDeniedException at RecipeServiceTest.kt:203
    This is because the processAndSaveImageFile method tries to access the file system, which is not allowed in the CI environment.

    @Test
    fun `createRecipe saves a new recipe and returns it`() {
        // Given
        val recipeTemplate = RecipeTemplate(
                id = 1,
                name = "Test Recipe",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creatorId = 1,
                postDate = LocalDateTime.MIN,
                components = listOf(),
                preparationSteps = listOf(),
                preferences = listOf(),
                imageUrl = ""
        )
        val file: MultipartFile = MockMultipartFile("file", "hello.png", "image/png", "some image".toByteArray())
        val expectedRecipe = Recipe(
                name = recipeTemplate.name,
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = testCreator,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        // Mocking
        `when`(recipeRepository.save(any())).thenAnswer { invocation ->
            invocation.arguments[0] as Recipe
        }
        `when`(userRepository.findById(1L)).thenReturn(Optional.of(testCreator))

        // When
        val result = recipeService.createRecipe(recipeTemplate, file)

        // Then
        verify(recipeRepository).save(any())
        verify(userRepository).findById(1L)
        assertTrue(areRecipesEqual(result, expectedRecipe))
    }

    private fun areRecipesEqual(recipe1: Recipe, recipe2: Recipe): Boolean {
        return recipe1.name == recipe2.name &&
                recipe1.creator == recipe2.creator &&
                recipe1.passiveCookingTime == recipe2.passiveCookingTime &&
                recipe1.activeCookingTime == recipe2.activeCookingTime &&
                recipe1.postDate == recipe2.postDate
                //image URL creation uses randomness, so we cannot predict the URL
    }*/

    @Test
    fun `deleteRecipe deletes a recipe successfully`() {
        // Given
        val recipeId = 1L
        val recipe = Recipe(
                id = recipeId,
                name = "Test Recipe",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = testCreator,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        // Mocking
        `when`(recipeRepository.findById(recipeId)).thenReturn(Optional.of(recipe))

        // When
        val result = recipeService.deleteRecipe(recipeId)

        // Then
        assertEquals(recipe, result)
        verify(recipeRepository).findById(recipeId)
        verify(recipeRepository).deleteById(recipeId)
    }

    @Test
    fun `deleteRecipe throws IdNotFoundException when recipe does not exist`() {
        val recipeId = 1L

        `when`(recipeRepository.findById(recipeId)).thenReturn(Optional.empty())

        assertThrows(IdNotFoundException::class.java) { recipeService.deleteRecipe(recipeId) }
    }

    /* When executing this method the following error occurs in GithubActions:
    java.nio.file.AccessDeniedException at RecipeServiceTest.kt:203
    This is because the processAndSaveImageFile method tries to access the file system, which is not allowed in the CI environment.

    @Test
    fun `alterRecipe updates an existing recipe and saves it`() {
        // Given
        val recipeId = 1L
        val newRecipeVersion = RecipeTemplate(
                id = recipeId,
                name = "Updated Recipe",
                creatorId = 1L,
                components = emptyList(),
                preparationSteps = emptyList(),
                preferences = emptyList(),
                postDate = LocalDateTime.MIN,
                activeCookingTime = 30,
                passiveCookingTime = 10,
                imageUrl = ""
        )
        val file: MultipartFile = MockMultipartFile("file", "updatedImage.png", "image/png", "updated image content".toByteArray())
        val recipeToAlter = Recipe(
                id = recipeId,
                name = "Old Name",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = testCreator,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        // Mocking
        `when`(recipeRepository.findById(recipeId)).thenReturn(Optional.of(recipeToAlter))
        `when`(recipeRepository.save(any())).thenAnswer { invocation ->
            invocation.arguments[0] as Recipe
        }

        // When
        val result = recipeService.alterRecipe(newRecipeVersion, file)

        // Then
        assertEquals(newRecipeVersion.name, result.name)
        assertEquals(newRecipeVersion.activeCookingTime, result.activeCookingTime)
        assertEquals(newRecipeVersion.passiveCookingTime, result.passiveCookingTime)
        verify(recipeRepository).save(any())
    }*/
}