package edu.kit.backend.api.service

import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.api.templates.converters.IngredientConverter
import edu.kit.backend.api.templates.converters.PreferenceConverter
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.data.model.Preference
import edu.kit.backend.data.model.Role
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import java.util.*

class UserServiceTest {

    @Mock
    private lateinit var userRepository: UserRepository

    @Mock
    private lateinit var recipeService: RecipeService

    @Mock
    private lateinit var preferenceService: PreferenceService

    @Mock
    private lateinit var ingredientService: IngredientService

    @Mock
    private lateinit var filterService: FilterService

    @InjectMocks
    private lateinit var userService: UserService

    private lateinit var testUser1: AppUser

    private lateinit var testUser2: AppUser

    private lateinit var testPreference1: Preference

    private lateinit var testPreference2: Preference

    private lateinit var testIngredient1: Ingredient

    private lateinit var testIngredient2: Ingredient

    private val preferenceConverter = PreferenceConverter()

    private val ingredientConverter = IngredientConverter()

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        testUser1 = AppUser(id = 1, username = "Test User1", password = "d", email = "d", role = Role.USER)
        testUser2 = AppUser(id = 2, username = "Test User2", password = "d", email = "e", role = Role.USER)

        testPreference1 = Preference(id = 1, name = "Preference1", isStrict = false)
        testPreference2 = Preference(id = 2, name = "Preference2", isStrict = false)

        testIngredient1 = Ingredient(id = 1, name = "Ingredient1")
        testIngredient2 = Ingredient(id = 2, name = "Ingredient2")
    }

    @Test
    fun `getAllUsers returns all users`() {
        val users = listOf(testUser1, testUser2)

        `when`(userRepository.findAll()).thenReturn(users)

        val result = userService.getAllUsers()

        assertEquals(users, result)
    }

    @Test
    fun `deleteUser deletes a user and returns it`() {
        val userId = 1L
        `when`(userRepository.findById(userId)).thenReturn(Optional.of(testUser1))

        val result = userService.deleteUser(userId)

        assertEquals(testUser1, result)
        verify(userRepository).deleteById(userId)
    }

    @Test
    fun `getUser returns the user with the given id`() {
        val userId = 1L

        `when`(userRepository.findById(userId)).thenReturn(Optional.of(testUser1))

        val result = userService.getUser(userId)

        assertEquals(testUser1, result)
    }

    @Test
    fun `setPreferences updates the user's preferences and saves the user`() {
        // Given
        assertTrue(testUser1.preferences.isEmpty())
        val preferences = listOf(preferenceConverter.convert(testPreference1), preferenceConverter.convert(testPreference2))

        // Mocking
        `when`(userRepository.findById(testUser1.id)).thenReturn(Optional.of(testUser1))
        `when`(preferenceService.getPreferenceById(testPreference1.id)).thenReturn(testPreference1)
        `when`(preferenceService.getPreferenceById(testPreference2.id)).thenReturn(testPreference2)
        `when`(userRepository.save(any())).thenAnswer { invocation ->
            invocation.getArgument(0)
        }

        // When
        val result = userService.setPreferences(testUser1.id, preferences)

        // Then
        assertEquals(2, result.preferences.size)
        assertEquals(testPreference1, result.preferences[0])
        assertEquals(testPreference2, result.preferences[1])
    }

    @Test
    fun `setBlacklist updates the user's blacklist and saves the user`() {
        // Given
        assertTrue(testUser1.blacklist.isEmpty())
        val blacklist = listOf(ingredientConverter.convert(testIngredient1), ingredientConverter.convert(testIngredient2))

        // Mocking
        `when`(userRepository.findById(testUser1.id)).thenReturn(Optional.of(testUser1))
        `when`(ingredientService.getIngredientById(testIngredient1.id)).thenReturn(testIngredient1)
        `when`(ingredientService.getIngredientById(testIngredient2.id)).thenReturn(testIngredient2)
        `when`(userRepository.save(any())).thenAnswer { invocation ->
            invocation.getArgument(0)
        }

        // When
        val result = userService.setBlacklist(testUser1.id, blacklist)

        // Then
        assertEquals(2, result.blacklist.size)
        assertTrue(result.blacklist.contains(testIngredient1))
        assertTrue(result.blacklist.contains(testIngredient2))
    }

    @Test
    fun `setSubsetWhitelist updates the user's subset whitelist and saves the user`() {
        // Given
        assertTrue(testUser1.subsetWhitelist.isEmpty())
        val subsetWhitelist = listOf(ingredientConverter.convert(testIngredient1), ingredientConverter.convert(testIngredient2))

        // Mocking
        `when`(userRepository.findById(testUser1.id)).thenReturn(Optional.of(testUser1))
        `when`(ingredientService.getIngredientById(testIngredient1.id)).thenReturn(testIngredient1)
        `when`(ingredientService.getIngredientById(testIngredient2.id)).thenReturn(testIngredient2)
        `when`(userRepository.save(any())).thenAnswer { invocation ->
            invocation.getArgument(0)
        }

        // When
        val result = userService.setSubsetWhitelist(testUser1.id, subsetWhitelist)

        // Then
        assertEquals(2, result.subsetWhitelist.size)
        assertTrue(result.subsetWhitelist.contains(testIngredient1))
        assertTrue(result.subsetWhitelist.contains(testIngredient2))
    }

    @Test
    fun `setSupersetWhitelist updates the user's superset whitelist and saves the user`() {
        // Given
        assertTrue(testUser1.supersetWhitelist.isEmpty())
        val supersetWhitelist = listOf(ingredientConverter.convert(testIngredient1), ingredientConverter.convert(testIngredient2))

        // Mocking
        `when`(userRepository.findById(testUser1.id)).thenReturn(Optional.of(testUser1))
        `when`(ingredientService.getIngredientById(testIngredient1.id)).thenReturn(testIngredient1)
        `when`(ingredientService.getIngredientById(testIngredient2.id)).thenReturn(testIngredient2)
        `when`(userRepository.save(any())).thenAnswer { invocation ->
            invocation.getArgument(0)
        }

        // When
        val result = userService.setSupersetWhitelist(testUser1.id, supersetWhitelist)

        // Then
        assertEquals(2, result.supersetWhitelist.size)
        assertTrue(result.supersetWhitelist.contains(testIngredient1))
        assertTrue(result.supersetWhitelist.contains(testIngredient2))
    }
}