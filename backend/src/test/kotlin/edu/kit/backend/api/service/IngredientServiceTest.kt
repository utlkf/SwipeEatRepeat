package edu.kit.backend.api.service

import edu.kit.backend.api.repository.IngredientRepository
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.exceptions.IdNotFoundException
import edu.kit.backend.exceptions.NameNotFoundException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import java.util.*

class IngredientServiceTest {

    @Mock
    lateinit var ingredientRepository: IngredientRepository

    @InjectMocks
    lateinit var ingredientService: IngredientService

    private lateinit var ingredient1: Ingredient
    private lateinit var ingredient2: Ingredient

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        ingredient1 = Ingredient(id = 1, name = "ingredient1")
        ingredient2 = Ingredient(id = 2, name = "ingredient2")
    }

    @Test
    fun `getIngredientById returns the correct ingredient`() {
        val id = 1L
        val ingredient = Ingredient(id, "ingredient1")

        `when`(ingredientRepository.findById(id)).thenReturn(Optional.of(ingredient))

        val result = ingredientService.getIngredientById(id)

        assertEquals(ingredient, result)
        verify(ingredientRepository).findById(id)
    }

    @Test
    fun `getIngredientById throws IdNotFoundException when ingredient does not exist`() {
        val id = 4L

        `when`(ingredientRepository.findById(id)).thenReturn(Optional.empty())

        assertThrows(IdNotFoundException::class.java) {ingredientService.getIngredientById(id)}
        verify(ingredientRepository).findById(id)
    }

    @Test
    fun `getAllIngredients returns all ingredients`() {
        val ingredients = listOf(Ingredient(1, "ingredient1"), Ingredient(2, "ingredient2"))

        `when`(ingredientRepository.findAll()).thenReturn(ingredients)

        val result = ingredientService.getAllIngredients()

        assertEquals(ingredients, result)
        verify(ingredientRepository).findAll()
    }

    @Test
    fun `getIngredientByName returns the correct ingredient when it exists`() {
        `when`(ingredientRepository.findByName(ingredient1.name)).thenReturn(ingredient1)

        val result = ingredientService.getIngredientByName(ingredient1.name)

        assertEquals(ingredient1, result)
        verify(ingredientRepository).findByName(ingredient1.name)
    }

    @Test
    fun `getIngredientByName creates and returns a new ingredient when it does not exist`() {
        val name = "ingredient3"

        `when`(ingredientRepository.findByName(name)).thenReturn(null)
        `when`(ingredientRepository.save(any())).thenAnswer {
            invocation -> invocation.arguments[0]
        }


        val result = ingredientService.getIngredientByName(name)

        assertNotNull(result)
        assertEquals(name, result.name)
        verify(ingredientRepository).findByName(name)
        verify(ingredientRepository).save(any())
    }

    @Test
    fun `deleteIngredient deletes and returns the correct ingredient when it exists`() {
        `when`(ingredientRepository.findByName(ingredient1.name)).thenReturn(ingredient1)

        val result = ingredientService.deleteIngredient(ingredient1.name)

        assertEquals(ingredient1, result)
        verify(ingredientRepository).findByName(ingredient1.name)
        verify(ingredientRepository).delete(ingredient1)
    }

    @Test
    fun `deleteIngredient throws NameNotFoundException when ingredient does not exist`() {
        val name = "ingredient4"

        `when`(ingredientRepository.findByName(name)).thenReturn(null)

        assertThrows(NameNotFoundException::class.java) {ingredientService.deleteIngredient(name)}
        verify(ingredientRepository).findByName(name)
    }
}