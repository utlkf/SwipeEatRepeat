package edu.kit.backend.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import edu.kit.backend.api.service.AuthenticationService
import edu.kit.backend.api.templates.LoginCredentials
import edu.kit.backend.api.templates.RegistrationCredentials
import edu.kit.backend.data.model.AuthenticationResponse
import edu.kit.backend.exceptions.DuplicateEmailException
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@ExtendWith(MockitoExtension::class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class AuthenticationControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var authenticationService: AuthenticationService

    private lateinit var loginCredentials: LoginCredentials
    private lateinit var registrationCredentials: RegistrationCredentials
    private lateinit var authenticationResponse: AuthenticationResponse
    private lateinit var authenticationResponseAsJson: String

    @BeforeEach
    fun setup() {
        loginCredentials = LoginCredentials("testUser", "testPassword")
        registrationCredentials = RegistrationCredentials("testUser", "testPassword", "test@email.com")
        authenticationResponse = AuthenticationResponse(accessToken = "JWT_TOKEN", refreshToken = "REFRESH_TOKEN", userId = 1)
        authenticationResponseAsJson = objectMapper.writeValueAsString(authenticationResponse)
    }

    @Test
    fun `authenticate returns JWT token on successful login`() {
        `when`(authenticationService.authentication(loginCredentials)).thenReturn(authenticationResponse)

        mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginCredentials)))
                .andExpect(status().isOk)
                .andExpect(content().string(authenticationResponseAsJson))
    }

    @Test
    fun `authenticate returns bad request on failed login`() {
        val exceptionMessage = "User not found"
        `when`(authenticationService.authentication(loginCredentials)).thenThrow(UsernameNotFoundException(exceptionMessage))

        mockMvc.perform(post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(loginCredentials)))
                .andExpect(status().isBadRequest)
                .andExpect(content().string(exceptionMessage))
    }

    @Test
    fun `register returns 200 on successful registration`() {
        `when`(authenticationService.registerUser(registrationCredentials)).thenReturn(authenticationResponse)

        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registrationCredentials)))
                .andExpect(status().isOk)
                .andExpect(content().string(authenticationResponseAsJson))
    }

    @Test
    fun `register returns bad request on duplicate email`() {
        val testMail = "test@mail.com"
        val exceptionMessage = "email $testMail is already registered"
        doAnswer { throw DuplicateEmailException(testMail) }
                .`when`(authenticationService).registerUser(registrationCredentials)

        mockMvc.perform(post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registrationCredentials)))
                .andExpect(status().isBadRequest)
                .andExpect(content().string(exceptionMessage))
    }
}
