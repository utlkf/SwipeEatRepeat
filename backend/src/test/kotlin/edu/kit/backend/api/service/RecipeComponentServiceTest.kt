package edu.kit.backend.api.service

import edu.kit.backend.api.repository.RecipeComponentRepository
import edu.kit.backend.data.model.*
import edu.kit.backend.exceptions.IdNotFoundException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.time.LocalDateTime
import java.util.*

/**
 * Test class for the RecipeComponentService.
 *
 * The code within this class was assisted by GitHub Copilot Chat, a large language model based AI tool.
 */
class RecipeComponentServiceTest {

    @Mock
    private lateinit var recipeComponentRepository: RecipeComponentRepository

    @InjectMocks
    private lateinit var recipeComponentService: RecipeComponentService

    private lateinit var user: AppUser

    private lateinit var recipe: Recipe

    private lateinit var ingredient: Ingredient

    private lateinit var measure: Measure

    private lateinit var recipeComponent: RecipeComponent

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        user = AppUser(id = 1, username = "Test User1", password = "d", email = "d", role = Role.USER)

        recipe = Recipe(
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        ingredient = Ingredient(id = 1, name = "ingredient1")

        measure = Measure(id = 1, name = "gram", unit = "g")

        recipeComponent = RecipeComponent(
                id = 1,
                recipe = recipe,
                ingredient = ingredient,
                measure = measure,
                quantity = 100
        )
    }

    @Test
    fun `getRecipeComponentById returns the correct RecipeComponent`() {
        `when`(recipeComponentRepository.findById(recipeComponent.id)).thenReturn(Optional.of(recipeComponent))

        val result = recipeComponentService.getRecipeComponentById(recipeComponent.id)

        assertEquals(recipeComponent, result)
        verify(recipeComponentRepository).findById(recipeComponent.id)
    }

    @Test
    fun `getRecipeComponentById throws IdNotFoundException when RecipeComponent does not exist`() {
        val id = 4L

        `when`(recipeComponentRepository.findById(id)).thenReturn(Optional.empty())

        assertThrows(IdNotFoundException::class.java) {recipeComponentService.getRecipeComponentById(id)}
        verify(recipeComponentRepository).findById(id)
    }

    @Test
    fun `deleteRecipeComponent deletes the correct RecipeComponent`() {
        `when`(recipeComponentRepository.findById(recipeComponent.id)).thenReturn(Optional.of(recipeComponent))

        recipeComponentService.deleteRecipeComponent(recipeComponent.id)

        verify(recipeComponentRepository).deleteById(recipeComponent.id)
    }
}