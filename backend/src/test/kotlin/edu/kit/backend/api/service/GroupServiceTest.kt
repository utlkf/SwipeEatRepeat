package edu.kit.backend.api.service

import edu.kit.backend.api.repository.GroupRepository
import edu.kit.backend.api.templates.GroupTemplate
import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.api.templates.UserTemplate
import edu.kit.backend.data.model.*
import edu.kit.backend.exceptions.IdNotFoundException
import edu.kit.backend.exceptions.InsufficientRightsException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import java.time.LocalDateTime
import java.util.*

/**
 * Test class for the GroupService.
 *
 * The code within this class was assisted by GitHub Copilot Chat, a large language model based AI tool.
 */
class GroupServiceTest {

    @Mock
    private lateinit var groupRepository: GroupRepository

    @Mock
    private lateinit var userService: UserService

    @Mock
    private lateinit var filterService: FilterService

    @Mock
    private lateinit var ingredientService: IngredientService

    @Mock
    private lateinit var preferenceService: PreferenceService

    @Mock
    private lateinit var recipeService: RecipeService

    @InjectMocks
    private lateinit var groupService: GroupService


    private lateinit var user1: AppUser
    private lateinit var user2: AppUser
    private lateinit var recipe1: Recipe
    private lateinit var recipe2: Recipe
    private lateinit var group1: Group
    private lateinit var group2: Group
    private lateinit var group1Template: GroupTemplate

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        user1 = AppUser(id = 1, username = "Test User1", password = "d", email = "d", role = Role.USER)
        user2 = AppUser(id = 2, username = "Test User2", password = "d", email = "e", role = Role.USER)

        recipe1 = Recipe(
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe2 = Recipe(
                name = "recipe2",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        group1 = Group(id = 1, admin = user1, name = "group1", groupCode = "1")
        group2 = Group(id = 2, admin = user1, name = "group2", groupCode = "2")
        group1Template = GroupTemplate(adminId = user1.id, name = "group1", groupCode = "1")
    }

    @Test
    fun `getGroupById returns the correct group`() {
        `when`(groupRepository.findById(group1.id)).thenReturn(Optional.of(group1))

        val result = groupService.getGroupById(group1.id)

        assertEquals(group1, result)
        verify(groupRepository).findById(group1.id)
    }

    @Test
    fun `getGroupById throws IdNotFoundException when group does not exist`() {
        val id = 4L

        `when`(groupRepository.findById(id)).thenReturn(Optional.empty())


        assertThrows(IdNotFoundException::class.java) {groupService.getGroupById(id)}
        verify(groupRepository).findById(id)
    }

    @Test
    fun `getAllGroups returns all groups`() {
        val groups = listOf(group1, group2)

        `when`(groupRepository.findAll()).thenReturn(groups)

        val result = groupService.getAllGroups()

        assertEquals(groups, result)
        verify(groupRepository).findAll()
    }

    @Test
    fun `createGroup creates and returns a new group`() {
        `when`(userService.getUser(group1Template.adminId)).thenReturn(user1)
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }

        val result = groupService.createGroup(group1Template)

        assertEquals(group1Template.name, result.name)
        assertEquals(group1Template.adminId, result.admin.id)
        assertEquals(setOf(userService.getUser(group1Template.adminId)), result.members)
        verify(groupRepository).save(any())
    }

    @Test
    fun `deleteGroup deletes and returns the correct group when user has sufficient rights`() {
        `when`(groupRepository.findByGroupCode(group1.groupCode)).thenReturn(group1)
        `when`(userService.getUser(user1.id)).thenReturn(user1)

        val result = groupService.deleteGroup(group1.groupCode, user1.id)

        assertEquals(group1, result)
        verify(groupRepository).delete(group1)
    }

    @Test
    fun `deleteGroup throws InsufficientRightsException when user does not have sufficient rights`() {
        `when`(groupRepository.findByGroupCode(group1.groupCode)).thenReturn(group1)
        `when`(userService.getUser(user2.id)).thenReturn(user2)


        assertThrows(InsufficientRightsException::class.java) {groupService.deleteGroup(group1.groupCode, user2.id)}
        verify(groupRepository, never()).delete(group1)
    }

    @Test
    fun `joinGroup adds user to group`() {
        `when`(groupRepository.findByGroupCode(group1.groupCode)).thenReturn(group1)
        `when`(userService.getUser(user2.id)).thenReturn(user2)
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }


        groupService.joinGroup(group1.groupCode, user2.id)

        assertTrue(group1.members.contains(user2))
        verify(groupRepository).save(group1)
    }

    @Test
    fun `leaveGroup removes user from group`() {
        group1.members.addAll(setOf(user1, user2))
        assertTrue(group1.members.contains(user1))
        `when`(groupRepository.findByGroupCode(group1.groupCode)).thenReturn(group1)
        `when`(userService.getUser(user1.id)).thenReturn(user1)
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }

        groupService.leaveGroup(group1.groupCode, user1.id)

        assertFalse(group1.members.contains(user1))
        verify(groupRepository).save(group1)
    }

    @Test
    fun `addRecipesToGroup adds recipes to group`() {
        val recipes = listOf(recipe1, recipe2)

        `when`(groupRepository.findById(group1.id)).thenReturn(Optional.of(group1))
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }

        groupService.addRecipesToGroup(group1.id, recipes)

        assertTrue(group1.recipeStack.containsAll(recipes))
        verify(groupRepository).save(group1)
    }

    @Test
    fun `clearRecipes clears recipes from group`() {
        `when`(groupRepository.findById(group1.id)).thenReturn(Optional.of(group1))
        `when`(userService.getUser(user1.id)).thenReturn(user1)
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }

        groupService.clearRecipes(group1.id, user1.id)

        assertTrue(group1.recipeStack.isEmpty())
        verify(groupRepository).save(group1)
    }

    @Test
    fun `editGroup edits group`() {
        val recipeTemplate = RecipeTemplate(
                id = 1,
                name = "Test Recipe",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creatorId = 1,
                postDate = LocalDateTime.MIN,
                components = listOf(),
                preparationSteps = listOf(),
                preferences = listOf(),
                imageUrl = ""
        )
        val ingredientTemplate = IngredientTemplate(2, "ingredient1")
        val user2Template = UserTemplate(2, "user2", "e")
        val groupTemplate = GroupTemplate(
                id = 1,
                adminId = 1,
                name = "new name",
                groupCode = "new code",
                members = listOf(user2Template),
                recipes = listOf(recipeTemplate),
                preferences = listOf(),
                blacklist = listOf(ingredientTemplate),
                supersetWhitelist = listOf(),
                subsetWhitelist = listOf()
        )

        `when`(groupRepository.findById(groupTemplate.id)).thenReturn(Optional.of(group1))
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }
        `when`(userService.getUser(groupTemplate.adminId)).thenReturn(user1)
        `when`(recipeService.getRecipeById(any())).thenReturn(recipe1)
        `when`(preferenceService.getPreferenceById(any())).thenReturn(Preference(1, "preference1", false))
        `when`(ingredientService.getIngredientById(any())).thenReturn(Ingredient(1, "ingredient1"))

        val result = groupService.editGroup(groupTemplate)

        // Things that can be altered
        assertNotEquals(groupTemplate.name, group1.name)
        assertEquals(groupTemplate.name, result.name)
        assertNotEquals(groupTemplate.groupCode, group1.groupCode)
        assertEquals(groupTemplate.groupCode, result.groupCode)
        // Things that can not be altered
        assertEquals(group1.id, result.id)
        assertEquals(group1.admin, result.admin)

        verify(groupRepository).save(result)
    }

    @Test
    fun `fillGroupWithRecipes adds filtered recipes to group according to group preferences`() {
        val preference = Preference(1, "preference1", false)
        val ingredient = Ingredient(1, "ingredient1")

        group1.preferences.add(preference)
        group1.blacklist.add(ingredient)

        `when`(groupRepository.findById(group1.id)).thenReturn(Optional.of(group1))
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }
        `when`(filterService.getFilteredRecipes(any(), any(), any(), any())).thenReturn(setOf(recipe1, recipe2))

        groupService.fillGroupWithRecipes(group1.id)

        assertTrue(group1.recipeStack.containsAll(setOf(recipe1, recipe2)))
        verify(groupRepository).save(group1)
        verify(filterService).getFilteredRecipes(group1.preferences, group1.blacklist, group1.supersetWhitelist, group1.subsetWhitelist)
    }

    @Test
    fun `saveMatch saves match to group`() {
        `when`(groupRepository.findById(group1.id)).thenReturn(Optional.of(group1))
        `when`(groupRepository.save(any())).thenAnswer { it.getArgument(0) }
        `when`(recipeService.getRecipeById(recipe2.id)).thenReturn(recipe2)

        val result = groupService.saveMatch(group1.id, recipe1.id)

        assertEquals(recipe2, result)
        assertEquals(recipe2, group1.match)
        verify(groupRepository).save(group1)
    }
}