package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.MeasureTemplate
import edu.kit.backend.data.model.Measure
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class MeasureConverterTest {

    private lateinit var measureConverter: MeasureConverter
    private lateinit var measure: Measure
    private lateinit var measureTemplate: MeasureTemplate

    @BeforeEach
    fun setUp() {
        measureConverter = MeasureConverter()
        measure = Measure(id = 0, name = "Test Measure", unit = "Test Unit")
        measureTemplate = MeasureTemplate(id = 0, name = "Test Measure", unit = "Test Unit")
    }

    @Test
    fun `correctly converts Measure to MeasureTemplate`() {
        assertEquals(measureConverter.convert(measure), measureTemplate)
    }
}