package edu.kit.backend.api.service

import edu.kit.backend.api.repository.MeasureRepository
import edu.kit.backend.data.model.Measure
import edu.kit.backend.exceptions.IdNotFoundException
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.*

/**
 * Test class for the MeasureService.
 *
 * The code within this class was assisted by GitHub Copilot Chat, a large language model based AI tool.
 */
class MeasureServiceTest {

    @Mock
    lateinit var measureRepository: MeasureRepository

    @InjectMocks
    lateinit var measureService: MeasureService

    private lateinit var measure1: Measure
    private lateinit var measure2: Measure

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        measure1 = Measure(id = 1, name = "Measure1", unit = "1")
        measure2 = Measure(id = 2, name = "Measure2", unit = "2")
    }

    @Test
    fun `getMeasureById returns the correct measure`() {
        `when`(measureRepository.findById(measure1.id)).thenReturn(Optional.of(measure1))

        val result = measureService.getMeasureById(measure1.id)

        assertEquals(measure1, result)
        verify(measureRepository).findById(measure1.id)
    }

    @Test
    fun `getMeasureById throws IdNotFoundException when measure does not exist`() {
        val id = 4L

        `when`(measureRepository.findById(id)).thenReturn(Optional.empty())

        assertThrows(IdNotFoundException::class.java) {measureService.getMeasureById(id)}
        verify(measureRepository).findById(id)
    }

    @Test
    fun `getAllMeasures returns all measures`() {
        val measures = listOf(measure1, measure2)

        `when`(measureRepository.findAll()).thenReturn(measures)

        val result = measureService.getAllMeasures()

        assertEquals(measures, result)
        verify(measureRepository).findAll()
    }
}