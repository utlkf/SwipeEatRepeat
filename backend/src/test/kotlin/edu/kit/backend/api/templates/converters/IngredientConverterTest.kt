package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.data.model.Ingredient
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class IngredientConverterTest {
    private lateinit var ingredientConverter: IngredientConverter
    private lateinit var ingredient: Ingredient
    private lateinit var ingredientTemplate: IngredientTemplate

    @BeforeEach
    fun setUp() {
        ingredientConverter = IngredientConverter()
        ingredient = Ingredient(id = 0, name = "Test Ingredient")
        ingredientTemplate = IngredientTemplate(id = 0, name = "Test Ingredient")
    }

    @Test
    fun `correctly converts Ingredient to IngredientTemplate`() {
        assertEquals(ingredientConverter.convert(ingredient), ingredientTemplate)
    }
}