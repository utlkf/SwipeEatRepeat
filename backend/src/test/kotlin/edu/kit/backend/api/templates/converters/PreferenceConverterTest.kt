package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.data.model.Preference
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PreferenceConverterTest {

    private lateinit var preferenceConverter: PreferenceConverter
    private lateinit var preference: Preference
    private lateinit var preferenceTemplate: PreferenceTemplate

    @BeforeEach
    fun setUp() {
        preferenceConverter = PreferenceConverter()
        preference = Preference(id = 0, name = "Test Preference", isStrict = false)
        preferenceTemplate = PreferenceTemplate(id = 0, name = "Test Preference")
    }

    @Test
    fun `correctly converts Preference to PreferenceTemplate`() {
        assertEquals(preferenceConverter.convert(preference), preferenceTemplate)
    }
}