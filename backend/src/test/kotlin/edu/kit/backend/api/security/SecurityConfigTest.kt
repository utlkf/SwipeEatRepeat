package edu.kit.backend.api.security

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class SecurityConfigTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun `test whitelist access`() {
        mockMvc.perform(get("/recipes"))
                .andExpect(status().isOk)
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `test admin access`() {
        mockMvc.perform(get("/admin/demo"))
                .andExpect(status().isOk)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `test admin access denied`() {
        mockMvc.perform(get("/admin/demo"))
                .andExpect(status().isForbidden)
    }

    @Test
    fun `test logout`() {
        mockMvc.perform(get("/logout"))
                .andExpect(status().isOk)
    }
}
