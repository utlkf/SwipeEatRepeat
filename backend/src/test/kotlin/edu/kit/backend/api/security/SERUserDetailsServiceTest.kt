package edu.kit.backend.api.security

import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Role
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.springframework.security.core.userdetails.UsernameNotFoundException

class SERUserDetailsServiceTest {

    @Mock
    private lateinit var userRepository: UserRepository

    @InjectMocks
    private lateinit var serUserDetailsService: SERUserDetailsService

    private lateinit var user: AppUser

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        user = AppUser(id = 1, username = "Test User1", password = "d", email = "d", role = Role.USER)
    }

    @Test
    fun `loadUserByUsername returns UserDetails for existing user`() {
        `when`(userRepository.findByUsername(user.username)).thenReturn(user)

        val userDetails = serUserDetailsService.loadUserByUsername(user.username)

        assertEquals(user.username, userDetails.username)
        verify(userRepository).findByUsername(user.username)
    }

    @Test
    fun `loadUserByUsername throws UsernameNotFoundException for non-existing user`() {
        val username = "nonExistingUser"

        `when`(userRepository.findByUsername(username)).thenReturn(null)

        assertThrows(UsernameNotFoundException::class.java) {
            serUserDetailsService.loadUserByUsername(username)
        }
        verify(userRepository).findByUsername(username)
    }
}