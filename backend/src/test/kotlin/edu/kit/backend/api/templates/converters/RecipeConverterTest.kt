package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Recipe
import edu.kit.backend.data.model.Role
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import java.time.LocalDateTime
import java.util.*

class RecipeConverterTest {

    private lateinit var recipeConverter: RecipeConverter
    private lateinit var recipe: Recipe
    private lateinit var recipeTemplate: RecipeTemplate

    @BeforeEach
    fun setUp() {
        // Inject mock converters using reflection
        recipeConverter = RecipeConverter()

        val testRecipe = Recipe(
            id = 1,
            name = "Test Recipe",
            passiveCookingTime = 10,
            activeCookingTime = 20,
            creator = AppUser(id = 1, username = "Test User", password = "d", email = "d", role = Role.USER),
            postDate = LocalDateTime.MIN,
            recipeComponents = mutableSetOf(),
            preparationSteps = mutableListOf(),
            preferences = mutableSetOf(),
            imageUrl = "Test Image URL"
        )

        recipe = testRecipe

        recipeTemplate = RecipeTemplate(
            id = 1,
            name = "Test Recipe",
            passiveCookingTime = 10,
            activeCookingTime = 20,
            creatorId = 1,
            postDate = LocalDateTime.MIN,
            components = listOf(),
            preparationSteps = listOf(),
            preferences = listOf(),
            imageUrl = "Test Image URL"
        )
    }

    @Test
    fun `correctly converts Recipe to RecipeTemplate`() {
        assertEquals(recipeConverter.convert(recipe), recipeTemplate)
    }
}