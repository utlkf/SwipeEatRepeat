package edu.kit.backend.api.service

import edu.kit.backend.api.repository.PreparationStepRepository
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.PreparationStep
import edu.kit.backend.data.model.Recipe
import edu.kit.backend.data.model.Role
import edu.kit.backend.exceptions.IdNotFoundException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.time.LocalDateTime
import java.util.*

class PreparationStepServiceTest {

    @Mock
    private lateinit var preparationStepRepository: PreparationStepRepository

    @InjectMocks
    private lateinit var preparationStepService: PreparationStepService

    private lateinit var user: AppUser
    private lateinit var recipe: Recipe
    private lateinit var preparationStep: PreparationStep

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        user = AppUser(id = 1, username = "Test User1", password = "d", email = "d", role = Role.USER)

        recipe = Recipe(
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        preparationStep = PreparationStep(id = 1, recipe = recipe, description = "test")
    }

    @Test
    fun `getRecipePreparationStepById returns the correct PreparationStep`() {
        `when`(preparationStepRepository.findById(preparationStep.id)).thenReturn(Optional.of(preparationStep))

        val result = preparationStepService.getRecipePreparationStepById(preparationStep.id)

        assertEquals(preparationStep, result)
        verify(preparationStepRepository).findById(preparationStep.id)
    }

    @Test
    fun `getRecipePreparationStepById throws IdNotFoundException when PreparationStep does not exist`() {
        val id = 4L

        `when`(preparationStepRepository.findById(id)).thenReturn(Optional.empty())

        assertThrows(IdNotFoundException::class.java) {preparationStepService.getRecipePreparationStepById(id)}
        verify(preparationStepRepository).findById(id)
    }

    @Test
    fun `deletePreparationStep deletes the correct PreparationStep`() {
        `when`(preparationStepRepository.findById(preparationStep.id)).thenReturn(Optional.of(preparationStep))

        preparationStepService.deletePreparationStep(preparationStep.id)

        verify(preparationStepRepository).deleteById(preparationStep.id)
    }
}