package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.MeasureTemplate
import edu.kit.backend.api.templates.RecipeComponentTemplate
import edu.kit.backend.data.model.Ingredient
import edu.kit.backend.data.model.Measure
import edu.kit.backend.data.model.Recipe
import edu.kit.backend.data.model.RecipeComponent
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import java.lang.reflect.Field

class RecipeComponentConverterTest{

    private lateinit var recipeComponentConverter: RecipeComponentConverter
    private lateinit var recipeComponent: RecipeComponent
    private lateinit var recipeComponentTemplate: RecipeComponentTemplate

    @BeforeEach
    fun setUp() {
        // Inject mock converters using reflection
        recipeComponentConverter = RecipeComponentConverter()

        val testIngredient = Ingredient(id = 1, name = "Test Ingredient")
        val testMeasure = Measure(id = 1, name = "Test Measure", unit = "Test Unit")

        recipeComponent = RecipeComponent(
                id = 1,
                recipe = mock(Recipe::class.java),
                ingredient = testIngredient,
                measure = testMeasure,
                quantity = 1
        )

        recipeComponentTemplate = RecipeComponentTemplate(
            id = 1,
            ingredient = IngredientTemplate(
                id = 1,
                name = "Test Ingredient"
            ),
            measure = MeasureTemplate(
                id = 1,
                name = "Test Measure",
                unit = "Test Unit"
            ),
            quantity = 1
        )
    }

    @Test
    fun `correctly converts RecipeComponent to RecipeComponentTemplate`() {
        assertEquals(recipeComponentConverter.convert(recipeComponent), recipeComponentTemplate)
    }
}