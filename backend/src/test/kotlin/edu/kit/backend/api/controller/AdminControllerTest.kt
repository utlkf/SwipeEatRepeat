package edu.kit.backend.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import edu.kit.backend.api.service.AuthenticationService
import edu.kit.backend.api.templates.RegistrationCredentials
import edu.kit.backend.data.model.AuthenticationResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.test.web.servlet.setup.MockMvcBuilders

@ExtendWith(MockitoExtension::class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class AdminControllerTest {

    @MockBean
    private lateinit var authenticationService: AuthenticationService

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    private lateinit var registrationCredentials: RegistrationCredentials
    private lateinit var expectedResponse: AuthenticationResponse

    @BeforeEach
    fun setup() {
        registrationCredentials = RegistrationCredentials(username = "admin", password = "password", email = "email")
        expectedResponse = AuthenticationResponse(accessToken = "token", refreshToken = "refreshToken", userId = 1L)
    }
    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `admin registration returns authentication response`() {
        `when`(authenticationService.registerAdmin(registrationCredentials)).thenReturn(expectedResponse)

        val result = mockMvc.perform(post("/admin/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(registrationCredentials)))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, AuthenticationResponse::class.java)
        assertEquals(expectedResponse, response)
        verify(authenticationService).registerAdmin(registrationCredentials)
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `admin demo endpoint returns success`() {
        mockMvc.perform(get("/admin/demo"))
                .andExpect(status().isOk)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `admin demo endpoint returns forbidden for non-admin user`() {
        mockMvc.perform(get("/admin/demo"))
                .andExpect(status().isForbidden)
    }
}