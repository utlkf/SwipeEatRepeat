package edu.kit.backend.api.service

import edu.kit.backend.data.model.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.MockitoAnnotations
import java.time.LocalDateTime


/**
 * Test class for the FilterService.
 * Contains test data to build a test suite for the applications recipe filtering.
 *
 * The code within this class was assisted by Gemini Advanced, a large language model based AI tool.
 */
class FilterServiceTest {

    @InjectMocks
    private lateinit var filterService: FilterService

    private lateinit var user1: AppUser

    private lateinit var recipe1: Recipe
    private lateinit var recipe2: Recipe
    private lateinit var recipe3: Recipe
    private lateinit var recipe4: Recipe

    private lateinit var ingredient1: Ingredient
    private lateinit var ingredient2: Ingredient
    private lateinit var ingredient3: Ingredient

    private lateinit var measure1: Measure
    private lateinit var measure2: Measure
    private lateinit var measure3: Measure

    private lateinit var preference1: Preference
    private lateinit var preference2: Preference
    private lateinit var preference3: Preference

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        ingredient1 = Ingredient(id = 1L, name = "Flour")
        ingredient2 = Ingredient(id = 2L, name = "Sugar")
        ingredient3 = Ingredient(id = 3L, name = "Egg")

        measure1 = Measure(id = 1L, name = "gram", unit = "g")
        measure2 = Measure(id = 2L, name = "milliliter", unit = "ml")
        measure3 = Measure(id = 3L, name = "pieces", unit = "pcs")

        preference1 = Preference(id = 1, name = "Preference1", isStrict = false)
        preference2 = Preference(id = 2, name = "Preference2", isStrict = false)
        preference3 = Preference(id = 2, name = "Preference3", isStrict = true)

        user1 = AppUser(id = 1, username = "Test User", password = "d", email = "d", role = Role.USER)

        recipe1 = Recipe(
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe2 = Recipe(
                name = "recipe2",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe3 = Recipe(
                name = "recipe3",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe4 = Recipe(
                name = "recipe4",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        val recipeComponent1 = RecipeComponent(
                recipe = recipe1,
                ingredient = ingredient1,
                measure = measure1,
                quantity = 200
        )

        val recipeComponent2 = RecipeComponent(
                recipe = recipe1,
                ingredient = ingredient2,
                measure = measure2,
                quantity = 100
        )

        val recipeComponent3 = RecipeComponent(
                recipe = recipe2,
                ingredient = ingredient3,
                measure = measure3,
                quantity = 1
        )

        recipe1.recipeComponents.add(recipeComponent1)
        recipe1.recipeComponents.add(recipeComponent2)
        recipe2.recipeComponents.add(recipeComponent2)
        recipe2.recipeComponents.add(recipeComponent3)
        recipe3.recipeComponents.add(recipeComponent1)

        recipe1.preferences.add(preference1)
        recipe2.preferences.add(preference2)
        recipe3.preferences.add(preference1)
        recipe3.preferences.add(preference2)
        recipe3.preferences.add(preference3)
        recipe4.preferences.add(preference3)
    }

    @Test
    fun `filtering by blacklist excludes recipes containing blacklisted ingredients`() {
        // Test Data
        val blacklist = setOf(ingredient2) // Blacklist "Sugar"
        val recipes = setOf(recipe1, recipe2, recipe3, recipe4)

        // Call the filter method
        val filteredRecipes = filterService.filterRecipesByPreferencesAndIngredients(recipes, emptySet(), blacklist, null, null)

        // Assertions
        assertEquals(setOf(recipe3, recipe4), filteredRecipes)  // Only recipe3 should remain (no "Sugar")
    }

    @Test
    fun `filtering by only strict preferences includes only recipes matching all strict preferences`() {
        // Test Data
        val preferences = setOf(preference3) // Only recipes with exactly "Preference3" which is strict
        val recipes = setOf(recipe1, recipe2, recipe3, recipe4)

        // Call the filter method
        val filteredRecipes = filterService.filterRecipesByPreferencesAndIngredients(recipes, preferences, emptySet(), null, null)

        // Assertions
        assertEquals(setOf(recipe3, recipe4), filteredRecipes)
    }

    @Test
    fun `filtering by strict preferences and non-strict preferences includes only recipes matching all strict preferences and at least one non strict one`() {
        // Test Data
        val preferences = setOf(preference3, preference2) // preference3 is strict and preference2 is non-strict
        val recipes = setOf(recipe1, recipe2, recipe3, recipe4)

        // Call the filter method
        val filteredRecipes = filterService.filterRecipesByPreferencesAndIngredients(recipes, preferences, emptySet(), null, null)

        // Assertions
        assertEquals(setOf(recipe3), filteredRecipes)
    }

    @Test
    fun `filtering by superset whitelist includes recipes containing only ingredients that are in the whitelist`() {
        // Test Data
        val whitelist = setOf(ingredient1, ingredient3) // "Flour" and "Egg"
        val recipes = setOf(recipe1, recipe2, recipe3, recipe4)

        // Call the filter method
        val filteredRecipes = filterService.filterRecipesByPreferencesAndIngredients(recipes, emptySet(), emptySet(), whitelist, null)

        // Assertions
        assertEquals(setOf(recipe3, recipe4), filteredRecipes) // Only recipe3 and recipe4 contain only whitelist ingredients
    }

    @Test
    fun `filtering by subset whitelist includes recipes containing a superset of the whitelist ingredients`() {
        // Test Data
        val whitelist = setOf(ingredient1, ingredient2) // "Flour" and "Sugar"
        val recipes = setOf(recipe1, recipe2, recipe3, recipe4)

        // Call the filter method
        val filteredRecipes = filterService.filterRecipesByPreferencesAndIngredients(recipes, emptySet(), emptySet(), null, whitelist)

        // Assertions
        assertEquals(setOf(recipe1), filteredRecipes)  // only recipe1 has both ingredients
    }

    @Test
    fun `combined filtering applies multiple filters together`() {
        // Test Data
        val blacklist = setOf(ingredient2) // Blacklist "Sugar"
        val preferences = setOf(preference3)    // Only recipes with exactly "Preference3" which is strict
        val whitelist = setOf(ingredient3)  // "Egg"
        val recipes = setOf(recipe1, recipe2, recipe3, recipe4)

        // Call the filter method
        val filteredRecipes = filterService.filterRecipesByPreferencesAndIngredients(recipes, preferences, blacklist, whitelist, null)

        // Assertions
        assertEquals(setOf(recipe4), filteredRecipes)  // Let's analyze why only recipe4 should match:
        // - recipe1: Excluded due to blacklisted "Sugar"
        // - recipe2: Excluded because it doesn't have the strict "Preference 3"
        // - recipe3: Excluded because while it has "Preference3", it has the ingredient "flour" which is not part of the whitelist
    }
}