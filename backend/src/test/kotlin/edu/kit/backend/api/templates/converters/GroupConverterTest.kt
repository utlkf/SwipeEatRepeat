package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.GroupTemplate
import edu.kit.backend.data.model.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class GroupConverterTest {

    private lateinit var groupConverter: GroupConverter
    private lateinit var group: Group
    private lateinit var groupTemplate: GroupTemplate

    @BeforeEach
    fun setUp() {
        // Inject mock converters using reflection
        groupConverter = GroupConverter()

        val testGroupAdmin = AppUser(id = 1, username = "Test User", password = "d", email = "d", role = Role.USER)
        val members = mutableSetOf<AppUser>()
        val recipeStack = mutableSetOf<Recipe>()
        val preferences = mutableSetOf<Preference>()
        val blacklist = mutableSetOf<Ingredient>()
        val supersetWhitelist = mutableSetOf<Ingredient>()
        val subsetWhitelist = mutableSetOf<Ingredient>()

        group = Group(
            admin = testGroupAdmin,
            name = "Test Group",
            groupCode = "TEST",
            members = members,
            recipeStack = recipeStack,
            preferences = preferences,
            blacklist = blacklist,
            supersetWhitelist = supersetWhitelist,
            subsetWhitelist = subsetWhitelist,
        )

        groupTemplate = GroupTemplate(
            adminId = 1,
            name = "Test Group",
            groupCode = "TEST",
            members = listOf(),
            recipes = listOf(),
            preferences = listOf(),
            blacklist = listOf(),
            supersetWhitelist = listOf(),
            subsetWhitelist = listOf(),
        )


    }


    @Test
    fun `correctly converts Group to GroupTemplate`() {
        assertEquals(groupConverter.convert(group), groupTemplate)
    }

}