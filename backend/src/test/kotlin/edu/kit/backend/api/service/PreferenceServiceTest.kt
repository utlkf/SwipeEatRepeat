package edu.kit.backend.api.service

import edu.kit.backend.api.repository.GroupRepository
import edu.kit.backend.api.repository.PreferenceRepository
import edu.kit.backend.api.repository.RecipeRepository
import edu.kit.backend.api.repository.UserRepository
import edu.kit.backend.data.model.*
import edu.kit.backend.exceptions.IdNotFoundException
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.kotlin.any
import java.time.LocalDateTime
import java.util.*

class PreferenceServiceTest {

    @Mock
    lateinit var preferenceRepository: PreferenceRepository

    @Mock
    lateinit var recipeRepository: RecipeRepository

    @Mock
    lateinit var userRepository: UserRepository

    @Mock
    lateinit var groupRepository: GroupRepository

    @InjectMocks
    lateinit var preferenceService: PreferenceService

    private lateinit var preference1: Preference
    private lateinit var preference2: Preference
    private lateinit var preference3: Preference

    private lateinit var group1: Group
    private val groupList = mutableListOf<Group>()

    private lateinit var recipe1: Recipe
    private val recipeList = mutableListOf<Recipe>()

    private lateinit var user1: AppUser
    private val userList = mutableListOf<AppUser>()

    @BeforeEach
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        preference1 = Preference(id = 1, name = "Preference1", isStrict = false)
        preference2 = Preference(id = 2, name = "Preference2", isStrict = false)
        preference3 = Preference(id = 2, name = "Preference3", isStrict = true)

        user1 = AppUser(id = 1, username = "Test User1", password = "d", email = "d", role = Role.USER)
        user1.preferences.add(preference1)

        recipe1 = Recipe(
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe1.preferences.add(preference1)

        group1 = Group(admin = user1, name = "Test Group1", groupCode = "ABC")
        group1.recipeStack.add(recipe1)
        group1.preferences.add(preference1)

        recipeList.add(recipe1)
        userList.add(user1)
        groupList.add(group1)
    }

    @Test
    fun `getAllPreferences returns all preferences`() {
        val preferences = listOf(preference1, preference2, preference3)

        `when`(preferenceRepository.findAll()).thenReturn(preferences)

        val result = preferenceService.getAllPreferences()

        assertEquals(preferences, result)
        verify(preferenceRepository).findAll()
    }

    @Test
    fun `getPreferenceById returns the correct preference`() {
        val id = 1L

        `when`(preferenceRepository.findById(id)).thenReturn(Optional.of(preference1))

        val result = preferenceService.getPreferenceById(id)

        assertEquals(preference1, result)
        verify(preferenceRepository).findById(id)
    }

    @Test
    fun `getPreferenceById throws IdNotFoundException when preference does not exist`() {
        val id = 4L

        `when`(preferenceRepository.findById(id)).thenReturn(Optional.empty())

        assertThrows(IdNotFoundException::class.java) {preferenceService.getPreferenceById(id)}
        verify(preferenceRepository).findById(id)
    }

    @Test
    fun `createFoodPreference saves the preference`() {
        val preference = Preference(id = 1L, name = "Preference1", isStrict = false)

        `when`(preferenceRepository.save(any())).thenAnswer {
            invocation -> invocation.getArgument(0)
        }

        val result = preferenceService.createFoodPreference(preference)

        assertEquals(preference, result)
        verify(preferenceRepository).save(preference)
    }

    @Test
    fun `alterPreference updates the preference`() {
        val updatedPreference = Preference(id = preference1.id, name = "UpdatedPreference", isStrict = true)

        `when`(preferenceRepository.findById(preference1.id)).thenReturn(Optional.of(preference1))
        `when`(preferenceRepository.save(any())).thenAnswer {
            invocation -> invocation.getArgument(0)
        }

        val result = preferenceService.alterPreference(preference1.id, updatedPreference)

        assertEquals(updatedPreference, result)
        verify(preferenceRepository).save(any())
    }

    @Test
    fun `deletePreference deletes the preference and disassociates it from all entities`() {
        `when`(preferenceRepository.findById(preference1.id)).thenReturn(Optional.of(preference1))
        `when`(recipeRepository.findByAnyPreference(listOf(preference1))).thenReturn(recipeList.filter { it.preferences.contains(preference1) })
        `when`(userRepository.findByAnyPreference(listOf(preference1))).thenReturn(userList.filter { it.preferences.contains(preference1) })
        `when`(groupRepository.findByAnyPreference(listOf(preference1))).thenReturn(groupList.filter { it.preferences.contains(preference1) })

        val result = preferenceService.deletePreference(preference1.id)

        assertEquals(preference1, result)
        assertFalse(recipe1.preferences.contains(preference1))
        assertFalse(user1.preferences.contains(preference1))
        assertFalse(group1.preferences.contains(preference1))
        assertTrue(group1.recipeStack.contains(recipe1))
        assertFalse(group1.recipeStack.firstOrNull { recipe ->  recipe == recipe1}!!.preferences.contains(preference1))
        verify(preferenceRepository).deleteById(preference1.id)
        verify(recipeRepository).findByAnyPreference(listOf(preference1))
        verify(userRepository).findByAnyPreference(listOf(preference1))
        verify(groupRepository).findByAnyPreference(listOf(preference1))
    }

}