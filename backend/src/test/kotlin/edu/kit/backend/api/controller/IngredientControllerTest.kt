package edu.kit.backend.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import edu.kit.backend.api.service.IngredientService
import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.converters.IngredientConverter
import edu.kit.backend.data.model.Ingredient
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@ExtendWith(MockitoExtension::class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class IngredientControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var ingredientService: IngredientService

    private lateinit var ingredient1: Ingredient
    private lateinit var ingredient2: Ingredient
    private lateinit var ingredientTemplate1: IngredientTemplate
    private lateinit var ingredientTemplate2: IngredientTemplate
    private lateinit var ingredientConverter: IngredientConverter

    @BeforeEach
    fun setup() {
        ingredientConverter = IngredientConverter()
        ingredient1 = Ingredient(id = 1, name = "Test Ingredient 1")
        ingredient2 = Ingredient(id = 2, name = "Test Ingredient 2")
        ingredientTemplate1 = ingredientConverter.convert(ingredient1)
        ingredientTemplate2 = ingredientConverter.convert(ingredient2)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getAllIngredients returns all ingredients`() {
        `when`(ingredientService.getAllIngredients()).thenReturn(listOf(ingredient1, ingredient2))

        mockMvc.perform(get("/ingredients").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].id").value(ingredientTemplate1.id))
                .andExpect(jsonPath("$[0].name").value(ingredientTemplate1.name))
                .andExpect(jsonPath("$[1].id").value(ingredientTemplate2.id))
                .andExpect(jsonPath("$[1].name").value(ingredientTemplate2.name))

        verify(ingredientService, times(1)).getAllIngredients()
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `createIngredient adds a new ingredient`() {
        `when`(ingredientService.getIngredientByName(ingredient1.name)).thenReturn(ingredient1)

        mockMvc.perform(post("/ingredients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ingredientTemplate1)))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(ingredientTemplate1.id))
                .andExpect(jsonPath("$.name").value(ingredientTemplate1.name))

        verify(ingredientService, times(1)).getIngredientByName(ingredient1.name)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `deleteIngredient removes an ingredient`() {
        `when`(ingredientService.deleteIngredient(ingredient1.name)).thenReturn(ingredient1)

        mockMvc.perform(delete("/ingredients")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ingredientTemplate1)))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(ingredientTemplate1.id))
                .andExpect(jsonPath("$.name").value(ingredientTemplate1.name))

        verify(ingredientService, times(1)).deleteIngredient(ingredient1.name)
    }
}
