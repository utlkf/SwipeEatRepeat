package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.PreparationStepTemplate
import edu.kit.backend.data.model.PreparationStep
import edu.kit.backend.data.model.Recipe
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock

class PreparationStepConverterTest {
    private lateinit var preparationStepConverter: PreparationStepConverter
    private lateinit var preparationStep: PreparationStep
    private lateinit var preparationStepTemplate: PreparationStepTemplate

    @BeforeEach
    fun setUp() {
        preparationStepConverter = PreparationStepConverter()
        preparationStep = PreparationStep(id = 0, description = "Test PreparationStep", recipe = mock(Recipe::class.java))
        preparationStepTemplate = PreparationStepTemplate(id = 0, description = "Test PreparationStep")
    }

    @Test
    fun `correctly converts PreparationStep to PreparationStepTemplate`() {
        assertEquals(preparationStepConverter.convert(preparationStep), preparationStepTemplate)
    }
}