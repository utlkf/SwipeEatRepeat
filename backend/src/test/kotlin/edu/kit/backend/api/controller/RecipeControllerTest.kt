package edu.kit.backend.api.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import edu.kit.backend.api.service.RecipeService
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.api.templates.UserTemplate
import edu.kit.backend.api.templates.converters.RecipeConverter
import edu.kit.backend.api.templates.converters.UserConverter
import edu.kit.backend.data.model.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.*
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*
import java.time.LocalDateTime

/**
 * Test class for the RecipeController.
 *
 * The code within this class was assisted by Phind, a large language model based AI tool.
 */
@ExtendWith(MockitoExtension::class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class RecipeControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var recipeService: RecipeService

    private lateinit var userConverter: UserConverter
    private lateinit var recipeConverter: RecipeConverter

    private lateinit var user1: AppUser
    private lateinit var user2: AppUser

    private lateinit var userTemplate1: UserTemplate
    private lateinit var userTemplate2: UserTemplate

    private lateinit var recipe1: Recipe
    private lateinit var recipe2: Recipe

    private lateinit var recipeTemplate1: RecipeTemplate
    private lateinit var recipeTemplate2: RecipeTemplate

    private lateinit var multipartFile: MockMultipartFile

    @BeforeEach
    fun setUp() {
        userConverter = UserConverter()
        recipeConverter = RecipeConverter()

        user1 = AppUser(id = 1L, username = "user1", email = "email1", password = "password", role = Role.USER)
        user2 = AppUser(id = 2L, username = "user2", email = "email2", password = "password", role = Role.USER)
        userTemplate1 = userConverter.convert(user1)
        userTemplate2 = userConverter.convert(user2)

        recipe1 = Recipe(
                id = 1,
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe2 = Recipe(
                id = 2,
                name = "recipe2",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipeTemplate1 = recipeConverter.convert(recipe1)
        recipeTemplate2 = recipeConverter.convert(recipe2)

        multipartFile = MockMultipartFile("file", "originalFilename.jpg", "image/jpeg", byteArrayOf())
    }

    @Test
    fun `getAllRecipes returns all recipes`() {
        `when`(recipeService.getAllRecipes()).thenReturn(listOf(recipe1, recipe2))

        val result = mockMvc.perform(get("/recipes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertEquals(recipeTemplate1, response[0])
        assertEquals(recipeTemplate2, response[1])
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `fetchNumberOfRecipes returns limited number of recipes`() {
        `when`(recipeService.fetchNumberOfRecipesRandom(2)).thenReturn(listOf(recipe1, recipe2))

        val result = mockMvc.perform(get("/recipes/limited").param("count", "2").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertEquals(recipeTemplate1, response[0])
        assertEquals(recipeTemplate2, response[1])
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getRecipeById returns a specific recipe`() {
        `when`(recipeService.getRecipeById(recipe1.id)).thenReturn(recipe1)

        val result = mockMvc.perform(get("/recipes/${recipe1.id}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, RecipeTemplate::class.java)

        assertEquals(recipeTemplate1, response)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `deleteRecipe removes a recipe`() {
        `when`(recipeService.deleteRecipe(recipe1.id)).thenReturn(recipe1)

        val result = mockMvc.perform(delete("/recipes/${recipe1.id}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, RecipeTemplate::class.java)

        assertEquals(recipeTemplate1, response)
        verify(recipeService).deleteRecipe(recipe1.id)
    }

    @Test
    fun `getImage returns a recipe image`() {
        val fileName = "test.jpg"
        `when`(recipeService.getImageByteArray(fileName)).thenReturn(multipartFile.bytes)

        mockMvc.perform(get("/recipes/images/${fileName}").contentType(MediaType.IMAGE_JPEG_VALUE))
                .andExpect(status().isOk)
                .andExpect(content().bytes(multipartFile.bytes))
    }

}