package edu.kit.backend.api.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import edu.kit.backend.api.service.IngredientService
import edu.kit.backend.api.service.RecipeService
import edu.kit.backend.api.service.UserService
import edu.kit.backend.api.templates.IngredientTemplate
import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.api.templates.RecipeTemplate
import edu.kit.backend.api.templates.UserTemplate
import edu.kit.backend.api.templates.converters.IngredientConverter
import edu.kit.backend.api.templates.converters.PreferenceConverter
import edu.kit.backend.api.templates.converters.RecipeConverter
import edu.kit.backend.api.templates.converters.UserConverter
import edu.kit.backend.data.model.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.verify
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.time.LocalDateTime

/**
 * Test class for the UserController.
 *
 * The code within this class was assisted by Phind, a large language model based AI tool.
 */
@ExtendWith(MockitoExtension::class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class UserControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var userService: UserService

    @MockBean
    private lateinit var ingredientService: IngredientService

    @MockBean
    private lateinit var recipeService: RecipeService

    private lateinit var recipeConverter: RecipeConverter
    private lateinit var userConverter: UserConverter
    private lateinit var ingredientConverter: IngredientConverter
    private lateinit var preferenceConverter: PreferenceConverter

    private lateinit var user1: AppUser
    private lateinit var user2: AppUser

    private lateinit var userTemplate1: UserTemplate
    private lateinit var userTemplate2: UserTemplate

    private lateinit var preference1: Preference
    private lateinit var preferenceTemplate1: PreferenceTemplate

    private lateinit var ingredient1: Ingredient
    private lateinit var ingredient2: Ingredient
    private lateinit var ingredient3: Ingredient

    private lateinit var ingredientTemplate1: IngredientTemplate
    private lateinit var ingredientTemplate2: IngredientTemplate
    private lateinit var ingredientTemplate3: IngredientTemplate

    private lateinit var recipe1: Recipe
    private lateinit var recipe2: Recipe

    private lateinit var recipeTemplate1: RecipeTemplate
    private lateinit var recipeTemplate2: RecipeTemplate

    @BeforeEach
    fun setup() {
        MockitoAnnotations.openMocks(this)
        ingredientConverter = IngredientConverter()
        preferenceConverter = PreferenceConverter()
        userConverter = UserConverter()
        recipeConverter = RecipeConverter()

        user1 = AppUser(id = 1L, username = "user1", email = "email1", password = "password", role = Role.USER)
        user2 = AppUser(id = 2L, username = "user2", email = "email2", password = "password", role = Role.USER)
        userTemplate1 = userConverter.convert(user1)
        userTemplate2 = userConverter.convert(user2)

        preference1 = Preference(id = 1L, name = "1", isStrict = false)
        preferenceTemplate1 = preferenceConverter.convert(preference1)

        ingredient1 = Ingredient(id = 1L, name = "Flour")
        ingredient2 = Ingredient(id = 2L, name = "Sugar")
        ingredient3 = Ingredient(id = 3L, name = "Egg")
        ingredientTemplate1 = ingredientConverter.convert(ingredient1)
        ingredientTemplate2 = ingredientConverter.convert(ingredient2)
        ingredientTemplate3 = ingredientConverter.convert(ingredient3)

        recipe1 = Recipe(
                name = "recipe1",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipe2 = Recipe(
                name = "recipe2",
                passiveCookingTime = 10,
                activeCookingTime = 20,
                creator = user1,
                postDate = LocalDateTime.MIN,
                imageUrl = ""
        )

        recipeTemplate1 = recipeConverter.convert(recipe1)
        recipeTemplate2 = recipeConverter.convert(recipe2)


        `when`(ingredientService.getIngredientById(ingredientTemplate1.id)).thenReturn(ingredient1)
        `when`(ingredientService.getIngredientById(ingredientTemplate2.id)).thenReturn(ingredient2)
        `when`(ingredientService.getIngredientById(ingredientTemplate3.id)).thenReturn(ingredient3)

        `when`(recipeService.getRecipeById(recipeTemplate1.id)).thenReturn(recipe1)
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `getAllUsers returns all users`() {
        val users = listOf(user1, user2)
        `when`(userService.getAllUsers()).thenReturn(users)

        val result = mockMvc.perform(get("/users").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        // unfortunately did not find a way to directly compare the response with the users list when using this kind of mapping
        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<UserTemplate>>() {})

        assertEquals(userTemplate1, response[0])
        assertEquals(userTemplate2, response[1])
        verify(userService).getAllUsers()
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `deleteUser deletes a user`() {
        `when`(userService.deleteUser(user1.id)).thenReturn(user1)

        val result = mockMvc.perform(delete("/users/1").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, UserTemplate::class.java)

        assertEquals(userTemplate1, response)
        verify(userService).deleteUser(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `setPreferences sets preferences for a user`() {
        val preferenceTemplates = listOf(preferenceTemplate1)

        // Mock the setPreferences method to modify the user's preferences and return the user
        `when`(userService.setPreferences(user1.id, preferenceTemplates)).thenAnswer {
            user1.preferences.add(preference1)
            user1
        }

        val result = mockMvc.perform(put("/users/1/preferences")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(preferenceTemplates)))
                .andExpect(status().isOk)
                .andReturn()

        // Alternative way would be to map the result to a Json Object, which requires the inclusion of a library for that matter
        assertTrue(result.response.contentAsString.contains("\"preferences\":[{\"id\":1,\"name\":\"1\",\"isStrict\":false}]"))
        assertTrue(result.response.contentAsString.contains("\"username\":\"user1\""))
        verify(userService).setPreferences(user1.id, preferenceTemplates)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getPreferences returns user preferences`() {
        val preferences = listOf(preference1)
        `when`(userService.getPreferences(user1.id)).thenReturn(preferences)

        val result = mockMvc.perform(get("/users/1/preferences").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<PreferenceTemplate>>() {})

        assertEquals(preferenceTemplate1, response[0])
        verify(userService).getPreferences(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `setBlacklist updates user blacklist`() {
        val ingredientTemplates = listOf(ingredientTemplate1, ingredientTemplate2)

        `when`(userService.setBlacklist(user1.id, ingredientTemplates)).thenAnswer {
            ingredientTemplates.forEach {
                user1.blacklist.add(ingredientService.getIngredientById(it.id))
            }
            user1
        }

        val result = mockMvc.perform(put("/users/1/blacklist")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ingredientTemplates)))
                .andExpect(status().isOk)
                .andReturn()

        assertTrue(result.response.contentAsString.contains("\"blacklist\":[{\"id\":1,\"name\":\"Flour\"},{\"id\":2,\"name\":\"Sugar\"}]"))
        assertTrue(result.response.contentAsString.contains("\"username\":\"user1\""))
        verify(userService).setBlacklist(user1.id, ingredientTemplates)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getBlacklist returns user blacklist`() {
        val ingredients = listOf(ingredient1, ingredient2)
        `when`(userService.getBlacklist(user1.id)).thenReturn(ingredients)

        val result = mockMvc.perform(get("/users/1/blacklist").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<IngredientTemplate>>() {})

        assertEquals(ingredientTemplate1, response[0])
        assertEquals(ingredientTemplate2, response[1])
        verify(userService).getBlacklist(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `setSupersetWhitelist updates user superset whitelist`() {
        val ingredientTemplates = listOf(ingredientTemplate1, ingredientTemplate2)

        `when`(userService.setSupersetWhitelist(user1.id, ingredientTemplates)).thenAnswer {
            ingredientTemplates.forEach {
                user1.supersetWhitelist.add(ingredientService.getIngredientById(it.id))
            }
            user1
        }

        val result = mockMvc.perform(put("/users/1/superset-whitelist")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ingredientTemplates)))
                .andExpect(status().isOk)
                .andReturn()

        assertTrue(result.response.contentAsString.contains("\"supersetWhitelist\":[{\"id\":1,\"name\":\"Flour\"},{\"id\":2,\"name\":\"Sugar\"}]"))
        assertTrue(result.response.contentAsString.contains("\"username\":\"user1\""))
        verify(userService).setSupersetWhitelist(user1.id, ingredientTemplates)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getSupersetWhitelist returns user superset whitelist`() {
        val ingredients = listOf(ingredient1, ingredient2)
        `when`(userService.getSupersetWhitelist(user1.id)).thenReturn(ingredients)

        val result = mockMvc.perform(get("/users/1/superset-whitelist").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<IngredientTemplate>>() {})

        assertEquals(ingredientTemplate1, response[0])
        assertEquals(ingredientTemplate2, response[1])
        verify(userService).getSupersetWhitelist(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `setSubsetWhitelist updates user subset whitelist`() {
        val ingredientTemplates = listOf(ingredientTemplate1, ingredientTemplate2)

        `when`(userService.setSubsetWhitelist(user1.id, ingredientTemplates)).thenAnswer {
            ingredientTemplates.forEach {
                user1.subsetWhitelist.add(ingredientService.getIngredientById(it.id))
            }
            user1
        }

        val result = mockMvc.perform(put("/users/1/subset-whitelist")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(ingredientTemplates)))
                .andExpect(status().isOk)
                .andReturn()

        assertTrue(result.response.contentAsString.contains("\"subsetWhitelist\":[{\"id\":1,\"name\":\"Flour\"},{\"id\":2,\"name\":\"Sugar\"}]"))
        assertTrue(result.response.contentAsString.contains("\"username\":\"user1\""))
        verify(userService).setSubsetWhitelist(user1.id, ingredientTemplates)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getSubsetWhitelist returns user subset whitelist`() {
        val ingredients = listOf(ingredient1, ingredient2)
        `when`(userService.getSubsetWhitelist(user1.id)).thenReturn(ingredients)

        val result = mockMvc.perform(get("/users/1/subset-whitelist").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<IngredientTemplate>>() {})

        assertEquals(ingredientTemplate1, response[0])
        assertEquals(ingredientTemplate2, response[1])
        verify(userService).getSubsetWhitelist(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getUserCreatedRecipes returns user created recipes`() {
        val recipes = listOf(recipe1, recipe2)
        `when`(userService.getAllRecipesCreatedByUser(user1.id)).thenReturn(recipes)

        val result = mockMvc.perform(get("/users/1/created-recipes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertEquals(recipeTemplate1, response[0])
        assertEquals(recipeTemplate2, response[1])
        verify(userService).getAllRecipesCreatedByUser(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getRecipes returns recipes for a user`() {
        val defaultNumberOfRecipes = 10
        val recipes = listOf(recipe1, recipe2)
        `when`(userService.fetchNumberOfRecipesForUser(defaultNumberOfRecipes, user1.id)).thenReturn(recipes)

        val result = mockMvc.perform(get("/users/1/recipes").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertEquals(recipeTemplate1, response[0])
        assertEquals(recipeTemplate2, response[1])
        verify(userService).fetchNumberOfRecipesForUser(defaultNumberOfRecipes, user1.id)
    }
    @Test
    @WithMockUser(roles = ["USER"])
    fun `addFavourite adds a recipe to user favourites`() {
        `when`(userService.addFavourite(user1.id, recipeTemplate1)).thenAnswer {
            user1.favouriteRecipes.add(recipeService.getRecipeById(recipeTemplate1.id))
            user1
        }
        userTemplate1 = userConverter.convert(user1)

        val result = mockMvc.perform(post("/users/1/favourites")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipeTemplate1)))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object: TypeReference<List<RecipeTemplate>>() {})

        assertEquals(recipeTemplate1.id, response[0].id)
        verify(userService).addFavourite(user1.id, recipeTemplate1)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getFavourites returns user favourite recipes`() {
        user1.favouriteRecipes.add(recipe1)
        `when`(userService.getUser(user1.id)).thenReturn(user1)

        val result = mockMvc.perform(get("/users/1/favourites").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertEquals(recipeTemplate1, response[0])
        verify(userService).getUser(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `removeFavourite removes a recipe from user favourites`() {
        `when`(userService.removeFavourite(user1.id, recipeTemplate1)).thenAnswer {
            user1.favouriteRecipes.remove(recipeService.getRecipeById(recipeTemplate1.id))
            user1
        }
        user1.favouriteRecipes.add(recipe1)

        val result = mockMvc.perform(patch("/users/1/favourites")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipeTemplate1)))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object: TypeReference<List<RecipeTemplate>>() {})

        assertFalse(user1.favouriteRecipes.contains(recipe1))
        assertTrue(response.isEmpty())
        verify(userService).removeFavourite(user1.id, recipeTemplate1)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `addAccepted adds a recipe to user accepted recipes`() {
        `when`(userService.addAccepted(user1.id, recipeTemplate1)).thenAnswer {
            user1.acceptedRecipes.add(recipeService.getRecipeById(recipeTemplate1.id))
            user1
        }

        val result = mockMvc.perform(post("/users/1/accepted")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipeTemplate1)))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object: TypeReference<List<RecipeTemplate>>() {})

        assertTrue(response.size == 1)
        assertEquals(recipeTemplate1, response[0])
        verify(userService).addAccepted(user1.id, recipeTemplate1)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getAccepted returns user accepted recipes`() {
        user1.acceptedRecipes.add(recipe1)
        `when`(userService.getUser(user1.id)).thenReturn(user1)

        val result = mockMvc.perform(get("/users/1/accepted").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertTrue(response.size == 1)
        assertEquals(recipeTemplate1, response[0])
        verify(userService).getUser(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `resetAccepted resets user accepted recipes`() {
        `when`(userService.resetAcceptedRecipes(user1.id)).thenAnswer {
            user1.acceptedRecipes.clear()
            user1
        }
        user1.acceptedRecipes.add(recipe1)

        val result = mockMvc.perform(patch("/users/1/accepted")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object: TypeReference<List<RecipeTemplate>>() {})

        assertTrue(response.isEmpty())
        assertTrue(user1.acceptedRecipes.isEmpty())
        verify(userService).resetAcceptedRecipes(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `addDeclined adds a recipe to user declined recipes`() {
        `when`(userService.addDeclined(user1.id, recipeTemplate1)).thenAnswer {
            user1.declinedRecipes.add(recipeService.getRecipeById(recipeTemplate1.id))
            user1
        }

        val result = mockMvc.perform(post("/users/1/declined")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(recipeTemplate1)))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object: TypeReference<List<RecipeTemplate>>() {})

        assertTrue(response.size == 1)
        assertEquals(recipeTemplate1, response[0])
        verify(userService).addDeclined(user1.id, recipeTemplate1)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getDeclined returns user declined recipes`() {
        user1.declinedRecipes.add(recipe1)
        `when`(userService.getUser(user1.id)).thenReturn(user1)

        val result = mockMvc.perform(get("/users/1/declined").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object : TypeReference<List<RecipeTemplate>>() {})

        assertTrue(response.size == 1)
        assertEquals(recipeTemplate1, response[0])
        verify(userService).getUser(user1.id)
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `resetDeclined resets user declined recipes`() {
        `when`(userService.resetDeclinedRecipes(user1.id)).thenAnswer {
            user1.declinedRecipes.clear()
            user1
        }
        user1.declinedRecipes.add(recipe1)

        val result = mockMvc.perform(patch("/users/1/declined")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andReturn()

        val response = objectMapper.readValue(result.response.contentAsString, object: TypeReference<List<RecipeTemplate>>() {})

        assertTrue(response.isEmpty())
        assertTrue(user1.declinedRecipes.isEmpty())
        verify(userService).resetDeclinedRecipes(user1.id)
    }

}