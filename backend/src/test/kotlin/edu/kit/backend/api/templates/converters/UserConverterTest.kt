package edu.kit.backend.api.templates.converters

import edu.kit.backend.api.templates.UserTemplate
import edu.kit.backend.data.model.AppUser
import edu.kit.backend.data.model.Role
import org.junit.jupiter.api.BeforeEach

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

class UserConverterTest {

    private lateinit var userConverter: UserConverter
    private lateinit var user: AppUser
    private lateinit var userTemplate: UserTemplate

    @BeforeEach
    fun setUp() {
        userConverter = UserConverter()
        user = AppUser(username = "Test User", password = "Test Password", email = "d", role = Role.USER)
        userTemplate = UserTemplate(id = 0, username = "Test User", email = "d")
    }

    @Test
    fun `correctly converts AppUser to UserTemplate`() {
        assertEquals(userConverter.convert(user), userTemplate)
    }
}