package edu.kit.backend.api.controller

import com.fasterxml.jackson.databind.ObjectMapper
import edu.kit.backend.api.service.PreferenceService
import edu.kit.backend.api.templates.PreferenceTemplate
import edu.kit.backend.api.templates.converters.PreferenceConverter
import edu.kit.backend.data.model.Preference
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.*

@ExtendWith(MockitoExtension::class)
@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class PreferenceControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @MockBean
    private lateinit var preferenceService: PreferenceService

    private lateinit var preference1: Preference
    private lateinit var preference2: Preference
    private lateinit var preferenceTemplate1: PreferenceTemplate
    private lateinit var preferenceTemplate2: PreferenceTemplate
    private lateinit var preferenceConverter: PreferenceConverter

    @BeforeEach
    fun setup() {
        preferenceConverter = PreferenceConverter()
        preference1 = Preference(id = 1, name = "Test Preference 1", isStrict = false)
        preference2 = Preference(id = 2, name = "Test Preference 2", isStrict = true)
        preferenceTemplate1 = preferenceConverter.convert(preference1)
        preferenceTemplate2 = preferenceConverter.convert(preference2)
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `createFoodLabel adds a new food label`() {
        `when`(preferenceService.createFoodPreference(preference1)).thenReturn(preference1)

        mockMvc.perform(post("/preferences")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(preferenceTemplate1)))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(preferenceTemplate1.id))
                .andExpect(jsonPath("$.name").value(preferenceTemplate1.name))
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `alterFoodLabel updates an existing food label`() {
        val newName = "Updated Preference 1"
        val preference1NewVersion = Preference(id = preference1.id, name = newName, isStrict = false)
        val preference1NewVersionTemplate = preferenceConverter.convert(preference1NewVersion)

        `when`(preferenceService.alterPreference(preference1.id, preference1NewVersion)).thenReturn(preference1NewVersion)

        mockMvc.perform(put("/preferences/${preference1.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(preference1NewVersionTemplate)))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(preference1NewVersionTemplate.id))
                .andExpect(jsonPath("$.name").value(preference1NewVersionTemplate.name))
    }

    @Test
    @WithMockUser(roles = ["ADMIN"])
    fun `deleteFoodLabel removes a food label`() {
        `when`(preferenceService.deletePreference(preference1.id)).thenReturn(preference1)

        mockMvc.perform(delete("/preferences/${preference1.id}"))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(preferenceTemplate1.id))
                .andExpect(jsonPath("$.name").value(preferenceTemplate1.name))
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getAllFoodLabels returns all food labels`() {
        `when`(preferenceService.getAllPreferences()).thenReturn(listOf(preference1, preference2))

        mockMvc.perform(get("/preferences").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$[0].id").value(preferenceTemplate1.id))
                .andExpect(jsonPath("$[0].name").value(preferenceTemplate1.name))
                .andExpect(jsonPath("$[1].id").value(preferenceTemplate2.id))
                .andExpect(jsonPath("$[1].name").value(preferenceTemplate2.name))
    }

    @Test
    @WithMockUser(roles = ["USER"])
    fun `getFoodLabelById returns a food label by id`() {
        `when`(preferenceService.getPreferenceById(preference1.id)).thenReturn(preference1)

        mockMvc.perform(get("/preferences/${preference1.id}").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.id").value(preferenceTemplate1.id))
                .andExpect(jsonPath("$.name").value(preferenceTemplate1.name))
    }
}
