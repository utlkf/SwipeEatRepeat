package edu.kit.backend.data.model

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test

/**
 * Tests for the [Preference] class.
 *
 * The code within this class was assisted by Gemini Advanced, a large language model by Google AI.
 */
class PreferenceTest {

    // Sample data for constructor tests
    private val sampleName = "Test Preference"
    private val sampleIsStrictValue = true

    @Test
    fun `constructor initializes properties correctly`() {
        val preference = Preference(name = sampleName, isStrict = sampleIsStrictValue)

        assertEquals(0L, preference.id) // Default ID should be 0
        assertEquals(sampleName, preference.name)
        assertEquals(sampleIsStrictValue, preference.isStrict)
        assertTrue(preference.setByUsers.isEmpty()) // Sets should be empty initially
        assertTrue(preference.setByGroups.isEmpty())
        assertTrue(preference.setForRecipes.isEmpty())
    }

    @Test
    fun `predefined food labels are correct`() {
        val allLabels = Preference.getAllFoodLabels()
        val expectedLabels = listOf(
            Preference(1, "vegan", false),
            Preference(2, "vegetarian", false),
            Preference(3, "meat", false),
            Preference(4, "fish", false),
            Preference(5, "gluten-free", true),
            Preference(6, "lactose-free", true)
        )
        assertEquals(expectedLabels, allLabels)
    }
}