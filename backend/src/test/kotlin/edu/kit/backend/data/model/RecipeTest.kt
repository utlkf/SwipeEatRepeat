package edu.kit.backend.data.model

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito.mock
import java.time.LocalDateTime

/**
 * Tests for the [Recipe] class.
 *
 * The code within this class was assisted by Gemini Advanced, a large language model by Google AI.
 */
class RecipeTest {

    private lateinit var mockName: String
    private var mockPassiveTime: Int = 0
    private var mockActiveTime: Int = 0
    private lateinit var mockCreator: AppUser
    private lateinit var mockUrl: String
    private lateinit var mockRecipe: Recipe

    @BeforeEach
    fun setUp() {
        mockName = "Test Recipe"
        mockPassiveTime = 10
        mockActiveTime = 20
        mockCreator = mock(AppUser::class.java)
        mockUrl = "Test Url"

        mockRecipe = Recipe(
                name = mockName,
                passiveCookingTime = mockPassiveTime,
                activeCookingTime = mockActiveTime,
                creator = mockCreator,
                imageUrl = mockUrl
        )
    }

    @Test
    fun `constructor initializes properties correctly`() {

        assertEquals(0L, mockRecipe.id) // Default ID
        assertEquals(mockName, mockRecipe.name)
        assertEquals(mockPassiveTime, mockRecipe.passiveCookingTime)
        assertEquals(mockActiveTime, mockRecipe.activeCookingTime)
        assertEquals(mockCreator, mockRecipe.creator)
        assertEquals(mockUrl, mockRecipe.imageUrl)

        // Check for an approximate current date & time
        assertTrue(mockRecipe.postDate.isAfter(LocalDateTime.now().minusMinutes(1)))
        assertTrue(mockRecipe.postDate.isBefore(LocalDateTime.now().plusMinutes(1)))

        // Assert collections are empty initially
        assertTrue(mockRecipe.recipeComponents.isEmpty())
        assertTrue(mockRecipe.preparationSteps.isEmpty())
        assertTrue(mockRecipe.preferences.isEmpty())
        assertTrue(mockRecipe.groups.isEmpty())
        assertTrue(mockRecipe.favourisedBy.isEmpty())
        assertTrue(mockRecipe.acceptedBy.isEmpty())
        assertTrue(mockRecipe.declinedBy.isEmpty())
        assertTrue(mockRecipe.matchedBy.isEmpty())
    }

    @Test
    fun `addition and deletion of recipe component works correctly`() {
        val mockRecipeComponent = mock(RecipeComponent::class.java)

        mockRecipe.recipeComponents.add(mockRecipeComponent)
        assertTrue(mockRecipe.recipeComponents.contains(mockRecipeComponent))

        mockRecipe.recipeComponents.remove(mockRecipeComponent)
        assertFalse(mockRecipe.recipeComponents.contains(mockRecipeComponent))
    }




}