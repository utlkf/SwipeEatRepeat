package edu.kit.backend.data.model

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.security.core.authority.SimpleGrantedAuthority


/**
 * Tests for the [Role] class.
 *
 * The code within this class was assisted by Gemini Advanced, a large language model by Google AI.
 */
class RoleTest {

    @Test
    fun `getAuthorities returns correct SimpleGrantedAuthority for USER role`() {
        val expectedAuthority = SimpleGrantedAuthority("ROLE_USER")
        val actualAuthority = Role.USER.getAuthorities()
        assertEquals(expectedAuthority, actualAuthority)
    }

    @Test
    fun `getAuthorities returns correct SimpleGrantedAuthority for ADMIN role`() {
        val expectedAuthority = SimpleGrantedAuthority("ROLE_ADMIN")
        val actualAuthority = Role.ADMIN.getAuthorities()
        assertEquals(expectedAuthority, actualAuthority)
    }

    @Test
    fun `getAuthorities returns correct SimpleGrantedAuthority for GUEST role`() {
        val expectedAuthority = SimpleGrantedAuthority("ROLE_GUEST")
        val actualAuthority = Role.GUEST.getAuthorities()
        assertEquals(expectedAuthority, actualAuthority)
    }

    @Test
    fun `getAuthorities returns correct SimpleGrantedAuthority for MANAGER role`() {
        val expectedAuthority = SimpleGrantedAuthority("ROLE_MANAGER")
        val actualAuthority = Role.MANAGER.getAuthorities()
        assertEquals(expectedAuthority, actualAuthority)
    }

}