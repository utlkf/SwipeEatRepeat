package edu.kit.backend.data

/**
 * test class for serialization and deserialization of User subclasses
 */
class UserTest {
    /*
        val lastLoginDate = LocalDateTime.of(2020, 12, 12,18,18)
        val creationDate = LocalDateTime.of(2020, 12,12,18,18)
        val permanentUser = PermanentUser("1", "Bo", "bo@mail.de", "0000",creationDate, lastLoginDate)
        val guestUser = Guest("1")

        @Test
        fun permanentUserToJson() {
            val jsonString = permanentUser.userToJson()
            assertEquals(jsonString, "{\"id\":\"1\",\"name\":\"Bo\",\"email\":\"bo@mail.de\",\"password\":\"0000\",\"dateCreated\":[2020,12,12,18,18],\"lastLogin\":[2020,12,12,18,18]}")
        }

        @Test
        fun jsonToPermanentUser() {
            val permanentUser2 = PermanentUser.fromJson("{\"id\":\"1\",\"name\":\"Bo\",\"email\":\"bo@mail.de\",\"password\":\"0000\",\"dateCreated\":[2020,12,12,18,18],\"lastLogin\":[2020,12,12,18,18]}")
            assertEquals(permanentUser, permanentUser2)
        }

        @Test
        fun guestUserToJson() {
            val jsonString = guestUser.userToJson()
            assertEquals(jsonString, """{"id":"1"}""")
        }

        @Test
        fun jsonToGuestUser() {
            val guestUser2 = Guest.fromJson("""{"id":"1"}""")
            assertEquals(guestUser, guestUser2)
        }

     */
}
