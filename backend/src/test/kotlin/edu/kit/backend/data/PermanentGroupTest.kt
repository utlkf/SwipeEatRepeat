package edu.kit.backend.data

class PermanentGroupTest {
    /*
        private lateinit var permanentGroup: PermanentGroup
        private lateinit var user1: User
        private lateinit var user2: PermanentUser
        private lateinit var recipe: Recipe

        /**
         * instantiation of a permanent group
         */
        @BeforeEach
        fun setUp() {
            val postDate = LocalDateTime.of(2000, 12, 12, 18, 18)
            val ingredient = Ingredient("1", "Fleisch")
            val measure = Measure.GRAM
            val recipeIngredient = Triple<Ingredient, Int, Measure>(ingredient, 100, measure)
            val step = PreparationStep("dont cut yourself")

            recipe = Recipe("1", "Wurst", 1, 2, "1", postDate, 2, listOf(recipeIngredient), listOf(step))

            user1 = Guest("1")
            user2 = PermanentUser("2", "bo", "bo@mail.de", "0000", postDate, postDate)

            permanentGroup = PermanentGroup("1", user2, "187", mutableSetOf(user2), listOf(recipe))
        }


        /**
         * tests whether a valid Json Object is deserialized how it is supposed to
         */
        @Test
        fun testFromJsonMethod() {
            // Arrange
            // the Json representing the permanentGroup project declared in setUp()
            val json = """{
                "id": "1",
                "admin": {
                "id": "2",
                "name": "bo",
                "email": "bo@mail.de",
                "password": "0000",
                "dateCreated": "2000-12-12T18:18:00",
                "lastLogin": "2000-12-12T18:18:00"
            },
                "code": "187",
                "members": [
                {
                    "id": "2",
                    "name": "bo",
                    "email": "bo@mail.de",
                    "password": "0000",
                    "dateCreated": "2000-12-12T18:18:00",
                    "lastLogin": "2000-12-12T18:18:00"
                }],
                "recipeStack": [
                {
                    "id": "1",
                    "name": "Wurst",
                    "passiveCookingTime": 1,
                    "activeCookingTime": 2,
                    "creatorId": "1",
                    "postDate": "2000-12-12T18:18:00",
                    "favCount": 2,
                    "ingredients": [
                    {
                        "first": {
                        "id": "1",
                        "name": "Fleisch"
                    },
                        "second": 100,
                        "third": "GRAM"
                    }
                    ],
                    "preparationSteps": [
                    {
                        "description": "dont cut yourself"
                    }
                    ]
                }
                ]
            }"""
            // Act
            val permanentGroup2 = PermanentGroup.fromJson(json)
            // Assert
            assertEquals(permanentGroup2.admin, user2)
            assertEquals(permanentGroup2.recipeStack, listOf(recipe))
            assertEquals(permanentGroup2.admin, permanentGroup.admin)
        }

     */
}
