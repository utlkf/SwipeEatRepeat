package edu.kit.backend.data

/**
 * Test functionality of Recipe data class
 */
class RecipeTest {
    /*
        val postDate = LocalDateTime.of(2000, 12, 12, 18, 18)
        val ingredient = Ingredient("1", "Fleisch")
        val measure = Measure.GRAM
        val recipeIngredient = Triple<Ingredient, Int, Measure>(ingredient, 100, measure)
        val step = PreparationStep("dont cut yourself")
        val testRecipe = Recipe("1", "Wurst", 1, 2, "1", postDate, 2, listOf(recipeIngredient), listOf(step))

        @AfterEach
        fun tearDown() {
        }

        /**
         * Tests method recipeToJson()
         */
        @Test
        fun recipeToJsonTest() {
            val jsonString = testRecipe.recipeToJson()
            assertEquals(jsonString, """{"id":"1","name":"Wurst","passiveCookingTime":1,"activeCookingTime":2,"creatorId":"1","postDate":[2000,12,12,18,18],"favCount":2,"ingredients":[{"first":{"id":"1","name":"Fleisch"},"second":100,"third":"GRAM"}],"preparationSteps":[{"description":"dont cut yourself"}]}""")
        }

        /**
         * Tests static method fromJson(json:String)
         */
        @Test
        fun jsonToRecipeTest() {
            val resultingRecipe = Recipe.fromJson("""{"id":"1","name":"Wurst","passiveCookingTime":1,"activeCookingTime":2,"creatorId":"1","postDate":[2000,12,12,18,18],"favCount":2,"ingredients":[{"first":{"id":"1","name":"Fleisch"},"second":100,"third":"GRAM"}],"preparationSteps":[{"description":"dont cut yourself"}]}""")
            assertEquals(testRecipe, resultingRecipe)
        }

     */
}
